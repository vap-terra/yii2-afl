<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 21.08.16
 * Time: 15:32
 */

namespace app\behaviors;


use Yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

class FileBehavior extends Behavior
{
    public $fileAttribute = 'file';
    public $fileNamePrefix = 'file';

    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_VALIDATE => 'beforeSave'
        ];
    }

    public function beforeSave( $event )
    {
        $file = $this->owner->getOldAttribute($this->fileAttribute);
        if (empty($this->owner->{$this->fileAttribute}) && !empty($file)) {
            $this->owner->{$this->fileAttribute} = $file;
        }
    }

    /**
     * @return string
     */
    public function getHref()
    {
        if( method_exists($this->owner,'getHref') ){
            return $this->owner->getHref();
        }
        return Yii::$app->storage->getHref($this->owner);
    }

    /**
     * @return string
     */
    public function getPath()
    {
        if( method_exists($this->owner,'getPath') ){
            return $this->owner->getPath();
        }
        return Yii::$app->storage->getPath($this->owner);
    }

    /**
     * @return string
     */
    public function getImageHref()
    {
        return $this->getHref() . $this->owner->{$this->fileAttribute};
    }

    /**
     * @return string
     */
    public function getImagePath()
    {
        return $this->getPath() . $this->owner->{$this->fileAttribute};
    }

    /**
     * @param $extension
     * @return string
     */
    public function generateImageFileName( $extension )
    {
        return $this->fileNamePrefix.'_'.md5($this->owner->id).'.' . $extension;
    }
    /**
     *
     */
    public function createDir()
    {
        $dir = $this->getPath();
        if( !is_dir( $dir ) ) {
            mkdir($dir, 0775, true);
        }
    }

    /**
     * @param UploadedFile $image
     * @param bool $upload
     * @param bool $deleteTempFile
     * @return bool
     */
    public function uploadImage($image, $upload=true, $deleteTempFile = true )
    {
        if( $image instanceof UploadedFile === false ) return false;
        $fileName = $this->generateImageFileName($image->extension);
        $filePath = $this->getPath() . $fileName;
        $this->createDir();
        if( $upload ) {
            $status = $image->saveAs( $filePath, $deleteTempFile  );
        } else {
            $status = copy( $image->tempName, $filePath );
            if( $status && $deleteTempFile ) {
                unlink($image->tempName);
            }
        }
        if ( $status ) {
            $this->owner->{$this->fileAttribute} = $fileName;
            $this->owner->save();
            return true;
        }
        return false;
    }
}