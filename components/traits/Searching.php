<?php
/**
 * Created by PhpStorm.
 * User: Викуша
 * Date: 29.09.2016
 * Time: 13:18
 */

namespace app\components\traits;


use app\components\interfaces\Searchable;

/**
 * Class Searching
 * @package app\components\traits
 *
 *
 */
trait Searching
{
    public $arFilter = [];

    public function rules()
    {
        if ($this->getScenario() === Searchable::SCENARIO) {
            return [[array_keys($this->getAttributes()), 'safe']];
        } else {
            return parent::rules();
        }
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[Searchable::SCENARIO]  = array_keys($this->getAttributes());

        return $scenarios;
    }

    /**
     * @param \yii\db\ActiveQuery $query
     * @param array $params
     * @return \yii\db\ActiveQuery
     */
    public function search($query, $params = [])
    {
        $this->unsetAttributes();

        $this->setAttributes($params);


        $attributes = array_keys($this->getAttributes());
        $tableName = static::tableName();

        $qAttributes = $this->qAttributes();
        if( !empty( $qAttributes ) ){
            foreach( $qAttributes as $qParams ){
                $fields = $qParams[0];
                if(  isset( $qParams[1] ) && isset( $params[$qParams[1]] ) ) {
                    $logic = isset( $qParams[2] ) ? $qParams[2] : '';
                    if (is_array($fields)) {
                        foreach( $fields as $num=>$field ){
                            $arCond = ['like',$tableName.'.'.$field,$params[$qParams[1]]];
                            if ($num && $logic && $logic='OR'){
                                $query->orWhere($arCond);
                            } else {
                                $query->andWhere($arCond);
                            }
                        }
                    } else {
                        $arCond = ['like',$tableName.'.'.$fields,$params[$qParams[1]]];
                        if ($logic && $logic='OR'){
                            $query->orWhere($arCond);
                        } else {
                            $query->andWhere($arCond);
                        }
                    }
                }
            }
        }
        foreach($attributes as $attribute) {
            $query->andFilterWhere(['like', "$tableName.$attribute", $this->{$attribute}]);
        }

        return $query;
    }

    public function unsetAttributes()
    {
        $attributes = array_keys($this->getAttributes());

        foreach($attributes as $attribute) {
            $this->setAttribute($attribute, null);
        }
    }

    public static function searchForm()
    {
        $form = new static;
        $form->setScenario(Searchable::SCENARIO);

        return $form;
    }
}