<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 20.03.16
 * Time: 19:38
 */

namespace app\components;


use app\models\ext\ConstantExt;
use Yii;
use yii\base\Exception;
use yii\base\InvalidParamException;
use yii\base\UserException;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;

class BaseController extends Controller
{
    const SUCCESS = 'success';
    const ERROR = 'error';

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * @return string | array
     * @throws Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public static function getSupportEmail()
    {
        /* @var $email ConstantExt */
        $email = ConstantExt::getByTid('support_email');
        if( !empty( $email ) && $email->value ) {
            return $email->value;
        } else if( !empty( Yii::$app->params['supportEmail'] ) ) {
            return Yii::$app->params['supportEmail'];
        }
        throw new Exception('configuration or constant variable is not set e-mail support');
    }

    /**
     * @return string | array
     * @throws Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public static function getAdminEmail()
    {
        /* @var $email ConstantExt */
        $email = ConstantExt::getByTid('admin_email');
        if( !empty( $email ) && $email->value ) {
            return $email->value;
        } else if( !empty( Yii::$app->params['adminEmail'] ) ) {
            return Yii::$app->params['adminEmail'];
        }
        throw new Exception('configuration or constant variable is not set e-mail administrator');
    }

    /**
     * @return string | array
     */
    public static function getNoReplyEmail()
    {
        /* @var $email ConstantExt */
        $email = ConstantExt::getByTid('no_reply_email');
        if( !empty( $email ) && $email->value ) {
            return $email->value;
        } else if( !empty( Yii::$app->params['noReplyEmail'] ) ) {
            return Yii::$app->params['noReplyEmail'];
        }
        return 'no-reply@'.strtr($_SERVER['SERVER_NAME'],['www.'=>'']);
    }

    /**
     * @return string | array
     * @throws Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public static function getAdminPhone()
    {
        /* @var $phone ConstantExt */
        $phone = ConstantExt::getByTid('admin_phone');
        if( !empty( $phone ) && $phone->value ) {
            return $phone->value;
        } else if( !empty( Yii::$app->params['adminPhone'] ) ) {
            return Yii::$app->params['adminPhone'];
        }
        throw new Exception('configuration or constant variable is not set phone administrator');
    }

    /**
     * @param $type
     * @param $message
     */
    public function setFlash($type,$message)
    {
        Yii::$app->getSession()->setFlash($type, $message);
    }

    /**
     * @param $message
     */
    public function setFlashNotice($message)
    {
        Yii::$app->getSession()->setFlash('notice', $message);
    }

    /**
     * @param $message
     */
    public function setFlashSuccess($message)
    {
        Yii::$app->getSession()->setFlash('success', $message);
    }

    /**
     * @param $message
     */
    public function setFlashError($message)
    {
        Yii::$app->getSession()->setFlash('error', $message);
    }

    /**
     * @param $message
     */
    public function setFlashInfo($message)
    {
        Yii::$app->getSession()->setFlash('info', $message);
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws ForbiddenHttpException
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        if ($this->view) {
        }
        if (parent::beforeAction($action)) {
            /*if (!Yii::$app->user->can('action',['action'=>$action],false)) {
                //throw new ForbiddenHttpException(Yii::t('app', 'Access denied'));
                //var_dump($action->id.' - Access denied');
            } else {
                //var_dump($action->id.' - Access allowed');
            }*/
            return true;
        }
        return false;
    }

    /**
     * @return string
     */
    public function actionError()
    {
        if (($exception = Yii::$app->getErrorHandler()->exception) === null) {
            return '';
        }

        if ($exception instanceof HttpException) {
            $code = $exception->statusCode;
        } else {
            $code = $exception->getCode();
        }
        if ($exception instanceof Exception) {
            $name = $exception->getName();
        } else {
            $name = Yii::t('yii', 'Error');
        }
        if ($code) {
            $name .= " (#$code)";
        }

        if ($exception instanceof UserException) {
            $message = $exception->getMessage();
        } else {
            $message = Yii::t('yii', 'An internal server error occurred.');
        }

        if( Yii::$app->user->isGuest ) {
            $this->layout = 'blank';
        }

        if (Yii::$app->getRequest()->getIsAjax()) {
            return "$name: $message";
        } else {
            return $this->render(
                'error',
                [
                    'name' => $name,
                    'message' => $message,
                    'exception' => $exception,
                ]
            );
        }

    }
}