<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 01.08.16
 * Time: 21:09
 */

namespace app\components;

use app\components\Util;
use ReflectionClass;
use Yii;
use yii\base\Module;
use yii\base\ViewNotFoundException;

class Widget extends \yii\base\Widget
{
    /**
     * @var $template string
     */
    public $template;
    /**
     * @var $module Module
     */
    public $module = null;
    /**
     * @return string
     */
    public static function getCanonicalName()
    {
        return Util::getFileNameByClass(get_called_class(),'-');
    }

    /**
     * Returns the directory containing the view files for this widget.
     * The default implementation returns the 'views' subdirectory under the directory containing the widget class file.
     * @return string the directory containing the view files for this widget.
     */
    public function getViewPath()
    {
        $class = new ReflectionClass($this);
        return dirname($class->getFileName()) . DIRECTORY_SEPARATOR . self::getCanonicalName();
    }
}