<?php
/**
 * Created by PhpStorm.
 * User: Викуша
 * Date: 29.09.2016
 * Time: 13:09
 */

namespace app\components\interfaces;


interface Searchable
{
    const SCENARIO = 'search';
    /**
     * @param \yii\db\ActiveQuery $query
     * @param array $params
     * @return \yii\db\ActiveQuery
     */
    public function search($query, $params);

    /**
     * @return array
     */
    public function qAttributes();
}
