<?php
namespace app\components;

use yii\base\ErrorException;
use yii\helpers\ArrayHelper;
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 05.02.15
 * Time: 20:09
 */
class Configurator
{
    const TYPE_CONSOLE = 'console';
    const TYPE_WEB = 'web';

    const ENV_DEVELOPMENT = 'dev';
    const ENV_STAGING = 'stage';
    const ENV_PRODUCTION = 'prod';
    const ENV_TEST = 'test';

    public $arEnvFilePrefix = [
        self::TYPE_WEB => 'web',
        self::TYPE_CONSOLE => 'console',
        self::ENV_DEVELOPMENT => 'development',
        self::ENV_STAGING => 'staging',
        self::ENV_PRODUCTION => 'production'
    ];

    protected $_mainConfigFile;
    protected $_environmentConfigFile;
    protected $_mainParamsFile;
    protected $_environmentParamFile;

    /**
     *
     * @param string $pathConfig
     * @param string $appType
     * @param string $environmentName
     * @throws ErrorException
     * @internal param string $pathConfigExt
     */
    public function __construct( $pathConfig, $appType = self::TYPE_WEB, $environmentName = self::ENV_DEVELOPMENT )
    {
        if( $environmentName !== self::ENV_PRODUCTION ) {
            defined('YII_ENV_DEV') or define('YII_ENV_DEV', true);
            defined('YII_DEBUG') or define('YII_DEBUG', true);
        } else {
            defined('YII_ENV_PROD') or define('YII_ENV_PROD', true);
        }
        defined('YII_ENV') or define('YII_ENV', $environmentName);
        defined('YII_APP_TYPE') or define('YII_APP_TYPE', $appType);

        $this->_mainConfigFile = $pathConfig . $this->getConfigFileName( $appType );
        $this->_mainParamsFile = $pathConfig . $this->getParamsFileName( $appType );

        $this->_environmentConfigFile = $pathConfig . $appType . DIRECTORY_SEPARATOR . $this->getConfigFileName( $environmentName );
        $this->_environmentParamFile = $pathConfig . $appType . DIRECTORY_SEPARATOR . $this->getParamsFileName( $environmentName );
    }

    /**
     * @param $environmentName
     * @return string
     * @throws ErrorException
     */
    public function getConfigFileName( $environmentName )
    {
        if( !empty( $this->arEnvFilePrefix[$environmentName] ) ) {
            return $this->arEnvFilePrefix[$environmentName] . '.php';
        }
        throw new ErrorException('Not found environment prefix by name '.$environmentName . ' for config file');
    }

    /**
     * @param $environmentName
     * @return string
     * @throws ErrorException
     */
    public function getParamsFileName( $environmentName )
    {
        if( !empty( $this->arEnvFilePrefix[$environmentName] ) ) {
            return $this->arEnvFilePrefix[$environmentName] . '-params.php';
        }
        throw new ErrorException('Not found environment prefix by name '.$environmentName . ' for params file');
    }

    /**
     * @return array
     */
    public function make()
    {
        $_config = $_configEnvironment = $_params = $_paramsEnvironment = [];
        if( is_file( $this->_mainParamsFile ) ) {
            $_params = require( $this->_mainParamsFile );
        }
        if( is_file( $this->_environmentParamFile ) ) {
            $_paramsEnvironment = require( $this->_environmentParamFile );
        }
        $_params = ArrayHelper::merge( $_params, $_paramsEnvironment );
        if( is_file( $this->_mainConfigFile ) ) {
            $_config = require( $this->_mainConfigFile );
        }
        if( empty( $_config['params'] ) ){
            $_config['params'] = [];
        }
        $_config['params'] = $_params;
        if( is_file( $this->_environmentConfigFile ) ) {
            $_configEnvironment = require( $this->_environmentConfigFile );
        }
        return ArrayHelper::merge( $_config, $_configEnvironment );
    }

}