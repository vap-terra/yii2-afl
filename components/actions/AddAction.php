<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 20.08.16
 * Time: 19:56
 */

namespace app\components\actions;



use app\models\BaseActiveRecord;
use Yii;

class AddAction extends ModelAction
{
    public $view = 'add';


    /**
     * @return string
     */
    public function run( )
    {
        /* @var $oModel BaseActiveRecord */
        $class = $this->modelClass;
        $oModel = new $class();
        if( !empty( $this->presetAttributesFromQuery ) ){
            foreach( $this->presetAttributesFromQuery as $attribute => $queryParam ){
                $oModel->{$attribute} = Yii::$app->request->getQueryParam($queryParam);
            }
        }
        return $this->controller->render(
            $this->view,
            [
                'model' => $oModel,
                'result' => []
            ]
        );
    }
}