<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 20.08.16
 * Time: 19:56
 */

namespace app\components\actions;


use app\models\BaseActiveRecord;
use yii\web\NotFoundHttpException;

class EditAction extends ModelAction
{
    public $view = 'edit';

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function run( $id )
    {
        /* @var $oModel BaseActiveRecord */
        $class = $this->modelClass;
        $oModel = $class::getByTid( $id );
        return $this->controller->render(
            $this->view,
            [
                'model' => $oModel,
                'result' => []
            ]
        );
    }
}