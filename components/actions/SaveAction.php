<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 20.08.16
 * Time: 19:56
 */

namespace app\components\actions;

use app\components\BaseController;
use app\models\BaseActiveRecord;
use Yii;
use yii\db\ActiveRecord;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\bootstrap\ActiveForm;
use yii\web\UploadedFile;


class SaveAction extends ModelAction
{
    public $view = 'edit';
    public $viewUpdate = 'add';
    public $modelMethod = 'save';
    public $messageContext = 'backend/layout';
    public $successMessage = 'Element {title} successfully saved!';
    public $errorMessage = 'Error saving element {title}';
    public $errorValidMessage = 'Please fill in all required fields';
    public $titleAttribute = 'name';
    public $fileAttribute;
    public $modelUploadMethod = 'uploadImage';
    public $redirect = false;
    public $requestType = 'post';// post | get | post+get
    public $afterSave;

    /**
     * @param ActiveRecord $model
     * @throws \yii\base\ExitException
     */
    public function redirect($model)
    {
        if( $this->redirect ) {
            if( is_array( $this->redirect ) ){
                $redirect = [];
                $url = array_shift($this->redirect);
                foreach($this->redirect as $key=>$value) {
                    if( $model->hasAttribute($value) ) {
                        if (is_numeric($key)) {
                            $key = $value;
                        }
                        $value = $model->{$value};
                    }
                    $redirect[$key] = $value;
                }
                array_unshift($redirect,$url);
                $this->controller->redirect($redirect);
            } else {
                $this->controller->redirect($this->redirect);
            }
            Yii::$app->end();
        }
    }
    /**
     * @param int $id
     * @return array|string
     * @throws NotFoundHttpException
     */
    public function run( $id = 0 )
    {

        /* @var $oModel BaseActiveRecord */
        $class = $this->modelClass;
        if( $id ) {
            $oModel = $class::getByTid( $id );
        } else {
            $oModel = new $class();
        }


        /* @var $result array */
        $result = [];
        $arValues = Yii::$app->request->post();
        if( $oModel->load( $arValues ) ) {

            if( Yii::$app->request->isAjax && Yii::$app->request->get('validate','') == 'true' ) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate( $oModel );
            }

            $name = $oModel->getAttribute( $this->titleAttribute);
            if( !$name ){
                $name = $oModel->id;
            }

            $validErrors = ActiveForm::validate($oModel);
            $ok = false;
            if( empty( $validErrors ) ) {
                $ok = $oModel->{$this->modelMethod}();
                $message = Yii::t( $this->messageContext, $ok?$this->successMessage:$this->errorMessage, ['title'=>$name] );
                if( $ok ) {
                    $result = [
                        'status'=>BaseController::SUCCESS,
                        'message'=>$message,
                        'errors' => []
                    ];

                    if( $this->fileAttribute && $oModel->hasAttribute( $this->fileAttribute) ){
                        $file = UploadedFile::getInstance($oModel, $this->fileAttribute);
                        if ($file) {
                            if ($file->error == 0) {
                                if (!$oModel->{$this->modelUploadMethod}($file, true, true)) {
                                    $oModel->addError($this->fileAttribute,BaseActiveRecord::getErrorUploadMessage(8, $file->name));
                                }
                            } else if ($file->error != 4) {
                                $oModel->addError($this->fileAttribute,BaseActiveRecord::getErrorUploadMessage($file->error, $file->name));
                            }
                        }
                    }
                    if( $this->afterSave && is_callable( $this->afterSave ) ) {
                        call_user_func($this->afterSave,$oModel,$this);
                    }
                }

                if( $oModel->hasErrors() ) {
                    $result = [
                        'status'=>BaseController::ERROR,
                        'message'=>Yii::t( $this->messageContext, $this->errorMessage, ['title'=>$name] ),
                        'errors' => $oModel->getErrors()
                    ];
                }
            } else {
                $message = Yii::t( $this->messageContext, $this->errorValidMessage, ['title'=>$name] );
                $result = [
                    'status' => BaseController::ERROR,
                    'message' => $message,
                    'errors' => $validErrors
                ];
            }

            if( Yii::$app->request->isAjax ) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $result;
            } else {
                $this->controller->setFlash($result['status'],$message);
                //return $this->controller->refresh();
                $this->redirect($oModel);
            }
        }

        $view = $this->view;
        if( !$oModel->isNewRecord ) {
            $view = $this->viewUpdate;
        }
        return $this->controller->render(
            $view,
            [
                'model' => $oModel,
                'result' => $result
            ]
        );

    }
}