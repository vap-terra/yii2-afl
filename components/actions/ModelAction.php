<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 20.08.16
 * Time: 20:02
 */

namespace app\components\actions;


use yii\base\Action;

class ModelAction extends Action
{
    public $view;
    public $modelClass;
    public $modelMethod;
    public $modelScenario;

    public $presetAttributesFromQuery = [];
}