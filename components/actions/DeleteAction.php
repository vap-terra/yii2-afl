<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 20.08.16
 * Time: 19:56
 */

namespace app\components\actions;

use app\components\BaseController;
use app\models\BaseActiveRecord;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class DeleteAction extends ModelAction
{
    public $view = 'delete';
    public $modelMethod = 'delete';
    public $messageContext = 'backend/layout';
    public $successMessage = 'Item {title} deleted successfully';
    public $errorMessage = 'Failed to remove an item {title}';
    public $titleAttribute = 'name';
    public $redirect = false;

    /**
     * @param $id
     * @return array | string
     * @throws NotFoundHttpException
     */
    public function run( $id )
    {
        /* @var $oModel BaseActiveRecord */
        $class = $this->modelClass;
        $oModel = $class::getByTid( $id );
        $name = $oModel->getAttribute( $this->titleAttribute);
        if( !$name ){
            $name = $oModel->id;
        }

        $ok = $oModel->{$this->modelMethod}();
        $message = Yii::t( $this->messageContext, $ok?$this->successMessage:$this->errorMessage, ['title'=>$name] );
        if( Yii::$app->request->isAjax ){
            $result = [
                'status' => $ok?BaseController::SUCCESS:BaseController::ERROR,
                'message' => $message,
                'errors' => $oModel->hasErrors()?$oModel->getErrors():[],
                'redirect' => $this->redirect?Yii::$app->urlManager->createUrl($this->redirect):false
            ];
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $result;
        }

        if( $ok ) {
            $this->controller->setFlashSuccess( $message );
        } else {
            $this->controller->setFlashError( $message );
        }

        if( $this->redirect ) {
            $this->controller->redirect($this->redirect);
            Yii::$app->end();
        }
        return $this->controller->render( $this->view, ['model'=>$oModel]);
    }
}