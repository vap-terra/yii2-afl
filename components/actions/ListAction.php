<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 20.08.16
 * Time: 19:56
 */

namespace app\components\actions;

use app\models\BaseActiveQuery;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;

class ListAction extends ModelAction
{
    public $view = 'index';
    public $prepareDataProvider = null;
    public $conditions = [];

    public function prepareDataProvider()
    {
        /* @var $query BaseActiveQuery */
        $query = call_user_func([$this->modelClass,'find']);
        if( !empty( $this->conditions ) ){
            foreach( $this->conditions as $parameter=>$attribute ) {
                $value = Yii::$app->request->get($parameter,null);
                if( $value !== null ){
                    $query->andWhere([$attribute=>$value]);
                }
            }
        }

        $countQuery = clone $query;
        return new ActiveDataProvider(
            [
                'query' => $query,
                'pagination' => [
                    'pageSize' => 30,
                    'pageParam' => 'page',
                    'totalCount' => $countQuery->count(),
                ],
            ]
        );
    }
    /**
     * @return string
     */
    public function run()
    {
        if( $this->prepareDataProvider ) {
            $dataProviderList = $this->prepareDataProvider;
        } else {
            $dataProviderList = $this->prepareDataProvider();
        }
        $context = [
            'dataProviderList' => $dataProviderList,
        ];
        if( Yii::$app->request->isAjax ) {
            return $this->controller->renderPartial($this->view,$context);
        }
        return $this->controller->render(
            $this->view,$context
        );
    }
}