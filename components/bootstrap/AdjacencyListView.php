<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 01.03.16
 * Time: 13:50
 */

namespace app\components\bootstrap;


use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ListView;

class AdjacencyListView extends ListView
{
    /**
     * linked model
     * @var $owner ActiveRecord
     */
    public $owner = null;

    /**
     * @var array the HTML attributes for the container of the rendering result of each data model.
     * The "tag" element specifies the tag name of the container element and defaults to "div".
     * If "tag" is false, it means no container element will be rendered.
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $subItemOptions = [];

    /**
     * @var array the HTML attributes for the container of the rendering result of each data model.
     * The "tag" element specifies the tag name of the container element and defaults to "div".
     * If "tag" is false, it means no container element will be rendered.
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $subContainerOptions = [];

    /**
     * @var array the HTML attributes for the container of the rendering result of each data model.
     * The "tag" element specifies the tag name of the container element and defaults to "div".
     * If "tag" is false, it means no container element will be rendered.
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $containerOptions = [];

    /**
     * @var string this property allows you to customize the HTML which is used to generate the drop down caret symbol,
     * which is displayed next to the button text to indicate the drop down functionality.
     * Defaults to `null` which means `<b class="caret"></b>` will be used. To disable the caret, set this property to be an empty string.
     */
    //public $dropDownCaret;

    /**
     * Initializes the view.
     */
    /*public function init()
    {
        if ($this->dropDownCaret === null) {
            $this->dropDownCaret = Html::tag('span', '', ['class' => 'fa caret-dropdown']);
        }
        parent::init();
    }*/

    /**
     * Renders all data models.
     * @return string the rendering result
     */
    public function renderItems()
    {
        $models = $this->dataProvider->getModels();
        $keys = $this->dataProvider->getKeys();
        $rows = [];
        foreach (array_values($models) as $index => $model) {
            $rows[] = $this->renderItem($model, $keys[$index], $index);
        }

        $content = implode($this->separator, $rows);
        $options = $this->containerOptions;
        $tag = ArrayHelper::remove($options, 'tag', 'div');
        if ($tag !== false) {
            $content = Html::tag($tag, $content, $options);
        }
        return $content;
    }

    /**
     * Renders a single data model.
     * @param mixed $model the data model to be rendered
     * @param mixed $key the key value associated with the data model
     * @param integer $index the zero-based index of the data model in the model array returned by [[dataProvider]].
     * @return string the rendering result
     */
    public function renderItem($model, $key, $index)
    {
        $subContent = $this->renderSubItems($model,$key,$index);

        if ($this->itemView === null) {
            $content = $key.$subContent;
        } elseif (is_string($this->itemView)) {
            $content = $this->getView()->render($this->itemView, array_merge([
                'model' => $model,
                'key' => $key,
                'index' => $index,
                'widget' => $this,
                'content' => $subContent,
                'owner' => $this->owner,
            ], $this->viewParams));
        } else {
            $content = call_user_func($this->itemView, $model, $key, $index, $this, $subContent, $this->owner);
        }

        $options = $this->itemOptions;
        $tag = ArrayHelper::remove($options, 'tag', 'div');
        if ($tag !== false) {
            $options['data-key'] = is_array($key) ? json_encode($key, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE) : (string) $key;
            if( $subContent ) {
                Html::addCssClass($options, ['class' => 'nav-parent dropdown']);
                /*if ($this->dropDownCaret !== '') {
                    $content = $this->dropDownCaret . ' ' .$content;
                }*/
            }
            return Html::tag($tag, $content, $options);
        } else {
            return $content;
        }
    }

    /**
     * Renders all data models.
     * @return string the rendering result
     */
    public function renderSubItems($model,$key,$index)
    {
        $subModels = $this->dataProvider->getSubModels($key);
        $subKeys = $this->dataProvider->getSubKeys($key);
        $rows = [];
        foreach (array_values($subModels) as $subIndex => $subModel) {
            $rows[] = $this->renderSubItem($subModel, $subKeys[$subIndex], $subIndex, $model,$key,$index);
        }

        $content = implode($this->separator, $rows);
        if( $content ) {
            $options = $this->subContainerOptions;
            $tag = ArrayHelper::remove($options, 'tag', 'div');
            if ($tag !== false) {
                $options['data-parent-key'] = is_array($key) ? json_encode($key, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE) : (string)$key;
                $content = Html::tag($tag, $content, $options);
            }
        }
        return $content;
    }

    /**
     * Renders a single data model.
     * @param mixed $model the data model to be rendered
     * @param mixed $key the key value associated with the data model
     * @param integer $index the zero-based index of the data model in the model array returned by [[dataProvider]].
     * @param $parentModel
     * @param $parentKey
     * @param $parentIndex
     * @return string the rendering result
     */
    public function renderSubItem($model, $key, $index, $parentModel, $parentKey, $parentIndex )
    {
        $subContent = $this->renderSubItems($model,$key,$index);
        if ($this->itemView === null) {
            $content = $key.$subContent;
        } elseif (is_string($this->itemView)) {
            $content = $this->getView()->render($this->itemView, array_merge([
                'model' => $model,
                'key' => $key,
                'index' => $index,
                'widget' => $this,
                'content' => $subContent,
                'owner' => $this->owner,
                'parentModel' => $parentModel,
                'parentKey' => $parentKey,
                'parentIndex' => $parentIndex
            ], $this->viewParams));
        } else {
            $content = call_user_func($this->itemView, $model, $key, $index, $this, $subContent, $this->owner, $parentModel, $parentKey, $parentIndex);
        }
        $options = $this->subItemOptions;
        $tag = ArrayHelper::remove($options, 'tag', 'div');
        if ($tag !== false) {
            $options['data-key'] = is_array($key) ? json_encode($key, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE) : (string)$key;
            if( $subContent ) {
                Html::addCssClass($options, ['class' => 'nav-parent dropdown']);
               /* if ($this->dropDownCaret !== '') {
                    $content = $this->dropDownCaret . ' ' .$content;
                }*/
            }
            return Html::tag($tag, $content, $options);
        } else {
            return $content;
        }
    }
}