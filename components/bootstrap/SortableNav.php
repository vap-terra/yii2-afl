<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 27.02.16
 * Time: 22:10
 */

namespace app\components\bootstrap;


use app\assets\NestedSortableAsset;
use Yii;
use yii\base\InvalidConfigException;
use yii\bootstrap\BootstrapAsset;
use yii\bootstrap\Dropdown;
use yii\bootstrap\Nav;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class SortableNav extends Nav
{

    public $optionsSubList = [];
    public $itemRoute = '';
    public $itemRouteParameter = '';

    /**
     * Initializes the widget.
     */
    public function init()
    {
        if ($this->dropDownCaret === null) {
            $this->dropDownCaret = Html::tag('span', '', ['class' => 'fa caret-dropdown']);
        }
        parent::init();
    }

    /**
     * Renders the widget.
     */
    public function run()
    {
        BootstrapAsset::register($this->getView());
        NestedSortableAsset::register($this->getView());
        return $this->renderItems();
    }

    /**
     * Renders widget items.
     */
    public function renderItems()
    {
        $items = [];
        foreach ($this->items as $i => $item) {
            if (isset($item['visible']) && !$item['visible']) {
                continue;
            }
            $items[] = $this->renderItem($item);
        }
        Html::addCssClass($this->options,['class'=>'sortable-groups']);
        return Html::tag('ul', implode("\n", $items), $this->options);
    }

    /**
     * Renders a widget's item.
     * @param string|array $item the item to render.
     * @return string the rendering result.
     * @throws InvalidConfigException
     */
    public function renderItem($item)
    {
        if (is_string($item)) {
            return $item;
        }
        if (!isset($item['label'])) {
            throw new InvalidConfigException("The 'label' option is required.");
        }
        if (!isset($item['id'])) {
            throw new InvalidConfigException("The 'id' option is required.");
        }
        $encodeLabel = isset($item['encode']) ? $item['encode'] : $this->encodeLabels;
        $label = $encodeLabel ? Html::encode($item['label']) : $item['label'];
        $options = ArrayHelper::getValue($item, 'options', []);
        $options['data-id'] = $item['id'];

        $items = ArrayHelper::getValue($item, 'items');
        $url = ArrayHelper::getValue($item, 'url', '#');
        $linkOptions = ArrayHelper::getValue($item, 'linkOptions', []);



        if (isset($item['active'])) {
            $active = ArrayHelper::remove($item, 'active', false);
        } else {
            $active = $this->isItemActive($item);
        }

        $linkOptions['data-toggle'] = 'dropdown';
        Html::addCssClass($options, ['widget' => 'nav-parent dropdown']);
        Html::addCssClass($linkOptions, ['widget' => 'dropdown-toggle']);
        Html::addCssClass($linkOptions, 'menu-item');
        $hasItems = false;
        if ($items !== null) {
            $hasItems = true;
            if (is_array($items)) {
                if ($this->activateItems) {
                    $items = $this->isChildActive($items, $active);
                }
                $items = $this->renderSubItems( $items, $item, [] );//renderDropdown($items, $item);
            }
        } else {
            //$items = Html::tag('ul', '', $this->optionsSubList);
            $items = $this->renderSubItems( [], $item, [] );//renderDropdown($items, $item);
        }


        if ($this->dropDownCaret !== '') {
            $dropDownCaret = [
                'class' => 'fa caret-dropdown fw'
            ];
            if($hasItems){
                Html::addCssClass($dropDownCaret, 'active');
            } else {
                $dropDownCaret['style'] = 'display:none';
            }
            $label =  Html::tag('span', '', $dropDownCaret) . ' ' .$label;
        }

        if ($this->activateItems && $active) {
            Html::addCssClass($options, 'active');
        }

        Html::addCssClass($linkOptions, 'menu-item');
        if( $this->itemRouteParameter ) {
            $itemLink = Html::a($label, Yii::$app->urlManager->createUrl([$this->itemRoute, $this->itemRouteParameter => $item['id']]), $linkOptions);
        } else {
            $itemLink = Html::tag('span',$label, $linkOptions);
        }

        //$button = Html::tag('button',$itemLink,['type'=>'button', 'class'=>'menu-item']);

        $edit = Html::a('<i class="fa fa-edit"></i>',Yii::$app->urlManager->createUrl([$this->route.'edit','id'=>$item['id']]),['class'=>'text-info btn-action']);
        $delete = Html::a('<i class="fa fa-remove"></i>',Yii::$app->urlManager->createUrl([$this->route.'delete','id'=>$item['id']]),['class'=>'text-danger btn-action']);
        $handler = Html::a('<i class="fa fa-arrows"></i>','#',['class'=>'draggable-handler']);

        $newWindow = Html::a('<i class="fa fa-external-link"></i>',$url,['target'=>'_blank']);

        $actionContainer = Html::tag('div',$edit.$newWindow.$delete.$handler,['class'=>'item-options']);

        $slideToggle = '';
        /*if( $items ) {
            $slideToggle = Html::a('<i class="fa fa-arrows"></i>', '#', ['class' => 'text-primary draggable-handler ui-sortable-handle']);
        }*/
        return Html::tag( 'li',  Html::tag('div',$slideToggle . $itemLink . $actionContainer) . $items, $options );
    }


    public function renderSubItems( $items, $parent, $options )
    {
        $subItems = [];
        foreach( $items as $item ) {
            $subItems[] = $this->renderSubItem( $item, $parent, $options );
        }
        Html::addCssClass( $this->optionsSubList, ['class'=>'nav nav-children'] );
        return Html::tag('ul', implode("\n", $subItems), $this->optionsSubList);
    }

    public function renderSubItem( $item, $parent, $options )
    {
        if (is_string($item)) {
            return $item;
        }
        if (!isset($item['label'])) {
            throw new InvalidConfigException("The 'label' option is required.");
        }
        if (!isset($item['id'])) {
            throw new InvalidConfigException("The 'id' option is required.");
        }
        $encodeLabel = isset($item['encode']) ? $item['encode'] : $this->encodeLabels;
        $label = $encodeLabel ? Html::encode($item['label']) : $item['label'];
        $options = ArrayHelper::getValue($item, 'options', []);
        $options['data-id'] = $item['id'];
        $items = ArrayHelper::getValue($item, 'items');
        $url = ArrayHelper::getValue($item, 'url', '#');
        $linkOptions = ArrayHelper::getValue($item, 'linkOptions', []);


        if (isset($item['active'])) {
            $active = ArrayHelper::remove($item, 'active', false);
        } else {
            $active = $this->isItemActive($item);
        }

        $linkOptions['data-toggle'] = 'dropdown';
        Html::addCssClass($options, ['widget' => 'nav-parent dropdown']);
        Html::addCssClass($linkOptions, ['widget' => 'dropdown-toggle']);
        Html::addCssClass($linkOptions, 'menu-item');

        $hasItems = false;
        if ($items !== null) {
            $hasItems = true;
            if (is_array($items)) {
                if ($this->activateItems) {
                    $items = $this->isChildActive($items, $active);
                }
                $items = $this->renderSubItems($items, $item, []);
            }
        } else {
            $items = Html::tag('ul', '', $this->optionsSubList);
        }

        if ($this->dropDownCaret !== '') {
            $dropDownCaret = [
                'class' => 'fa caret-dropdown fw'
            ];
            if($hasItems){
                Html::addCssClass($dropDownCaret, 'active');
            } else {
                $dropDownCaret['style'] = 'display:none';
            }
            $label =  Html::tag('span', '', $dropDownCaret) . ' ' .$label;
        }

        if ($this->activateItems && $active) {
            Html::addCssClass($options, 'active');
        }

        Html::addCssClass($linkOptions, 'menu-item');

        if( $this->itemRouteParameter ) {
            $itemLink = Html::a($label, Yii::$app->urlManager->createUrl([$this->itemRoute, $this->itemRouteParameter => $item['id']]), $linkOptions);
        } else {
            $itemLink = Html::tag('span',$label, $linkOptions);
        }
        //$button = Html::tag('button',$itemLink,['type'=>'button', 'class'=>'menu-item']);

        $edit = Html::a('<i class="fa fa-edit"></i>',Yii::$app->urlManager->createUrl([$this->route.'edit','id'=>$item['id']]),['class'=>'text-info btn-action']);
        $delete = Html::a('<i class="fa fa-remove"></i>',Yii::$app->urlManager->createUrl([$this->route.'delete','id'=>$item['id']]),['class'=>'text-danger btn-action']);
        $handler = Html::a('<i class="fa fa-arrows"></i>','#',['class'=>'draggable-handler']);

        $newWindow = Html::a('<i class="fa fa-external-link"></i>',$url,['target'=>'_blank']);

        $actionContainer = Html::tag('div',$edit.$newWindow.$delete.$handler,['class'=>'item-options']);

        $slideToggle = '';
        /*if( $items ) {
            $slideToggle = Html::a('<i class="fa fa-arrows"></i>', '#', ['class' => 'text-primary draggable-handler ui-sortable-handle']);
        }*/

        return Html::tag('li',  Html::tag('div',$slideToggle . $itemLink . $actionContainer) . $items, $options);
    }

    /**
     * Renders the given items as a dropdown.
     * This method is called to create sub-menus.
     * @param array $items the given items. Please refer to [[Dropdown::items]] for the array structure.
     * @param array $parentItem the parent item information. Please refer to [[items]] for the structure of this array.
     * @return string the rendering result.
     * @since 2.0.1
     */
    protected function renderDropdown($items, $parentItem)
    {
        return Dropdown::widget([
            'options' => ArrayHelper::getValue($parentItem, 'dropDownOptions', []),
            'items' => $items,
            'encodeLabels' => $this->encodeLabels,
            'clientOptions' => false,
            'view' => $this->getView(),
        ]);
    }
}