<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 15.06.16
 * Time: 22:35
 */

namespace app\components\bootstrap;


use yii\base\InvalidConfigException;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;

class Nav extends \yii\bootstrap\Nav
{
    /**
     * Renders the given items as a dropdown.
     * This method is called to create sub-menus.
     * @param array $items the given items. Please refer to [[Dropdown::items]] for the array structure.
     * @param array $parentItem the parent item information. Please refer to [[items]] for the structure of this array.
     * @return string the rendering result.
     * @since 2.0.1
     */
    protected function renderDropdown($items, $parentItem)
    {
        $htmlItems = [];
        foreach ($items as $i => $item) {
            if (isset($item['visible']) && !$item['visible']) {
                continue;
            }
            $htmlItems[] = $this->renderItem($item);
        }
        $options = ArrayHelper::getValue($parentItem, 'dropDownOptions', []);
        Html::addCssClass($options, ['widget' => 'dropdown-menu']);
        return Html::tag('ul', implode("\n", $htmlItems), $options);
    }

    /**
     * Renders a widget's item.
     * @param string|array $item the item to render.
     * @return string the rendering result.
     * @throws InvalidConfigException
     */
    public function renderItem($item)
    {
        if (is_string($item)) {
            return $item;
        }
        if (!isset($item['label'])) {
            throw new InvalidConfigException("The 'label' option is required.");
        }
        $encodeLabel = isset($item['encode']) ? $item['encode'] : $this->encodeLabels;
        $label = $encodeLabel ? Html::encode($item['label']) : $item['label'];
        $options = ArrayHelper::getValue($item, 'options', []);
        $items = ArrayHelper::getValue($item, 'items');
        $url = ArrayHelper::getValue($item, 'url', '#');
        $linkOptions = ArrayHelper::getValue($item, 'linkOptions', []);
        $iconClass = ArrayHelper::getValue($item, 'icon', '');

        if (isset($item['active'])) {
            $active = ArrayHelper::remove($item, 'active', false);
        } else {
            $active = $this->isItemActive($item);
        }

        if (empty($items)) {
            $items = '';
        } else {
            $linkOptions['data-toggle'] = 'dropdown';
            Html::addCssClass($options, ['widget' => 'dropdown']);
            Html::addCssClass($linkOptions, ['widget' => 'dropdown-toggle']);
            if ($this->dropDownCaret !== '') {
                $label .= ' ' . $this->dropDownCaret;
            }
            if (is_array($items)) {
                if ($this->activateItems) {
                    $items = $this->isChildActive($items, $active);
                }
                $items = $this->renderDropdown($items, $item);
            }
        }

        if ($this->activateItems && $active) {
            if (!empty($items)) {
                Html::addCssClass($options, 'nav-expanded');
            }
            Html::addCssClass($options, 'nav-active');
        }

        $icon = Html::tag('i','',['class'=>$iconClass,'aria-hidden'=>'true']);
        $label = Html::tag('span',$label);

        return Html::tag('li', Html::a($icon.$label, $url, $linkOptions) . $items, $options);
    }

    /**
     * Check to see if a child item is active optionally activating the parent.
     * @param array $items @see items
     * @param boolean $active should the parent be active too
     * @return array @see items
     */
    protected function isChildActive($items, &$active)
    {

        foreach ($items as $i => $child) {
            if (ArrayHelper::remove($items[$i], 'active', false) || $this->isItemActive($child)) {
                if( !empty($child['items']) ) {
                    Html::addCssClass($items[$i]['options'], 'nav-expanded');
                }
                Html::addCssClass($items[$i]['options'], 'nav-active');
                if ($this->activateParents) {
                    $active = true;
                }
            }
        }
        return $items;
    }
}