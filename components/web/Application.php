<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 24.07.16
 * Time: 13:11
 */

namespace app\components\web;


class Application extends \yii\web\Application
{

    public function getName()
    {
        return 'Сайт даром CMS';
    }

    public function getVersion()
    {
        return '3.0.0';
    }

}