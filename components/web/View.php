<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 27.07.16
 * Time: 15:24
 */

namespace app\components\web;


class View extends \yii\web\View
{
    public $onlyMain = false;
    public $menuItem;
    public $structureItem;
    public $chainItems;
    public $currentItem;
    public $header = '';
    public $subHeader = '';
}