<?php
/**
 * Created by PhpStorm.
 * SmsSender.php
 * @author Polyankin Semen Vicktorovich Vol][v, http://vap-terra.ru
 * @date 17.05.15
 * @time 20:11
 * @version 0.0.1
 * Copyright © 2014 VAP-TERRA, http://vap-terra.ru
 */

namespace app\components;

use app\models\ext\ConstantExt;
use Exception;
use yii\base\Component;
use yii\web\NotFoundHttpException;

class SmsruSender extends Component
{

    const hostSms = 'http://sms.ru';

    const urlSmsSend = '/sms/send';
    const urlSmsMail = '/sms/mail';
    const urlSmsStatus = '/sms/status';
    const urlSmsCost = '/sms/cost';
    const urlMyBalance = '/my/balance';
    const urlMyLimit = '/my/limit';
    const urlMySenders = '/my/senders';
    const urlAuthGetToken = '/auth/get_token';
    const urlAuthCheck = '/auth/check';
    const urlStoplistAdd = '/stoplist/add';
    const urlStoplistDel = '/stoplist/del';
    const urlStoplistGet = '/stoplist/get';

    const emptyStatus = -10000;
    const okStatus = 100;
    static $maxTime;
    static private $_useCurl = true;

    public static $arResultStatuses = array(
        self::urlSmsSend => array(
            100 => 'Сообщение принято к отправке. На следующих строчках вы найдете идентификаторы отправленных сообщений в том же порядке, в котором вы указали номера, на которых совершалась отправка.',
            200 => 'Неправильный api_id',
            201 => 'Не хватает средств на лицевом счету',
            202 => 'Неправильно указан получатель',
            203 => 'Нет текста сообщения',
            204 => 'Имя отправителя не согласовано с администрацией',
            205 => 'Сообщение слишком длинное (превышает 8 СМС)',
            206 => 'Будет превышен или уже превышен дневной лимит на отправку сообщений',
            207 => 'На этот номер (или один из номеров) нельзя отправлять сообщения, либо указано более 100 номеров в списке получателей',
            208 => 'Параметр time указан неправильно',
            209 => 'Вы добавили этот номер (или один из номеров) в стоп-лист',
            210 => 'Используется GET, где необходимо использовать POST',
            211 => 'Метод не найден',
            212 => 'Текст сообщения необходимо передать в кодировке UTF-8 (вы передали в другой кодировке)',
            220 => 'Сервис временно недоступен, попробуйте чуть позже.',
            230 => 'Сообщение не принято к отправке, так как на один номер в день нельзя отправлять более 60 сообщений.',
            300 => 'Неправильный token (возможно истек срок действия, либо ваш IP изменился)',
            301 => 'Неправильный пароль, либо пользователь не найден',
            302 => 'Пользователь авторизован, но аккаунт не подтвержден (пользователь не ввел код, присланный в регистрационной смс)'
        ),
        self::urlSmsMail => array(),
        self::urlSmsStatus => array(
            -1 => 'Сообщение не найдено.',
            100 => 'Сообщение находится в нашей очереди',
            101 => 'Сообщение передается оператору',
            102 => 'Сообщение отправлено (в пути)',
            103 => 'Сообщение доставлено',
            104 => 'Не может быть доставлено: время жизни истекло',
            105 => 'Не может быть доставлено: удалено оператором',
            106 => 'Не может быть доставлено: сбой в телефоне',
            107 => 'Не может быть доставлено: неизвестная причина',
            108 => 'Не может быть доставлено: отклонено',
            200 => 'Неправильный api_id',
            210 => 'Используется GET, где необходимо использовать POST',
            211 => 'Метод не найден',
            220 => 'Сервис временно недоступен, попробуйте чуть позже.',
            300 => 'Неправильный token (возможно истек срок действия, либо ваш IP изменился)',
            301 => 'Неправильный пароль, либо пользователь не найден',
            302 => 'Пользователь авторизован, но аккаунт не подтвержден (пользователь не ввел код, присланный в регистрационной смс)',
        ),
        self::urlSmsCost => array(
            100 => 'Запрос выполнен. На второй строчке будет указана стоимость сообщения. На третьей строчке будет указана его длина.',
            200 => 'Неправильный api_id',
            202 => 'Неправильно указан получатель',
            207 => 'На этот номер нельзя отправлять сообщения',
            210 => 'Используется GET, где необходимо использовать POST',
            211 => 'Метод не найден',
            220 => 'Сервис временно недоступен, попробуйте чуть позже.',
            300 => 'Неправильный token (возможно истек срок действия, либо ваш IP изменился)',
            301 => 'Неправильный пароль, либо пользователь не найден',
            302 => 'Пользователь авторизован, но аккаунт не подтвержден (пользователь не ввел код, присланный в регистрационной смс)',
        ),
        self::urlMyBalance => array(
            100 => 'Запрос выполнен. На второй строчке вы найдете ваше текущее состояние баланса.',
            200 => 'Неправильный api_id',
            210 => 'Используется GET, где необходимо использовать POST',
            211 => 'Метод не найден',
            220 => 'Сервис временно недоступен, попробуйте чуть позже.',
            300 => 'Неправильный token (возможно истек срок действия, либо ваш IP изменился)',
            301 => 'Неправильный пароль, либо пользователь не найден',
            302 => 'Пользователь авторизован, но аккаунт не подтвержден (пользователь не ввел код, присланный в регистрационной смс)',
        ),
        self::urlMyLimit => array(
            100 => 'Запрос выполнен. На второй строчке вы найдете количество номеров, на которое вы можете отправлять сообщения внутри дня. На третьей строчке - количество номеров, на которые вы уже отправили сообщения внутри текущего дня.',
            200 => 'Неправильный api_id',
            210 => 'Используется GET, где необходимо использовать POST',
            211 => 'Метод не найден',
            220 => 'Сервис временно недоступен, попробуйте чуть позже.',
            300 => 'Неправильный token (возможно истек срок действия, либо ваш IP изменился)',
            301 => 'Неправильный пароль, либо пользователь не найден',
            302 => 'Пользователь авторизован, но аккаунт не подтвержден (пользователь не ввел код, присланный в регистрационной смс)',
        ),
        self::urlMySenders => array(
            100 => 'Запрос выполнен. На второй и последующих строчках вы найдете ваших одобренных отправителей, которые можно использовать в параметре &from= метода sms/send.',
            200 => 'Неправильный api_id',
            210 => 'Используется GET, где необходимо использовать POST',
            211 => 'Метод не найден',
            220 => 'Сервис временно недоступен, попробуйте чуть позже.',
            300 => 'Неправильный token (возможно истек срок действия, либо ваш IP изменился)',
            301 => 'Неправильный пароль, либо пользователь не найден',
            302 => 'Пользователь авторизован, но аккаунт не подтвержден (пользователь не ввел код, присланный в регистрационной смс)',
        ),
        self::urlAuthGetToken => array(),
        self::urlAuthCheck => array(
            100 => 'ОК, номер телефона и пароль совпадают.',
            300 => 'Неправильный token (возможно истек срок действия, либо ваш IP изменился)',
            301 => 'Неправильный пароль, либо пользователь не найден',
            302 => 'Пользователь авторизован, но аккаунт не подтвержден (пользователь не ввел код, присланный в регистрационной смс)',
        ),
        self::urlStoplistAdd => array(
            100 => 'Номер добавлен в стоплист.',
            202 => 'Номер телефона в неправильном формате',
        ),
        self::urlStoplistDel => array(
            100 => 'Номер удален из стоплиста.',
            202 => 'Номер телефона в неправильном формате',
        ),
        self::urlStoplistGet => array(
            100 => 'Запрос обработан. На последующих строчках будут идти номера телефонов, указанных в стоплисте в формате номер;примечание.',
        ),
    );

    public static $arOkStatuses = array(
        self::urlSmsSend => array(100),
        self::urlSmsMail => array(),
        self::urlSmsStatus => array(100, 101, 102, 103),
        self::urlSmsCost => array(100),
        self::urlMyBalance => array(100),
        self::urlMyLimit => array(100),
        self::urlMySenders => array(100),
        self::urlAuthGetToken => array(),
        self::urlAuthCheck => array(100),
        self::urlStoplistAdd => array(100),
        self::urlStoplistDel => array(100),
        self::urlStoplistGet => array(100),
    );

    public $apiId;
    public $login;
    public $password;
    public $sender = '';
    public $test = false;

    protected $_partnerId = 17795;
    protected $_time = '';


    protected $_to = array();
    protected $_text = '';
    protected $_transliteration = false;

    protected $_multiMessages = array();
    protected $_token = '';
    protected $_lastRequestType = '';
    protected $_lastAnswer = null;
    protected $_lastStatusCode = self::emptyStatus;

    /*public function __construct( $apiId, $login='', $password='' )
    {
        self::$maxTime = 7 * 24 * 60 * 60;
        self::$_useCurl  = function_exists('curl_init');
        $this->apiId    = $apiId;
        $this->login    = $login;
        $this->password = $password;
    }*/
    /**
     *
     */
    public function init()
    {
        parent::init();
        self::$maxTime = 7 * 24 * 60 * 60;
        self::$_useCurl = function_exists('curl_init');
        try {
            $apiKey = ConstantExt::getByTid('api_key_smsru');
            $this->apiId = $apiKey->value;
        } catch( NotFoundHttpException $e ){
        }
    }

    /**
     * @param $phone
     * @return int
     */
    static public function preparePhone($phone)
    {
        /** @var array $replaces */
        static $replaces = array('.' => '', '-' => '', '+' => '', '(' => '', ')' => '', ' ' => '');
        return strtr($phone, $replaces);
    }

    /**
     * @param $phoneNum
     * @param $text
     * @return $this
     */
    public function multi($phoneNum, $text)
    {
        if (strlen($text) > 0) {
            if (is_numeric($phoneNum)) {
                $phoneNum = self::preparePhone($phoneNum);
                if ($phoneNum) {
                    $this->_multiMessages[md5($phoneNum . $text)] = array($phoneNum, $text);
                }
            } else if (is_string($phoneNum)) {
                $phoneNum = explode(',', strtr($phoneNum, array(';' => ',')));
            }
            if (is_array($phoneNum)) {
                foreach ($phoneNum as $num) {
                    $this->multi($num, $text);
                }
            }
        }
        return $this;
    }

    /**
     * @param $phoneNum
     * @return $this
     */
    public function to($phoneNum)
    {
        if (is_string($phoneNum) && strpos($phoneNum, ',') !== false) {
            $phoneNum = explode(',', $phoneNum);
        }
        if (is_array($phoneNum)) {
            foreach ($phoneNum as $num) {
                $this->to($num);
            }
        } else {
            $phoneNum = self::preparePhone($phoneNum);
            if (!empty($phoneNum)) {
                $this->_to[$phoneNum] = $phoneNum;
            }
        }
        return $this;
    }

    public function sender($sender)
    {
        $this->sender = $sender;
        return $this;
    }

    public function partner($partnerId)
    {
        $this->_partnerId = $partnerId;
        return $this;
    }

    public function time($time)
    {
        $this->_time = $time;
        return $this;
    }

    public function test($test = true)
    {
        $this->test = $test;
        return $this;
    }

    /**
     * @return $this
     */
    public function clearTo()
    {
        $this->_to = array();
        return $this;
    }

    /**
     * @return $this
     */
    public function clearMultiMessages()
    {
        $this->_multiMessages = array();
        return $this;
    }

    /**
     * @param $urlRequestType
     * @param $params
     * @return bool
     * @throws Exception
     */
    public function query($urlRequestType, $params)
    {
        $this->_lastStatusCode = self::emptyStatus;
        $this->_lastRequestType = $urlRequestType;
        $url = self::hostSms . $urlRequestType;
        $body = null;
        if (strlen($this->apiId) == 0) {
            throw new Exception('Not found api key for sms.ru service');
        }
        $params['api_id'] = $this->apiId;
        if ($this->_partnerId) {
            $params['partner_id'] = $this->_partnerId;
        }
        if ($this->password
            && $this->login
            && function_exists('hash')
        ) {
            $this->authGetToken();
            $params['login'] = $this->login;
            $params['sha512'] = hash('sha512', $this->password . $this->_token . $this->apiId);
            $params['token'] = $this->_token;
        }

        if (self::$_useCurl) {
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            $body = curl_exec($ch);
            curl_close($ch);
        } else {
            $query = '';
            foreach ($params as $key => $value) {
                if ($query) {
                    $query .= '&';
                }
                $query .= $key . '=' . urlencode($value);
            }
            $body = file_get_contents($url . '?' . $query);
        }
        if ($body) {
            $this->_lastAnswer = explode("\n", $body);
            $this->_lastStatusCode = array_shift($this->_lastAnswer);
            if ($this->_lastStatusCode == 100) {
                return true;
            }
        }
        return false;
    }

    //sms/send
    /**
     * @param $text
     * @return bool
     * @throws Exception
     */
    public function sms($text)
    {
        $this->_lastStatusCode = self::emptyStatus;
        if (!strlen($text) || !count($this->_to)) {
            return null;
        }
        $params = array(
            'to' => implode(',', $this->_to),
            'text' => $text//mb_convert_encoding( $text, 'utf-8' )
        );
        if ($this->_time && $this->_time < (time() + self::$maxTime)) {
            $params['time'] = $this->_time;
        }
        if ($this->_transliteration) {
            $params['translit'] = '1';
        }
        if ($this->test) {
            $params['test'] = '1';
        }
        if ($this->sender) {
            $params['from'] = $this->sender;
        }

        if ($this->query(self::urlSmsSend, $params)) {
            return $this->_lastAnswer;
        }
        return null;
    }

    public function multiSms()
    {
        $this->_lastStatusCode = self::emptyStatus;
        if (!count($this->_multiMessages)) {
            return null;
        }
        $params = array();
        foreach ($this->_multiMessages as $phone => $text) {
            $params['multi[' . $phone . ']'] = mb_convert_encoding($text, 'utf-8');
        }
        if ($this->_time && $this->_time < (time() + self::$maxTime)) {
            $params['time'] = $this->_time;
        }
        if ($this->_transliteration) {
            $params['translit'] = '1';
        }
        if ($this->test) {
            $params['test'] = '1';
        }
        if ($this->sender) {
            $params['from'] = $this->sender;
        }
        if ($this->query(self::urlSmsSend, $params)) {
            return $this->_lastAnswer;
        }
        return null;
    }
    //sms/mail
    /**
     * @param $text
     */
    public function smsEmail($text)
    {

    }

    //sms/status
    /**
     * @param $id
     * @return bool
     * @throws Exception
     */
    public function statusSms($id)
    {
        $params = array(
            'id' => $id
        );
        return $this->query(self::urlSmsStatus, $params);
    }

    //sms/cost
    /**
     * @param $text
     */
    public function costSms($text)
    {
    }

    //my/balance
    /**
     *
     */
    public function balance()
    {
    }

    //my/limit
    /**
     * @param $text
     */
    public function limit($text)
    {
    }

    //my/senders
    /**
     * @param $text
     */
    public function senders($text)
    {
    }

    //auth/get_token
    /**
     * @return mixed|string
     */
    public function authGetToken()
    {
        if (self::$_useCurl) {
            $ch = curl_init(self::hostSms . self::urlAuthGetToken);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
            $this->_token = curl_exec($ch);
            curl_close($ch);
        } else {
            $this->_token = file_get_contents(self::hostSms . self::urlAuthGetToken);
        }
        return $this->_token;
    }

    //auth/check
    /**
     *
     */
    public function authCheck()
    {
        return $this->query(self::urlAuthCheck, array());
    }
    //stoplist/add
    /**
     *
     */
    public function addStoplist()
    {
    }

    //stoplist/del
    public function delStoplist()
    {
    }

    //stoplist/get
    public function getStoplist()
    {
    }

    //sms/ucs
    public function ucs($xmlText)
    {
    }

    public function getStatusCode()
    {
        return $this->_lastStatusCode;
    }

    public function getStatusCodeText()
    {
        if ($this->_lastRequestType
            && isset(self::$arResultStatuses[$this->_lastRequestType])
            && isset(self::$arResultStatuses[$this->_lastRequestType][$this->_lastStatusCode])
        ) {
            return self::$arResultStatuses[$this->_lastRequestType][$this->_lastStatusCode];
        }
        return 'undefined';
    }

    public function isOkStatus()
    {
        if ($this->_lastRequestType
            && isset(self::$arOkStatuses[$this->_lastRequestType])
            && in_array($this->_lastStatusCode, self::$arOkStatuses[$this->_lastRequestType])
        ) {
            return true;
        }
        return $this->_lastStatusCode === self::okStatus;
    }

    public function isEmptyStatus()
    {
        return $this->_lastStatusCode === self::emptyStatus;
    }
}
