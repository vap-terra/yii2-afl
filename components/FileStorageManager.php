<?php
/**
 * Created by PhpStorm.
 * FileStorageManager.php
 * @package /
 * @author Polyankin Semen Vicktorovich Vol][v, http://vap-terra.ru
 * @date 15.07.15
 * @time 10:18
 * @version 0.0.1
 * Copyright © 2014 VAP-TERRA, http://vap-terra.ru
 */

namespace app\components;

use Yii;
use yii\base\Component;
use yii\base\Model;
use yii\db\ActiveRecord;

class FileStorageManager extends Component
{
    const CLUSTER_BASE = 1024;

    static public $SI_PREFIX = array('B', 'KB', 'MB', 'GB', 'TB', 'EB', 'ZB', 'YB');

    const TYPE_DOC = 'docs';

    const TYPE_SCRIPT = 'scripts';
    const TYPE_IMAGE = 'images';
    const TYPE_ICONS = 'icons';
    const TYPE_ARCHIVE = 'archives';
    const TYPE_AUDIO = 'audio';
    const TYPE_VIDEO = 'video';
    const TYPE_UNTRUSTED = 'untrusted';

    /**
     * @var array $FILE_GROUPS
     */
    static public $FILE_GROUPS = [
        'txt'  => self::TYPE_DOC,
        'htm'  => self::TYPE_SCRIPT,
        'html' => self::TYPE_SCRIPT,
        'php'  => self::TYPE_SCRIPT,
        'css'  => self::TYPE_SCRIPT,
        'js'   => self::TYPE_SCRIPT,
        'json' => self::TYPE_SCRIPT,
        'xml'  => self::TYPE_SCRIPT,
        'swf'  => self::TYPE_SCRIPT,
        // images
        'png'  => self::TYPE_IMAGE,
        'jpe'  => self::TYPE_IMAGE,
        'jpeg' => self::TYPE_IMAGE,
        'jpg'  => self::TYPE_IMAGE,
        'gif'  => self::TYPE_IMAGE,
        'bmp'  => self::TYPE_IMAGE,
        'ico'  => self::TYPE_IMAGE,
        'tiff' => self::TYPE_IMAGE,
        'tif'  => self::TYPE_IMAGE,
        'svg'  => self::TYPE_IMAGE,
        'svgz' => self::TYPE_IMAGE,
        // archives
        'zip' => self::TYPE_ARCHIVE,
        'rar' => self::TYPE_ARCHIVE,
        'exe' => self::TYPE_ARCHIVE,
        'msi' => self::TYPE_ARCHIVE,
        'cab' => self::TYPE_ARCHIVE,
        // audio/video
        'mp3' => self::TYPE_AUDIO,
        'org' => self::TYPE_AUDIO,
        'wav' => self::TYPE_AUDIO,
        'qt'  => self::TYPE_VIDEO,
        'mov' => self::TYPE_VIDEO,
        'flv'  => self::TYPE_VIDEO,
        'mpeg' => self::TYPE_VIDEO,
        'avi' => self::TYPE_VIDEO,
        'mp4' => self::TYPE_VIDEO,
        // adobe
        'pdf' => self::TYPE_DOC,
        'psd' => self::TYPE_IMAGE,
        'ai'  => self::TYPE_IMAGE,
        'eps' => self::TYPE_IMAGE,
        'ps'  => self::TYPE_IMAGE,
        // ms office
        'doc' => self::TYPE_DOC,
        'rtf' => self::TYPE_DOC,
        'xls' => self::TYPE_DOC,
        'ppt' => self::TYPE_DOC,
        // open office
        'odt' => self::TYPE_DOC,
        'ods' => self::TYPE_DOC,
    ];

    public $domainName = '';

    public $basePath = '';

    protected $_currentLevel = 0;

    /**
     *
     */
    public function init()
    {
        Yii::setAlias('@storage','@webroot/'.$this->basePath);
        parent::init();
    }

    /**
     * @param $sSize
     * @return int|string
     */
    static public function convertPHPSizeToBytes($sSize)
    {
        if (is_numeric($sSize)) {
            return $sSize;
        }
        $sSuffix = substr($sSize, -1);
        $iValue = substr($sSize, 0, -1);
        switch (strtoupper($sSuffix)) {
            case 'P':
                $iValue *= 1024;
            case 'T':
                $iValue *= 1024;
            case 'G':
                $iValue *= 1024;
            case 'M':
                $iValue *= 1024;
            case 'K':
                $iValue *= 1024;
                break;
        }
        return $iValue;
    }

    /**
     * @return mixed
     */
    static public function getMaximumFileUploadSize()
    {
        return min(self::convertPHPSizeToBytes(ini_get('post_max_size')), self::convertPHPSizeToBytes(ini_get('upload_max_filesize')));
    }

    /**
     * @param $bytes
     * @return array
     */
    static public function getSHUBytes($bytes)
    {
        $class = min((int)log($bytes, self::CLUSTER_BASE), count(self::$SI_PREFIX) - 1);
        return array(sprintf('%1.2f', $bytes / pow(self::CLUSTER_BASE, $class)), self::$SI_PREFIX[$class]);
    }

    /**
     * @param $fileName
     * @return mixed
     */
    static function getFileBaseName($fileName)
    {
        return pathinfo($fileName, PATHINFO_BASENAME);
    }

    /**
     * @param $fileName
     * @return mixed
     */
    static function getFileName($fileName)
    {
        return pathinfo($fileName, PATHINFO_FILENAME);
    }

    /**
     * @param $fileName
     * @return string
     */
    static function getFileExtension($fileName)
    {
        return strtolower(pathinfo($fileName, PATHINFO_EXTENSION));
    }

    /**
     * @param Model|null $model
     * @return string
     */
    function getModelRelativePath(Model $model = null)
    {
        $modelPath = '';
        if ($model) {
            if ($model instanceof ActiveRecord) {
                $modelPath = 'items/' . strtr($model->tableName(), ['{' => '', '}' => '', '%' => '']) . '/';
            } else {
                $modelPath = 'items/' . $model->formName() . '/';
            }
        }
        return $modelPath;
    }

    /**
     * @param $fileName
     * @param Model|null $model
     * @param null $subDirs
     * @return string
     */
    function getTmpFileHref($fileName, Model $model = null, $subDirs = null)
    {
        return $this->getTmpHref($model) .
        self::getFileRelativePathByExt(self::getFileExtension($fileName), $subDirs) .
        $fileName;
    }

    /**
     * @param $fileName
     * @param Model|null $model
     * @param null $subDirs
     * @return string
     */
    function getTmpFilePath($fileName, Model $model = null, $subDirs = null)
    {
        return $this->getTmpPath($model) .
        self::getFileRelativePathByExt(self::getFileExtension($fileName), $subDirs) .
        $fileName;
    }

    /**
     * @param $fileName
     * @param Model|null $model
     * @param null $subDirs
     * @return string
     */
    function getTmpPathByFile($fileName, Model $model = null, $subDirs = null)
    {
        return $this->getTmpPath($model) .
        self::getFileRelativePathByExt(self::getFileExtension($fileName), $subDirs);
    }

    /**
     * @param $fileName
     * @param Model|null $model
     * @param null $subDirs
     * @return string
     */
    function getFileHref($fileName, Model $model = null, $subDirs = null)
    {
        return $this->getHref() .
        self::getFileRelativePathByExt(self::getFileExtension($fileName), $subDirs) .
        $fileName;
    }

    /**
     * @param $fileName
     * @param Model|null $model
     * @param null $subDirs
     * @return string
     */
    function getFilePath($fileName, Model $model = null, $subDirs = null)
    {
        return $this->getPath($model) .
        self::getFileRelativePathByExt(self::getFileExtension($fileName), $subDirs) .
        $fileName;
    }

    /**
     * @param $fileName
     * @param Model|null $model
     * @param null $subDirs
     * @return string
     */
    function getIconPath($fileName, Model $model = null, $subDirs = null)
    {
        $path = '';
        if ($subDirs) {
            if (!is_array($subDirs)) {
                $path = '/' . $subDirs . '/';
            } else if (count($subDirs)) {
                $path = '/' . implode('/', $subDirs) . '/';
            }
        }
        return $this->getPath($model) . self::TYPE_ICONS . $path . '/' . $fileName;
    }
    /**
     * @param $fileName
     * @param Model|null $model
     * @param null $subDirs
     * @return string
     */
    function getIconHref($fileName, Model $model = null, $subDirs = null)
    {
        $path = '';
        if ($subDirs) {
            if (!is_array($subDirs)) {
                $path = '/' . $subDirs . '/';
            } else if (count($subDirs)) {
                $path = '/' . implode('/', $subDirs) . '/';
            }
        }
        return $this->getHref() . self::TYPE_ICONS . $path . '/' . $fileName;
    }

    /**
     * @param Model|null $model
     * @return string
     */
    function getTmpPath(Model $model = null)
    {
        return Yii::getAlias('@storage') . '/tmp/' . self::getModelRelativePath($model);
    }

    /**
     * @return string
     */
    function getStorageHref()
    {
        return ( $this->domainName ? ('//' . $this->domainName . '/') : '/' ) . $this->basePath . '/';
    }

    /**
     * @param Model|null $model
     * @return string
     */
    function getTmpHref(Model $model = null)
    {
        return $this->getStorageHref() . 'tmp/' . self::getModelRelativePath($model);
    }

    /**
     * @param Model|null $model
     * @return string
     */
    function getPath(Model $model = null)
    {
        return Yii::getAlias('@storage') . '/' . self::getModelRelativePath($model);
    }

    /**
     * @param Model|null $model
     * @return string
     */
    function getHref(Model $model = null)
    {
        return $this->getStorageHref() . self::getModelRelativePath($model);
    }

    /**
     * @param string $extension
     * @return string
     */
    static public function getFileGroup($extension)
    {
        $group = 'untrusted';
        if ($extension && isset(self::$FILE_GROUPS[$extension])) {
            $group = self::$FILE_GROUPS[$extension];
        }
        return $group;
    }

    /**
     * @param string $extension
     * @param string $subDirs
     * @return string
     */
    static public function getFileRelativePathByExt($extension, $subDirs = null)
    {
        $path = '';
        if ($subDirs) {
            if (!is_array($subDirs)) {
                $path = '/' . $subDirs . '/';
            } else if (count($subDirs)) {
                $path = '/' . implode('/', $subDirs) . '/';
            }
        }
        return self::getFileGroup($extension) . $path . '/';
    }

    /**
     * @param string $str
     * @param int $level
     * @param int $type
     * @return mixed
     */
    static public function getNestingDirPath($str, $level = 3, $type = 0)
    {
        $str = intval($str);
        $level = intval($level);
        $sId = sprintf('%0' . $level . 'd', $str);
        $aPath = array();
        for ($i = 0; $i < $level; $i++) {
            $aPath[$i] = $sId{$i};
        }
        if ($type == 0) {
            return implode('/', $aPath);
        }
        return $aPath;
    }

    /**
     * @param $pathFile
     * @return array
     */
    public function getFileInfo($pathFile/*, $relativePathFolder*/)
    {
        if (!empty($pathFile) && is_file($pathFile)) {
            $path = pathinfo($pathFile, PATHINFO_DIRNAME);
            $mTime = filemtime($pathFile);
            $fileName = self::getFileBaseName($pathFile);
            $ext = self::getFileExtension($pathFile);
            return [
                'num' => 0,
                'name' => $fileName,
                'label' => self::getFileName($fileName),
                'ext' => $ext,
                'url' => self::getFileHref($fileName),//'//'.$this->domainName . $relativePathFolder .'/'. $fileName,
                'path' => $path,
                'relativePath' => '/' . self::getFileRelativePathByExt($ext, ''),
                'date_update' => Yii::$app->formatter->asDate(date('Y-m-d', $mTime)),
                'time_update' => Yii::$app->formatter->asTime(date('H:i:s', $mTime)),
                'group' => self::getFileGroup($ext),
            ];
        }
        return [];
    }

    /**
     * @param string $folder
     * @param array $excludeFiles
     * @return array
     */
    public function getFilesInFolder($folder = '/images', $excludeFiles = [])
    {
        if (strpos($folder, '/') !== 0) {
            $folder = '/' . $folder;
        }
        $path = Yii::getAlias('@storage' . $folder);
        $handle = opendir($path);
        $files = [];
        if (!empty($handle)) {
            $num = 1;
            while (($fileName = readdir($handle)) !== false) {
                $subPath = $path . '/' . $fileName;
                if (is_file($subPath) && !isset($excludeFiles[$fileName])) {
                    $mTime = filemtime($subPath);
                    $ext = self::getFileExtension($fileName);
                    $item = self::getFileInfo($subPath);/*[
                        'num' => $num,
                        'name' => $fileName,
                        'label' => self::getFileName($fileName),
                        'ext' => $ext,
                        'url' => '//'.$this->domainName . $folder .'/'. $fileName,
                        'path' => $subPath,
                        'date_update' => Yii::$app->formatter->asDate(date('Y-m-d',$mTime)),
                        'time_update' => Yii::$app->formatter->asTime(date('H:i:s',$mTime)),
                        'group' => self::getFileGroup( $ext ),
                    ];*/
                    $files[$fileName] = $item;
                    $num++;
                }
            }
            closedir($handle);
        }
        return $files;
    }

    /**
     * @param string $folder
     * @param int $level
     * @param array $excludeFolders
     * @return array
     */
    public function getFolders($folder = '/', $level = 0, $excludeFolders = [])
    {
        if (strpos($folder, '/') !== 0) {
            $folder = '/' . $folder;
        }
        $this->_currentLevel = 0;
        return $this->_getFolderRecursive($folder, $level, $excludeFolders);
    }

    /**
     * @param string $folder
     * @param int $level
     * @param array $excludeFolders
     * @return array
     */
    protected function _getFolderRecursive($folder = '/', $level = 0, $excludeFolders = [])
    {
        $path = Yii::getAlias('@storage' . $folder);
        $handle = opendir($path);
        $folders = [];
        if (!empty($handle)) {
            while (($subFolder = readdir($handle)) !== false) {
                $subPath = $path . '/' . $subFolder;
                if (is_dir($subPath) && !isset($excludeFolders[$subFolder])) {
                    //$ctime = filectime($subPath);
                    $item = [
                        'label' => $subFolder,
                        'url' => '//' . $this->domainName . '/' . $subFolder,
                        'path' => $subPath,
                    ];

                    if (!$level || $level < $this->_currentLevel) {
                        $this->_currentLevel++;
                        $item['items'] = $this->_getFolderRecursive($subFolder, $level, $excludeFolders);
                        $this->_currentLevel--;
                    }

                    $folders[$subFolder] = $item;
                }
            }
            closedir($handle);
        }
        return $folders;
    }
}