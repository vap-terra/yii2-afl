<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 15.10.16
 * Time: 16:26
 */

namespace app\components;


use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\FileHelper;


class Theme extends \yii\base\Theme
{
    const TYPE_FRONTEND = 'frontend';
    const TYPE_BACKEND = 'backend';

    public $type;

    public function applyTo($path)
    {
        $pathMap = $this->pathMap;

        if (empty($pathMap)) {
            if (($basePath = $this->getBasePath()) === null) {
                throw new InvalidConfigException('The "basePath" property must be set.');
            }
            $pathMap = [Yii::$app->getBasePath() => [$basePath]];
        }

        $path = FileHelper::normalizePath($path);
        foreach ($pathMap as $from => $tos) {
            $from = FileHelper::normalizePath(Yii::getAlias($from)) . DIRECTORY_SEPARATOR;
            if (strpos($path, $from) === 0) {
                $n = strlen($from);
                foreach ((array) $tos as $to) {
                    $to = FileHelper::normalizePath(Yii::getAlias($to)) . DIRECTORY_SEPARATOR;
                    $file = $to . substr($path, $n);
                    if (is_file($file)) {
                        return $file;
                    }
                }
            }
        }
        return $path;
    }
}