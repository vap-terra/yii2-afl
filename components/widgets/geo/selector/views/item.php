<?php

/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 24.10.2016
 * Time: 21:28
 */
use app\components\web\View;
use yii\bootstrap\ActiveForm;
use yii\db\ActiveRecord;

/* @var $this View */
/* @var $title string */
/* @var $objects [] */
/* @var $model ActiveRecord */
/* @var $form ActiveForm */

app\components\widgets\geo\selector\GeoSelectorControllerAsset::register($this);
$this->registerJs("
    $(document).ready( function() {
        /*$('.linker-wrapper').GeoSelectorController(
            {
                popup: '$.fancybox',
                showLoad: function(){\$.fancybox.showLoading();},
                hideLoad: function(){\$.fancybox.hideLoading();}
            }
        );*/
    });", View::POS_END, 'form-init');
?>
<div class="geo-selector">
    <div class="geo-selected-items">
        <p><a href="#"><?php echo $title?></a></p>
        <p>Страна, регион, район, населенный пункт, район населенного пункта <a href="#">изменить</a></p>
    </div>
    <div class="hidden">
        <div class="geo-selector-wrapper">
            <div class="geo-selector-lists-wrapper">
                <?php foreach( $objects as $object ){ ?>
                <div class="objects-wrapper">
                    <h3><?php echo $object['title'];?></h3>
                    <div class="objects-list-wrapper"></div>
                    <?php if( !empty( $object['attribute'] ) ){ ?>
                    <?php echo $form->field($model,$object['attribute'])->hiddenInput();?>
                    <?php } ?>
                </div>
                <?php } ?>
            </div>
            <button type="button">Выбрать</button>
            <button type="button">Отменить</button>
        </div>
    </div>
</div>
