<?php

/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 24.10.2016
 * Time: 20:41
 */

namespace app\components\widgets\geo\selector;

class GeoSelectorWidget extends \app\components\Widget
{
    public $title = '';
    public $objects = [
        'country' => [
            'title'=>'Страна',
            'attribute' => 'geo_country_id',
            'controller' => null,
            'query' => null,
            'parent' => null,
        ],
        'region' => [
            'title'=>'Регион',
            'attribute'=>'geo_region_id',
            'controller' => null,
            'query' => null,
            'parent' => [
                'country'=>'geo_country_id'
            ],
        ],
        'regionArea' => [
            'title'=>'Район',
            'attribute' => 'geo_region_area_id',
            'controller' => null,
            'query' => null,
            'parent' => [
                'region'=>'geo_region_id'
            ],
        ],
        'locality' => [
            'title'=>'Населенный пункт',
            'attribute'=>'geo_locality_id',
            'controller' => null,
            'query' => null,
            'parent' => [
                'regionArea'=>'geo_region_area_id'
            ],
        ],
        'localityArea' => [
            'title'=>'Район населенного пункта',
            'attribute'=>'geo_locality_area_id',
            'controller' => null,
            'query' => null,
            'parent' => [
                'locality'=>'geo_locality_id'
            ],
        ],
    ];
    public $needSelect;
    public $form;
    public $model;

    public function run()
    {


        return $this->render(
            'item',
            [
                'form' => $this->form,
                'model' => $this->model,
                'objects' => $this->objects,
                'title' => $this->title,
            ]
        );
    }
}