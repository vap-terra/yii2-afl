<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 25.07.16
 * Time: 9:38
 */

namespace app\components\widgets\geo\selector;

use yii\web\AssetBundle;

class GeoSelectorControllerAsset extends AssetBundle
{
    public $sourcePath = '@app/components/widgets/geo/selector/src';
    public $css = [
        'css/style.css',
    ];

    public $js = [
        'js/geo-selector-controller.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
        'app\assets\jquery\UIAsset'
    ];
}