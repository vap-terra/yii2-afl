<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 18.10.16
 * Time: 13:15
 */
use app\components\web\View;
use app\components\widgets\linker\LinkerWidget;
use yii\bootstrap\Button;
use yii\bootstrap\ButtonGroup;
use yii\data\ActiveDataProvider;

/* @var $widget LinkerWidget */
/* @var $dataProvider ActiveDataProvider */


$url = '';

app\components\widgets\linker\LinkerControllerAsset::register($this);
$this->registerJs("jQuery('#{$widget->id}').LinkerController();", View::POS_END, 'linker-init-'.$widget->id);
?>
<div class="linker-wrapper" id="<?php echo $widget->id?>">
    <div class="form-group linker-container">
        <h1><?php echo $widget->model->getAttributeLabel($widget->attribute);?></h1>
        <div class="relation-list-wrapper">
            <?php
            $keys = $dataProvider->getKeys();
            foreach( $dataProvider->getModels() as $index=>$relatedModel ){
                $key = $keys[$index];
            ?>
            <div class="relation-item">
                <span class="relation-item-info">
                    <?php
                        echo strtr($widget->templateTitle,$relatedModel->getReplacementTagsValues());
                    ?>
                </span>
                <span class="relation-actions">
                <?php
                echo $widget->renderActions($key,$index);
                ?>
                </span>
            </div>
            <?php } ?>
        </div>
        <?php
            echo $widget->renderAction($widget->createAction);
        ?>
    </div>
</div>
