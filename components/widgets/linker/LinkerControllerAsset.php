<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 25.07.16
 * Time: 9:38
 */

namespace app\components\widgets\linker;

use yii\web\AssetBundle;

class LinkerControllerAsset extends AssetBundle
{
    public $sourcePath = '@app/components/widgets/linker/src';
    public $css = [
        'css/style.css',
    ];

    public $js = [
        'js/linker-controller.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
        'app\assets\jquery\UIAsset'
    ];
}