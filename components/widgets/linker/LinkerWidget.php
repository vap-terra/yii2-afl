<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 18.10.16
 * Time: 13:06
 */

namespace app\components\widgets\linker;


use app\models\BaseActiveQuery;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Button;
use yii\bootstrap\ButtonGroup;
use yii\bootstrap\Widget;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;

class LinkerWidget extends Widget
{
    public $options = ['class' => 'action-column'];
    public $itemOptions = ['class' => 'action-column'];
    public $actionPanelOptions = ['class' => 'action-column'];
    /**
     * @var ActiveForm $form
     */
    public $form;
    public $model;
    public $attribute;
    public $relatedModelClass;
    public $relationModelClass;
    public $relationName;


    public $controller;
    public $urlCreator;

    public $templateActions = '{sortable} {edit} {delete}';
    public $templateTitle = '{name}';
    public $createAction = 'add';

    public $actions = [
        '{sortable}'=>[
            'tagName' => 'button',
            'label' => '<i class="glyphicon glyphicon-move"></i>',
            'encodeLabel' => false,
            'options' => [
                'type'=>'button',
                'class'=>'btn btn-default btn-sm',
                'href'=>null,
            ]
        ],
        '{edit}'=>[
            'tagName' => 'button',
            'label' => '<i class="glyphicon glyphicon-pencil"></i>',
            'encodeLabel' => false,
            'options' => [
                'type'=>'button',
                'class'=>'btn btn-info btn-sm',
                'href'=>null,
            ]
        ],
        '{delete}'=>[
            'tagName' => 'button',
            'label' => '<i class="glyphicon glyphicon-minus-sign"></i>',
            'encodeLabel' => false,
            'options' => [
                'type'=>'button',
                'class'=>'btn btn-danger btn-sm',
                'href'=>null,
            ]
        ],
        '{add}'=>[
            'tagName' => 'button',
            'label' => '<i class="glyphicon glyphicon-plus-sign"></i>',
            'encodeLabel' => false,
            'options' => [
                'type'=>'button',
                'class'=>'btn btn-success btn-sm',
                'href'=>null,
            ]
        ],
    ];

    public $sortable = [];

    public $pageSize = 30;
    public $pageParam = 'page';

    public function init()
    {
        parent::init();
        if( !$this->controller ){
            $this->controller = Yii::$app->controller->getRoute();
        }
    }

    public function run()
    {
        /* @var $query BaseActiveQuery */
        $query = call_user_func([$this->relatedModelClass,'find']);

        $countQuery = clone $query;
        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
                'pagination' => [
                    'pageSize' => $this->pageSize,
                    'pageParam' => $this->pageParam,
                    'totalCount' => $countQuery->count(),
                ],
            ]
        );

        return $this->render(
            'list',
            [
                'widget' => $this,
                'dataProvider' => $dataProvider,
            ]
        );
    }

    public function renderAction($action,$key=null,$index=null,$num=null)
    {
        $options = [
            'tagName' => 'button',
            'label' => Yii::t('app/layout',$action),
            'encodeLabel' => false,
            'options' => [
                'type'=>'button',
                'class'=>'btn btn-sm',
                'href'=>null,
            ]
        ];

        if( isset( $this->actions['{'.$action.'}'] ) ){
            $options = array_merge($options,$this->actions['{'.$action.'}']);
        }
        if( empty( $options['options'] ) ){
            $options['options'] = [];
        }

        if( empty( $options['options']['href'] ) ){
            $options['options']['href'] = $this->createUrl($action,$this->model,$key,$index);
        }

        if( empty( $options['options']['class'] ) ){
            $options['options']['class'] = 'btn btn-sm';
        }
        $options['options']['class'] .= ' btn-action';
        $options['options']['data-action'] = $action;
        $options['options']['data-index'] = $index;
        return Button::widget($options);
    }

    public function renderActions($key,$index)
    {
        $buttons = [];
        if( preg_match_all("/{(\\w+)}/",$this->templateActions,$matches) ){
            foreach( $matches[1] as $num=>$action ){
                $buttons[] = $this->renderAction($action,$key,$index,$num);
            }
        }
        return ButtonGroup::widget( [ 'buttons' =>$buttons, 'options'=>['class'=>'btn-group-sm'] ] );
    }

    public function createUrl($action, $model=null, $key=null, $index=null)
    {
        if (is_callable($this->urlCreator)) {
            return call_user_func($this->urlCreator, $action, $model, $key, $index, $this);
        } else {
            $params = [];
            if( !is_null($key) ) {
                $params = is_array($key) ? $key : ['id' => (string)$key];
            }
            $params[0] = $this->controller ? $this->controller . '/' . $action : $action;

            return Url::toRoute($params);
        }
    }
}