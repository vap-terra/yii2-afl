/**
 * Created by volhv on 18.10.16.
 */
(function($){
    'use strict';

    var instanceName = 'LinkerController';

    var LinkerController = function($container,options){
        return this.initialize($container,options);
    };

    LinkerController.prototype = {
        options: {},
        xslResponder: null,
        $container: null,
        $rows: null,
        defaults: {},
        initialize: function ($container, options)
        {
            this.$container = $container;
            this
                .setData()
                .setOptions(options)
                .setVars()
                .build()
                .events();
            return this;
        },
        setData: function ()
        {
            this.$container.data(instanceName, this);
            return this;
        },
        setVars: function ()
        {
            var self = this;
            return this;
        },
        setOptions: function (opts)
        {
            this.options = $.extend(true, {}, this.defaults, opts);
            return this;
        },
        build: function ()
        {
            var self = this;
            this.$rows = this.$container.find('tr');
            return this;
        },
        loadData: function(eventName,$element,params)
        {
            var self = this;
            $.ajax(
                params
            ).done(
                function (response, textStatus, jqXHR )
                {
                    var status = 'done';
                    try {
                        response = $.parseJSON(response);
                    } catch( e ){
                        //console.log( e );
                    }
                    if( typeof( response.status ) !== 'undefined' && response.status == 'error' ){
                        status = 'fail';
                        console.log(status + ' ' + eventName);
                    }
                    self.$container.trigger(status+eventName, [$element, response, params]);
                    //$element.trigger(status+eventName, [response, params]);
                }
            ).fail(
                function ( jqXHR, textStatus, errorThrown )
                {
                    self.$container.trigger('fail'+eventName, [$element, jqXHR, params]);
                    //$element.trigger('fail'+eventName);
                }
            ).always(
                function ( data, textStatus, errorThrown ) //data|jqXHR, textStatus, jqXHR|errorThrown
                {
                    self.$container.trigger('always'+eventName, [$element, data, params]);
                    //$element.trigger('always'+eventName, [params]);
                }
            );
        },
        events: function ()
        {
            var self = this;
            this.$container.find('.btn-action').each( function(){
                var $btn = $(this);
                var url = $btn.attr('href');
                if (typeof( url ) == 'undefined') {
                    url = $btn.attr('data-href');
                }
                $btn.on('click',function(){
                    try {
                        var params = {
                            url: url,
                            data: {},
                            type: 'get',
                        };
                        self.loadData($btn.attr('data-action'), $btn, params);
                    } catch ( e ){
                        console.log(e);
                    }
                    return false;
                });
            } );
            this.$container.on('doneadd',function(){ console.log('done add'); });
            this.$container.on('donesave',function(){ console.log('done save'); });
            this.$container.on('donedelete',function(){ console.log('done delete'); });
            this.$container.on('donesortable',function(){ console.log('done sortable'); });
            return this;
        }
    };

    if (typeof( $.fn.LinkerController ) == 'undefined')
    {
        $.fn.LinkerController = function ()
        {
            var options = {};
            if (
                typeof(arguments[0]) !== 'undefined'
                && arguments[0] != null
                && arguments[0] instanceof Object != false
            ) {
                $.extend(options, arguments[0]);
            }

            return $(this).each(
                function ()
                {
                    var $this = $(this);
                    if ($this.data(instanceName)) {
                        return $this.data(instanceName);
                    } else {
                        return new LinkerController($this,options);
                    }
                }
            );
        }
    }

})(jQuery);