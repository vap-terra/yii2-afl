<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 10.03.16
 * Time: 16:05
 */

namespace app\components;


use app\models\BaseActiveRecord;
use dosamigos\transliterator\TransliteratorHelper;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Inflector;
use yii\web\Response;

class FileManagerController extends BaseController
{
    public $imgExtensions = [
        'png', 'jpe', 'jpeg', 'jpg', 'gif', 'bmp', 'ico', 'tiff', 'tif', 'svg', 'svgz'
    ];
    public $acceptedOrigins = array(
        "http://localhost",
        "http://vs1.loc",
    );
    public $excludeFolders = [
        'items'=>true,
        'tmp'=>true,
        '.'=>true,
        '..'=>true,
        'webstat'=>true
    ];
    public $excludeFiles = [
        '.htaccess'=>true,
        '.gitignore'=>true,
        'robots.txt'=>true
    ];

    public function init()
    {
        parent::init();
        if( !empty( Yii::$app->params['accepted_origins'] ) && is_array(Yii::$app->params['accepted_origins']) ) {
            $this->acceptedOrigins = Yii::$app->params['accepted_origins'];
        }
        if( !empty( Yii::$app->params['file_manager_exclude_folders'] ) && is_array(Yii::$app->params['file_manager_exclude_folders']) ) {
            $this->excludeFolders = Yii::$app->params['file_manager_exclude_folders'];
        }
        if( !empty( Yii::$app->params['img_extensions'] ) && is_array(Yii::$app->params['img_extensions']) ) {
            $this->imgExtensions = Yii::$app->params['img_extensions'];
        }
    }

    public function behaviors()
    {
        return array_merge_recursive(
            parent::behaviors(),
            [
                'access' => [
                    'class' => AccessControl::className(),
                    'only' => ['login','index','update','create','view','upload','load-files','load-folders','create-folder','delete-folder','delete-file','download-file'],
                    'rules' => [
                        [
                            'allow' => true,
                            'roles' => ['@'],
                        ],
                    ],
                ],
            ]
        );
    }

    public function actionIndex( $folder='', $type='', $editor='', $lang='' )
    {
        $allowedExtensions = array_keys( FileStorageManager::$FILE_GROUPS );

        $defaultFolder = 'images';

        //$type = Yii::$app->request->getQueryParam('type');
        //$editor = Yii::$app->request->getQueryParam('editor');
        //$lang = Yii::$app->request->getQueryParam('lang');

        if( empty( $folder ) ){
            $folder = $defaultFolder;
            if( $type == 1 ) {
                $folder = FileStorageManager::TYPE_DOC;
            } else if( $type == 2 ) {
                $folder = FileStorageManager::TYPE_IMAGE;
            } else if( $type == 3 ) {
                $folder = FileStorageManager::TYPE_VIDEO;
            }
        }
        //$folder = Yii::$app->request->getQueryParam('folder');

        $arFolders = Yii::$app->storage->getFolders('/',1,$this->excludeFolders);
        ksort($arFolders);
        $arFiles = Yii::$app->storage->getFilesInFolder($folder,$this->excludeFiles);
        ksort($arFiles);


        if( $editor ) {
            $this->layout = '@app/views/backend/themes/protoadmin/layouts/file-manager';
        }

        return $this->render(
            'index',
            [
                'canUploadFiles' => true,
                'editor' => $editor,
                'lang' => $lang,
                'type' => $type,
                'imgExtensions' => $this->imgExtensions,
                'allowedExtensions' => $allowedExtensions,
                'folder'=>$folder,
                'folders' => $arFolders,
                'files' => $arFiles
            ]
        );
    }

    public function actionLoadFiles( $folder )
    {

    }

    public function actionLoadFolders( $folder )
    {

    }

    public function actionCreateFolder()
    {

    }

    public function actionDeleteFolder()
    {

    }

    public function actionDeleteFile( $folder )
    {
        $file = Yii::$app->request->get('file');
        if(!empty($folder)) {
            $folder = '/'.$folder;
        }
        $result = [];
        if( is_array( $file ) ) {
            foreach( $file as $item ) {
                $path = Yii::$app->storage->basePath.$folder.'/'.strtr($item,['..'=>'']);
                if( is_file($path) && unlink($path) ) {
                    $result[$item] = ['status' => BaseController::SUCCESS];
                } else {
                    $result[$item] = ['status' => BaseController::ERROR];
                }
            }
        } else {
            $path = Yii::$app->storage->basePath.$folder.'/'.strtr($file,['..'=>'']);
            if( is_file($path) && unlink($path) ) {
                $result[$file] = ['status' => BaseController::SUCCESS];
            } else {
                $result[$file] = ['status' => BaseController::ERROR];
            }
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        echo json_encode(
            array(
                'result' => $result,
            )
        );
    }

    public function actionDownloadFile( $file )
    {
        return Yii::$app->response->sendFile( Yii::$app->storage->getFilePath($file) );
    }

    public function actionUpload( $folder='' )
    {
        reset($_FILES);
        $file = current($_FILES);

        if (isset($_SERVER['HTTP_ORIGIN'])) {
            if (in_array($_SERVER['HTTP_ORIGIN'], $this->acceptedOrigins)) {
                header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
            } else {
                header("HTTP/1.0 403 Origin Denied");
                exit();
            }
        }

        if ($file) {
            if ($file['error'] == 0) {

                if ( !is_uploaded_file($file['tmp_name']) ) {
                    header("HTTP/1.0 500 Server Error");
                    Yii::$app->end(500);
                }

                $ext = Util::toLower(pathinfo($file['name'], PATHINFO_EXTENSION));
                // Sanitize input
                if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", $file['name'])) {
                    //$file['name'] = ($file['name']).'.'.FileStorageManager::getFileExtension($file['name']);
                    $file['name'] = Inflector::slug( TransliteratorHelper::process( pathinfo($file['name'], PATHINFO_FILENAME) ), '-', true ).'.'.$ext;
                    //header("HTTP/1.0 500 Invalid file name.");
                    //exit();
                }

                // Verify extension
                if (!in_array($ext, array_keys( FileStorageManager::$FILE_GROUPS ))) {
                    header("HTTP/1.0 500 Invalid extension. ".$file['name']);
                    Yii::$app->end(500);
                }

                $path = Yii::$app->storage->getFilePath($file['name']);
                $href = Yii::$app->storage->getFileHref($file['name']);
                if( !move_uploaded_file($file['tmp_name'], $path) ){
                    header("HTTP/1.0 500 Error upload file");
                    Yii::$app->end(500);;
                }

                header("HTTP/1.0 200 OK");
                $fileInfo = Yii::$app->storage->getFileInfo( $path );
                $file = $this->renderAjax(
                    'file',
                    [
                        'file' => $fileInfo,
                        'folder' => $fileInfo['relativePath']
                    ]
                );
                Yii::$app->response->format = Response::FORMAT_JSON;

                return array(
                    'location' => $href,
                    'view' => $file,
                    'info' => $fileInfo,
                );

            } else if ($file['error'] != 4) {
                header('HTTP/1.0 500 Upload Error ['.BaseActiveRecord::getErrorUploadMessage($file['error'], $file['name']).']');
                Yii::$app->end(500);
            }
        }
        header("HTTP/1.0 500 Server Error");
        Yii::$app->end(500);
    }
}