<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 01.03.16
 * Time: 14:31
 */
namespace  app\components\data;

use yii\base\InvalidConfigException;
use yii\data\ActiveDataProvider;
use yii\db\QueryInterface;

class AdjacencyListDataProvider extends ActiveDataProvider
{
    private $_totalRootCount = 0;

    private $_subModels = [];

    private $_subKeys = [];

    public $parentKey = 'parent_id';

    /**
     * @inheritdoc
     */
    protected function prepareModels()
    {
        if (!$this->query instanceof QueryInterface) {
            throw new InvalidConfigException('The "query" property must be an instance of a class that implements the QueryInterface e.g. yii\db\Query or its subclasses.');
        }
        $query = clone $this->query;
        $query->where([$this->parentKey=>null]);
        if (($pagination = $this->getPagination()) !== false) {
            $pagination->totalCount = $this->getTotalCount();
            $query->limit($pagination->getLimit())->offset($pagination->getOffset());
        }
        if (($sort = $this->getSort()) !== false) {
            $query->addOrderBy($sort->getOrders());
        }

        return $query->all($this->db);
    }


    protected function prepareSubModels( $parentKeyValue )
    {
        if (!$this->query instanceof QueryInterface) {
            throw new InvalidConfigException('The "query" property must be an instance of a class that implements the QueryInterface e.g. yii\db\Query or its subclasses.');
        }
        $query = clone $this->query;
        $query->where([$this->parentKey=>$parentKeyValue]);
        /*if (($pagination = $this->getPagination()) !== false) {
            $pagination->totalCount = $this->getTotalCount();
            $query->limit($pagination->getLimit())->offset($pagination->getOffset());
        }
        if (($sort = $this->getSort()) !== false) {
            $query->addOrderBy($sort->getOrders());
        }*/

        return $query->all($this->db);
    }

    /**
     * @inheritdoc
     */
    protected function prepareTotalCount()
    {
        if (!$this->query instanceof QueryInterface) {
            throw new InvalidConfigException('The "query" property must be an instance of a class that implements the QueryInterface e.g. yii\db\Query or its subclasses.');
        }
        $query = clone $this->query;
        $query->where([$this->parentKey=>null]);
        return (int) $query->limit(-1)->offset(-1)->orderBy([])->count('*', $this->db);
    }

    /**
     * Prepares the data models and keys.
     *
     * This method will prepare the data models and keys that can be retrieved via
     * [[getModels()]] and [[getKeys()]].
     *
     * This method will be implicitly called by [[getModels()]] and [[getKeys()]] if it has not been called before.
     * @param $parentKeyValue
     * @param boolean $forcePrepare whether to force data preparation even if it has been done before.
     */
    public function prepareSub( $parentKeyValue, $forcePrepare = false)
    {
        if ($forcePrepare || empty( $this->_subModels[ $parentKeyValue ] ) ) {
            $this->_subModels[$parentKeyValue] = $this->prepareSubModels( $parentKeyValue );
        }
        if ($forcePrepare || empty( $this->_subKeys[$parentKeyValue] ) ) {
            $this->_subKeys[$parentKeyValue] = $this->prepareKeys($this->_subModels[ $parentKeyValue ]);
        }
    }

    public function getSubModels( $parentKeyValue )
    {
        $this->prepareSub( $parentKeyValue );
        return $this->_subModels[$parentKeyValue];
    }

    public function getSubKeys( $parentKeyValue )
    {
        $this->prepareSub( $parentKeyValue );
        return $this->_subKeys[$parentKeyValue];
    }
}