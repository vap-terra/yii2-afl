<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 12.06.16
 * Time: 12:12
 */

namespace app\components;

class Util
{
    static public function firstCharToUpper( $str )
    {
        return mb_strtoupper( mb_substr($str, 0, 1) ) . mb_substr( $str, 1, mb_strlen( $str ), 'UTF-8' );
    }
    static public function getClassName( $str ) {
        return self::firstCharToUpper( self::toLower( $str ) );
    }
    /**
     * @param string $class
     * @param string $delimiter
     * @return string
     */
    static public function getFileNameByClass( $class, $delimiter='_' ) {
        $pattern = '/([A-Z][^A-Z]+)/';
        if(preg_match_all($pattern, $class, $matches) ) {
            $class = implode( $delimiter, $matches[1] );
        }
        return self::toLower($class);
    }

    static public function getClassNameByFileName( $str )
    {
        $parts = explode( '_', $str );
        $cnt = sizeof( $parts );
        if( $cnt ) {
            $str = '';
            for( $i=0; $i<$cnt; $i++ ) {
                $str .= self::getClassName( $parts[ $i ] );
            }
        }
        return $str;
    }

    static public function toUpper( $str )
    {
        return mb_strtoupper($str);
    }

    static public function toLower( $str )
    {
        return mb_strtolower($str);
    }
}