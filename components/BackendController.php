<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 27.06.16
 * Time: 23:29
 */

namespace app\components;


use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

class BackendController extends BaseController
{
    public function init()
    {
        parent::init();
        if (Yii::$app->user->isGuest) {
            $this->layout = 'panel-sign';
        }
    }

    public function beforeAction($action)
    {
        if( parent::beforeAction($action) ){
            return true;
        }
        return false;
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout','restore-password','index','update','create','view'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    /*public function actions()
    {
        return array_merge(
            parent::actions(),
            [
                'generate-tid' => [
                    'class' => 'yii\web\ErrorAction',
                ],
            ]
        );
    }*/
}