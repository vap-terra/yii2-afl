<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 27.06.16
 * Time: 23:29
 */

namespace app\components;


use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

class FrontendController extends BaseController
{

    /**
     * @param $route
     * @param array $params
     */
    public function partOfPage($route, $params = [])
    {
    }

    /**
     * @param $route
     * @param array $params
     */
    public function runAsUrl($route, $params = [])
    {
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if( parent::beforeAction($action) ){
            return true;
        }
        return false;
    }
}