<?php
/**
 * @version 1.00.00, 07/16/2011
 * @author  Polyankin Semen Viktorovich Vol][v by @link http://vap-terra.ru
 */

abstract class GeoipService 
{

    protected $_ip;

    protected $_geoLocations = array(
        'user_ip'  => '',
        'inetnum'  => '',
        'country'  => '',
        'city'     => '',
        'region'   => '',
        'district' => '',
        'lat'      => '',
        'lng'      => '',
        'org'      => ''
    );

    public function __construct( $userIp ) 
    {
        $this->_ip = $userIp;
    }

    abstract public function loadData();

    abstract public function isLoad();

    public function getData() 
    {
        return $this->_geoLocations;
    }

}
