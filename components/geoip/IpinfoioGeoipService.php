<?php
/**
 * @version 1.00.00, 07/16/2011
 * @author  Polyankin Semen Viktorovich Vol][v by @link http://vap-terra.ru
 */

class IpinfoioGeoipService extends GeoipService 
{
    public function loadData() 
    {
/*
$ curl ipinfo.io/8.8.8.8
{
  "ip": "8.8.8.8",
  "hostname": "google-public-dns-a.google.com",
  "loc": "37.385999999999996,-122.0838",
  "org": "AS15169 Google Inc.",
  "city": "Mountain View",
  "region": "California",
  "country": "US",
  "phone": 650
}
*/
        $ch = curl_init( );
        curl_setopt( $ch, CURLOPT_URL, 'http://ipinfo.io/' . $this->_ip . '/json'  );
        curl_setopt( $ch, CURLOPT_HEADER, false );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_TIMEOUT, 25 );
        curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, 25 );
        curl_setopt( $ch, CURLOPT_USERAGENT, 'PHP Bot VapGin' );
        $answerJson = curl_exec($ch);
        curl_close($ch);
        if( $answerJson ) {
            $data = json_decode( $answerJson, true );
            if( isset( $data['country'] ) 
                && $data['country']
                && isset( $data['ip'] ) 
                && $data['ip'] ) {
                $this->_geoLocations['user_ip']  = $data['ip'];
                $this->_geoLocations['country']  = $data['country'];
                $this->_geoLocations['city']     = $data['city'];
                $this->_geoLocations['region']   = $data['region'];
                $data['loc'] = explode(',',$data['loc']);
                if( !isset( $data['loc'][1] ) )
                    $data['loc'][1] = '';
                $this->_geoLocations['lat']      = $data['loc'][0];
                $this->_geoLocations['lng']      = $data['loc'][1];
                $this->_geoLocations['org']      = $data['org'];
                //$_SESSION['LAST_GEOIP_LOCATION'] = $this->_geoLocations;
                //выставляем куку
                //SetCookie('geoip_location',serialize($this->_geoLocations),time()+86400);
            }
        }
    }
    
    public function isLoad() {
        return !empty($this->_geoLocations['user_ip']);
    }
}
