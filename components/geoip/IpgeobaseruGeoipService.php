<?php
/**
 * @version 1.00.00, 07/16/2011
 * @author  Polyankin Semen Viktorovich Vol][v by @link http://vap-terra.ru
 */

class IpgeobaseruGeoipService extends GeoipService 
{
    public function loadData() 
    {
        $ch = curl_init( );
        curl_setopt( $ch, CURLOPT_URL, 'http://ipgeobase.ru:7020/geo?ip=' . $this->_ip  );
        curl_setopt( $ch, CURLOPT_HEADER, false );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_TIMEOUT, 25 );
        curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, 25 );
        curl_setopt( $ch, CURLOPT_USERAGENT, 'PHP Bot VapGin' );
        $answerXml = curl_exec($ch);
        curl_close($ch);

/*$answerXml = '<?xml version="1.0" encoding="windows-1251"?>
<ip-answer>
<ip value="95.153.206.29"><inetnum>95.153.128.0 - 95.153.255.255</inetnum><country>RU</country><city>Краснодар</city><region>Краснодарский край</region><district>Южный федеральный округ</district><lat>45.042149</lat><lng>38.980640</lng></ip>
</ip-answer>';*/
        //htmlspecialchars( $answerXml );
        iconv_set_encoding( "internal_encoding", "UTF-8" );
        if( $answerXml ) {
            if( strpos( $answerXml, 'windows-1251') !== false ) {
                mb_convert_encoding( $answerXml, 'utf-8', 'cp1251' );
                //$answerXml = strtr( $answerXml, 'windows-1251','utf-8');
            }
            if( strpos( $answerXml, '<?xml version="1.0"') !== false ) {
                $dom = new domDocument('1.0', 'windows-1251'); // Создаём XML-документ версии 1.0 с кодировкой utf-8
                if( $dom->loadXML( $answerXml ) ) { // Загружаем XML-документ из файла в объект DOM
                    $root = $dom->documentElement; // Получаем корневой элемент
                    $childs = $root->childNodes; // Получаем дочерние элементы у корневого элемента
                    /* Перебираем полученные элементы */
                    for ($i = 0; $i < $childs->length; $i++) {
                        $ip = $childs->item( $i );
                        if( $ip->nodeType == 1 && $ip->nodeName == 'ip' ) {
                            $this->_geoLocations['user_ip']  = $ip->getAttribute('value');
                            $params = $ip->childNodes;
                            for ($j = 0; $j < $params->length; $j++) {
                                $val = $params->item( $j );
                                if( $val->nodeType == 1 ) {
                                    $this->_geoLocations[$val->nodeName]  = trim( $val->nodeValue );
                                }
                            }
                        }
                    }
                }
            }
        }

    }
    
    public function isLoad() {
        return !empty($this->_geoLocations['user_ip']);
    }

}
