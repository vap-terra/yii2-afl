<?php
/**
 * @version 1.00.00, 07/16/2011
 * @author  Polyankin Semen Viktorovich Vol][v by @link http://vap-terra.ru
 */
require_once('GeoipService.php');
class Geoip extends CApplicationComponent 
{
    
    public $geoipServices = array(
        'Ipgeobaseru',
        'Ipinfoio'
    );
    
    protected $_userIp;
    
    public function getIp()
    {
        if( is_null( $this->_userIp ) ) {
            //определяем ip пользователя
            $patternIp     = '/([0-9]{3})\.([0-9]{3})\.([0-9]{3})/';
            $regs          = array();
            $this->_userIp = filter_input(INPUT_SERVER, 'REMOTE_ADDR');
            if( isset( $_SERVER['HTTP_CLIENT_IP'] )  
                && preg_match( $patternIp, $_SERVER['HTTP_CLIENT_IP'], $regs) ) {
                $this->_userIp = $_SERVER['HTTP_CLIENT_IP'];
            } else if( isset( $_SERVER['HTTP_X_FORWARDED_FOR'] ) 
                       && preg_match( $patternIp, $_SERVER['HTTP_X_FORWARDED_FOR'], $regs) ) {
                $this->_userIp = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else  if( isset( $_SERVER['REMOTE_ADDR'] ) 
                       && preg_match( $patternIp, $_SERVER['REMOTE_ADDR'], $regs) ) {
            	$this->_userIp = $_SERVER['REMOTE_ADDR'];
            }
        }
        return $this->_userIp;
    }
    
    public function getGeo() 
    {
        if( isset( $_SESSION['LAST_GEOIP_LOCATION'] ) ) {
            //выставляем куку
            SetCookie('geoip_location',serialize($_SESSION['LAST_GEOIP_LOCATION']),time()+86400);
            return $_SESSION['LAST_GEOIP_LOCATION'];
        } else if( isset( $_COOKIE['geoip_location'] ) && $_COOKIE['geoip_location'] ) {
            $_SESSION['LAST_GEOIP_LOCATION'] = unserialize( $_COOKIE['geoip_location'] );
            return $_SESSION['LAST_GEOIP_LOCATION'];
        } else {
            foreach( $this->geoipServices as $service ) {
                $service = $service.'GeoipService';
                require_once($service.'.php');
                $oService = new $service( $this->getIp() );
                $oService->loadData();
                if( $oService->isLoad() ) {
                    $geoLocations = $oService->getData();
                    break;
                }
            }
            if( isset( $geoLocations[ 'inetnum' ] )
                && $geoLocations[ 'inetnum' ] 
                && isset( $geoLocations[ 'city' ] )
                && $geoLocations[ 'city' ] ) {
                $_SESSION['LAST_GEOIP_LOCATION'] = $geoLocations;
                //выставляем куку
                SetCookie('geoip_location',serialize($geoLocations),time()+86400);
                return $_SESSION['LAST_GEOIP_LOCATION'];
            }
        }
        return null;
    }

}
