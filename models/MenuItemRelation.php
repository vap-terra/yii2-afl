<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "menu_item_relation".
 *
 * @property integer $id
 * @property integer $menu_item_id
 * @property string $type
 * @property integer $item_id
 * @property integer $main
 * @property integer $position
 * @property integer $active
 * @property string $params
 *
 * @property MenuItem $menuItem
 */
class MenuItemRelation extends \app\models\BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu_item_relation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['menu_item_id', 'type', 'item_id'], 'required'],
            [['menu_item_id', 'item_id', 'main', 'position', 'active'], 'integer'],
            [['type', 'params'], 'string'],
            [['menu_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => MenuItem::className(), 'targetAttribute' => ['menu_item_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/menu-item-relation', 'ID'),
            'menu_item_id' => Yii::t('app/menu-item-relation', 'Menu Item ID'),
            'type' => Yii::t('app/menu-item-relation', 'Type'),
            'item_id' => Yii::t('app/menu-item-relation', 'Item ID'),
            'main' => Yii::t('app/menu-item-relation', 'Main'),
            'position' => Yii::t('app/menu-item-relation', 'Position'),
            'active' => Yii::t('app/menu-item-relation', 'Active'),
            'params' => Yii::t('app/menu-item-relation', 'Params'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuItem()
    {
        return $this->hasOne(MenuItem::className(), ['id' => 'menu_item_id']);
    }
}
