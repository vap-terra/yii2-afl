<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "geo_locality".
 *
 * @property integer $id
 * @property integer $geo_country_id
 * @property integer $geo_region_id
 * @property integer $geo_region_area_id
 * @property string $name
 * @property string $coordinates
 * @property string $yandex_map_coordinates
 * @property string $google_map_coordinates
 *
 * @property BoardItem[] $boardItems
 * @property GeoRegion $geoRegion
 * @property GeoRegionArea $geoRegionArea
 * @property GeoCountry $geoCountry
 * @property GeoLocalityArea[] $geoLocalityAreas
 * @property MetroLine[] $metroLines
 * @property User[] $users
 */
class GeoLocality extends BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'geo_locality';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['geo_country_id', 'geo_region_id', 'geo_region_area_id', 'name'], 'required'],
            [['geo_country_id', 'geo_region_id', 'geo_region_area_id'], 'integer'],
            [['name'], 'string', 'max' => 80],
            [['coordinates', 'yandex_map_coordinates', 'google_map_coordinates'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/models', 'ID'),
            'geo_country_id' => Yii::t('app/models', 'Geo Country ID'),
            'geo_region_id' => Yii::t('app/models', 'Geo Region ID'),
            'geo_region_area_id' => Yii::t('app/models', 'Geo Region Area ID'),
            'name' => Yii::t('app/models', 'Name'),
            'coordinates' => Yii::t('app/models', 'Coordinates'),
            'yandex_map_coordinates' => Yii::t('app/models', 'Yandex Map Coordinates'),
            'google_map_coordinates' => Yii::t('app/models', 'Google Map Coordinates'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBoardItems()
    {
        return $this->hasMany(BoardItem::className(), ['geo_locality_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGeoRegion()
    {
        return $this->hasOne(GeoRegion::className(), ['id' => 'geo_region_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGeoRegionArea()
    {
        return $this->hasOne(GeoRegionArea::className(), ['id' => 'geo_region_area_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGeoCountry()
    {
        return $this->hasOne(GeoCountry::className(), ['id' => 'geo_country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGeoLocalityAreas()
    {
        return $this->hasMany(GeoLocalityArea::className(), ['geo_locality_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMetroLines()
    {
        return $this->hasMany(MetroLine::className(), ['geo_locality_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['geo_locality_id' => 'id']);
    }
}
