<?php
namespace app\models\data;

use app\models\ext\ShopItemExt;
use app\models\ShopItem;
use DateTime;
use Yii;
use yii\web\Cookie;

class ShopCart
{

	public static $cartKey = 'cart';
	public static $cart = [];

	//public static $costKey = 'cost';
	public static $propertyKey = 'properties';


	public static function getParamsIndex($params) 
	{
		return (!empty($params)&&is_array($params))?md5( json_encode($params) ):'';
	}
	
    public static function get()
    {
		if( empty( self::$cart ) ) {
            $cookies = Yii::$app->response->cookies;
            if (($value = $cookies->getValue(self::$cartKey)) !== null) {
                self::$cart = json_decode($value, true);
            } else {
                $cookies = Yii::$app->request->cookies;
                if (($value = $cookies->getValue(self::$cartKey)) !== null) {
                    self::$cart = json_decode($value, true);
                } else {
                    self::$cart = [];
                }
            }
		}
        return self::$cart;
    }
    
    public static function save($cart)
    {
        self::$cart = $cart;
		$cookies = Yii::$app->response->cookies;
		$cookies->add(
			new Cookie(
				[
					'expire' => time() + (60 * 60 * 24 * 31),
					'path' => '/',
					'name' => self::$cartKey,
					'value' => json_encode($cart),
				]
			)
		);
    }
    
    public static function clear()
    {
        self::$cart = null;
		Yii::$app->response->cookies->remove(self::$cartKey);
    }
    
    public static function getIds()
    {
    	$ids = [];
	    $cart = self::get();
        if( !empty( $cart ) ) {
        	foreach( $cart as $position=>$item ){
        		$ids[] = $item['itemId'];
        	}
        }
        return $ids;
    }
    
    public static function getItems($shopItemId)
    {
    	$cart = self::get();
    	$items = [];
		foreach( $cart as $position=>$item ) {
			if( $item['itemId'] == $shopItemId ) {
				$items[] = $item;
			}
		}
    	return $items;
    }
    
    public static function getItem($shopItemId,$params=[])
    {
    	$cart = self::get();
    	$idx = self::getParamsIndex($params);
		foreach( $cart as $position=>$item ) {
			if( $item['itemId'] == $shopItemId && $item['key'] == $idx ) {
				return $item;
			}
		}
    	return null;
    }
    
    public static function getItemQuantity($shopItemId,$params=[])
    {
    	$item = self::getItem($shopItemId,$params);
    	if( !empty( $item ) ){
    		return $item['quantity'];
    	}
    	return 0.00;
    }
    
	public static function add($shopItemId,$quantity=1,$params=[],$period=[])
	{
		$cart = self::get();
		$item = self::getItem($shopItemId,$params);
		if( empty( $item ) ) {

			$item = [
				'key' => self::getParamsIndex($params),
				'position' => count($cart)+1,
				'itemId' => $shopItemId,
				'quantity' => 0.00,
				'period' => $period,
				'property' => $params,
			];
			$cart[$item['position']] = $item;
		}
		$cart[$item['position']]['quantity'] += $quantity;
		self::save($cart);
		return $item;
	}
	
	public static function delete($positionId)
	{
		$cart = self::get();
		$item = null;
		if( isset( $cart[$positionId] ) ) {
			$item = $cart[$positionId];
			unset($cart[$positionId]);
		}
		self::save($cart);
		return $item;
	}
	
	public static function change($positionId,$quantity=1,$params=[],$period=[])
	{
		$cart = self::get();
		$item = null;
		if( isset( $cart[$positionId] ) ) {
			$item = $cart[$positionId];
			$item['key'] = self::getParamsIndex($params);
			$item['quantity'] = $quantity;
			if( !empty( $period ) ){
				$item['period'] = $period;
			}
			if( !empty( $params ) ){
				$item['property'] = $params;
			}
			$cart[$positionId] = $item;
		}
		self::save($cart);
		return $item;
	}
    
    public static function getCount()
    {
        $cart = self::get();
        $quantity = 0.00;
        if( !empty( $cart ) ){
        	foreach( $cart as $position=>$item ) {
        		$quantity += $item['quantity'];
        	}
        }
        return $quantity;
    }
    
    /*public static function addToTotalCost( $val )
    {
    	self::saveTotalCost( self::getTotalCost() + $val );
    	return self::getTotalCost();
    }
    
    public static function getTotalCost()
    {
        if (($value = Yii::$app->request->cookies->getValue(self::$costKey))!==null) {
            $cost = (float) $value;
        } else {
            $cost = 0.00;
        }
        return $cost;
    }
    
    public static function saveTotalCost($val)
    {
		Yii::$app->response->cookies->add(
			new Cookie(
				[
					'expire' => time() + (60 * 60 * 24 * 31),
					'path' => '/',
					'name' => self::$costKey,
					'value' => $val,
				]
			)
		);
    }*/
    
    public static function counted()
    {
    	$cart = self::get();
    	$sum = 0.00;
    	if( $cart ){
    		foreach( $cart as $position => $item ) {
    			$oShopItem = ShopItemExt::findOne(['=','id',(int)$item['itemId']]);
    			if( !empty( $oShopItem ) ) {
    				
                    $period = (!empty($item['period']))?$item['period'][0]:[];
                    $d = $d2 = $interval = $days = null;
                    if( !empty($period['start']) && !empty( $period['end'] ) ){
                    	$d = new DateTime($period['start']);
                    	$d2 = new DateTime($period['end']);
                    	$interval = $d->diff($d2);
                    	$days = $interval->format('%a');
                    	if( $days == 0 ) $days = 1;
                    	$sum += $oShopItem->getPrice() * $item['quantity'] * $days;
                	} else {
                		$sum += $oShopItem->getPrice() * $item['quantity'];
                	}
    			} else {
    				self::delete($position);
    			}
    		}
    	}
    	//self::saveTotalCost($sum);
    	return $sum;
    }
    
    public static function getPositionPeriod($positionId,$index=0) 
    {
		$cart = self::get();
		if( isset( $cart[$positionId] ) ) {
			$item = $cart[$positionId];
			if( !empty( $item['period'][$index] ) ) {
				return $item['period'][$index];
			}
		}
		return null;
    }
    
    
    public static function order() 
    {
    	
    }
}
