<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "banner".
 *
 * @property integer $id
 * @property integer $banner_group_id
 * @property integer $geo_country_id
 * @property integer $geo_region_id
 * @property integer $geo_region_area_id
 * @property integer $geo_locality_id
 * @property string $name
 * @property string $link
 * @property string $type
 * @property string $content
 * @property string $image
 * @property integer $click_count
 * @property integer $active
 * @property integer $position
 */
class Banner extends \app\models\BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'banner';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['banner_group_id', 'name'], 'required'],
            [['banner_group_id', 'geo_country_id', 'geo_region_id', 'geo_region_area_id', 'geo_locality_id', 'click_count', 'active', 'position'], 'integer'],
            [['type', 'content'], 'string'],
            [['name', 'link', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/banner', 'ID'),
            'banner_group_id' => Yii::t('app/banner', 'Banner Group ID'),
            'geo_country_id' => Yii::t('app/banner', 'Geo Country ID'),
            'geo_region_id' => Yii::t('app/banner', 'Geo Region ID'),
            'geo_region_area_id' => Yii::t('app/banner', 'Geo Region Area ID'),
            'geo_locality_id' => Yii::t('app/banner', 'Geo Locality ID'),
            'name' => Yii::t('app/banner', 'Name'),
            'link' => Yii::t('app/banner', 'Link'),
            'type' => Yii::t('app/banner', 'Type'),
            'content' => Yii::t('app/banner', 'Content'),
            'image' => Yii::t('app/banner', 'Image'),
            'click_count' => Yii::t('app/banner', 'Click Count'),
            'active' => Yii::t('app/banner', 'Active'),
            'position' => Yii::t('app/banner', 'Position'),
        ];
    }
}
