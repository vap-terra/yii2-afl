<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "menu_item_content".
 *
 * @property integer $menu_item_id
 * @property integer $content_id
 * @property integer $position
 * @property integer $active
 *
 * @property MenuItem $menuItem
 * @property Content $content
 */
class MenuItemContent extends BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu_item_content';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['menu_item_id', 'content_id'], 'required'],
            [['menu_item_id', 'content_id', 'position', 'active'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'menu_item_id' => Yii::t('app/models', 'Menu Item ID'),
            'content_id' => Yii::t('app/models', 'Content ID'),
            'position' => Yii::t('app/models', 'Position'),
            'active' => Yii::t('app/models', 'Active'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuItem()
    {
        return $this->hasOne(MenuItem::className(), ['id' => 'menu_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContent()
    {
        return $this->hasOne(Content::className(), ['id' => 'content_id']);
    }
}
