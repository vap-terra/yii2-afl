<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form_element".
 *
 * @property integer $id
 * @property string $tid
 * @property integer $form_id
 * @property integer $required
 * @property integer $active
 * @property string $type
 * @property string $name
 * @property string $hint
 * @property string $description
 * @property string $params
 * @property integer $position
 *
 * @property Form $form
 */
class FormElement extends BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'form_element';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tid', 'form_id', 'required', 'active', 'name'], 'required'],
            [['form_id', 'required', 'active', 'position'], 'integer'],
            [['type', 'params'], 'string'],
            [['tid', 'name', 'hint', 'description'], 'string', 'max' => 255],
            [['form_id'], 'exist', 'skipOnError' => true, 'targetClass' => Form::className(), 'targetAttribute' => ['form_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/form-element', 'ID'),
            'tid' => Yii::t('app/form-element', 'Tid'),
            'form_id' => Yii::t('app/form-element', 'Form ID'),
            'required' => Yii::t('app/form-element', 'Required'),
            'active' => Yii::t('app/form-element', 'Active'),
            'type' => Yii::t('app/form-element', 'Type'),
            'name' => Yii::t('app/form-element', 'Name'),
            'hint' => Yii::t('app/form-element', 'Hint'),
            'description' => Yii::t('app/form-element', 'Description'),
            'params' => Yii::t('app/form-element', 'Params'),
            'position' => Yii::t('app/form-element', 'Position'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForm()
    {
        return $this->hasOne(Form::className(), ['id' => 'form_id']);
    }
}
