<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 22.02.16
 * Time: 23:51
 */

namespace app\models\ext;


use app\models\BaseActiveQuery;
use app\models\Language;
use yii\helpers\ArrayHelper;

class LanguageExt extends Language
{
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(UserExt::className(), ['language_id' => 'id']);
    }

    /**
     * @param int $status
     * @return array
     */
    public static function getArOptionList( $status=BaseActiveQuery::ALL )
    {
        $query = self::find();
        if( $status != BaseActiveQuery::ALL ) {
            $query->where(['=','active',(int)$status]);
        }
        $items = $query->all();
        if( empty( $items ) ) return [];
        return ArrayHelper::map($query->all(),'id','name');
    }
}