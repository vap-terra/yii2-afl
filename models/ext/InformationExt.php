<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 28.01.16
 * Time: 16:51
 */

namespace app\models\ext;


use app\models\BaseActiveQuery;
use app\models\Information;
use Yii;
use yii\base\Exception;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

class InformationExt extends Information
{
    const CACHE_DURATION = 86400;//1 * 24 * 60 * 60;

    /*const INACTIVE = 0;
    const ACTIVE = 1;
    const ALL = 2;*/

    /**
     * @var $currentInformation InformationExt
     */
    public static $currentInformation;

    /**
     * @var $currentGroup InformationGroupExt
     */
    public static $currentGroup;

    /**
     * @var $currentItem InformationItemExt
     */
    public static $currentItem;


    /**
     * @return ActiveQuery
     */
    public function getMenuItem()
    {
        return $this->hasOne(MenuItemExt::className(), ['id' => 'menu_item_id']);
    }
    /**
     * @return ActiveQuery
     */
    public function getRootInformationGroup()
    {
        return $this->getInformationGroups()->andWhere(['information_group_id'=>null]);
    }

    /**
     * @return ActiveQuery
     */
    public function getInformationGroups()
    {
        return $this->hasMany(InformationGroupExt::className(), ['information_id' => 'id']);
    }

    /**
     * @param int $informationGroupId
     * @param int $status
     * @return ActiveQuery
     */
    public function getInformationGroupsByGroupId( $informationGroupId=0, $status = BaseActiveQuery::ALL )
    {
        if( ( $informationGroupId = (int)$informationGroupId ) <= 0 ) {
            $informationGroupId = null;
        }

        $query = $this->getInformationGroups()
            ->where(['information_group_id'=>$informationGroupId]);
        if( $status!==BaseActiveQuery::ALL ) {
            $query->andWhere(['=','active',(int)$status]);
        }
        return $query;
    }

    /**
     * @return ActiveQuery
     */
    public function getInformationItems()
    {
        return $this->hasMany(InformationItemExt::className(), ['information_id' => 'id']);
    }


    public static function getInformation( $id )
    {

    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return MenuExt::getUrlForItem($this);
    }

    /**
     * @return string
     */
    public function getHref( )
    {
        return Yii::$app->storage->getHref( $this );
    }

    /**
     * @return string
     */
    public function getPath( )
    {
        return Yii::$app->storage->getPath( $this );
    }

    public function getByPath( $path )
    {
        /* @var $oInformationGroup InformationGroupExt */
        /* @var $oInformationItem InformationItemExt */

        if( strpos( $path, '/' )===0 ) {
            $path = substr( $path, 1);
        }
        if( strrpos( $path, '/' ) === strlen( $path ) - 1 ) {
            $path = substr( $path, 0, strlen( $path ) - 1 );
        }
        $path = explode('/',$path);
        $items = [];
        $item = null;
        foreach( $path as $num => $id ) {
            try {
                $item = InformationGroupExt::getByTid($id,$item,$this);
            } catch( Exception $e ) {
                $item = InformationItemExt::getByTid($id,$item,$this);
            }
            $items[] = $item;
        }
        return $items;
    }

    /**
     * @param $item
     * @param bool $lastLink
     * @return array
     * @internal param MenuItemExt|int|string $id
     */
    public function getBreadCrumbs( $lastLink=false )
    {
        $items = [];
        $items[] = [
            'label' => $this->name,
            'url' =>  $this->getUrl()
        ];
        return $items;
    }

    public static function getArOptionList($status = BaseActiveQuery::ACTIVE)
    {
        /* @var $qInformation ActiveQuery */
        /* @var $arInformation InformationExt */

        $qInformation = InformationExt::find();
        if( $status !== BaseActiveQuery::ALL ) {
            $qInformation->andWhere(['=','active', ($status == BaseActiveQuery::ACTIVE) ? 1 : 0]);
        }
        $arInformation = $qInformation->all();

        if( empty( $arInformation ) ) return [];

        return ArrayHelper::map($arInformation,'id','name');
    }

    public static function getArOptionListOptgroup($status = BaseActiveQuery::ACTIVE)
    {
        /* @var $qInformation ActiveQuery */
        /* @var $arInformation InformationExt[] */
        /* @var $oInformation InformationExt */

        $qInformation = InformationExt::find();
        if( $status !== BaseActiveQuery::ALL ) {
            $qInformation->andWhere(['=','active', ($status == BaseActiveQuery::ACTIVE) ? 1 : 0]);
        }
        $arInformation = $qInformation->all();

        if( empty( $arInformation ) ) return [];
        $items = [];
        foreach( $arInformation as $oInformation ) {
            $items[$oInformation->id] = [ 'label' => $oInformation->name ];
        }
        return $items;
    }

    /**
     * @param string $tid
     * @param int $parentInformationGroupId
     * @param int $level
     * @param int $status
     * @return string
     */
    public static function getItemsCacheKeyName($tid, $parentInformationGroupId=-1, $level=0, $status=BaseActiveQuery::ACTIVE)
    {
        return 'information_groups_' . $tid . '_'.(string)$parentInformationGroupId. '_' . (string)$level . '_' . (string)$status . (!empty(self::$currentItem) ? ('_' . self::$currentItem->id) : '');
    }

    /**
     * @param int $id
     * @param int $parentInformationGroupId
     * @param int $level
     * @param int $status
     * @return array|mixed
     */
    public static function getArMenuNavItems($id, $parentInformationGroupId=0, $level=0, $status=BaseActiveQuery::ALL)
    {
        /* @var $oInformation InformationExt */
        /* @var $arRootGroups InformationGroupExt[] */
        /* @var $oGroup InformationGroupExt */
        /* @var $oSubGroup InformationGroupExt */

        $cacheKey = self::getItemsCacheKeyName( $id, $parentInformationGroupId, $level, $status);
        Yii::$app->cache->delete($cacheKey);
        $items = Yii::$app->cache->get($cacheKey);

        if (empty($items)) {
            $items = [];
            $oInformation = InformationExt::getById($id);


            $arRootGroups = $oInformation->getInformationGroupsByGroupId( $parentInformationGroupId, $status )->all();

            if( $parentInformationGroupId <= 0 && !empty( $arRootGroups ) ){
                $arRootGroups = $arRootGroups[0]->children;
            }

            if( !empty( $arRootGroups ) ) {
                foreach( $arRootGroups as $oGroup ) {
                    $item = $oGroup->getArMenuNavItem( /*self::isActiveGroup( $oGroup )*/ );

                    $arGroups = $oGroup->children;
                    if( !empty( $arGroups ) ) {
                        $subItems = [];
                        foreach ( $arGroups as $oSubGroup ) {
                            $subItem = $oSubGroup->getArMenuNavItem( /*self::isActiveGroup( $oSubGroup )*/ );
                            $subItems[$oSubGroup->id] = $subItem;
                            /*if( $oSubGroup->information_group_id ) {
                                $subItems[$oSubGroup->information_group_id] = $subItem;
                            } else {
                                $subItems[$oSubGroup->id] = $subItem;
                            }*/
                        }
                        $item['items'] = $subItems;
                    }
                    $items[$oGroup->id] = $item;
                }
            }
            Yii::$app->cache->set($cacheKey, $items, self::CACHE_DURATION);
        }

        return $items;
    }
    /**
     * @return false|int
     * @throws \Exception
     */
    public function delete()
    {
        $arGroups = $this->informationGroups;
        if( !empty($arGroups) ) {
            foreach ( $arGroups as $oGroup ) {
                $oGroup->delete();
            }
        }
        $arProperties = $this->informationProperties;
        if( !empty($arProperties) ) {
            foreach ( $arProperties as $oProperty ) {
                $oProperty->delete();
            }
        }
        return parent::delete();
    }
}