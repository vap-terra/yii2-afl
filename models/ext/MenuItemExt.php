<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 26.01.16
 * Time: 14:59
 */

namespace app\models\ext;


use app\components\web\View;
use app\models\BaseActiveQuery;
use app\models\MenuItem;
use paulzi\adjacencyList\AdjacencyListBehavior;
use paulzi\autotree\AutoTreeTrait;
use paulzi\nestedsets\NestedSetsBehavior;
use paulzi\sortable\SortableBehavior;
use Yii;
use yii\base\InvalidParamException;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

class MenuItemExt extends MenuItem
{

    use AutoTreeTrait;

    public function behaviors()
    {
        return [
            [
                'class' => NestedSetsBehavior::className(),
                'treeAttribute' => 'menu_id',
                'leftAttribute' => 'lb',
                'rightAttribute' => 'rb',
                'depthAttribute' => 'depth',
            ],
            [
                'class' => AdjacencyListBehavior::className(),
                'parentAttribute' => 'menu_item_id',
                'sortable'=>[
                    'class' => SortableBehavior::className(),
                    'query' => ['menu_item_id'],
                    'sortAttribute' => 'position',
                    'step' => 100,
                ],
            ],
            /*
            'timestamp' => [
                'class' => 'yii\behaviors\BlameableBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['dt_creation', 'dt_update'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['dt_update'],
                ],
                'value' => function() { return date('Y-m-d H:i:s');  },
            ],
            'userid'=>[
                'class' => 'yii\behaviors\BlameableBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT =>[ 'creator_user_id', 'updater_user_id' ],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updater_user_id'],
                ],
                'value' => function() { return ( !Yii::$app->request->isConsoleRequest && !Yii::$app->user->isGuest )?Yii::$app->user->id:0; },
            ],
            'ip'=>[
                'class' => 'yii\behaviors\BlameableBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT =>[ 'ip_creation', 'ip_update' ],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['ip_update'],
                ],
                'value' => function() { return (!Yii::$app->request->isConsoleRequest)?Yii::$app->request->getUserIP():'127.0.0.1'; },
            ],
            */
            'slug' => [
                'class' => 'app\behaviors\Transliteration',
                'in_attribute' => 'name',
                'out_attribute' => 'tid',
                'translit' => true
            ],
            'photo' => [
                'class' => 'app\behaviors\FileBehavior',
                'fileAttribute' => 'image',
                'fileNamePrefix' => 'photo',
            ],
        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    /**
     * @return $this
     */
    public function makeRoot()
    {
        return $this->autoTreeCall('makeRoot', ['ns', 'al', 'ni', 'mp'], [], false);
    }

    /**
     * @param \yii\db\BaseActiveRecord $node
     * @return $this
     */
    public function prependTo($node)
    {
        return $this->autoTreeCall('prependTo', ['ns', 'al', 'ni', 'mp'], [$node], false);
    }

    /**
     * @param \yii\db\BaseActiveRecord $node
     * @return $this
     */
    public function appendTo($node)
    {
        return $this->autoTreeCall('appendTo', ['ns', 'al', 'ni', 'mp'], [$node], false);
    }

    /**
     * @param \yii\db\BaseActiveRecord $node
     * @return $this
     */
    public function insertBefore($node)
    {
        return $this->autoTreeCall('insertBefore', ['ns', 'al', 'ni', 'mp'], [$node], false);
    }

    /**
     * @param \yii\db\BaseActiveRecord $node
     * @return $this
     */
    public function insertAfter($node)
    {
        return $this->autoTreeCall('insertAfter', ['ns', 'al', 'ni', 'mp'], [$node], false);
    }

    public function beforeSave($insert)
    {
        if( parent::beforeSave($insert) ) {
            $image = $this->getOldAttribute('image');
            if (empty($this->image) && !empty($image)) {
                $this->image = $image;
            }
            return true;
        }
        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContents()
    {
        return $this->hasMany(ContentExt::className(), ['menu_item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformations()
    {
        return $this->hasMany(InformationExt::className(), ['menu_item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenu()
    {
        return $this->hasOne(MenuExt::className(), ['id' => 'menu_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuItem()
    {
        return $this->hasOne(MenuItemExt::className(), ['id' => 'menu_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuItems()
    {
        return $this->hasMany( MenuItemExt::className(), ['menu_item_id' => 'id'] );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuItemBlocks()
    {
        return $this->hasMany(MenuItemBlockExt::className(), ['menu_item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuItemContents()
    {
        return $this->hasMany(MenuItemContentExt::className(), ['menu_item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuItemRelations()
    {
        return $this->hasMany(MenuItemRelationExt::className(), ['menu_item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotMainMenuItemRelations()
    {
        return $this->getMenuItemRelations()->andWhere(['=','main',0])->orderBy(['position'=>SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMainMenuItemRelations()
    {
        return $this->getMenuItemRelations()->andWhere(['=','main',1])->orderBy(['position'=>SORT_ASC]);
    }

    public function getUrl()
    {
        $url = '';
        $arOMenuItems = array_merge( $this->getParents()->andWhere(['is not','menu_item_id',null])->all(), [$this] );
        foreach( $arOMenuItems as $oItem ) {
            if( $url ) {
                $url .= '/';
            }
            $url .= ( $oItem->tid ? $oItem->tid : $oItem->id );
        }
        return '/'.trim($url,'/');
    }

    /**
     * @return string
     */
    public function getHref( )
    {
        if( $this->menu_id ) {
            $oMenu = $this->getMenu()->one();
            return $oMenu->getHref($this) . 'item_' . $this->id . '/';
        }
        throw new InvalidParamException('For Menu item not set Menu');
    }

    /**
     * @return string
     */
    public function getPath( )
    {
        if( $this->menu_id ) {
            $oMenu = $this->getMenu()->one();
            return $oMenu->getPath($this) . 'item_' . $this->id . '/';
        }
        throw new InvalidParamException('For Menu item not set Menu');
    }

    /**
     * @return string
     */
    /*public function getImageHref()
    {
        return $this->getHref() . $this->image;
    }*/

    /**
     * @return string
     */
    /*public function getImagePath()
    {
        return $this->getPath() . $this->image;
    }*/

    /**
     * @param $extension
     * @return string
     */
    /*public function generateImageFileName( $extension )
    {
        return 'photo_'.md5($this->id).'.' . $extension;
    }*/

    /**
     *
     */
    /*public function createDir()
    {
        $dir = $this->getPath();
        if( !is_dir( $dir ) ) {
            mkdir($dir, 0775, true);
        }
    }*/

    /**
     * @param UploadedFile $image
     * @param bool $upload
     * @param bool $deleteTempFile
     * @return bool
     */
    /*public function uploadImage($image, $upload=true, $deleteTempFile = true )
    {
        if( $image instanceof UploadedFile === false ) return false;
        $fileName = $this->generateImageFileName($image->extension);
        $filePath = $this->getPath() . $fileName;
        $this->createDir();
        if( $upload ) {
            $status = $image->saveAs( $filePath, $deleteTempFile  );
        } else {
            $status = copy( $image->tempName, $filePath );
            if( $status && $deleteTempFile ) {
                unlink($image->tempName);
            }
        }
        if ( $status ) {
            $this->image = $fileName;
            $this->save();
            return true;
        }
        return false;
    }*/

    public function delete()
    {
        if( $this->menuItemRelations ){
            $this->unlinkAll('menuItemRelations',true);
        }
        if( $this->contents ) {
            $this->unlinkAll('contents',false);
        }
        if( $this->menuItemBlocks ){
            $this->unlinkAll('menuItemBlocks',true);
        }
        /*if( $this->informations ){
            $this->unlinkAll('informations',false);
        }*/
        foreach( $this->children as $child ){
            $child->delete();
        }
        return parent::delete();
    }

    /**
     * @param int|string $toId
     * @param int|string $afterId
     * @return bool
     */
    public function relocation( $toId=0, $afterId=0 )
    {
        $parentItem = null;
        if( $toId ) {
            $parentItem = self::findOne($toId);
        }

        $afterItem = null;
        if( $afterId ) {
            $afterItem = self::findOne($afterId);
        }

        if( $afterItem ) {
            $this->menu_id = $afterItem->menu_id;
            $this->menu_item_id = $afterItem->menu_item_id;
            $this->depth = $afterItem->depth;
            $this->insertAfter($afterItem);
        } else if( $parentItem ) {
            $this->menu_id = $parentItem->menu_id;
            $this->menu_item_id = $parentItem->id;
            $this->depth = $parentItem->depth + 1;
            $this->prependTo($parentItem);
        } else {
            $root = self::findOne(['menu_id'=>$this->menu_id,'depth'=>1,'menu_item_id'=>null]);
            if( $root ) {
                $this->menu_item_id = $root->id;
                $this->depth = $root->depth+1;
                $this->prependTo($root);
            }
        }

        return $this->save();
    }
    /**
     * @param int | string $id
     * @return array|null|\yii\db\ActiveRecord
     * @throws NotFoundHttpException
     */
    public static function getByTid( $id = '/', $oMenuParentItem = null )
    {

        /* @var $qMenuItem ActiveQuery */
        $qMenuItem = self::find();

        if( preg_match( '/^[\d]+$/', $id ) ) {
            $qMenuItem->where(['=','id',$id]);
        } else {
            $qMenuItem->where(['=','tid',$id]);
        }

        if( $oMenuParentItem ) {
            $qMenuItem->andWhere(['=','board_group_id',$oMenuParentItem->id]);
        }

        /* @var $oMenuItem /common/models/ext/ */
        $oMenuItem = $qMenuItem->one();
        if( !$oMenuItem ) {
            throw new NotFoundHttpException( Yii::t('app/app', '[404] Not Found menu item {id}', ['id'=>$id] ) );
        }

        return $oMenuItem;
    }

    /**
     * @param int $order
     * @param int $status
     * @return ActiveQuery
     */
    public function getQueryContents( $order = SORT_ASC, $status = BaseActiveQuery::ACTIVE )
    {
        /* @var $qContentsLink ActiveQuery */
        $qContentsLink = $this->getMenuItemContents()->orderBy(['position' => $order]);

        if ( $status !== BaseActiveQuery::ALL ) {
            $qContentsLink->andWhere(['=', 'active', ($status == BaseActiveQuery::ACTIVE) ? 1 : 0]);
        }

        return $qContentsLink;
    }

    /**
     * @param int $order
     * @param int $status
     * @return \string[]
     */
    public function getStrContents( $order = SORT_ASC, $status = BaseActiveQuery::ACTIVE )
    {
        /* @var $oMenuItem MenuItemExt */
        /* @var $qContentsLink ActiveQuery */
        /* @var $arStrContents string[] */
        $arStrContents = [];

        $qContentsLink = $this->getQueryContents( $order, $status );

        $arContentsLink = $qContentsLink->all();

        foreach ($arContentsLink as $oContentLink) {
            $arStrContents[] = $oContentLink->content->content;
        }

        return $arStrContents;
    }

    /**
     * @param int $order
     * @param int $status
     * @return BaseActiveQuery
     */
    public function getRelationContents( $order = SORT_ASC, $status = BaseActiveQuery::ACTIVE )
    {
        /* @var $qRelations BaseActiveQuery */
        $qRelations = $this->getMenuItemRelations()->orderBy(['position' => $order]);

        if ( $status !== BaseActiveQuery::ALL ) {
            $qRelations->andWhere(['=', 'active', ($status == BaseActiveQuery::ACTIVE) ? 1 : 0]);
        }

        return $qRelations;
    }

    /**
     * @param View $view
     * @param string[] $arUnsortedPath
     * @param int $order
     * @param int $status
     * @return \string[]
     */
    public function getArRelationContents( $view, $arUnsortedPath, $order = SORT_ASC, $status = BaseActiveQuery::ACTIVE )
    {
        $arStrContents = [];
        /* @var $oRelation MenuItemRelationExt */
        foreach ($this->getRelationContents()->all() as $oRelation) {
            $content = $oRelation->getContent($view, $arUnsortedPath);
            if( $view->onlyMain ){
                return [$content];
            }
            if ($content) {
                $arStrContents[] = $content;
            }
        }
        return $arStrContents;
    }

    /**
     * @param string $groupTid
     * @param int $order
     * @param int $status
     * @return ActiveQuery
     */
    public function getQueryBlocks( $groupTid, $order = SORT_ASC, $status = BaseActiveQuery::ACTIVE )
    {
        /* @var $qContentsLink ActiveQuery */
        $qContentsLink = $this->getMenuItemBlocks()->orderBy(['position' => $order]);

        if ( $status !== BaseActiveQuery::ALL ) {
            $qContentsLink->andWhere(['=', 'active', ($status == BaseActiveQuery::ACTIVE) ? 1 : 0]);
        }

        return $qContentsLink;
    }

    /**
     * @param string $groupTid
     * @param int $order
     * @param int $status
     * @return \string[]
     */
    public function getStrBlocks( $groupTid, $order = SORT_ASC, $status = BaseActiveQuery::ACTIVE )
    {
        /* @var $oMenuItem MenuItemExt */
        /* @var $oBlock BlockExt */
        /* @var $qContentsLink ActiveQuery */
        /* @var $arStrContents string[] */
        $arStrContents = [];

        $qContentsLink = $this->getQueryBlocks( $groupTid, $order, $status );

        $arContentsLink = $qContentsLink->all();

        foreach ($arContentsLink as $oContentLink) {
            $oBlock = $oContentLink->block;
            $arStrContents[] = $oBlock->generate( $oContentLink->menu_item_id );
        }

        return $arStrContents;
    }

    /*public function getParents()
    {
        /* @var $oMenuItem MenuItemExt */
        /*$parents = [];
        $oMenuItem = $this->getMenuItem()->one();
        if( !empty( $oMenuItem ) ) {
            $parents = array_merge( [ $oMenuItem ], $oMenuItem->getParents() );
        }

        return $parents;
    }*/

    public function getFirstParent()
    {
        $parents = $this->parents;
        return array_pop( $parents );
    }

    /**
     * @param bool $active
     * @return array
     */
    public function getArMenuNavItem( $active = false )
    {
        return [
            'id' => $this->id,
            'label' => $this->name,
            'url' => $this->link?$this->link:$this->getUrl(),
            'active' => $active,
            'visible' => true,
            'items' => null,
        ];
    }

    public function getArMenuNavItems($level=0,$status = BaseActiveQuery::ACTIVE)
    {
        /* @var $arMenuItems MenuItemExt[] */
        $arItem = $this->getArMenuNavItem(MenuExt::isActiveMenuItem($this));
        if( $level <= 0 || $this->depth <= $level ) {
            $qMenuItems = $this->getChildren($this->depth+1);
            if ($status !== BaseActiveQuery::ALL ) {
                $qMenuItems->andWhere(['=', 'active', ($status == BaseActiveQuery::ACTIVE) ? 1 : 0]);
            }
            $arMenuItems = $qMenuItems->all();
            if (!empty($arMenuItems)) {
                foreach ($arMenuItems as $oMenuItem) {
                    $arItem['items'][] = $oMenuItem->getArMenuNavItems($level, $status);
                }
            }
        }
        return $arItem;
    }

    public function saveRelations( $values )
    {
        $arMenuItemRelations = $this->getMenuItemRelations()->orderBy(['position'=>SORT_ASC])->all();
        $arRelationValues = ArrayHelper::getValue($values,'MenuItemRelationExt',[]);
        // update content items with relations
        if( !empty( $arMenuItemRelations ) ) {
            foreach( $arMenuItemRelations as $oMenuItemRelation ) {
                /* @var $oMenuItemRelation MenuItemRelationExt */
                /* @var $oContent ContentExt */
                if( isset( $arRelationValues[$oMenuItemRelation->id] ) ) {
                    //TODO update relation and content
                    if( $oMenuItemRelation->load( $arRelationValues[$oMenuItemRelation->id], '' ) ){
                        $oMenuItemRelation->menu_item_id = $this->id;
                        switch( $oMenuItemRelation->type ) {
                            case MenuItemRelationExt::TYPE_CONTENT :
                                if( isset( $values['ContentExt'][$oMenuItemRelation->id] ) ) {
                                    $contentData = $values['ContentExt'][$oMenuItemRelation->id];
                                    if( !empty( $contentData['id'] ) ) {
                                        $oContent = ContentExt::getById($contentData['id']);
                                    } else {
                                        $oContent = new ContentExt();
                                    }

                                    $oContent->load($contentData,'');

                                    //$oContent->content = $contentData['content'];
                                    //$oContent->name = $contentData['name'];
                                    if( $oMenuItemRelation->main ) {
                                        $oContent->menu_item_id = $this->id;
                                        $oContent->name = $this->name;
                                    }
                                    if( $oContent->save() ) {
                                        $oMenuItemRelation->item_id = $oContent->id;
                                    } else {
                                        if( $oMenuItemRelation->type !== $oMenuItemRelation->getOldAttribute('type') ){
                                            $oMenuItemRelation->type = $oMenuItemRelation->getOldAttribute('type');
                                        }
                                    }
                                }
                                break;
                        }
                        $oMenuItemRelation->save();
                    }
                    unset( $arRelationValues[$oMenuItemRelation->id] );
                } else {
                    //TODO delete relation
                    $oMenuItemRelation->delete();
                }
            }
        }

        // add new content items with relations
        if( !empty( $arRelationValues ) ) {
            foreach( $arRelationValues as $num => $params ) {
                $oMenuItemRelation = new MenuItemRelationExt();
                if( empty( $params['position'] ) ){
                    $params['position'] = MenuItemRelationExt::getMax('position',0,['=','menu_item_id',$this->id])+10;
                }
                if( $oMenuItemRelation->load( $params, '' ) ){
                    $oMenuItemRelation->menu_item_id = $this->id;
                    switch( $oMenuItemRelation->type ) {
                        case MenuItemRelationExt::TYPE_CONTENT :
                            if( isset( $values['ContentExt'][$num] ) ) {
                                $contentData = $values['ContentExt'][$num];
                                $oContent = new ContentExt();

                                $oContent->load($contentData,'');

                                /*$oContent->content = $contentData['content'];
                                $oContent->name = $contentData['name'];*/
                                if( $oMenuItemRelation->main ) {
                                    $oContent->menu_item_id = $this->id;
                                    $oContent->name = $this->name;
                                }

                                if ($oContent->save()) {
                                    $oMenuItemRelation->item_id = $oContent->id;
                                    $oMenuItemRelation->save();
                                }
                            }
                            break;
                        default:
                            $oMenuItemRelation->save();
                            break;
                    }
                }
            }
        }
    }

}