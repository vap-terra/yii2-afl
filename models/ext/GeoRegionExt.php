<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 09.02.16
 * Time: 13:06
 */

namespace app\models\ext;


use app\models\BaseActiveQuery;
use app\models\GeoRegion;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\data\Sort;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class GeoRegionExt extends GeoRegion
{
    public function generateDataFilter()
    {
        $this->setScenario(self::SCENARIO_SEARCH);
        if( $this->load($_GET+$_POST) && !$this->validate() ){
            $this->attributes = [];
        }
        return $this;
    }

    /**
     * @return Sort
     */
    public static function generateDataSort()
    {
        return new Sort(
            [
                //'route' => $route,
                'defaultOrder' => [
                    'name' => SORT_ASC,
                ],
                'enableMultiSort' => true,
                'attributes' => [
                    'id' => [
                        'default' => SORT_ASC,
                    ],
                    'name' => [
                        'default' => SORT_ASC,
                    ],
                ],
            ]
        );
    }

    /**
     * @param $query BaseActiveQuery
     * @return Pagination
     */
    public static function generateDataPagination( $query )
    {
        $countQuery = clone $query;
        return new Pagination(
            [
                'totalCount' => $countQuery->count(),
                'pageSize' => 30,
                'pageParam' => 'page',
            ]
        );
    }

    /**
     * @param $query BaseActiveQuery
     * @return ActiveDataProvider
     */
    public static function generateDataProvider( $query )
    {
        return new ActiveDataProvider(
            [
                'query' => self::find(),
                'pagination' => self::generateDataPagination( $query ),
                'sort' => self::generateDataSort(),
            ]
        );
    }

    /**
     * @return array
     */
    public static function generateGridViewColumns()
    {
        return [
            ['class'=>'yii\grid\SerialColumn'],
            ['class'=>'yii\grid\CheckboxColumn'],
            'id',
            'name',
            'region_areas' => [
                'format' => 'html',
                'value' => function($model) {
                    /* @var $model GeoCountryExt */
                    $url = Yii::$app->urlManager->createUrl(
                        ['/backend/geo-region-area/index', 'geo_country_region_id' => $model->id]
                    );
                    return Html::a('<span class="glyphicon glyphicon-link"></span>'.$model->getGeoRegionAreas()->count(), $url, []);
                }
            ],
            [
                'class'=>'yii\grid\ActionColumn',
                'headerOptions' => ['width' => '160'],
                'template' => '{edit} {delete}',
                'buttons' => [
                    'edit' => function ($url, $model, $key) {
                        $url = Yii::$app->urlManager->createUrl(
                            ['/backend/geo-region/edit','id'=>$model->id,'geo_country_id'=>$model->geo_country_id]
                        );
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>',$url,[]);
                    },
                    'delete' => function ($url, $model, $key) {
                        $url = Yii::$app->urlManager->createUrl(
                            ['/backend/geo-region/delete','id'=>$model->id,'geo_country_id'=>$model->geo_country_id]
                        );
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>',$url,[]);
                    },
                ],
            ],
        ];
    }

    /**
     * @param int $geoCountryId
     * @param int $status
     * @return array
     */
    public static function getArOptionList( $geoCountryId=0, $status=BaseActiveQuery::ALL )
    {
        $query = self::find();
        if($geoCountryId) {
            $query->andWhere(['=', 'geo_country_id', (int)$geoCountryId]);
        }
        /*if( $status != BaseActiveQuery::ALL ) {
            $query->andWhere(['=','active',(int)$status]);
        }*/
        return ArrayHelper::map($query->all(),'id','name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGeoLocalities()
    {
        return $this->hasMany(GeoLocalityExt::className(), ['geo_region_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGeoLocalityAreas()
    {
        return $this->hasMany(GeoLocalityAreaExt::className(), ['geo_region_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGeoCountry()
    {
        return $this->hasOne(GeoCountryExt::className(), ['id' => 'geo_country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGeoRegionAreas()
    {
        return $this->hasMany(GeoRegionAreaExt::className(), ['geo_region_id' => 'id']);
    }

}