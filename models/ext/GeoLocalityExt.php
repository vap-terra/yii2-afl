<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 02.02.16
 * Time: 13:56
 */

namespace app\models\ext;


use app\models\BaseActiveQuery;
use app\models\GeoLocality;
use yii\helpers\ArrayHelper;

class GeoLocalityExt extends GeoLocality
{
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGeoRegion()
    {
        return $this->hasOne(GeoRegionExt::className(), ['id' => 'geo_region_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGeoRegionArea()
    {
        return $this->hasOne(GeoRegionAreaExt::className(), ['id' => 'geo_region_area_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGeoCountry()
    {
        return $this->hasOne(GeoCountryExt::className(), ['id' => 'geo_country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGeoLocalityAreas()
    {
        return $this->hasMany(GeoLocalityAreaExt::className(), ['geo_locality_id' => 'id']);
    }

    /**
     * @param int $geoRegionAreaId
     * @param int $geoRegionId
     * @param int $geoCountryId
     * @param int $status
     * @return array
     */
    public static function getArOptionList( $geoRegionAreaId=0, $geoRegionId=0, $geoCountryId=0, $status=BaseActiveQuery::ALL )
    {

        $query = self::find();
        if($geoRegionAreaId) {
            $query->andWhere(['=', 'geo_region_area_id', (int)$geoRegionAreaId]);
        }
        if($geoRegionId) {
            $query->andWhere(['=', 'geo_region_id', (int)$geoRegionId]);
        }
        if($geoCountryId) {
            $query->andWhere(['=', 'geo_country_id', (int)$geoCountryId]);
        }
        /*if( $status != BaseActiveQuery::ALL ) {
            $query->andWhere(['=','active',(int)$status]);
        }*/
        return ArrayHelper::map($query->all(),'id','name');
    }
}