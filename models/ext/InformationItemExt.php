<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 28.01.16
 * Time: 17:33
 */

namespace app\models\ext;


use app\components\FileStorageManager;
use app\models\InformationItem;
use Yii;
use yii\base\InvalidParamException;
use yii\db\ActiveRecord;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

class InformationItemExt extends InformationItem
{
    public $del_img;

    public function __construct(array $config = [])
    {
        parent::__construct($config);
        if( $this->isNewRecord ) {
            $this->dt_creation = $this->dt_beginning = date('Y-m-d H:i:s');
        }
    }

    public function rules()
    {
        return array_merge_recursive(
            parent::rules(),
            [
                [['image'], 'file', 'extensions' => 'gif, jpg, jpe, jpeg, png'],
                [['del_img'], 'boolean'],
            ]
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformation()
    {
        return $this->hasOne(InformationExt::className(), ['id' => 'information_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformationGroup()
    {
        return $this->hasOne(InformationGroupExt::className(), ['id' => 'information_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformationItemGroupProperties()
    {
        return $this->hasMany(InformationItemGroupPropertyExt::className(), ['information_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformationItemPropertyValues()
    {
        return $this->hasMany(InformationItemPropertyValueExt::className(), ['information_item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformationPropertyValueFiles()
    {
        return $this->hasMany(InformationPropertyValueFileExt::className(), ['information_item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformationPropertyValueIntegers()
    {
        return $this->hasMany(InformationPropertyValueIntegerExt::className(), ['information_item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformationPropertyValueStrings()
    {
        return $this->hasMany(InformationPropertyValueStringExt::className(), ['information_item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformationPropertyValueTexts()
    {
        return $this->hasMany(InformationPropertyValueTextExt::className(), ['information_item_id' => 'id']);
    }

    /**
     * @param array $includeIds
     * @param array $excludeIds
     * @return \yii\db\ActiveQuery
     */
    public function getInformationProperties( $includeIds = [], $excludeIds = [] )
    {
        $query = $this->information->getInformationProperties();
        if( $this->information_group_id ) {
            $query->orWhere(['=',InformationItemGroupPropertyExt::tableName().'.information_group_id',$this->information_group_id]);
            $parents = $this->informationGroup->parents;
        }

        if( !empty( $parents ) ) {
            foreach( $parents as $oInformationGroup ) {
                $query->orWhere(['=',InformationItemGroupPropertyExt::tableName().'.information_group_id',$oInformationGroup->id]);
                $query->andWhere(['=',InformationItemGroupPropertyExt::tableName().'.recursive',1]);
            }
        }
        if( $this->information_group_id || !empty( $parents ) ) {
            $query->leftJoin(InformationItemGroupPropertyExt::tableName(),InformationItemGroupPropertyExt::tableName().'.information_property_id'.'='.InformationPropertyExt::tableName().'.id');
        }
        if( !empty( $includeIds ) ) {
            $query->andWhere(['IN',InformationPropertyExt::tableName().'.id',$includeIds]);
        }
        if( !empty( $excludeIds ) ) {
            $query->andWhere(['NOT IN',InformationPropertyExt::tableName().'.id',$excludeIds]);
        }
        return $query;
    }

    public function getInformationPropertyValues( $propertyId )
    {
        $oProperty = InformationPropertyExt::getById($propertyId);
        return $oProperty->getValues($this->id)->all();
    }

    public function beforeSave($insert)
    {
        if( parent::beforeSave($insert) ) {
            $image = $this->getOldAttribute('image');
            if (empty($this->image) && !empty($image)) {
                $this->image = $image;
            }
            return true;
        }
        return false;
    }
    
    public function delete()
    {
        // delete all properties
        $arProps = $this->getInformationPropertyValueIntegers()->all();
        if( !empty( $arProps ) ) {
            foreach ($arProps as $prop) {
                $prop->delete();
            }
        }
        $arProps = $this->getInformationPropertyValueStrings()->all();
        foreach( $arProps as $prop ) {
            $prop->delete();
        }
        $arProps = $this->getInformationPropertyValueTexts()->all();
        foreach( $arProps as $prop ) {
            $prop->delete();
        }
        $arProps = $this->getInformationPropertyValueFiles()->all();
        foreach( $arProps as $prop ) {
            $prop->delete();
        }
        $path = $this->getPath();
        if( $this->image && is_file($path . $this->image)) {
            unlink($path . $this->image);
        }
        return parent::delete();
    }
    
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\BlameableBehavior',
                'attributes' => [
                    //ActiveRecord::EVENT_BEFORE_VALIDATE => ['dt_creation','dt_update'],
                    ActiveRecord::EVENT_BEFORE_INSERT => ['dt_creation', 'dt_update'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['dt_update'],
                ],
                'value' => function() { return date('Y-m-d H:i:s');  },
            ],
            'userid'=>[
                'class' => 'yii\behaviors\BlameableBehavior',
                'attributes' => [
                    //ActiveRecord::EVENT_BEFORE_VALIDATE => ['creator_user_id','updater_user_id'],
                    ActiveRecord::EVENT_BEFORE_INSERT => [ 'creator_user_id', 'updater_user_id' ],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updater_user_id'],
                ],
                'value' => function() { return ( !Yii::$app->request->isConsoleRequest && !Yii::$app->user->isGuest )?Yii::$app->user->id:0; },
            ],
            'ip'=>[
                'class' => 'yii\behaviors\BlameableBehavior',
                'attributes' => [
                    //ActiveRecord::EVENT_BEFORE_VALIDATE => ['ip_creation','ip_update'],
                    ActiveRecord::EVENT_BEFORE_INSERT =>[ 'ip_creation', 'ip_update' ],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['ip_update'],
                ],
                'value' => function() { return (!Yii::$app->request->isConsoleRequest)?Yii::$app->request->getUserIP():'127.0.0.1'; },
            ],
            'slug' => [
                'class' => 'app\behaviors\Transliteration',
                'in_attribute' => 'name',
                'out_attribute' => 'tid',
                'translit' => true
            ]
        ];
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        /* @var $oInformationGroup InformationGroupExt*/
        /* @var $oInformation InformationExt*/
        $url = $this->tid ? $this->tid : $this->id ;
        if( $this->information_group_id ) {
            $oInformationGroup = $this->getInformationGroup()->one();
            return $oInformationGroup->getUrl() . '/' . $url;
        } else if( $this->information_id ) {
            $oInformation = $this->getInformation()->one();
            return $oInformation->getUrl() . '/' . $url;
        }
        throw new InvalidParamException('For Information system item not set information group or information system');
    }

    /**
     * @return string
     */
    public function getHref( )
    {
        /* @var $oInformation InformationExt */
        if( $this->information_id ) {
            $oInformation = $this->getInformation()->one();
            return $oInformation->getHref($this) .
            'items/' .
            FileStorageManager::getNestingDirPath($this->id, 3, 0) .
            /*'group_' . (string)$this->information_group_id .*/
            '/item_' . $this->id . '/';
        }
        throw new InvalidParamException('For Information item not set Information');
    }

    /**
     * @return string
     */
    public function getPath( )
    {
        /* @var $oInformation InformationExt */
        if( $this->information_id ) {
            $oInformation = $this->getInformation()->one();
            return $oInformation->getPath($this) .
            'items/' .
            FileStorageManager::getNestingDirPath($this->id, 3, 0) .
            /*'group_' . (string)$this->information_group_id .*/ '/item_' .
            $this->id . '/';
        }
        throw new InvalidParamException('For Information item not set Information');
    }

    /**
     * @return string
     */
    public function getImageHref()
    {
        return $this->getHref() . $this->image;
    }

    /**
     * @return string
     */
    public function getImagePath()
    {
        return $this->getPath() . $this->image;
    }

    /**
     * @param $extension
     * @return string
     */
    public function generateImageFileName( $extension )
    {
        return 'photo_'.md5($this->id).'.' . $extension;
    }
    /**
     *
     */
    public function createDir()
    {
        $dir = $this->getPath();
        if( !is_dir( $dir ) ) {
            mkdir($dir, 0775, true);
        }
    }

    /**
     * @param UploadedFile $image
     * @param bool $upload
     * @param bool $deleteTempFile
     * @return bool
     */
    public function uploadImage($image, $upload=true, $deleteTempFile = true )
    {
        if( $image instanceof UploadedFile === false ) return false;
        $fileName = $this->generateImageFileName($image->extension);
        $filePath = $this->getPath() . $fileName;
        $this->createDir();
        if( $upload ) {
            $status = $image->saveAs( $filePath, $deleteTempFile  );
        } else {
            $status = copy( $image->tempName, $filePath );
            if( $status && $deleteTempFile ) {
                unlink($image->tempName);
            }
        }
        if ( $status ) {
            $this->image = $fileName;
            $this->save();
            return true;
        }
        return false;
    }

    /**
     * @param int | string $id
     * @param null $oInformationParentItem
     * @param null $oInformation
     * @return array|null|ActiveRecord
     * @throws NotFoundHttpException
     */
    public static function getByTid( $id, $oInformationParentItem = null, $oInformation=null )
    {

        /* @var $qInformationItem ActiveQuery */
        $qInformationItem = self::find();

        if( preg_match( '/^[\d]+$/', $id ) ) {
            $qInformationItem->where(['=','id',$id]);
        } else {
            $qInformationItem->where(['=','tid',$id]);
        }

        if( $oInformationParentItem ) {
            $qInformationItem->andWhere(['=','information_group_id',$oInformationParentItem->id]);
        }

        if( $oInformation ) {
            $qInformationItem->andWhere(['=','information_id',$oInformation->id]);
        }

        /* @var $oInformationItem InformationItemExt */
        $oInformationItem = $qInformationItem->one();
        if( empty( $oInformationItem ) ) {
            throw new NotFoundHttpException( Yii::t('app/app', '[404] Not Found information item {id}', [ 'id' => $id ] ) );
        }

        return $oInformationItem;
    }


    /**
     * @param int $afterNumNotLink
     * @param bool $lastLink
     * @return array
     */
    public function getBreadCrumbs( $afterNumNotLink=0, $lastLink=false )
    {
        /* @var $oInformationItem InformationItemExt */
        /* @var $oInformationGroup InformationGroupExt */
        $items = $this->information->getBreadCrumbs();
        if( $this->information_group_id ) {
            $items = $this->informationGroup->getBreadCrumbs( $afterNumNotLink, true );
        }
        $item = [];
        $item['label'] = $this->name;
        if ( $lastLink ) {
            $item['url'] = '/' . $this->getUrl();
        }
        $items[] = $item;
        return $items;
    }
}