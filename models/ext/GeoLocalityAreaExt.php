<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 29.01.16
 * Time: 15:02
 */

namespace app\models\ext;


use app\models\BaseActiveQuery;
use app\models\GeoLocalityArea;
use yii\helpers\ArrayHelper;

class GeoLocalityAreaExt extends GeoLocalityArea
{
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGeoLocality()
    {
        return $this->hasOne(GeoLocalityExt::className(), ['id' => 'geo_locality_id']);
    }

    /**
     * @param int $geoLocalityId
     * @param int $geoRegionAreaId
     * @param int $geoRegionId
     * @param int $geoCountryId
     * @param int $status
     * @return array
     */
    public static function getArOptionList( $geoLocalityId=0, $geoRegionAreaId=0, $geoRegionId=0, $geoCountryId=0, $status=BaseActiveQuery::ALL )
    {
        $query = self::find();
        if($geoLocalityId) {
            $query->andWhere(['=', 'geo_locality_id', (int)$geoLocalityId]);
        }
        if($geoRegionAreaId) {
            $query->andWhere(['=', 'geo_region_area_id', (int)$geoRegionAreaId]);
        }
        if($geoRegionId) {
            $query->andWhere(['=', 'geo_region_id', (int)$geoRegionId]);
        }
        if($geoCountryId) {
            $query->andWhere(['=', 'geo_country_id', (int)$geoCountryId]);
        }
        $query = self::find()->where( [ '=', 'geo_locality_id',(int)$geoLocalityId] );
        /*if( $status != BaseActiveQuery::ALL ) {
            $query->andWhere(['=','active',(int)$status]);
        }*/
        return ArrayHelper::map($query->all(),'id','name');
    }
}