<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 27.07.16
 * Time: 14:13
 */

namespace app\models\ext;


use app\models\Comment;
use Yii;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

class CommentExt extends Comment
{
    /**
     * @inheritdoc
     */
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\BlameableBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['dt_creation'/*, 'dt_update'*/],
                    //ActiveRecord::EVENT_BEFORE_UPDATE => ['dt_update'],
                ],
                'value' => function() { return date('Y-m-d H:i:s');  },
            ],
            'userid'=>[
                'class' => 'yii\behaviors\BlameableBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT =>[ 'creator_user_id'/*, 'updater_user_id' */],
                    //ActiveRecord::EVENT_BEFORE_UPDATE => ['updater_user_id'],
                ],
                'value' => function() { return ( !Yii::$app->request->isConsoleRequest && !Yii::$app->user->isGuest )?Yii::$app->user->id:0; },
            ],
            'userip'=>[
                'class' => 'yii\behaviors\BlameableBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT =>[ 'ip_creation'/*, 'ip_update' */],
                    //ActiveRecord::EVENT_BEFORE_UPDATE => ['ip_update'],
                ],
                'value' => function() { return (!Yii::$app->request->isConsoleRequest)?Yii::$app->request->getUserIP():'127.0.0.1'; },
            ],
            'author_photo' => [
                'class' => 'app\behaviors\FileBehavior',
                'fileAttribute' => 'author_photo',
                'fileNamePrefix' => 'photo_author',
            ],
        ];
    }

    public function beforeSave($insert)
    {
        if( parent::beforeSave($insert) ) {
            return true;
        }
        return false;
    }
}