<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 14.03.16
 * Time: 6:44
 */

namespace app\models\ext;


use app\components\FileStorageManager;
use app\models\Banner;
use yii\base\InvalidParamException;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

class BannerExt extends Banner
{
    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'app\behaviors\FileBehavior',
                'fileAttribute' => 'image',
                'fileNamePrefix' => 'photo',
            ],
        ];
    }

    public function beforeSave($insert)
    {
        if( parent::beforeSave($insert) ) {
            /*$image = $this->getOldAttribute('image');
            if (empty($this->image) && !empty($image)) {
                $this->image = $image;
            }*/
            return true;
        }
        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBannerGroup()
    {
        return $this->hasOne(BannerGroupExt::className(), ['id' => 'banner_group_id']);
    }

    /**
     * @return string
     */
    public function getHref( )
    {
        /* @var $oBannerGroupExt BannerGroupExt */
        if( $this->banner_group_id ) {
            $oBannerGroupExt = $this->getBannerGroup()->one();
            return $oBannerGroupExt->getHref($this) .
            'items/' .
            FileStorageManager::getNestingDirPath($this->id, 3, 0) .
            /*'group_' . (string)$this->information_group_id .*/
            '/item_' . $this->id . '/';
        }
        throw new InvalidParamException('For banner not set group');
    }

    /**
     * @return string
     */
    public function getPath( )
    {
        /* @var $oBannerGroupExt BannerGroupExt */
        if( $this->banner_group_id ) {
            $oBannerGroupExt = $this->getBannerGroup()->one();
            return $oBannerGroupExt->getPath($this) .
            'items/' .
            FileStorageManager::getNestingDirPath($this->id, 3, 0) .
            /*'group_' . (string)$this->information_group_id .*/ '/item_' .
            $this->id . '/';
        }
        throw new InvalidParamException('For banner not set group');
    }

    /**
     * @return string
     */
    /*public function getImageHref()
    {
        return $this->getHref() . $this->image;
    }*/

    /**
     * @return string
     */
    /*public function getImagePath()
    {
        return $this->getPath() . $this->image;
    }*/

    /**
     * @param $extension
     * @return string
     */
    /*public function generateImageFileName( $extension )
    {
        return 'photo_'.md5($this->id).'.' . $extension;
    }*/
    /**
     *
     */
    /*public function createDir()
    {
        $dir = $this->getPath();
        if( !is_dir( $dir ) ) {
            mkdir($dir, 0775, true);
        }
    }*/

    /**
     * @param UploadedFile $image
     * @param bool $upload
     * @param bool $deleteTempFile
     * @return bool
     */
    /*public function uploadImage($image, $upload=true, $deleteTempFile = true )
    {
        if( $image instanceof UploadedFile === false ) return false;
        $fileName = $this->generateImageFileName($image->extension);
        $filePath = $this->getPath() . $fileName;
        $this->createDir();
        if( $upload ) {
            $status = $image->saveAs( $filePath, $deleteTempFile  );
        } else {
            $status = copy( $image->tempName, $filePath );
            if( $status && $deleteTempFile ) {
                unlink($image->tempName);
            }
        }
        if ( $status ) {
            $this->image = $fileName;
            $this->save();
            return true;
        }
        return false;
    }*/
}