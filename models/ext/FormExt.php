<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 12.07.16
 * Time: 8:10
 */

namespace app\models\ext;


use app\components\BaseController;
use app\models\BaseActiveQuery;
use app\models\Form;
use dosamigos\transliterator\TransliteratorHelper;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

class FormExt extends Form
{
    public static $maxSessionCount = 50;

    protected $_sessionNumber = 0;

    protected $_sessionGuid;

    protected $_generateId;

    protected $_values = [];
    protected $_completedStatus = 0;

    protected $_replacementTags = [];
    protected $_replacementTagsValues = [];

    public function getSessionKey()
    {
        return 'session_form_'.($this->tid?$this->tid:$this->id);
    }

    public function getSessionNumber()
    {
        $numKey = 'num';
        $session = Yii::$app->session;
        $session->open();

        if( empty($this->_sessionNumber) ) {
            $key = $this->getSessionKey();
            $arData = $session->get($key);
            if( empty( $arData ) || !isset( $arData[$numKey] ) || $arData[$numKey] > self::$maxSessionCount ) {
                $arData = [$numKey => 0];
            }
            $arData[$numKey] += 1;
            $this->_sessionNumber = $arData[$numKey];
            $session->set($key,$arData);
        }
        return $this->_sessionNumber;
    }

    public function getSessionGuid()
    {
        $number = $this->getSessionNumber();
        if( empty( $this->_sessionGuid ) ) {
            $key = $this->getSessionKey();
            $session = Yii::$app->session;
            $this->_sessionGuid = md5(uniqid());
            $arData = $session->get($key);
            $arData[$number]=$this->_sessionGuid;
            $session->set($key,$arData);
        }
        return $this->_sessionGuid;
    }

    public function getAction()
    {
        return !empty($this->action)
            ? $this->action
            : Yii::$app->urlManager->createUrl(
                [
                    '/frontend/form/send',
                    'id'=>$this->tid?$this->tid:$this->id
                ]
            );
    }

    public function getUrl()
    {
        return Yii::$app->urlManager->createUrl(
            [
                '/frontend/form/view',
                'id'=>$this->tid?$this->tid:$this->id
            ]
        );
    }

    public function generateId()
    {
        if( empty( $this->_generateId ) ) {
            $this->_generateId = 'form_' . ($this->tid ? $this->tid : $this->id) . '_' . $this->getSessionNumber();
        }
        return $this->_generateId;
    }

    public function getReplacementTags()
    {
        if( empty( $this->_replacementTags ) ){
            $this->_replacementTags = [
                '{datetime}'=>[
                    'title' => 'дата и время'
                ],
                '{date}'=>[
                    'title' => 'дата'
                ],
                '{time}'=>[
                    'title' => 'время'
                ],
                '{domain}'=>[
                    'title' => 'имя домена'
                ],
                '{form_id}'=>[
                    'title' => 'идентификатор формы'
                ],
                '{form_name}'=>[
                    'title' => 'название формы',
                ],
                '{admin_phone}'=>[
                    'title'=>'номер телефона куратора',
                ],
                '{admin_email}'=>[
                    'title'=>'эл. адрес куратора',
                ],
            ];
            foreach ($this->formElements as $element) {
                $this->_replacementTags['{'.$element->tid.'}'] = [
                    'title' => 'значение из поля '. $element->name
                ];
            }
        }
        return $this->_replacementTags;
    }
    public function getReplacementTagsValues()
    {
        if( empty( $this->_replacementTagsValues ) ){
            $this->_replacementTagsValues = [
                '{date}'=>$date = date('Y.m.d'),
                '{time}'=>$time = date('H:i:s'),
                '{datetime}'=>$date.' '.$time,
                '{domain}'=>strtr($_SERVER['HTTP_HOST'],['www.'=>'']),
                '{form_id}'=>$this->id,
                '{form_name}'=>$this->name,
                '{admin_phone}'=>$this->phone,
                '{admin_email}'=>$this->email,
            ];
            foreach ($this->formElements as $element) {
                $this->_replacementTagsValues['{'.$element->tid.'}'] = $this->getCompletedValue($element->tid);
            }
        }
        return $this->_replacementTagsValues;
    }

    public function replaceTags( $str )
    {
        return strtr($str,$this->getReplacementTagsValues());
    }

    public function completed( $income )
    {
        $this->_completedStatus = 0;
        $this->_values = [];
        /* @var $elements FormElementExt[] */
        $elements = $this->getFormElements()->andWhere(['=','active',1])->all();
        foreach( $elements as $element ){
            $value = isset($income[$element->tid])?$income[$element->tid]:null;
            switch( $element->type ){
                case FormElementExt::TYPE_LIST:
                    $value = (int)$value;
                    break;
                case FormElementExt::TYPE_INTEGER:
                    $value = (int)$value;
                    break;
                case FormElementExt::TYPE_CHECKBOX:
                    $value = (boolean)$value;
                    break;
                case FormElementExt::TYPE_PASSWORD:
                    $value = (string)$value;
                    break;
                case FormElementExt::TYPE_DATE:
                    $value = (string)strip_tags($value);
                    break;
                case FormElementExt::TYPE_TIME:
                    $value = (string)strip_tags($value);
                    break;
                case FormElementExt::TYPE_DATETIME:
                    $value = (string)strip_tags($value);
                    break;
                case FormElementExt::TYPE_EMAIL:
                    $value = (string)strip_tags($value);
                    if( !preg_match( '/.+@.+\..+/i', $value ) ) {
                        $this->addError($element->tid,Yii::t('app','Wrong email value in field {name}',['{name}'=>$element->name]));
                    }
                    break;
                case FormElementExt::TYPE_PHONE:
                    $value = '+'.preg_replace( '/[^\d]/', '', (string)strip_tags( $value ) );
                    if( !preg_match( '/^((\d|\+\d)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/', $value ) ) {
                        $this->addError($element->tid,Yii::t('app','Wrong phone value in field {name}',['{name}'=>$element->name]));
                    }
                    break;
                case FormElementExt::TYPE_URL:
                    $value = (string)strip_tags($value);
                    break;
                case FormElementExt::TYPE_TEXTEDIT:
                    break;
                case FormElementExt::TYPE_TEXTAREA:
                case FormElementExt::TYPE_INPUT:
                default:
                    $value = (string)strip_tags($value);
                    break;
            }

            if( $element->required && empty( $value ) ) {
                $this->addError($element->tid,Yii::t('app','Feel the field {name}',['{name}'=>$element->name]));
            } else {
                $this->_values[$element->tid] = $value;
            }

        }

        if( !$this->hasErrors() ){
            $completed = new FormCompletedExt();
            $completed->form_id = $this->id;
            $completed->setValues( $this->_values );
            if( !$this->collect_data || $completed->save() ){
                // send notify
                // SMS notify
                $statusAdminPhone = $statusUserPhone = false;
                $statusAdminEmail = $statusUserEmail = false;
                if ( $this->notify_admin_sms && ( $this->phone || ConstantExt::getValueByTid('phone_notify') ) ) {
                    $smsText = $this->replaceTags($this->sms_admin_text);
                    if( $this->transliteration_sms ){
                        $smsText = TransliteratorHelper::process( $smsText );
                    }
                    Yii::$app->smser->clearTo()->to($this->phone?$this->phone:ConstantExt::getValueByTid('phone_notify'))->sms($smsText);
                    $statusAdminPhone = Yii::$app->smser->isOkStatus();
                }
                if ( $this->notify_user_sms && $this->field_phone && $this->getCompletedValue($this->field_phone)!==null ) {
                    $smsText = $this->replaceTags($this->sms_user_text);
                    if( $this->transliteration_sms ){
                        $smsText = TransliteratorHelper::process( $smsText );
                    }
                    Yii::$app->smser->clearTo()->to($this->getCompletedValue($this->field_phone))->sms($smsText);
                    $statusUserPhone = Yii::$app->smser->isOkStatus();
                }

                // e-mail notify
                $context = array(
                    'form'=>$this,
                    'formElements'=>$this->formElements,
                    'completedForm'=>$completed,
                    'signature'=>ConstantExt::getByTid('letter_signature')->value
                );
                $emails = explode(',',strtr( $this->email?$this->email:BaseController::getAdminEmail(), [';'=>',', ' '=>',',"\n"=>''] ) );
                if( $this->notify_admin_email && !empty( $emails ) ) {
                    /* @var $mailer \yii\swiftmailer\Mailer */
                    $messages = [];
                    foreach ($emails as $email) {
                        $messages[] = Yii::$app->mailer->compose($this->template_admin, $context)
                            ->setFrom(BaseController::getNoReplyEmail())
                            ->setSubject($this->replaceTags($this->subject_admin))
                            ->setTo($email);
                    }
                    $statusAdminEmail = Yii::$app->mailer->sendMultiple($messages);
                }

                if( $this->notify_user_email && $this->field_email && $this->getCompletedValue($this->field_email)!==null ){
                    $emails = explode(',',strtr( $this->getCompletedValue($this->field_email), [';'=>',', ' '=>',',"\n"=>''] ) );
                    $statusUserEmail = Yii::$app->mailer->compose($this->template_user, $context)
                        ->setFrom(BaseController::getNoReplyEmail())
                        ->setTo($emails[0])
                        ->setSubject($this->replaceTags($this->subject_user))
                        ->send();
                }

                /*if( !$statusAdminEmail && $statusAdminPhone!==SendSms ) {

                }*/
                return true;
            }
        }
        return false;
    }

    public function getCompletedValue($tid)
    {
        if( isset( $this->_values[$tid] ) ){
            return $this->_values[$tid];
        }
        return null;
    }

    public function getCompletedMessage()
    {
        if( !empty( $this->_values ) && !$this->hasErrors()){
            return 'Спасибо! Заявка отправлена.';
        }
        return 'Ошибка при заполнении формы';
    }

    /**
     * @param string $tmpPath
     * @param array $extType
     * @return array [$messages,$errors]
     */
    public function uploadFilesToTemp( $tmpPath, $extType )
    {
        $messages = array();
        $errors = array();
        $filesUploadErrors = array(
            1 => 'Размер принятого файла превысил максимально допустимый размер',//, который задан директивой upload_max_filesize конфигурационного файла php.ini.',
            2 => 'Размер загружаемого файла превысил значение',// MAX_FILE_SIZE, указанное в HTML-форме',
            3 => 'Загружаемый файл был получен только частично',
            6 => 'Отсутствует временная папка',
            7 => 'Не удалось записать файл на сервере.',
            8 => 'сервер остановил загрузку файла.'
        );
        //$rootPath = ;
        $formIdKey = 'form_' . $this->id;
        if (!isset($_SESSION[$formIdKey])) {
            $_SESSION[$formIdKey] = array();
        }
        mkdir($tmpPath,0775,true);

        $aForm_Fields = $this->getFormElements()->andWhere(['in','type',[FormElementExt::TYPE_FILE,FormElementExt::TYPE_IMAGE]])->all();
        //Проверяем на соответствие заполнения обязательным полям
        foreach ($aForm_Fields as $oForm_Field) {
            if ($oForm_Field->type == 2 && isset($_FILES[$oForm_Field->name])) {
                $aFileData = $_FILES[$oForm_Field->name];
                if ($aFileData['error'] == 0) {
                    $parts = explode('.', $aFileData['name']);
                    $extension = mb_strtolower($parts[count($parts) - 1], 'UTF-8');
                    if (!in_array($extension, $extType)) {
                        $errors[] = 'Формат файла:' . $aFileData['name'] . ' - не поддерживается';
                    } else {
                        /*if (!is_dir($rootPath . $tmpPath)) {
                            mkdir($rootPath . $tmpPath, 0777, true);
                        }*/
                        $uploadFileName = md5_file($aFileData['tmp_name']) . '.' . $extension;
                        $uploadFilePath = $tmpPath . $uploadFileName;
                        //if( !is_file( $uploadFilePath ) ) {
                        if (move_uploaded_file($aFileData['tmp_name'], $uploadFilePath)) {
                            $aNFileData = array();
                            $aNFileData['type'] = $aFileData['type'];
                            $aNFileData['real_name'] = $aFileData['name'];
                            $aNFileData['path'] = $uploadFilePath;
                            $aNFileData['name'] = $uploadFileName;
                            $aNFileData['src'] = $tmpPath . $uploadFileName;
                            $_response['response']['file'] = $aNFileData;
                            $aNFileData['tmp_name'] = $aNFileData['path'];
                            $aNFileData['error'] = $aFileData['error'];
                            $_SESSION[$formIdKey][$oForm_Field->name] = $aNFileData;
                            $_response['status'] = 'success';
                            $messages[] = 'Файл:' . $aFileData['name'] . ' - успешно загружен';
                        } else {
                            $errors[] = 'Не удалось сохранить файл:' . $aFileData['name'];
                        }
                        /*} else {
                            $errors[] = 'Файл: ' . $aFileData['name'] . ' - уже загружен на сервер.';
                        }*/
                    }
                } else {
                    if ($aFileData['error'] != 4) {
                        $errors[] = 'Ошибка: ' . $filesUploadErrors[$aFileData['error']] . ' при загрузке файла:' . $aFileData['name'];
                    }
                    $aFileData = null;
                }
            }
        }
        return array($messages,$errors);
    }

    public static function getUserTemplatesList()
    {
        return array(
            'form-completed-user'=>'Стандартный[пользователю]'
        );
    }

    public static function getAdminTemplatesList()
    {
        return array(
            'form-completed-admin'=>'Стандартный[администратору]'
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFormCompleteds()
    {
        return $this->hasMany(FormCompletedExt::className(), ['form_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFormElements()
    {
        return $this->hasMany(FormElementExt::className(), ['form_id' => 'id']);
    }

    /**
     * @param int $status
     * @return array
     */
    public static function getArOptionList( $status=BaseActiveQuery::ALL )
    {
        $query = self::find();
        if( $status != BaseActiveQuery::ALL ) {
            $query->where(['=','active',(int)$status]);
        }
        return ArrayHelper::map($query->all(),'id','name');
    }

    /**
     * @return false|int
     * @throws \Exception
     */
    public function delete()
    {
        $arElements = $this->formElements;
        if( !empty($arElements) ) {
            foreach ( $arElements as $oElement ) {
                $oElement->delete();
            }
        }
        $arCompleteds = $this->formCompleteds;
        if( !empty($arCompleteds) ) {
            foreach ( $arCompleteds as $oCompleted ) {
                $oCompleted->delete();
            }
        }
        return parent::delete();
    }
}