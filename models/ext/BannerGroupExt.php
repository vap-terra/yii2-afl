<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 14.03.16
 * Time: 6:59
 */

namespace app\models\ext;


use app\models\BannerGroup;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;


class BannerGroupExt extends BannerGroup
{
    const INACTIVE = 0;
    const ACTIVE = 1;
    const ALL = 2;

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBanners()
    {
        return $this->hasMany(BannerExt::className(), ['banner_group_id' => 'id']);
    }

    /**
     * @return string
     */
    public function getHref( )
    {
        return Yii::$app->storage->getHref( $this );
    }

    /**
     * @return string
     */
    public function getPath( )
    {
        return Yii::$app->storage->getPath( $this );
    }

    /**
     * @param int $status
     * @return array
     */
    public static function getArOptionList( $status=self::ALL )
    {
        $query = self::find();
        /*if( $status != self::ALL ) {
            $query->where(['=','active',(int)$status]);
        }*/
        return ArrayHelper::map($query->all(),'id','name');
    }
}