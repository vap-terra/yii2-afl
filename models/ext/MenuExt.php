<?php

namespace app\models\ext;


use app\models\BaseActiveQuery;
use app\models\Menu;
use app\models\MenuItem;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\BaseActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

//TODO make menu as nested tree
//TODO in menu item add recursive use layout
//TODO in menu item add recursive use template
//TODO in menu item add recursive use sub head
class MenuExt extends Menu
{
    const CACHE_DURATION = 86400;//1 * 24 * 60 * 60;

    /**
     * @var $currentItem MenuItemExt
     */
    public static $currentItem;

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuItems()
    {
        return $this->hasMany(MenuItemExt::className(), ['menu_id' => 'id']);
    }

    /**
     * @param string $tid
     * @param int $parentMenuItemId
     * @param int $level
     * @param int $status
     * @return string
     */
    public static function getItemsCacheKeyName($tid, $parentMenuItemId=-1, $level = 0, $status = BaseActiveQuery::ACTIVE)
    {
        return 'menu_items_' . $tid . '_'.(string)$parentMenuItemId. '_' . (string)$level . '_' . (string)$status . (!empty(self::$currentItem) ? ('_' . self::$currentItem->id) : '');
    }

    /**
     * @param $item \app\models\BaseActiveRecord
     * @param bool $asRule
     * @return string
     */
    public static function getUrlForItem( $item, $asRule=false )
    {
        $url = '';
        $oMenuItem = self::getMenuItemForItem( $item );
        if( $oMenuItem ) {
            $url = $oMenuItem->getUrl();
        } else if( $asRule ) {
            // TODO create url for item from urlManager rules
        }
        return $url;
    }

    /**
     * @param $item \app\models\BaseActiveRecord
     * @return array|null|MenuItemExt
     */
    public static function getMenuItemForItem( $item )
    {
        /* @var $oRelation MenuItemRelationExt */
        $name = $item->getBaseName();
        $pk = is_array($item->primaryKey)?$item->primaryKey[0]:$item->primaryKey;
        $oRelation = MenuItemRelationExt::find()
            ->where(['=','item_id',$pk])
            ->andWhere(['=','type',$name])
            ->andWhere(['=','main',1])
            ->one();
        return !empty($oRelation)?$oRelation->menuItem:null;
    }

    /**
     * @return string
     */
    public function getHref( )
    {
        return Yii::$app->storage->getHref($this);
    }

    /**
     * @return string
     */
    public function getPath( )
    {
        return Yii::$app->storage->getPath($this);
    }

    /**
     * @param string $id
     * @return array|MenuItemExt[]
     */
    public static function getModuleMenuItems( $id = '' )
    {
        $qMenuItem = MenuItemExt::find();
        $qMenuItem->leftJoin('menu_item_relation','menu_item_relation.menu_item_id=menu_item.id');
        $qMenuItem->where(['not in','menu_item_relation.type',[MenuItemRelationExt::TYPE_CONTENT,MenuItemRelationExt::TYPE_WIDGET]]);
        $qMenuItem->andWhere(['=','menu_item_relation.main',1]);
        if( !empty( $id ) ){
            if( preg_match( '/^[\d]+$/', $id ) ) {
                $qMenuItem->andWhere(['menu_item.id'=>$id]);
            } else {
                $qMenuItem->andWhere(['menu_item.tid'=>$id]);
            }
        }
        return ( !empty( $id ) ) ? $qMenuItem->one() : $qMenuItem->all();
    }

    /**
     * @param string $path
     * @param null | array &$arUnsortedPath - not to dismantle part of the menu items
     * @return array|MenuItemExt|null|ActiveRecord
     * @throws \yii\web\NotFoundHttpException
     */
    public static function getByPath( $path, &$arUnsortedPath = null )
    {
        if( empty($path) || $path == '/' ) {
            $path = Yii::$app->params['menu_item_index_tid'];
        }
        if( strpos( $path, '/' )===0 ) {
            $path = substr( $path, 1);
        }
        if( strrpos( $path, '/' ) === strlen( $path ) - 1 ) {
            $path = substr( $path, 0, strlen( $path ) - 1 );
        }
        if( strlen( $path ) ) {
            $ids = explode('/', $path);
        } else {
            $ids = ['/'];
        }

        $arUnsortedPath = [];
        $arOMenuItems = MenuExt::getModuleMenuItems( );
        $oMenuItem = null;
        $isFound = false;

        foreach( $arOMenuItems as $_oItemMenu ) {
            foreach( $ids as $id ) {
                if( $isFound ) {
                    $arUnsortedPath[] = $id;
                } else if( !$isFound && ( $id == $_oItemMenu->id || $id == $_oItemMenu->tid ) ) {
                    $oMenuItem = $_oItemMenu;
                    $isFound = true;
                }
            }
            if( $isFound ) {
                break;
            }
        }

        if( empty( $oMenuItem ) ) {
            $oMenuItem = MenuItemExt::getByTid( $ids[ count($ids)-1 ] );
        }

        return $oMenuItem;
    }

    /**
     * @param int|string $id
     * @return MenuExt
     * @throws \yii\web\NotFoundHttpException
     */
    public static function getByTid($id)
    {
        /* @var $qMenu ActiveQuery */
        $qMenu = self::find();

        if( preg_match( '/^[\d]+$/', $id ) ) {
            $qMenu->where(['=','id',$id]);
        } else {
            $qMenu->where(['=','tid',$id]);
        }

        /* @var $oMenu /common/models/ext/MenuExt */
        $oMenu = $qMenu->one();
        if( !$oMenu ) {
            throw new NotFoundHttpException( Yii::t('app/app', '[404] Not Found menu {id}', ['id'=>$id] ) );
        }

        return $oMenu;
    }

    /**
     * @param int|string|MenuItemExt $id
     * @return MenuItemExt
     * @throws \yii\web\NotFoundHttpException
     */
    public static function getItemByTid($id)
    {
        $oMenuItem = $id;

        if (
            $oMenuItem instanceof MenuItemExt == false
            &&
            $oMenuItem instanceof MenuItem == false
        ) {
            $oMenuItem = MenuItemExt::getByTid($id);
        }
        return $oMenuItem;
    }

    /**
     * @param int|string|MenuItemExt $id
     * @return bool
     * @throws \yii\web\NotFoundHttpException
     */
    public static function isActiveMenuItem($id)
    {
        /* @var $oMenuItem MenuItemExt */
        $oMenuItem = self::getItemByTid($id);
        if (!empty(self::$currentItem)) {
            if ( self::$currentItem->id == $oMenuItem->id ) {
                return true;
            } else if ( self::$currentItem->menu_item_id == $oMenuItem->id ) {
                return true;
            }
            $arOMenuItems = self::$currentItem->parents;
            foreach( $arOMenuItems as $oItem ) {
                if( $oItem->id == $oMenuItem->id ) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @param string $tid
     * @param int $parentMenuItemId
     * @param int $level
     * @param int $status
     * @return array|mixed
     */
    public static function getArMenuNavItems($tid, $parentMenuItemId=0, $level = 0, $status = BaseActiveQuery::ACTIVE)
    {
        /* @var $oMenuItem MenuItemExt */
        /* @var $mMenu MenuExt */
        /* @var $oMenu MenuExt */
        //$cacheKey = self::getItemsCacheKeyName($tid, $parentMenuItemId, $level, $status);

        //Yii::$app->cache->delete($cacheKey);

        //$items = Yii::$app->cache->get($cacheKey);
        //if (empty($items)) {
            $items = [];

            $oMenu = self::getMenu($tid, $status)->one();

            if (!empty($oMenu)) {
                $qMenuItems = $oMenu->getMenuItems();
                if ($status !== BaseActiveQuery::ALL && $parentMenuItemId > 0) {
                    $qMenuItems->andWhere(['=', 'active', ($status == BaseActiveQuery::ACTIVE) ? 1 : 0]);
                }
                if( $level > 0 ) {
                    $qMenuItems->andWhere(['<=', 'depth', $level]);
                }
                if( $parentMenuItemId > 0 ) {
                    $qMenuItems->andWhere(['=', 'menu_item_id', $parentMenuItemId]);
                } else {
                    $qMenuItems->andWhere(['menu_item_id'=>null]);
                }

                $arOMenuItems = $qMenuItems->all();

                if( !empty( $arOMenuItems ) && $parentMenuItemId <= 0 ){
                    $qDescendants = $arOMenuItems[0]->getChildren($arOMenuItems[0]->depth+1);
                    if ($status !== BaseActiveQuery::ALL ) {
                        $qDescendants->andWhere(['=', 'active', ($status == BaseActiveQuery::ACTIVE) ? 1 : 0]);
                    }
                    $arOMenuItems = $qDescendants->all();
                }

                if( !empty( $arOMenuItems ) ) {
                    foreach ($arOMenuItems as $oMenuItem) {
                        $items[] = $oMenuItem->getArMenuNavItems($level,$status);
                    }
                }
            }
            //Yii::$app->cache->set($cacheKey, $items, self::CACHE_DURATION);
        //}
        return $items;
    }

    /**
     * @param int|string|MenuItemExt $id
     * @param $tid
     * @param bool $lastLink
     * @return array
     */
    public static function getBreadCrumbs($id, $tid, $lastLink=false)
    {
        $items = [];
        $oMenuItem = self::getItemByTid($id);
        if ($oMenuItem->tid !== Yii::$app->params['menu_item_index_tid']) {
            $arOMenuItems = array_merge( $oMenuItem->getParents()->andWhere(['is not','menu_item_id',null])->all(), [$oMenuItem] );
            $count = count( $arOMenuItems );
            foreach( $arOMenuItems as $num => $oItem ) {
                /* @var $oItem MenuItemExt */
                $item = [];
                $item['label'] = !empty($oItem->header) ? $oItem->header : $oItem->name;
                if ( $num < $count - 1 || ( $num == $count - 1 && $lastLink ) ) {
                    $item['url'] = $oItem->getUrl();
                }
                $items[] = $item;
            }
        }
        return $items;
    }

    /**
     * @param $tid
     * @param int $status
     * @return $this
     */
    public static function getMenu($tid, $status = BaseActiveQuery::ACTIVE)
    {
        $query = self::find()
            ->where(['=', 'tid', $tid]);
        if ($status !== BaseActiveQuery::ALL) {
            $query->andWhere(['=', 'active', ($status == BaseActiveQuery::ACTIVE) ? 1 : 0]);
        }
        return $query;
    }

    /**
     * @param int|string|MenuItemExt $id
     * @param string $defaultLayout
     * @return string
     */
    public static function getMenuItemLayout($id, $defaultLayout = 'main')
    {
        /* @var $oMenuItem MenuItemExt */
        $oMenuItem = self::getItemByTid($id);
        $layout = $defaultLayout;
        if (!empty($oMenuItem->layout)) {
            $layout = $oMenuItem->layout;
        } else if (!empty(Yii::$app->params['default_layout'])) {
            $layout = Yii::$app->params['default_layout'];
        }
        return $layout;
    }

    /**
     * @param int|string|MenuItemExt $id
     * @param string $defaultView
     * @return string
     */
    public static function getMenuItemTemplate($id, $defaultView = 'index')
    {
        /* @var $oMenuItem MenuItemExt */
        $oMenuItem = self::getItemByTid($id);
        $viewName = $defaultView;
        if ($oMenuItem->tid !== Yii::$app->params['menu_item_index_tid']
            &&
            $oMenuItem->id !== Yii::$app->params['menu_item_index_tid']
        ) {
            $viewName = !empty(Yii::$app->params['default_template']) ? Yii::$app->params['default_template'] : 'page';
            if (!empty($oMenuItem->template)) {
                $viewName = $oMenuItem->template;
            }
        }
        return $viewName;
    }

    /**
     * @param int|string|MenuItemExt $id
     * @param int $status
     * @return \string[]
     */
    public static function getMenuItemStrContents($id, $status = BaseActiveQuery::ACTIVE)
    {
        /* @var $oMenuItem MenuItemExt */
        /* @var $arStrContents string[] */
        //$arStrContents = [];
        $oMenuItem = self::getItemByTid($id);
        return $oMenuItem->getStrContents( SORT_ASC, $status );

    }

    public static function getArOptionList($status = BaseActiveQuery::ACTIVE)
    {
        /* @var $qMenu ActiveQuery */
        /* @var $arMenu MenuExt[] */

        $qMenu = MenuExt::find();
        if( $status !== BaseActiveQuery::ALL ) {
            $qMenu->andWhere(['=','active', ($status == BaseActiveQuery::ACTIVE) ? 1 : 0]);
        }
        $arMenu = $qMenu->all();

        if( empty( $arMenu ) ) return [];

        return ArrayHelper::map($arMenu,'id','title');
    }

    public static function getArOptionListOptgroup($status = BaseActiveQuery::ACTIVE)
    {
        /* @var $qMenu ActiveQuery */
        /* @var $oMenu MenuExt */
        /* @var $arMenu MenuExt[] */

        $qMenu = MenuExt::find();
        if( $status !== BaseActiveQuery::ALL ) {
            $qMenu->andWhere(['=','active', ($status == BaseActiveQuery::ACTIVE) ? 1 : 0]);
        }
        $arMenu = $qMenu->all();

        if( empty( $arMenu ) ) return [];
        $items = [];
        foreach( $arMenu as $oMenu ) {
            $items[$oMenu->id] = [ 'label' => $oMenu->title ];
        }
        return $items;
    }

    /**
     * @param int|string $tid
     * @param int $parentMenuItemId
     * @param int $status
     * @return array
     * @throws NotFoundHttpException
     */
    public static function getArItemsOptionList( $tid, $parentMenuItemId = -1, $status = BaseActiveQuery::ACTIVE )
    {
        /* @var $qMenuItems ActiveQuery */
        /* @var $oMenu MenuExt */
        /* @var $oMenuItem MenuItemExt */
        $oMenu = self::getByTid($tid);
        $qMenuItems = $oMenu->getMenuItems();
        if( $parentMenuItemId > 0 ) {
            $qMenuItems->andWhere(['=', 'menu_item_id', $parentMenuItemId]);
        } else {
            $qMenuItems->andWhere(['menu_item_id' => null]);
        }
        if ($status !== BaseActiveQuery::ALL) {
            $qMenuItems->where(['=', 'active', ($status == BaseActiveQuery::ACTIVE) ? 1 : 0]);
        }
        $items = [];
        $arOMenuItems = $qMenuItems->all();
        if( !empty( $arOMenuItems ) ) {
            foreach ($arOMenuItems as $oMenuItem) {
                $items[$oMenuItem->id] = ($oMenuItem->depth>0?str_repeat("-",$oMenuItem->depth*2):'').' '.$oMenuItem->name;
                $arSubMenuItems = $oMenuItem->descendants;
                if( !empty( $arSubMenuItems ) ) {
                    foreach ( $arSubMenuItems as $oSubMenuItems ) {
                        $items[$oSubMenuItems->id] = ($oSubMenuItems->depth>0?str_repeat("-",$oSubMenuItems->depth*2):'').' '.$oSubMenuItems->name;
                    }
                }
            }
        }
        return $items;
    }

    /**
     * @param int $parentMenuItemId
     * @param int $status
     * @return array
     * @throws NotFoundHttpException
     */
    public static function getArItemsOptionListGroupByMenu( $parentMenuItemId = -1, $status = BaseActiveQuery::ACTIVE )
    {
        /* @var $qMenuItems ActiveQuery */
        /* @var $oMenu MenuExt */
        /* @var $oMenuItem MenuItemExt */
        $items = [];

        $qMenu = MenuExt::find();
        if( $status !== BaseActiveQuery::ALL ) {
            $qMenu->andWhere(['=','active', ($status == BaseActiveQuery::ACTIVE) ? 1 : 0]);
        }
        $arMenu = $qMenu->all();

        if( empty( $arMenu ) ) return [];
        foreach( $arMenu as $oMenu ) {
            $qMenuItems = $oMenu->getMenuItems();
            if( $parentMenuItemId > 0 ) {
                $qMenuItems->andWhere(['=', 'menu_item_id', $parentMenuItemId]);
            } else {
                $qMenuItems->andWhere(['menu_item_id' => null]);
            }
            if ($status !== BaseActiveQuery::ALL) {
                $qMenuItems->where(['=', 'active', ($status == BaseActiveQuery::ACTIVE) ? 1 : 0]);
            }
            $groupItems = [];
            $arOMenuItems = $qMenuItems->all();
            if( !empty( $arOMenuItems ) ) {
                foreach ($arOMenuItems as $oMenuItem) {
                    $groupItems[$oMenuItem->id] = ($oMenuItem->depth>0?str_repeat("-",$oMenuItem->depth*2):'').' '.$oMenuItem->name;
                    $arSubMenuItems = $oMenuItem->descendants;
                    if( !empty( $arSubMenuItems ) ) {
                        foreach ( $arSubMenuItems as $oSubMenuItems ) {
                            $groupItems[$oSubMenuItems->id] = ($oSubMenuItems->depth>0?str_repeat("-",$oSubMenuItems->depth*2):'').' '.$oSubMenuItems->name;
                        }
                    }
                }
            }
            $items[$oMenu->id] = $groupItems;
        }

        return $items;
    }
}