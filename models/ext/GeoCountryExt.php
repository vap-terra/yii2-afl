<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 04.02.16
 * Time: 14:35
 */

namespace app\models\ext;


use app\models\BaseActiveQuery;
use app\models\GeoCountry;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\data\Sort;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class GeoCountryExt extends GeoCountry
{

    /**
     * @param int $status
     * @return array
     */
    public static function getArOptionList( $status=BaseActiveQuery::ALL )
    {
        $query = self::find();
        /*if( $status != BaseActiveQuery::ALL ) {
            $query->where(['=','active',(int)$status]);
        }*/
        $items = $query->all();
        if( empty( $items ) ) return [];
        return ArrayHelper::map($query->all(),'id','name');
    }

    public function generateDataFilter()
    {

    }

    /**
     * @return Sort
     */
    public static function generateDataSort()
    {
        return new Sort(
            [
                //'route' => $route,
                'defaultOrder' => [
                    'name' => SORT_ASC,
                ],
                'enableMultiSort' => true,
                'attributes' => [
                    'id' => [
                        'default' => SORT_ASC,
                    ],
                    'name' => [
                        'default' => SORT_ASC,
                    ],
                    'tid' => [
                        'default' => SORT_ASC,
                    ],
                    'tid2' => [
                        'default' => SORT_ASC,
                    ],
                    'iso' => [
                        'default' => SORT_ASC,
                    ],
                    'phone_code' => [
                        'default' => SORT_ASC,
                    ],
                ],
            ]
        );
    }

    /**
     * @param $query BaseActiveQuery
     * @return Pagination
     */
    public static function generateDataPagination( $query )
    {
        $countQuery = clone $query;
        return new Pagination(
            [
                'totalCount' => $countQuery->count(),
                'pageSize' => 30,
                'pageParam' => 'page',
            ]
        );
    }

    /**
     * @param $query BaseActiveQuery
     * @return ActiveDataProvider
     */
    public static function generateDataProvider( $query )
    {
        return new ActiveDataProvider(
            [
                'query' => self::find(),
                'pagination' => self::generateDataPagination( $query ),
                'sort' => self::generateDataSort(),
            ]
        );
    }

    /**
     * @return array
     */
    public static function generateGridViewColumns()
    {
        return [
            ['class'=>'yii\grid\SerialColumn'],
            ['class'=>'yii\grid\CheckboxColumn'],
            'id',
            'name',
            'tid',
            'tid2',
            'iso',
            'phone_code',
            'regions'=>[
                'format'=>'html',
                'value'=>function($model){
                    /* @var $model GeoCountryExt */
                    $url = Yii::$app->urlManager->createUrl(['/backend/geo-region/index', 'geo_country_id' => $model->id]);
                    return Html::a('<span class="glyphicon glyphicon-link"></span>'.$model->getGeoRegions()->count(), $url, []);
                }
            ],
            [
                'class'=>'yii\grid\ActionColumn',
                'headerOptions' => ['width' => '160'],
                'template' => '{edit} {delete}',
                'buttons' => [
                    'edit' => function ($url, $model, $key) {
                        $url = Yii::$app->urlManager->createUrl(['/backend/geo-country/edit','id'=>$model->id]);
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>',$url,[]);
                    },
                    'delete' => function ($url, $model, $key) {
                        $url = Yii::$app->urlManager->createUrl(['/backend/geo-country/delete','id'=>$model->id]);
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>',$url,[]);
                    },
                ],
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGeoRegions()
    {
        return $this->hasMany(GeoRegionExt::className(), ['geo_country_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGeoRegionAreas()
    {
        return $this->hasMany(GeoRegionAreaExt::className(), ['geo_country_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGeoLocalities()
    {
        return $this->hasMany(GeoLocalityExt::className(), ['geo_country_id' => 'id']);
    }
}