<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 28.01.16
 * Time: 13:20
 */

namespace app\models\ext;


use app\models\MenuItemBlock;

class MenuItemBlockExt extends MenuItemBlock
{
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuItem()
    {
        return $this->hasOne(MenuItemExt::className(), ['id' => 'menu_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlock()
    {
        return $this->hasOne(BlockExt::className(), ['id' => 'block_id']);
    }
}