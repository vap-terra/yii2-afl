<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 28.01.16
 * Time: 17:32
 */

namespace app\models\ext;


use app\models\BaseActiveQuery;
use app\models\InformationGroup;
use paulzi\adjacencyList\AdjacencyListBehavior;
use paulzi\autotree\AutoTreeTrait;
use paulzi\nestedsets\NestedSetsBehavior;
use paulzi\sortable\SortableBehavior;
use Yii;
use yii\base\InvalidParamException;
use yii\db\ActiveQuery;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

class InformationGroupExt extends InformationGroup
{

    use AutoTreeTrait;

    public function behaviors()
    {
        return [
            [
                'class' => NestedSetsBehavior::className(),
                'treeAttribute' => 'information_id',
                'leftAttribute' => 'lb',
                'rightAttribute' => 'rb',
            ],
            [
                'class' => AdjacencyListBehavior::className(),
                'parentAttribute' => 'information_group_id',
                'sortable'=>[
                    'class' => SortableBehavior::className(),
                    'query' => ['information_group_id'],
                    'sortAttribute' => 'position',
                    'step' => 10,
                ],
            ],
            'slug' => [
                'class' => 'app\behaviors\Transliteration',
                'in_attribute' => 'name',
                'out_attribute' => 'tid',
                'translit' => true
            ],
        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    public function beforeSave($insert)
    {
        if( parent::beforeSave($insert) ) {
            $image = $this->getOldAttribute('image');
            if (empty($this->image) && !empty($image)) {
                $this->image = $image;
            }
            return true;
        }
        return false;
    }

    /**
     * @param int|string $toId
     * @param int|string $afterId
     * @return bool
     */
    public function relocation( $toId=0, $afterId=0 )
    {
        $parentItem = null;
        if( $toId ) {
            $parentItem = self::findOne($toId);
        }

        $afterItem = null;
        if( $afterId ) {
            $afterItem = self::findOne($afterId);
        }
        if( $afterItem ) {
            //$this->information_group_id = $afterItem->information_group_id;
            //$this->depth = $afterItem->depth;
            $this->insertAfter($afterItem);
        } else if( $parentItem ){
            //$this->information_group_id = $parentItem->id;
            //$this->depth = $parentItem->depth + 1;
            $this->prependTo($parentItem);
        } else {
            $root = self::findOne(['information_id'=>$this->information_id,'depth'=>1,'information_group_id'=>null]);
            if( $root ) {
                //$this->information_group_id = $root->id;
                //$this->depth = $root->depth + 1;
                $this->prependTo($root);
            }
        }

        /*var_dump( $this );
        die();*/

        return $this->save();
    }

    public function delete()
    {
        /*$items = $this->getInformationItems()->all();
        if( !empty( $items ) ) {
            for ($i = 0, $cnt = count($items); $i < $cnt; $i++) {
                $items[$i]->delete();
            }
        }*/
        if( $this->informationItems ){
            $this->unlinkAll('informationItems',true);
        }
        if( $this->informationItemGroupProperties ){
            $this->unlinkAll('informationItemGroupProperties',true);
        }

        $path = $this->getPath();
        if( $this->image && is_file($path . $this->image)) {
            unlink($path . $this->image);
        }

        foreach( $this->children as $child ){
            $child->delete();
        }
        return parent::delete();
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformation()
    {
        return $this->hasOne(InformationExt::className(), ['id' => 'information_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformationGroup()
    {
        return $this->hasOne(InformationGroupExt::className(), ['id' => 'information_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformationGroups()
    {
        return $this->hasMany(InformationGroupExt::className(), ['information_group_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformationItems()
    {
        return $this->hasMany(InformationItemExt::className(), ['information_group_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformationItemGroupProperties()
    {
        return $this->hasMany(InformationItemGroupPropertyExt::className(), ['information_group_id' => 'id']);
    }

    /**
     * @param array $includeIds
     * @param array $excludeIds
     * @return \yii\db\ActiveQuery
     */
    public function getInformationProperties( $includeIds = [], $excludeIds = [] )
    {
        $query = $this->information->getInformationProperties();

        $query->orWhere(['=',InformationItemGroupPropertyExt::tableName().'.information_group_id',$this->id]);
        $parents = $this->parents;
        if( !empty( $parents ) ) {
            foreach( $parents as $oInformationGroup ) {
                $query->orWhere(['=',InformationItemGroupPropertyExt::tableName().'.information_group_id',$oInformationGroup->id]);
                $query->andWhere(['=',InformationItemGroupPropertyExt::tableName().'.recursive',1]);
            }
        }

        $query->leftJoin(InformationItemGroupPropertyExt::tableName(),InformationItemGroupPropertyExt::tableName().'.information_property_id'.'='.InformationPropertyExt::tableName().'.id');

        if( !empty( $includeIds ) ) {
            $query->andWhere(['IN',InformationPropertyExt::tableName().'.id',$includeIds]);
        }
        if( !empty( $excludeIds ) ) {
            $query->andWhere(['NOT IN',InformationPropertyExt::tableName().'.id',$excludeIds]);
        }
        return $query;
    }
    
    /**
     * @return string
     */
    public function getUrl()
    {
        /* @var $oInformationGroup InformationGroupExt*/
        /* @var $oInformation InformationExt*/
        $url = $this->tid ? $this->tid : $this->id ;
        if( $this->information_group_id > 0  ) {
            //TODO make get list of parents and concat parents urls
            $oInformationGroup = $this->getInformationGroup()->one();
            return $oInformationGroup->getUrl() . '/' . $url;
        } else if( $this->information_id ) {
            $oInformation = $this->getInformation()->one();
            return $oInformation->getUrl();
        }
        throw new InvalidParamException('For Information system item not set information group or information system');
    }

    /**
     * @return string
     */
    public function getHref( )
    {
        /* @var $oInformation InformationExt*/
        if( $this->information_id ) {
            $oInformation = $this->getInformation()->one();
            return $oInformation->getHref($this) .'group_' . $this->id . '/';
        }
        throw new InvalidParamException('For Information group not set Information');
    }

    /**
     * @return string
     */
    public function getPath( )
    {
        /* @var $oInformation InformationExt*/
        if( $this->information_id ) {
            $oInformation = $this->getInformation()->one();
            return $oInformation->getPath($this) . 'group_' . $this->id . '/';
        }
        throw new InvalidParamException('For Information group not set Information');
    }

    /**
     * @return string
     */
    public function getImageHref()
    {
        return $this->getHref() . $this->image;
    }

    /**
     * @return string
     */
    public function getImagePath()
    {
        return $this->getPath() . $this->image;
    }

    /**
     * @param $extension
     * @return string
     */
    public function generateImageFileName( $extension )
    {
        return 'photo_'.md5($this->id).'.' . $extension;
    }

    /**
     *
     */
    public function createDir()
    {
        $dir = $this->getPath();
        if( !is_dir( $dir ) ) {
            mkdir($dir, 0775, true);
        }
    }

    /**
     * @param UploadedFile $image
     * @param bool $upload
     * @param bool $deleteTempFile
     * @return bool
     */
    public function uploadImage($image, $upload=true, $deleteTempFile = true )
    {
        if( $image instanceof UploadedFile === false ) return false;
        $fileName = $this->generateImageFileName($image->extension);
        $filePath = $this->getPath() . $fileName;
        $this->createDir();
        if( $upload ) {
            $status = $image->saveAs( $filePath, $deleteTempFile  );
        } else {
            $status = copy( $image->tempName, $filePath );
            if( $status && $deleteTempFile ) {
                unlink($image->tempName);
            }
        }
        if ( $status ) {
            $this->image = $fileName;
            $this->save();
            return true;
        }
        return false;
    }

    /**
     * @param int | string $id
     * @param null $oInformationParentItem
     * @param null $oInformation
     * @return array|null|\yii\db\ActiveRecord
     * @throws NotFoundHttpException
     */
    public static function getByTid( $id, $oInformationParentItem = null, $oInformation=null )
    {

        /* @var $qInformationGroup ActiveQuery */
        $qInformationGroup = self::find();

        if( preg_match( '/^[\d]+$/', $id ) ) {
            $qInformationGroup->where(['=','id',$id]);
        } else {
            $qInformationGroup->where(['=','tid',$id]);
        }

        if( $oInformationParentItem ) {
            $qInformationGroup->andWhere(['=','information_group_id',$oInformationParentItem->id]);
        }

        if( $oInformation ) {
            $qInformationGroup->andWhere(['=','information_id',$oInformation->id]);
        }

        /* @var $oInformationGroup InformationGroupExt */
        $oInformationGroup = $qInformationGroup->one();
        if( empty( $oInformationGroup ) ) {
            throw new NotFoundHttpException( Yii::t('app/app', '[404] Not Found information group {id}',[ 'id'=>$id ] ) );
        }

        return $oInformationGroup;
    }

    /**
     * @param bool $active
     * @return array
     */
    public function getArMenuNavItem( $active = false )
    {
        return [
            'id' => $this->id,
            'label' => $this->name,
            'url' => $this->getUrl(),
            'active' => $active,
            'visible' => true,
            'items' => null,
        ];
    }

    /**
     * @param int $informationId
     * @param int $parentInformationGroupId
     * @param int $status
     * @return array
     */
    public static function getArOptionList($informationId, $parentInformationGroupId = -1, $status = BaseActiveQuery::ALL )
    {
        /* @var $qInformationGroups ActiveQuery */
        /* @var $oInformation InformationExt */
        /* @var $arInformationGroups InformationGroupExt[] */
        /* @var $oInformationGroup InformationGroupExt */
        //$oInformation = InformationExt::getBytId($informationId);
        $oInformation = InformationExt::findOne($informationId);
        if( empty($oInformation) ) return [];
        $qInformationGroups = $oInformation->getInformationGroups();
        if( $parentInformationGroupId > -1 ) {
            $qInformationGroups->andWhere(['=', 'information_group_id', $parentInformationGroupId]);
        } else {
            $qInformationGroups->andWhere(['is', 'information_group_id', null]);
        }
        if ($status !== BaseActiveQuery::ALL) {
            $qInformationGroups->where(['=', 'active', ($status == BaseActiveQuery::ACTIVE) ? 1 : 0]);
        }
        $items = [];
        $arInformationGroups = $qInformationGroups->all();
        if( !empty( $arInformationGroups ) ) {
            foreach ($arInformationGroups as $oInformationGroup) {
                $items[$oInformationGroup->id] = ($oInformationGroup->depth>0?(str_repeat("-",$oInformationGroup->depth*2).' '):'').$oInformationGroup->name;
                $arSubInformationGroups = $oInformationGroup->descendants;
                if( !empty( $arSubInformationGroups ) ) {
                    foreach ( $arSubInformationGroups as $oSubInformationGroup ) {
                        $items[$oSubInformationGroup->id] = str_repeat("-",$oSubInformationGroup->depth*2).' '.$oSubInformationGroup->name;
                    }
                }
            }
        }
        return $items;
    }


    /**
     * @param int $parentInformationGroupId
     * @param int $status
     * @return array
     */
    public static function getArOptionListGroupByInformation( $parentInformationGroupId = -1, $status = BaseActiveQuery::ALL )
    {
        /* @var $qInformationGroups ActiveQuery */
        /* @var $oInformation InformationExt */
        /* @var $arInformations InformationExt[] */
        /* @var $arInformationGroups InformationGroupExt[] */
        /* @var $oInformationGroup InformationGroupExt */

        $qInformation = InformationExt::find();
        if ($status !== BaseActiveQuery::ALL) {
            $qInformation->where(['=', 'active', ($status == BaseActiveQuery::ACTIVE) ? 1 : 0]);
        }
        $arInformations = $qInformation->all();
        if( empty( $arInformations ) ) {
            return [];
        }

        $items = [];
        foreach( $arInformations as $oInformation ) {

            $qInformationGroups = $oInformation->getInformationGroups();
            if ($parentInformationGroupId > -1) {
                $qInformationGroups->andWhere(['=', 'information_group_id', $parentInformationGroupId]);
            } else {
                $qInformationGroups->andWhere(['is', 'information_group_id', null]);
            }
            if ($status !== BaseActiveQuery::ALL) {
                $qInformationGroups->where(['=', 'active', ($status == BaseActiveQuery::ACTIVE) ? 1 : 0]);
            }
            $groups = [];
            $arInformationGroups = $qInformationGroups->all();
            if (!empty($arInformationGroups)) {
                foreach ($arInformationGroups as $oInformationGroup) {
                    $groups[$oInformationGroup->id] = ($oInformationGroup->depth>0?(str_repeat("-",($oInformationGroup->depth-1)*2).' '):'').$oInformationGroup->name;
                    $arSubInformationGroups = $oInformationGroup->descendants;
                    if (!empty($arSubInformationGroups)) {
                        foreach ($arSubInformationGroups as $oSubInformationGroup) {
                            $groups[$oSubInformationGroup->id] = str_repeat("-", $oSubInformationGroup->depth * 2) . ' ' . $oSubInformationGroup->name;
                        }
                    }
                }
            }
            $items[$oInformation->id] = $groups;
        }

        return $items;
    }

    /**
     * @param int $afterNumNotLink
     * @param bool $lastLink
     * @return array
     */
    public function getBreadCrumbs( $afterNumNotLink=0, $lastLink=false )
    {
        /* @var $oInformationGroup InformationGroupExt */
        $items = $this->information->getBreadCrumbs();
        $arGroups = array_reverse( array_merge( [$this], $this->parents ) );
        $count = count( $arGroups );
        foreach( $arGroups as $num => $oInformationGroup ) {
            if( $oInformationGroup->information_group_id > 0 ) {
                $item = [];
                $item['label'] = !empty($oInformationGroup->header) ? $oInformationGroup->header : $oInformationGroup->name;
                if (($num == $count - 1 && !$afterNumNotLink && $lastLink)
                    || ($afterNumNotLink && $num < $count - 1 && $num < $afterNumNotLink)
                    || (!$afterNumNotLink && ($num < $count - 1 || ($lastLink && $num == $count - 1)))
                ) {
                    $item['url'] = $oInformationGroup->getUrl();
                }
                $items[] = $item;
            }
        }
        return $items;
    }
}