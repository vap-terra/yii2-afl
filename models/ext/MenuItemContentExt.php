<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 28.01.16
 * Time: 13:21
 */

namespace app\models\ext;


use app\models\MenuItemContent;

class MenuItemContentExt extends MenuItemContent
{
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuItem()
    {
        return $this->hasOne(MenuItemExt::className(), ['id' => 'menu_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContent()
    {
        return $this->hasOne(ContentExt::className(), ['id' => 'content_id']);
    }
}