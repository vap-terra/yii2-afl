<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 28.01.16
 * Time: 13:19
 */

namespace app\models\ext;


use app\models\BaseActiveQuery;
use app\models\Content;
use yii\helpers\ArrayHelper;

class ContentExt extends Content
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge_recursive(
            parent::rules(),
            [
                [['name'], 'required', 'on' => ['update', 'insert']]
            ]
        );
    }

    /**
     * @param int $status
     * @return array
     */
    public static function getArOptionList( $status=BaseActiveQuery::ALL )
    {
        $query = self::find();
        /*if( $status != BaseActiveQuery::ALL ) {
            $query->where(['=','active',(int)$status]);
        }*/
        $items = $query->all();
        if( empty( $items ) ) return [];
        return ArrayHelper::map($query->all(),'id','name');
    }

}