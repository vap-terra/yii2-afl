<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 12.07.16
 * Time: 8:11
 */

namespace app\models\ext;

use app\models\FormElement;
use paulzi\sortable\SortableBehavior;
use yii\db\Expression;

class FormElementExt extends FormElement
{
    //TODO make dynamic types
    const TYPE_CHECKBOX = 'checkbox';
    const TYPE_LIST = 'list';
    const TYPE_INPUT = 'input';
    const TYPE_INTEGER = 'integer';
    const TYPE_TEXTAREA = 'textarea';
    const TYPE_DATE = 'date';
    const TYPE_DATETIME = 'datetime';
    const TYPE_URL = 'url';
    const TYPE_EMAIL = 'email';
    const TYPE_PHONE = 'phone';
    const TYPE_PASSWORD = 'password';
    const TYPE_TEXTEDIT = 'textedit';
    const TYPE_TIME = 'time';
    const TYPE_FILE = 'file';
    const TYPE_IMAGE = 'image';

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'position' => [
                'class' => SortableBehavior::className(),
                'query' => ['form_id'],
                'sortAttribute' => 'position',
                'step' => 10,
            ]
        ];
    }

    public static function getTypeList()
    {
        return [
            self::TYPE_CHECKBOX => 'checkbox',
            self::TYPE_LIST => 'list',
            self::TYPE_INPUT => 'input',
            self::TYPE_INTEGER => 'integer',
            self::TYPE_TEXTAREA => 'textarea',
            self::TYPE_DATE => 'date',
            self::TYPE_DATETIME => 'datetime',
            self::TYPE_URL => 'url',
            self::TYPE_EMAIL => 'email',
            self::TYPE_PHONE => 'phone',
            self::TYPE_PASSWORD => 'password',
            self::TYPE_TEXTEDIT => 'textedit',
            self::TYPE_TIME => 'time',
            self::TYPE_FILE => 'file',
            self::TYPE_IMAGE => 'image',
        ];
    }

    public function getListValues()
    {
        return !empty($this->params)?json_decode($this->params,true):[];
    }

    public function reposition( $number )
    {
        if ($number < 0) {
            $number = 0;
        }
        $count = $this->form->getFormElements()->orderBy(['position'=>SORT_ASC])->count();

        if (!$count || $number + 1 >= $count) {
            $this->position = self::getMax('position', 0,['=','form_id',$this->form_id]) + 10;
            return $this->save();
        }

        $arItems = $this->form->getFormElements()->andWhere(['!=','id',$this->id])->orderBy(['position'=>SORT_ASC])->all();
        foreach ($arItems as $num => $item) {
            if ($num == $number) {
                $this->position = $item->position;
                if( $this->save() ){

                    self::updateAll(
                        [
                            'position'=>new Expression('position + 10')
                        ],
                        'form_id='.$this->form_id.' and position>='.$item->position.' and id!='.$this->id
                    );

                    return true;
                }
                break;
            }
        }
        return false;
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForm()
    {
        return $this->hasOne(FormExt::className(), ['id' => 'form_id']);
    }
}