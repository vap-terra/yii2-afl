<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 28.01.16
 * Time: 17:35
 */

namespace app\models\ext;


use app\models\Constant;
use yii\base\Exception;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

class ConstantExt extends Constant
{
    const INACTIVE = 0;
    const ACTIVE = 1;
    const ALL = 2;

    public static function getByTid( $id )
    {
        /* @var $qConstant ActiveQuery */
        $qConstant = self::find();

        if( preg_match( '/^[\d]+$/', $id ) ) {
            $qConstant->where(['=','id',$id]);
        } else {
            $qConstant->where(['=','tid',$id]);
        }
        $oConstant = $qConstant->one();
        if( empty( $oConstant ) ) {
            throw new NotFoundHttpException('Not found constant by id: '.$id);
        }
        return $oConstant;
    }

    public static function getValueByTid( $id )
    {
        try {
            $oConstant = self::getByTid($id);
            return $oConstant->value;
        } catch( Exception $e ){
            return '';
        }
    }


    /**
     * @param int $status
     * @return array
     */
    public static function getArOptionList( $status=self::ALL )
    {
        $query = self::find();
        /*if( $status != self::ALL ) {
            $query->where(['=','active',(int)$status]);
        }*/
        $items = $query->all();
        if( empty( $items ) ) return [];
        return ArrayHelper::map($query->all(),'id','name');
    }
}