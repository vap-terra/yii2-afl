<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 05.07.16
 * Time: 23:55
 */

namespace app\models\ext;


use app\models\MenuItemRelation;
use paulzi\sortable\SortableBehavior;
use Yii;
use yii\db\Expression;

class MenuItemRelationExt extends MenuItemRelation
{
    //TODO make dynamic types
    const TYPE_CONTENT = 'content';
    const TYPE_INFORMATION = 'information';
    const TYPE_FORM = 'form';
    const TYPE_BLOCK = 'block';
    const TYPE_BANNER = 'banner';
    const TYPE_COMMENT = 'comment';
    const TYPE_QUESTION = 'question';
    const TYPE_SHOP = 'shop';
    const TYPE_SHOP_CART = 'shop-cart';
    const TYPE_WIDGET = 'widget';


    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'position' => [
                'class' => SortableBehavior::className(),
                'query' => ['menu_item_id'],
                'sortAttribute' => 'position',
                'step' => 10,
            ]
        ];
    }

    public static function getMenuItemTypes()
    {
        return [
            self::TYPE_CONTENT => Yii::t('app/menu-item-relation','Content'),
            self::TYPE_FORM => Yii::t('app/menu-item-relation','Form'),
            self::TYPE_INFORMATION => Yii::t('app/menu-item-relation','Information'),
            //self::TYPE_BLOCK => Yii::t('app/menu-item-relation','Block'),
            //self::TYPE_BANNER => Yii::t('app/menu-item-relation','Banner'),
            self::TYPE_COMMENT => Yii::t('app/menu-item-relation','Comment'),
            //self::TYPE_QUESTION => Yii::t('app/menu-item-relation','Question'),
            self::TYPE_SHOP => Yii::t('app/menu-item-relation','Shop'),
            self::TYPE_SHOP_CART => Yii::t('app/menu-item-relation','Shop cart'),
            //self::TYPE_WIDGET => Yii::t('app/menu-item-relation','Widget'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuItem()
    {
        return $this->hasOne(MenuItemExt::className(), ['id' => 'menu_item_id']);
    }

    public function getContent($view,$arUnsortedPath)
    {
        $item = $this->getItem(false);
        switch ($this->type) {
            case self::TYPE_CONTENT:
                if( $item ) {
                    return $item->content;
                }
                break;
            case self::TYPE_WIDGET:
                break;
            /* case self::TYPE_FORM:
            case self::TYPE_INFORMATION: */
            default :
                /* @var $controller Controller */
                $controller = Yii::$app->createController('frontend/' . $this->type);
                if ($controller) {
                    if ($this->main) {
                        return $controller[0]->runAsUrl(trim(implode('/', $arUnsortedPath)), [$this, $view]);
                    } else {
                        return $controller[0]->partOfPage(trim(implode('/', $arUnsortedPath)), [$this, $view]);
                    }
                }
                break;
        }
        return null;
    }

    public function getItem( $createEmpty=false )
    {
        $model = null;
        switch( $this->type ) {
            case self::TYPE_CONTENT:
                if( $this->item_id ) {
                    $model =  ContentExt::getById($this->item_id);
                } else if( $createEmpty ) {
                    $model = new ContentExt();
                }
                break;
            case self::TYPE_FORM:
                if( $this->item_id ) {
                    $model =  FormExt::getById($this->item_id);
                } else if( $createEmpty ) {
                    $model = new FormExt();
                }
                break;
            case self::TYPE_INFORMATION:
                if( $this->item_id ) {
                    $model =  InformationExt::getById($this->item_id);
                } else if( $createEmpty ) {
                    $model = new InformationExt();
                }
                break;
            case self::TYPE_BLOCK:
                if( $this->item_id ) {
                    $model =  BlockExt::getById($this->item_id);
                } else if( $createEmpty ) {
                    $model = new BlockExt();
                }
                break;
            case self::TYPE_BANNER:
                if( $this->item_id ) {
                    $model =  BannerExt::getById($this->item_id);
                } else if( $createEmpty ) {
                    $model = new BannerExt();
                }
                break;
            case self::TYPE_WIDGET:
                //$model =  BannerExt::getById($this->item_id);
                break;
        }
        return $model;
    }

    public function reposition( $number )
    {
        if ($number < 0) {
            $number = 0;
        }
        $count = $this->menuItem->getMenuItemRelations()->orderBy(['position'=>SORT_ASC])->count();

        if (!$count || $number + 1 >= $count) {
            $this->position = self::getMax('position', 0,['=','menu_item_id',$this->menu_item_id]) + 10;
            return $this->save();
        }

        $arItems = $this->menuItem->getMenuItemRelations()->andWhere(['!=','id',$this->id])->orderBy(['position'=>SORT_ASC])->all();
        foreach ($arItems as $num => $item) {
            if ($num == $number) {
                $this->position = $item->position;
                if( $this->save() ){

                    self::updateAll(
                        [
                            'position'=>new Expression('position + 10')
                        ],
                        'menu_item_id='.$this->menu_item_id.' and position>='.$item->position.' and id!='.$this->id
                    );

                    return true;
                }
                break;
            }
        }
        return false;
    }

    public function delete()
    {
        switch( $this->type ) {
            case MenuItemRelationExt::TYPE_CONTENT :
                if( $this->main ) {
                    $oContent = ContentExt::getById($this->item_id);
                    $oContent->menu_item_id = null;
                    $oContent->save();
                }
                break;
        }
        return parent::delete();
    }
}