<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 04.02.16
 * Time: 16:19
 */

namespace app\models\ext;


use app\models\BaseActiveQuery;
use app\models\GeoRegionArea;
use yii\helpers\ArrayHelper;

class GeoRegionAreaExt extends GeoRegionArea
{

    /**
     * @param int $geoRegionId
     * @param int $geoCountryId
     * @param int $status
     * @return array
     */
    public static function getArOptionList( $geoRegionId=0, $geoCountryId=0, $status=BaseActiveQuery::ALL )
    {

        $query = self::find();
        if($geoRegionId) {
            $query->andWhere(['=', 'geo_region_id', (int)$geoRegionId]);
        }
        if($geoCountryId) {
            $query->andWhere(['=', 'geo_country_id', (int)$geoCountryId]);
        }
        /*if( $status != BaseActiveQuery::ALL ) {
            $query->andWhere(['=','active',(int)$status]);
        }*/
        return ArrayHelper::map($query->all(),'id','name');
    }
}