<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 28.02.16
 * Time: 21:19
 */

namespace app\models\ext;


use app\models\BaseActiveQuery;
use app\models\ListValue;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

class ListValueExt extends ListValue
{
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getListValueItems()
    {
        return $this->hasMany(ListValueItemExt::className(), ['list_value_id' => 'id']);
    }

    /**
     * @param int $status
     * @return array
     */
    public static function getArOptionList( $status=BaseActiveQuery::ALL )
    {
        $query = self::find();
        /*if( $status != BaseActiveQuery::ALL ) {
            $query->where(['=','active',(int)$status]);
        }*/
        return ArrayHelper::map($query->all(),'id','name');
    }

    /**
     * @param int $id
     * @param int $status
     * @return array
     */
    public static function getArItemsOptionList( $id, $status=BaseActiveQuery::ALL )
    {
        $query = ListValueExt::find()->where(['=','id',$id]);
        /*if( $status != self::ALL ) {
            $query->where(['=','active',(int)$status]);
        }*/
        $oListValue = $query->one();
        if( !empty( $oListValue ) ) {
            $query = $oListValue->getListValueItems();
            if ($status != BaseActiveQuery::ALL) {
                $query->andWhere(['=', 'active', (int)$status]);
            }
            return ArrayHelper::map($query->all(), 'id', 'name');
        }
        return [];
    }
}