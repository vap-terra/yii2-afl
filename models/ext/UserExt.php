<?php
namespace app\models\ext;

use app\models\BaseActiveQuery;
use app\models\User;
use Yii;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * Class UserExt
 * @package app\models\ext
 *
 * @property CompanyExt[] $companies
 * @property ShopItemPriceExt[] $shopItemPrices
 * @property ShopOrderExt[] $shopOrders
 * @property ShopOrderExt[] $shopOrders0
 * @property GeoLocalityExt $geoLocality
 * @property LanguageExt $language
 * @property GeoCountryExt $geoCountry
 * @property UserRelationExt[] $userRelations
 */
class UserExt extends User implements IdentityInterface
{
    /**
     *
     */
    const ROOT = 'root';
    /**
     *
     */
    const ADMIN = 'admin';

    /**
     *
     */
    const EMPLOYEE = 'employee';
    /**
     *
     */
    const USER = 'user';
    /**
     *
     */
    const GUEST = 'guest';

    public $repeat_password;

    public $password;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge_recursive(
            parent::rules(),
            [

                ['is_blocked', 'default', 'value' => 0],
                ['is_blocked', 'in', 'range' => [0,1]],

                [['photo'], 'file', 'extensions' => 'gif, jpg, jpe, jpeg, png'],

                ['role', 'default', 'value' => self::USER],
                ['role', 'in', 'range' => [self::ROOT, self::ADMIN, self::EMPLOYEE, self::USER]],

                ['login', 'filter', 'filter' => 'trim'],
                ['login', 'required'],
                ['login', 'unique', 'targetClass' => '\app\models\ext\UserExt', 'message' => 'This login has already been taken.'],
                ['login', 'string', 'min' => 2, 'max' => 255],

                ['email', 'filter', 'filter' => 'trim'],
                ['email', 'required'],
                ['email', 'email'],
                ['email', 'string', 'max' => 255],
                ['email', 'unique', 'targetClass' => '\app\models\ext\UserExt', 'message' => 'This email address has already been taken.'],

                ['phone', 'filter', 'filter' => 'trim'],
                ['phone', 'required'],
                ['phone', 'unique', 'targetClass' => '\app\models\ext\UserExt', 'message' => 'This phone number has already been taken.'],
                ['phone', 'string', 'min' => 2, 'max' => 255],

                ['password', 'required', 'on'=>['changePassword','insert']],
                ['password', 'string', 'min' => 6, 'on'=>['changePassword','insert']],
                ['password', 'validatePassword', 'on'=>['changePassword','insert']],

                ['repeat_password', 'required', 'on'=>['changePassword','insert']],
                ['repeat_password', 'string', 'min' => 6, 'on'=>['changePassword','insert']],

                ['repeat_password', 'compare', 'compareAttribute'=>'password', 'on'=>['changePassword','insert']],

                [
                    [
                        'is_blocked','password_hash','auth_key','activation_code','password_reset_token','creator_user_id',
                        'dt_creation','ip_creation','reason_blocking', 'blocker_user_id','ip_last','dt_last',
                        'updater_user_id','dt_update','ip_update','photo_width','photo_height'
                    ], 'safe'
                ],
            ]
        );
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge_recursive(
            parent::attributeLabels(),
            [
                'repeat_password' => Yii::t('app/user', 'Repeat Password'),
                'password' => Yii::t('app/user', 'Password'),
            ]
        );
    }


    public static function getArOptionList($status = BaseActiveQuery::ALL)
    {
        /* @var $qShop BaseActiveQuery */
        /* @var $arShop ShopExt */

        $arItems = self::find()
            //->active($status)
            ->all();

        if( empty( $arItems ) ) return [];

        return ArrayHelper::map($arItems,'id','login');
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\BlameableBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['dt_creation', 'dt_update'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['dt_update'],
                ],
                'value' => function() { return date('Y-m-d H:i:s');  },
            ],
            'userid'=>[
                'class' => 'yii\behaviors\BlameableBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT =>[ 'creator_user_id', 'updater_user_id' ],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updater_user_id'],
                ],
                'value' => function() { return ( !Yii::$app->request->isConsoleRequest && !Yii::$app->user->isGuest )?Yii::$app->user->id:0; },
            ],
            'userip'=>[
                'class' => 'yii\behaviors\BlameableBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT =>[ 'ip_creation', 'ip_update' ],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['ip_update'],
                ],
                'value' => function() { return (!Yii::$app->request->isConsoleRequest)?Yii::$app->request->getUserIP():'127.0.0.1'; },
            ],
            'phone'=>[
                'class' => 'yii\behaviors\BlameableBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT =>['phone'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['phone'],
                ],
                'value' => function() { return strtr( $this->phone, ['+'=>'','('=>'',')'=>'',' '=>'','-'=>''] ); },
            ],
            /*'photo'=>[
                'class' => ImageBehavior::className(),
                'fileAttribute' => 'photo',
                //'sizeAttribute',
                //'extensionAttribute',
                //'mimeAttribute',
                //'upload' => true,
                //'loadAsTmp' => false,
                //'deleteTmp' => true,
                //'tmpFile' => false,
                'attributes' => [
                ]
            ]*/
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(CompanyExt::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShopItemPrices()
    {
        return $this->hasMany(ShopItemPriceExt::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShopOrders()
    {
        return $this->hasMany(ShopOrderExt::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShopOrders0()
    {
        return $this->hasMany(ShopOrderExt::className(), ['update_user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGeoLocality()
    {
        return $this->hasOne(GeoLocalityExt::className(), ['id' => 'geo_locality_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(LanguageExt::className(), ['id' => 'language_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGeoCountry()
    {
        return $this->hasOne(GeoCountryExt::className(), ['id' => 'geo_country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserRelations()
    {
        return $this->hasMany(UserRelationExt::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserGroups()
    {
        return $this->hasMany(UserGroupExt::className(), ['id' => 'user_group_id'])
            ->viaTable('user_group_relation', ['user_id' => 'id']);
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        $auth = Yii::$app->authManager;
        $name = $this->role ? $this->role : self::GUEST;
        /*$role = $auth->getRole($name);
        if (!$insert) {
            $auth->revokeAll($this->id);
        }*/
        //$auth->assign($role, $this->id);
        parent::afterSave($insert, $changedAttributes);
    }

    public function beforeSave($insert)
    {
        if( parent::beforeSave($insert) ) {
            if ($this->isNewRecord) {
                $this->auth_key = \Yii::$app->security->generateRandomString();
            }
            $photo = $this->getOldAttribute('photo');
            if (empty($this->photo) && !empty($photo)) {
                $this->photo = $photo;
            }
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'is_blocked' => 0,]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by login
     *
     * @param string $login
     * @return static|null
     */
    public static function findByLogin($login)
    {
        return static::findOne(['login' => $login, 'is_blocked' => 0,]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }
        return static::findOne([
            'password_reset_token' => $token,
            'is_blocked' => 0,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * @return bool
     */
    public function getIsUser()
    {
        return $this->role === self::USER;
    }

    /**
     * @return bool
     */
    public function getIsEmployee()
    {
        return $this->role === self::EMPLOYEE;
    }

    /**
     * @return bool
     */
    public function getIsAdmin()
    {
        return $this->role === self::ADMIN;
    }

    /**
     * @return bool
     */
    public function getIsRoot()
    {
        return $this->role === self::ROOT;
    }

    /**
     * @param string $format
     * @return string
     */
    public function getFullName( $format = '{surname} {name} {patronymic}' )
    {
        return trim(
            strtr(
                $format,
                [
                    '{surname}'=>$this->surname,
                    '{name}'=>$this->name,
                    '{patronymic}'=>$this->patronymic
                ]
            )
        );
    }

    /**
     * @return bool
     */
    public function isOnline()
    {
        return false;
    }

    /**
     *
     */
    public function online()
    {
    }

    /**
     *
     */
    public function offline()
    {
    }

    public function getTitleGender()
    {
        $genders = self::getOptionsGenderList();
        if( !empty( $genders[$this->gender] ) ) {
            return $genders[$this->gender];
        }
        return '';
    }

    /**
     * @return array
     */
    static public function getOptionsGenderList()
    {
        return [
            '1' => Yii::t('app/user','male'),
            '2' => Yii::t('app/user','female')
        ];
    }

    public function getTitleRole()
    {
        $roles = self::getOptionsRoleList();
        if( !empty( $roles[$this->role] ) ) {
            return $roles[$this->role];
        }
        return '';
    }
    /**
     * @return array
     */
    static public function getOptionsRoleList()
    {
        return [
            self::ROOT => Yii::t('app/user',self::ROOT),
            self::ADMIN => Yii::t('app/user',self::ADMIN),
            self::EMPLOYEE =>  Yii::t('app/user',self::EMPLOYEE),
            self::USER =>  Yii::t('app/user',self::USER),
        ];
    }


    /**
     * @return string
     */
    public function getHref( )
    {
        return Yii::$app->storage->getHref($this);
    }

    /**
     * @return string
     */
    public function getPath( )
    {
        return Yii::$app->storage->getPath($this);
    }

    /**
     * @return string
     */
    public function getImageHref()
    {
        return $this->getHref() . $this->photo;
    }

    /**
     * @return string
     */
    public function getImagePath()
    {
        return $this->getPath() . $this->photo;
    }

    /**
     * @param $extension
     * @return string
     */
    public function generateImageFileName( $extension )
    {
        return 'photo_'.md5($this->id).'.' . $extension;
    }
    /**
     *
     */
    public function createDir()
    {
        $dir = $this->getPath();
        if( !is_dir( $dir ) ) {
            mkdir($dir, 0775, true);
        }
    }

    /**
     * @param UploadedFile $image
     * @param bool $upload
     * @param bool $deleteTempFile
     * @return bool
     */
    public function uploadImage($image, $upload=true, $deleteTempFile = true )
    {
        if( $image instanceof UploadedFile === false ) return false;
        $fileName = $this->generateImageFileName($image->extension);
        $filePath = $this->getPath() . $fileName;
        $this->createDir();
        if( $upload ) {
            $status = $image->saveAs( $filePath, $deleteTempFile  );
        } else {
            $status = copy( $image->tempName, $filePath );
            if( $status && $deleteTempFile ) {
                unlink($image->tempName);
            }
        }
        if ( $status ) {
            $this->photo = $fileName;
            $this->save();
            return true;
        }
        return false;
    }

}
