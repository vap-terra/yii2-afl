<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 28.01.16
 * Time: 17:31
 */

namespace app\models\ext;


use app\models\BlockGroupLink;

class BlockGroupLinkExt extends BlockGroupLink
{
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlock()
    {
        return $this->hasOne(BlockExt::className(), ['id' => 'block_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlockGroup()
    {
        return $this->hasOne(BlockGroupExt::className(), ['id' => 'block_group_id']);
    }
}