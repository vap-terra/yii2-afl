<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 29.06.16
 * Time: 23:14
 */

namespace app\models\ext;

use app\models\InformationProperty;
use Yii;
use yii\web\NotFoundHttpException;

class InformationPropertyExt extends InformationProperty
{
    //TODO make dynamic types
    const TYPE_FILE = 'file';
    const TYPE_INTEGER = 'integer';
    const TYPE_STRING = 'string';
    const TYPE_TEXT = 'text';
    const TYPE_BOOLEAN = 'boolean';
    const TYPE_LIST_VALUE = 'list_value';

    public $value = '';
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformationItemGroupProperties()
    {
        return $this->hasMany(InformationItemGroupPropertyExt::className(), ['information_property_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformationItemPropertyValues()
    {
        return $this->hasMany(InformationItemPropertyValueExt::className(), ['information_property_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformation()
    {
        return $this->hasOne(InformationExt::className(), ['id' => 'information_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getListValue()
    {
        return $this->hasOne(ListValueExt::className(), ['id' => 'list_value_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformationPropertyValueFiles()
    {
        return $this->hasMany(InformationPropertyValueFileExt::className(), ['information_property_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformationPropertyValueIntegers()
    {
        return $this->hasMany(InformationPropertyValueIntegerExt::className(), ['information_property_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformationPropertyValueStrings()
    {
        return $this->hasMany(InformationPropertyValueStringExt::className(), ['information_property_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformationPropertyValueTexts()
    {
        return $this->hasMany(InformationPropertyValueTextExt::className(), ['information_property_id' => 'id']);
    }

    /**
     * @param $informationItemId
     * @return null|\yii\db\ActiveQuery
     * @throws \TypeError
     */
    public function getValues($informationItemId)
    {
        $query = null;
        switch( $this->type ) {
            case self::TYPE_FILE:
                $query = $this->getInformationPropertyValueFiles();
                break;
            case self::TYPE_STRING:
                $query = $this->getInformationPropertyValueStrings();
                break;
            case self::TYPE_TEXT:
                $query = $this->getInformationPropertyValueTexts();
                break;
            case self::TYPE_INTEGER:
            case self::TYPE_BOOLEAN:
            case self::TYPE_LIST_VALUE:
                $query = $this->getInformationPropertyValueIntegers();
                break;
        }
        if( empty($query) ){
            throw new \TypeError( 'Property type:'.$this->type.' not supported!' );
        }
        $query->andWhere(['=','information_item_id',$informationItemId]);
        return $query;
    }

    /**
     * @return InformationPropertyValueFileExt|InformationPropertyValueIntegerExt|InformationPropertyValueStringExt|InformationPropertyValueTextExt
     * @throws \TypeError
     */
    public function getValueModel()
    {
        switch( $this->type ) {
            case self::TYPE_FILE:
                return new InformationPropertyValueFileExt();
                break;
            case self::TYPE_STRING:
                return new InformationPropertyValueStringExt();
                break;
            case self::TYPE_TEXT:
                return new InformationPropertyValueTextExt();
                break;
            case self::TYPE_INTEGER:
            case self::TYPE_BOOLEAN:
            case self::TYPE_LIST_VALUE:
                return new InformationPropertyValueIntegerExt();
                break;
        }
        throw new \TypeError( 'Property type:'.$this->type.' not supported!' );
    }

    public static function getArTypesOptionList()
    {
        return [
            self::TYPE_FILE => Yii::t('app\models',self::TYPE_FILE),
            self::TYPE_INTEGER => Yii::t('app\models',self::TYPE_INTEGER),
            self::TYPE_STRING => Yii::t('app\models',self::TYPE_STRING),
            self::TYPE_TEXT => Yii::t('app\models',self::TYPE_TEXT),
            self::TYPE_BOOLEAN => Yii::t('app\models',self::TYPE_BOOLEAN),
            self::TYPE_LIST_VALUE => Yii::t('app\models',self::TYPE_LIST_VALUE),
        ];
    }
}