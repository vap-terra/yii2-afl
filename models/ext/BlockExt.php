<?php

namespace app\models\ext;

use app\models\Block;
use Yii;
use yii\base\InvalidParamException;
use yii\web\NotFoundHttpException;

class BlockExt extends Block
{

    const TYPE_TEXT = 'text';
    const TYPE_CODE = 'code';
    const TYPE_WIDGET = 'widget';
    /**
     * @param int $menuItemId
     * @return string
     */
    public function generate( $menuItemId )
    {
        $generateName = 'generate'.mb_strtoupper(  mb_substr( $this->type, 0, 1 ) ).mb_substr( $this->type, 1 );
        if( method_exists( $this, $generateName ) ) {
            return $this->$generateName($menuItemId);
        }
        throw new InvalidParamException('Undefined block type');
    }

    /**
     * @param int $menuItemId
     * @return string
     */
    protected function generateText( $menuItemId )
    {
        /* @var $oMenuItem MenuItemExt */
        /* @var $oMenuItemBlock MenuItemBlockExt */
        $oMenuItem = MenuItemExt::getByTid($menuItemId);
        $oMenuItemBlock = $oMenuItem->getMenuItemBlocks()->andWhere(['=','block_id',$this->id])->one();
        $view = 'default';
        if( !empty( $oMenuItemBlock->template ) ) {
            $view = $oMenuItemBlock->template;
        } else if( !empty( $this->template ) ) {
            $view = $this->template;
        }
        return \Yii::$app->getView()->render(
            'blocks/'.$view,
            [
                'oBlock' => $this,
                'oMenuItemBlock' => $oMenuItemBlock
            ]
        );
    }

    /**
     * @param int $menuItemId
     * @return string
     */
    protected function generateCode($menuItemId)
    {
        ob_start();
        eval( $this->content );
        $this->content = ob_get_clean();

        return $this->generateText($menuItemId);
    }

    protected function generateWidgetClassName()
    {
        $widgetName = $this->widget;
        $arNamespace = explode('\\',$widgetName);
        if ( count( $arNamespace ) == 2 ) {
            if( isset( \Yii::$app->modules[$arNamespace[0]] ) ) {
                if( isset( \Yii::$app->modules[$arNamespace[0]]['class'] ) ) {
                    $arModuleNamespace = explode('\\',\Yii::$app->modules[$arNamespace[0]]['class']);
                    array_pop($arModuleNamespace);
                    $widgetClassName = implode('\\',$arModuleNamespace). '\widgets\\' . $arNamespace[1];
                } else {
                    $widgetClassName = 'common\modules\\' . $arNamespace[0] . '\widgets\\' . $arNamespace[1];
                }
            }
        } else if ( count( $arNamespace ) == 1 ) {
            $widgetClassName = 'frontend\widgets\\'.$widgetName;
        }

        if( empty($widgetClassName) ){
            $widgetClassName = $widgetName;
        }
        return $widgetClassName;
    }

    /**
     * @param int $menuItemId
     * @return string
     */
    protected function generateWidget($menuItemId)
    {
        $widgetClassName = $this->generateWidgetClassName();

        if( !class_exists( $widgetClassName ) ) {
            $file = \Yii::getAlias( '@'.strtr( $widgetClassName.'.php','\\','/') );
            require_once( $file );
        }

        $this->content = $widgetClassName::widget(
            json_decode($this->parameters, true)
        );
        return $this->generateText($menuItemId);
    }

    /**
     * @return array
     */
    static public function getArTypesOptionList()
    {
        return [
            self::TYPE_TEXT => Yii::t('app/block',self::TYPE_TEXT),
            self::TYPE_CODE => Yii::t('app/block',self::TYPE_CODE),
            self::TYPE_WIDGET =>  Yii::t('app/block',self::TYPE_WIDGET),
        ];
    }
}