<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 12.07.16
 * Time: 8:09
 */

namespace app\models\ext;

use app\models\FormCompleted;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;

class FormCompletedExt extends FormCompleted
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\BlameableBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['dt_creation'],
                ],
                'value' => function() { return new Expression('UTC_TIMESTAMP()')/*date('Y-m-d H:i:s')*/;  },
            ],
            /*'userid'=>[
                'class' => 'yii\behaviors\BlameableBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT =>[ 'creator_user_id'],
                ],
                'value' => function() { return ( !Yii::$app->request->isConsoleRequest && !Yii::$app->user->isGuest )?Yii::$app->user->id:0; },
            ],*/
            'userip'=>[
                'class' => 'yii\behaviors\BlameableBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT =>[ 'ip' ],
                ],
                'value' => function() { return (!Yii::$app->request->isConsoleRequest)?Yii::$app->request->getUserIP():'127.0.0.1'; },
            ],
        ];
    }

    /**
     * @param $arValues array
     */
    public function setValues( $arValues )
    {
        $this->elements = json_encode($arValues);
    }

    /**
     * @return array
     */
    public function getValues()
    {
        return json_decode($this->elements,true);
    }

    public function getStrValues()
    {
        $str = '';
        $ar = $this->getValues();
        foreach( $ar as $name=>$value ){
            $str .= '<p>'.$name.': '.$value.'</p>';
        }
        return $str;
    }
}