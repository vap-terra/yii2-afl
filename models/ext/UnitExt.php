<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 04.02.16
 * Time: 14:37
 */

namespace app\models\ext;


use app\models\BaseActiveQuery;
use app\models\Unit;
use yii\helpers\ArrayHelper;

class UnitExt extends Unit
{
    /**
     * @param int $status
     * @return array
     */
    public static function getArOptionList( $status=BaseActiveQuery::ALL )
    {
        $query = self::find();
        /*if( $status != BaseActiveQuery::ALL ) {
            $query->where(['=','active',(int)$status]);
        }*/
        $items = $query->all();
        if( empty( $items ) ) return [];
        return ArrayHelper::map($query->all(),'id','name');
    }
}