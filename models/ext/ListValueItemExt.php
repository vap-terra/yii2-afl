<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 28.02.16
 * Time: 21:20
 */

namespace app\models\ext;


use app\models\ListValueItem;
use yii\web\NotFoundHttpException;

class ListValueItemExt extends ListValueItem
{
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getListValue()
    {
        return $this->hasOne(ListValueExt::className(), ['id' => 'list_value_id']);
    }

}