<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "geo_locality_area".
 *
 * @property integer $id
 * @property integer $geo_country_id
 * @property integer $geo_region_id
 * @property integer $geo_region_area_id
 * @property integer $geo_locality_id
 * @property string $name
 *
 * @property BoardItem[] $boardItems
 */
class GeoLocalityArea extends BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'geo_locality_area';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['geo_country_id', 'geo_region_id', 'geo_region_area_id', 'geo_locality_id', 'name'], 'required'],
            [['geo_country_id', 'geo_region_id', 'geo_region_area_id', 'geo_locality_id'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/models', 'ID'),
            'geo_country_id' => Yii::t('app/models', 'Geo Country ID'),
            'geo_region_id' => Yii::t('app/models', 'Geo Region ID'),
            'geo_region_area_id' => Yii::t('app/models', 'Geo Region Area ID'),
            'geo_locality_id' => Yii::t('app/models', 'Geo Locality ID'),
            'name' => Yii::t('app/models', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBoardItems()
    {
        return $this->hasMany(BoardItem::className(), ['geo_locality_area_id' => 'id']);
    }
}
