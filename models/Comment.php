<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "comment".
 *
 * @property integer $id
 * @property string $belong
 * @property integer $belong_item_id
 * @property string $author_name
 * @property string $author_photo
 * @property string $author_post
 * @property string $author_company
 * @property string $author_locality
 * @property string $content
 * @property string $dt_creation
 * @property string $ip_creation
 * @property integer $creator_user_id
 * @property integer $active
 * @property integer $position
 */
class Comment extends \app\models\BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['belong', 'content'], 'string'],
            [['belong_item_id', 'creator_user_id', 'active', 'position'], 'integer'],
            [['content'], 'required'],
            [['dt_creation'], 'safe'],
            [['author_name', 'author_photo', 'author_post', 'author_company'], 'string', 'max' => 255],
            [['author_locality'], 'string', 'max' => 100],
            [['ip_creation'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/comment', 'ID'),
            'belong' => Yii::t('app/comment', 'Belong'),
            'belong_item_id' => Yii::t('app/comment', 'Belong Item ID'),
            'author_name' => Yii::t('app/comment', 'Author Name'),
            'author_photo' => Yii::t('app/comment', 'Author Photo'),
            'author_post' => Yii::t('app/comment', 'Author Post'),
            'author_company' => Yii::t('app/comment', 'Author Company'),
            'author_locality' => Yii::t('app/comment', 'Author Locality'),
            'content' => Yii::t('app/comment', 'Content'),
            'dt_creation' => Yii::t('app/comment', 'Dt Creation'),
            'ip_creation' => Yii::t('app/comment', 'Ip Creation'),
            'creator_user_id' => Yii::t('app/comment', 'Creator User ID'),
            'active' => Yii::t('app/comment', 'Active'),
            'position' => Yii::t('app/comment', 'Position'),
        ];
    }
}
