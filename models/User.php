<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $role
 * @property string $login
 * @property string $auth_key
 * @property string $security_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $activation_code
 * @property string $surname
 * @property string $name
 * @property string $patronymic
 * @property integer $gender
 * @property string $phone
 * @property string $email
 * @property string $photo
 * @property integer $photo_width
 * @property integer $photo_height
 * @property integer $language_id
 * @property integer $geo_country_id
 * @property integer $geo_locality_id
 * @property string $dt_last
 * @property string $ip_last
 * @property integer $is_blocked
 * @property integer $blocker_user_id
 * @property string $reason_blocking
 * @property string $dt_blocked
 * @property string $ip_blocked
 * @property integer $creator_user_id
 * @property string $dt_creation
 * @property string $ip_creation
 * @property integer $updater_user_id
 * @property string $dt_update
 * @property string $ip_update
 *
 * @property Company[] $companies
 * @property ShopItemPrice[] $shopItemPrices
 * @property ShopOrder[] $shopOrders
 * @property ShopOrder[] $shopOrders0
 * @property GeoLocality $geoLocality
 * @property Language $language
 * @property GeoCountry $geoCountry
 * @property UserRelation[] $userRelations
 */
class User extends \app\models\BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['role', 'login', 'password_hash', 'surname', 'name', 'phone', 'email'], 'required'],
            [['role', 'reason_blocking'], 'string'],
            [['gender', 'photo_width', 'photo_height', 'language_id', 'geo_country_id', 'geo_locality_id', 'is_blocked', 'blocker_user_id', 'creator_user_id', 'updater_user_id'], 'integer'],
            [['dt_last', 'dt_blocked', 'dt_creation', 'dt_update'], 'safe'],
            [['login', 'activation_code'], 'string', 'max' => 20],
            [['auth_key', 'security_key'], 'string', 'max' => 32],
            [['password_hash', 'password_reset_token', 'photo'], 'string', 'max' => 255],
            [['surname', 'name', 'patronymic'], 'string', 'max' => 100],
            [['phone'], 'string', 'max' => 13],
            [['email'], 'string', 'max' => 50],
            [['ip_last', 'ip_blocked', 'ip_creation', 'ip_update'], 'string', 'max' => 30],
            [['geo_locality_id'], 'exist', 'skipOnError' => true, 'targetClass' => GeoLocality::className(), 'targetAttribute' => ['geo_locality_id' => 'id']],
            [['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'id']],
            [['geo_country_id'], 'exist', 'skipOnError' => true, 'targetClass' => GeoCountry::className(), 'targetAttribute' => ['geo_country_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/user', 'ID'),
            'role' => Yii::t('app/user', 'Role'),
            'login' => Yii::t('app/user', 'Login'),
            'auth_key' => Yii::t('app/user', 'Auth Key'),
            'security_key' => Yii::t('app/user', 'Security Key'),
            'password_hash' => Yii::t('app/user', 'Password Hash'),
            'password_reset_token' => Yii::t('app/user', 'Password Reset Token'),
            'activation_code' => Yii::t('app/user', 'Activation Code'),
            'surname' => Yii::t('app/user', 'Surname'),
            'name' => Yii::t('app/user', 'Name'),
            'patronymic' => Yii::t('app/user', 'Patronymic'),
            'gender' => Yii::t('app/user', 'Gender'),
            'phone' => Yii::t('app/user', 'Phone'),
            'email' => Yii::t('app/user', 'Email'),
            'photo' => Yii::t('app/user', 'Photo'),
            'photo_width' => Yii::t('app/user', 'Photo Width'),
            'photo_height' => Yii::t('app/user', 'Photo Height'),
            'language_id' => Yii::t('app/user', 'Language ID'),
            'geo_country_id' => Yii::t('app/user', 'Geo Country ID'),
            'geo_locality_id' => Yii::t('app/user', 'Geo Locality ID'),
            'dt_last' => Yii::t('app/user', 'Dt Last'),
            'ip_last' => Yii::t('app/user', 'Ip Last'),
            'is_blocked' => Yii::t('app/user', 'Is Blocked'),
            'blocker_user_id' => Yii::t('app/user', 'Blocker User ID'),
            'reason_blocking' => Yii::t('app/user', 'Reason Blocking'),
            'dt_blocked' => Yii::t('app/user', 'Dt Blocked'),
            'ip_blocked' => Yii::t('app/user', 'Ip Blocked'),
            'creator_user_id' => Yii::t('app/user', 'Creator User ID'),
            'dt_creation' => Yii::t('app/user', 'Dt Creation'),
            'ip_creation' => Yii::t('app/user', 'Ip Creation'),
            'updater_user_id' => Yii::t('app/user', 'Updater User ID'),
            'dt_update' => Yii::t('app/user', 'Dt Update'),
            'ip_update' => Yii::t('app/user', 'Ip Update'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(Company::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShopItemPrices()
    {
        return $this->hasMany(ShopItemPrice::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShopOrders()
    {
        return $this->hasMany(ShopOrder::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShopOrders0()
    {
        return $this->hasMany(ShopOrder::className(), ['update_user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGeoLocality()
    {
        return $this->hasOne(GeoLocality::className(), ['id' => 'geo_locality_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGeoCountry()
    {
        return $this->hasOne(GeoCountry::className(), ['id' => 'geo_country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserRelations()
    {
        return $this->hasMany(UserRelation::className(), ['user_id' => 'id']);
    }
}
