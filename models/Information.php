<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "information".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $active
 * @property string $title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property string $title_group
 * @property string $meta_keywords_group
 * @property string $meta_description_group
 * @property string $title_item
 * @property string $meta_keywords_item
 * @property string $meta_description_item
 * @property integer $allow_comment
 * @property integer $items_on_page
 * @property string $format_date
 *
 * @property InformationGroup[] $informationGroups
 * @property InformationItem[] $informationItems
 * @property InformationProperty[] $informationProperties
 */
class Information extends \app\models\BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'information';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['description'], 'string'],
            [['active', 'allow_comment', 'items_on_page'], 'integer'],
            [['name', 'title', 'meta_keywords', 'meta_description', 'title_group', 'meta_keywords_group', 'meta_description_group', 'title_item', 'meta_keywords_item', 'meta_description_item'], 'string', 'max' => 255],
            [['format_date'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/information', 'ID'),
            'name' => Yii::t('app/information', 'Name'),
            'description' => Yii::t('app/information', 'Description'),
            'active' => Yii::t('app/information', 'Active'),
            'title' => Yii::t('app/information', 'Title'),
            'meta_keywords' => Yii::t('app/information', 'Meta Keywords'),
            'meta_description' => Yii::t('app/information', 'Meta Description'),
            'title_group' => Yii::t('app/information', 'Title Group'),
            'meta_keywords_group' => Yii::t('app/information', 'Meta Keywords Group'),
            'meta_description_group' => Yii::t('app/information', 'Meta Description Group'),
            'title_item' => Yii::t('app/information', 'Title Item'),
            'meta_keywords_item' => Yii::t('app/information', 'Meta Keywords Item'),
            'meta_description_item' => Yii::t('app/information', 'Meta Description Item'),
            'allow_comment' => Yii::t('app/information', 'Allow Comment'),
            'items_on_page' => Yii::t('app/information', 'Items On Page'),
            'format_date' => Yii::t('app/information', 'Format Date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformationGroups()
    {
        return $this->hasMany(InformationGroup::className(), ['information_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformationItems()
    {
        return $this->hasMany(InformationItem::className(), ['information_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformationProperties()
    {
        return $this->hasMany(InformationProperty::className(), ['information_id' => 'id']);
    }
}
