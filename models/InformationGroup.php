<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "information_group".
 *
 * @property integer $id
 * @property string $tid
 * @property integer $information_id
 * @property integer $information_group_id
 * @property string $name
 * @property string $header
 * @property string $sub_header
 * @property string $description
 * @property string $content
 * @property string $title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property string $title_item
 * @property string $meta_description_item
 * @property string $meta_keywords_item
 * @property integer $position
 * @property integer $active
 * @property integer $depth
 * @property integer $lb
 * @property integer $rb
 * @property string $image
 * @property integer $image_width
 * @property integer $image_height
 *
 * @property Information $information
 * @property InformationItemGroupProperty[] $informationItemGroupProperties
 */
class InformationGroup extends BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'information_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['information_id', 'name', 'depth', 'lb', 'rb'], 'required'],
            [['information_id', 'information_group_id', 'position', 'active', 'depth', 'lb', 'rb', 'image_width', 'image_height'], 'integer'],
            [['description', 'content'], 'string'],
            [['tid'], 'string', 'max' => 100],
            [['name', 'header', 'sub_header', 'title', 'meta_description', 'meta_keywords', 'title_item', 'meta_description_item', 'meta_keywords_item', 'image'], 'string', 'max' => 255],
            [['information_id', 'rb'], 'unique', 'targetAttribute' => ['information_id', 'rb'], 'message' => 'The combination of Information ID and Rb has already been taken.'],
            [['information_id'], 'exist', 'skipOnError' => true, 'targetClass' => Information::className(), 'targetAttribute' => ['information_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/information-group', 'ID'),
            'tid' => Yii::t('app/information-group', 'Tid'),
            'information_id' => Yii::t('app/information-group', 'Information ID'),
            'information_group_id' => Yii::t('app/information-group', 'Information Group ID'),
            'name' => Yii::t('app/information-group', 'Name'),
            'header' => Yii::t('app/information-group', 'Header'),
            'sub_header' => Yii::t('app/information-group', 'Sub Header'),
            'description' => Yii::t('app/information-group', 'Description'),
            'content' => Yii::t('app/information-group', 'Content'),
            'title' => Yii::t('app/information-group', 'Title'),
            'meta_description' => Yii::t('app/information-group', 'Meta Description'),
            'meta_keywords' => Yii::t('app/information-group', 'Meta Keywords'),
            'title_item' => Yii::t('app/information-group', 'Title Item'),
            'meta_description_item' => Yii::t('app/information-group', 'Meta Description Item'),
            'meta_keywords_item' => Yii::t('app/information-group', 'Meta Keywords Item'),
            'position' => Yii::t('app/information-group', 'Position'),
            'active' => Yii::t('app/information-group', 'Active'),
            'depth' => Yii::t('app/information-group', 'Depth'),
            'lb' => Yii::t('app/information-group', 'Lb'),
            'rb' => Yii::t('app/information-group', 'Rb'),
            'image' => Yii::t('app/information-group', 'Image'),
            'image_width' => Yii::t('app/information-group', 'Image Width'),
            'image_height' => Yii::t('app/information-group', 'Image Height'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformation()
    {
        return $this->hasOne(Information::className(), ['id' => 'information_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformationItems()
    {
        return $this->hasMany(InformationItem::className(), ['information_group_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformationGroup()
    {
        return $this->hasOne(InformationGroup::className(), ['id' => 'information_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformationGroups()
    {
        return $this->hasMany(InformationGroup::className(), ['information_group_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformationItemGroupProperties()
    {
        return $this->hasMany(InformationItemGroupProperty::className(), ['information_group_id' => 'id']);
    }
}
