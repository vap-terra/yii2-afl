<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "menu_item".
 *
 * @property integer $id
 * @property string $tid
 * @property integer $menu_id
 * @property integer $menu_item_id
 * @property string $layout
 * @property string $template
 * @property string $name
 * @property string $header
 * @property string $sub_header
 * @property string $title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property string $link
 * @property integer $position
 * @property integer $active
 * @property integer $depth
 * @property integer $rb
 * @property integer $lb
 * @property string $image
 * @property integer $image_width
 * @property integer $image_height
 *
 * @property Content[] $contents
 * @property Information[] $informations
 * @property Menu $menu
 * @property MenuItemBlock[] $menuItemBlocks
 * @property MenuItemContent[] $menuItemContents
 * @property MenuItemRelation[] $menuItemRelations
 */
class MenuItem extends BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['menu_id', 'name', 'rb', 'lb'], 'required'],
            [['menu_id', 'menu_item_id', 'position', 'active', 'depth', 'rb', 'lb', 'image_width', 'image_height'], 'integer'],
            [['tid', 'name', 'header', 'sub_header', 'title', 'meta_keywords', 'meta_description', 'link', 'image'], 'string', 'max' => 255],
            [['layout', 'template'], 'string', 'max' => 50],
            [['menu_id'], 'exist', 'skipOnError' => true, 'targetClass' => Menu::className(), 'targetAttribute' => ['menu_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/menu-item', 'ID'),
            'tid' => Yii::t('app/menu-item', 'Tid'),
            'menu_id' => Yii::t('app/menu-item', 'Menu ID'),
            'menu_item_id' => Yii::t('app/menu-item', 'Menu Item ID'),
            'layout' => Yii::t('app/menu-item', 'Layout'),
            'template' => Yii::t('app/menu-item', 'Template'),
            'name' => Yii::t('app/menu-item', 'Name'),
            'header' => Yii::t('app/menu-item', 'Header'),
            'sub_header' => Yii::t('app/menu-item', 'Sub Header'),
            'title' => Yii::t('app/menu-item', 'Title'),
            'meta_keywords' => Yii::t('app/menu-item', 'Meta Keywords'),
            'meta_description' => Yii::t('app/menu-item', 'Meta Description'),
            'link' => Yii::t('app/menu-item', 'Link'),
            'position' => Yii::t('app/menu-item', 'Position'),
            'active' => Yii::t('app/menu-item', 'Active'),
            'depth' => Yii::t('app/menu-item', 'Depth'),
            'rb' => Yii::t('app/menu-item', 'Rb'),
            'lb' => Yii::t('app/menu-item', 'Lb'),
            'image' => Yii::t('app/menu-item', 'Image'),
            'image_width' => Yii::t('app/menu-item', 'Image Width'),
            'image_height' => Yii::t('app/menu-item', 'Image Height'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContents()
    {
        return $this->hasMany(Content::className(), ['menu_item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformations()
    {
        return $this->hasMany(Information::className(), ['menu_item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenu()
    {
        return $this->hasOne(Menu::className(), ['id' => 'menu_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuItemBlocks()
    {
        return $this->hasMany(MenuItemBlock::className(), ['menu_item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuItemContents()
    {
        return $this->hasMany(MenuItemContent::className(), ['menu_item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuItemRelations()
    {
        return $this->hasMany(MenuItemRelation::className(), ['menu_item_id' => 'id']);
    }
}
