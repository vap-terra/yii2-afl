<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form_completed".
 *
 * @property integer $id
 * @property integer $form_id
 * @property string $elements
 * @property string $dt_creation
 * @property string $dt_view
 * @property integer $processed
 * @property string $dt_processed
 * @property string $ip
 * @property integer $notify_user_email
 * @property integer $notify_user_sms
 *
 * @property Form $form
 */
class FormCompleted extends \app\models\BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'form_completed';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['form_id'], 'required'],
            [['form_id', 'processed', 'notify_user_email', 'notify_user_sms'], 'integer'],
            [['elements'], 'string'],
            [['dt_creation', 'dt_view', 'dt_processed'], 'safe'],
            [['ip'], 'string', 'max' => 30],
            [['form_id'], 'exist', 'skipOnError' => true, 'targetClass' => Form::className(), 'targetAttribute' => ['form_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/form-completed', 'ID'),
            'form_id' => Yii::t('app/form-completed', 'Form ID'),
            'elements' => Yii::t('app/form-completed', 'Elements'),
            'dt_creation' => Yii::t('app/form-completed', 'Dt Creation'),
            'dt_view' => Yii::t('app/form-completed', 'Dt View'),
            'processed' => Yii::t('app/form-completed', 'Processed'),
            'dt_processed' => Yii::t('app/form-completed', 'Dt Processed'),
            'ip' => Yii::t('app/form-completed', 'Ip'),
            'notify_user_email' => Yii::t('app/form-completed', 'Notify User Email'),
            'notify_user_sms' => Yii::t('app/form-completed', 'Notify User Sms'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForm()
    {
        return $this->hasOne(Form::className(), ['id' => 'form_id']);
    }
}
