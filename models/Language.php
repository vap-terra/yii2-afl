<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "language".
 *
 * @property integer $id
 * @property string $tid
 * @property string $locale
 * @property string $charset
 * @property string $name
 * @property integer $default
 * @property integer $active
 * @property string $dt_update
 * @property string $dt_creation
 *
 * @property User[] $users
 */
class Language extends BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'language';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tid', 'locale', 'charset', 'name', 'dt_update', 'dt_creation'], 'required'],
            [['default', 'active'], 'integer'],
            [['dt_update', 'dt_creation'], 'safe'],
            [['tid'], 'string', 'max' => 3],
            [['locale'], 'string', 'max' => 20],
            [['charset'], 'string', 'max' => 30],
            [['name'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/language', 'ID'),
            'tid' => Yii::t('app/language', 'Tid'),
            'locale' => Yii::t('app/language', 'Locale'),
            'charset' => Yii::t('app/language', 'Charset'),
            'name' => Yii::t('app/language', 'Name'),
            'default' => Yii::t('app/language', 'Default'),
            'active' => Yii::t('app/language', 'Active'),
            'dt_update' => Yii::t('app/language', 'Dt Update'),
            'dt_creation' => Yii::t('app/language', 'Dt Creation'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['language_id' => 'id']);
    }
}
