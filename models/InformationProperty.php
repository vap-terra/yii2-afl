<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "information_property".
 *
 * @property integer $id
 * @property integer $information_id
 * @property string $type
 * @property integer $list_value_id
 * @property string $name
 * @property string $title
 * @property integer $multiple
 *
 * @property InformationItemGroupProperty[] $informationItemGroupProperties
 * @property InformationItemPropertyValue[] $informationItemPropertyValues
 * @property Information $information
 * @property InformationPropertyValueFile[] $informationPropertyValueFiles
 * @property InformationPropertyValueInteger[] $informationPropertyValueIntegers
 * @property InformationPropertyValueString[] $informationPropertyValueStrings
 * @property InformationPropertyValueText[] $informationPropertyValueTexts
 */
class InformationProperty extends BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'information_property';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['information_id', 'name', 'title'], 'required'],
            [['information_id', 'list_value_id', 'multiple'], 'integer'],
            [['type'], 'string'],
            [['name', 'title'], 'string', 'max' => 255],
            [['information_id'], 'exist', 'skipOnError' => true, 'targetClass' => Information::className(), 'targetAttribute' => ['information_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/information-property', 'ID'),
            'information_id' => Yii::t('app/information-property', 'Information ID'),
            'type' => Yii::t('app/information-property', 'Type'),
            'list_value_id' => Yii::t('app/information-property', 'List Value ID'),
            'name' => Yii::t('app/information-property', 'Name'),
            'title' => Yii::t('app/information-property', 'Title'),
            'multiple' => Yii::t('app/information-property', 'Multiple'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformationItemGroupProperties()
    {
        return $this->hasMany(InformationItemGroupProperty::className(), ['information_property_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformationItemPropertyValues()
    {
        return $this->hasMany(InformationItemPropertyValue::className(), ['information_property_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformation()
    {
        return $this->hasOne(Information::className(), ['id' => 'information_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformationPropertyValueFiles()
    {
        return $this->hasMany(InformationPropertyValueFile::className(), ['information_property_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformationPropertyValueIntegers()
    {
        return $this->hasMany(InformationPropertyValueInteger::className(), ['information_property_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformationPropertyValueStrings()
    {
        return $this->hasMany(InformationPropertyValueString::className(), ['information_property_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformationPropertyValueTexts()
    {
        return $this->hasMany(InformationPropertyValueText::className(), ['information_property_id' => 'id']);
    }
}
