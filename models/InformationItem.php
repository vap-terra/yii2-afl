<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "information_item".
 *
 * @property integer $id
 * @property string $tid
 * @property integer $information_id
 * @property integer $information_group_id
 * @property string $name
 * @property string $description
 * @property string $content
 * @property string $header
 * @property string $sub_header
 * @property string $title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property string $image
 * @property integer $image_width
 * @property integer $image_height
 * @property integer $position
 * @property integer $active
 * @property string $dt_beginning
 * @property string $dt_ending
 * @property integer $creator_user_id
 * @property string $dt_creation
 * @property string $ip_creation
 * @property integer $updater_user_id
 * @property string $dt_update
 * @property string $ip_update
 *
 * @property Information $information
 * @property InformationGroup $informationGroup
 * @property InformationItemGroupProperty[] $informationItemGroupProperties
 * @property InformationItemPropertyValue[] $informationItemPropertyValues
 * @property InformationPropertyValueFile[] $informationPropertyValueFiles
 * @property InformationPropertyValueInteger[] $informationPropertyValueIntegers
 * @property InformationPropertyValueString[] $informationPropertyValueStrings
 * @property InformationPropertyValueText[] $informationPropertyValueTexts
 */
class InformationItem extends BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'information_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['information_id', 'name'], 'required'],
            [['information_id', 'information_group_id', 'image_width', 'image_height', 'position', 'active', 'creator_user_id', 'updater_user_id'], 'integer'],
            [['description', 'content'], 'string'],
            [['dt_beginning', 'dt_ending', 'dt_creation', 'dt_update'], 'safe'],
            [['tid'], 'string', 'max' => 100],
            [['name', 'header', 'sub_header', 'title', 'meta_keywords', 'meta_description', 'image'], 'string', 'max' => 255],
            [['ip_creation', 'ip_update'], 'string', 'max' => 30],
            [['information_id'], 'exist', 'skipOnError' => true, 'targetClass' => Information::className(), 'targetAttribute' => ['information_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/information-item', 'ID'),
            'tid' => Yii::t('app/information-item', 'Tid'),
            'information_id' => Yii::t('app/information-item', 'Information ID'),
            'information_group_id' => Yii::t('app/information-item', 'Information Group ID'),
            'name' => Yii::t('app/information-item', 'Name'),
            'description' => Yii::t('app/information-item', 'Description'),
            'content' => Yii::t('app/information-item', 'Content'),
            'header' => Yii::t('app/information-item', 'Header'),
            'sub_header' => Yii::t('app/information-item', 'Sub Header'),
            'title' => Yii::t('app/information-item', 'Title'),
            'meta_keywords' => Yii::t('app/information-item', 'Meta Keywords'),
            'meta_description' => Yii::t('app/information-item', 'Meta Description'),
            'image' => Yii::t('app/information-item', 'Image'),
            'image_width' => Yii::t('app/information-item', 'Image Width'),
            'image_height' => Yii::t('app/information-item', 'Image Height'),
            'position' => Yii::t('app/information-item', 'Position'),
            'active' => Yii::t('app/information-item', 'Active'),
            'dt_beginning' => Yii::t('app/information-item', 'Dt Beginning'),
            'dt_ending' => Yii::t('app/information-item', 'Dt Ending'),
            'creator_user_id' => Yii::t('app/information-item', 'Creator User ID'),
            'dt_creation' => Yii::t('app/information-item', 'Dt Creation'),
            'ip_creation' => Yii::t('app/information-item', 'Ip Creation'),
            'updater_user_id' => Yii::t('app/information-item', 'Updater User ID'),
            'dt_update' => Yii::t('app/information-item', 'Dt Update'),
            'ip_update' => Yii::t('app/information-item', 'Ip Update'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformation()
    {
        return $this->hasOne(Information::className(), ['id' => 'information_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformationGroup()
    {
        return $this->hasOne(InformationGroup::className(), ['id' => 'information_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformationItemGroupProperties()
    {
        return $this->hasMany(InformationItemGroupProperty::className(), ['information_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformationItemPropertyValues()
    {
        return $this->hasMany(InformationItemPropertyValue::className(), ['information_item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformationPropertyValueFiles()
    {
        return $this->hasMany(InformationPropertyValueFile::className(), ['information_item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformationPropertyValueIntegers()
    {
        return $this->hasMany(InformationPropertyValueInteger::className(), ['information_item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformationPropertyValueStrings()
    {
        return $this->hasMany(InformationPropertyValueString::className(), ['information_item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformationPropertyValueTexts()
    {
        return $this->hasMany(InformationPropertyValueText::className(), ['information_item_id' => 'id']);
    }
}
