<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "list_value".
 *
 * @property integer $id
 * @property string $name
 *
 * @property ListValueItem[] $listValueItems
 */
class ListValue extends BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'list_value';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/models', 'ID'),
            'name' => Yii::t('app/models', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getListValueItems()
    {
        return $this->hasMany(ListValueItem::className(), ['list_value_id' => 'id']);
    }
}
