<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 04.03.16
 * Time: 6:33
 */

namespace app\models;


use app\components\Util;
use Yii;
use yii\db\ActiveRecord;
use yii\web\NotFoundHttpException;

class BaseActiveRecord extends ActiveRecord
{
    const SCENARIO_UPDATE = 'update';
    const SCENARIO_INSERT = 'insert';
    const SCENARIO_SEARCH = 'search';

    /**
     * @return array
     */
    public static function getArMoneyFormats()
    {
        return [
            '### #.##0 C'=>'1 000 Руб',
            '### #.##2 C'=>'1 000.00 Руб',
            '####.##0 C'=>'1000 Руб',
            '####.##2 C'=>'1000.00 Руб',
            '### #,##2 C'=>'1 000,00 Руб',
            '####,##2 C'=>'1000,00 Руб',
        ];
    }

    /**
     * @return array
     */
    public static function getArDateFormats()
    {
        return [
            'd F Y'=>'01 января 2016',
            'd.m.Y'=>'01.01.2016',
            'd/m/Y'=>'01/01/2016',
            'd.m.y'=>'01.01.16',
            'd/m/y'=>'01/01/16',
        ];
    }

    /**
     * @var $_baseName string
     */
    protected $_baseName;

    /**
     * @return string
     */
    public function getBaseName()
    {
        if( empty( $this->_baseName ) ){
            $this->_baseName = Util::getFileNameByClass(
                strtr(
                    get_called_class(),
                    [
                        'Ext'=>''
                    ]
                )
            );

        }
        return $this->_baseName;
    }

    /**
     * @param $attribute
     * @return bool
     */
    public function switchBoolean($attribute)
    {
        if( $this->hasAttribute($attribute) ){
            $this->{$attribute} = 1 - $this->{$attribute};
            return $this->save();
        }
        $this->addError($attribute,'Not exist attribute '. $attribute);
        return false;
    }

    public function getReplacementTagsValues()
    {
        $replacementTagsValues = [];
        foreach ($this->attributes as $attribute=>$value) {
            $replacementTagsValues['{'.$attribute.'}'] = $value;
        }
        return $replacementTagsValues;
    }

    /**
     * @param $attribute
     * @param int $default
     * @param string|array|null $condition
     * @return int|mixed
     */
    public static function getMax($attribute,$default=0,$condition=null)
    {
        $query = self::find();
        if( !empty( $condition ) ){
            $query->andWhere($condition);
        }
        $max = $query->max($attribute);
        if( empty( $max ) ){
            $max = $default;
        }
        return $max;
    }

    /**
     * @param $attribute
     * @param int $default
     * @return int|mixed
     */
    public static function getMin($attribute,$default=0)
    {
        $min = self::find()->min($attribute);
        if( empty( $min ) ){
            $min = $default;
        }
        return $min;
    }

    public static $PrivilegedFileUploadErrors = [
        0=>'There is no error, the file{file} uploaded with success',
        1=>'The uploaded file{file} exceeds the upload_max_filesize directive in php.ini',
        2=>'The uploaded file{file} exceeds the MAX_FILE_SIZE directive that was specified in the HTML form',
        3=>'The uploaded file{file} was only partially uploaded',
        4=>'No file was uploaded',
        6=>'When upload file{file} missing a temporary folder',
        7=>'Failed to write file{file} to disk.',
        8=>'A PHP extension stopped the file{file} upload.',
    ];

    public static $FileUploadErrors = [
        0=>'There is no error, the file{file} uploaded with success',
        1=>'The uploaded file{file} is large',
        2=>'The uploaded file{file} is large',
        3=>'This file{file} cannot be uploaded',
        4=>'No file was uploaded',
        6=>'This file{file} cannot be uploaded.',
        7=>'This file{file} cannot be uploaded.',
        8=>'This file{file} cannot be uploaded.',
    ];

    public static function getErrorUploadMessage( $errorNum, $fileName='' )
    {
        $error = Yii::t('app/models','Error upload file!');
        if( !Yii::$app->user->isGuest && !Yii::$app->user->identity->isUser ) {
            if( isset( self::$PrivilegedFileUploadErrors[$errorNum] ) ) {
                $error = Yii::t('app/models',self::$PrivilegedFileUploadErrors[$errorNum],['file'=>' '.$fileName]);
            }
        } else {
            if( isset( self::$FileUploadErrors[$errorNum] ) ) {
                $error = Yii::t('app/models',self::$FileUploadErrors[$errorNum],['file'=>' '.$fileName]);
            }
        }
        return $error;
    }

    /**
     * @param $id
     * @return null|static
     * @throws NotFoundHttpException
     */
    public static function getById( $id )
    {
        $model = self::findOne(['id'=>(int)$id]);
        if( empty($model) ){
            throw new NotFoundHttpException('Not found element: '.$id);
        }
        return $model;
    }

    /**
     * @param int | string $id
     * @return array|null|\yii\db\ActiveRecord
     * @throws NotFoundHttpException
     */
    public static function getByTid( $id )
    {
        if( preg_match( '/^[\d]+$/', $id ) ) {
            $model = self::findOne(['id'=>(int)$id]);
        } else {
            $model = self::findOne(['tid'=>$id]);
        }
        if( empty($model) ){
            throw new NotFoundHttpException('Not found element: '.$id);
        }
        return $model;
    }

    public static function find()
    {
        return Yii::createObject(BaseActiveQuery::className(), [get_called_class()]);
    }

}