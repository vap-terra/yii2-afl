<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "banner_group".
 *
 * @property integer $id
 * @property string $name
 * @property integer $active
 */
class BannerGroup extends BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'banner_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['active'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/banner-group', 'ID'),
            'name' => Yii::t('app/banner-group', 'Name'),
            'active' => Yii::t('app/banner-group', 'Active'),
        ];
    }
}
