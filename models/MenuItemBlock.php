<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "menu_item_block".
 *
 * @property integer $id
 * @property integer $menu_item_id
 * @property integer $block_id
 * @property string $template
 * @property integer $position
 * @property integer $active
 * @property integer $show_header
 * @property integer $show_description
 *
 * @property MenuItem $menuItem
 * @property Block $block
 */
class MenuItemBlock extends BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu_item_block';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['menu_item_id', 'block_id', 'template', 'position'], 'required'],
            [['menu_item_id', 'block_id', 'position', 'active', 'show_header', 'show_description'], 'integer'],
            [['template'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/models', 'ID'),
            'menu_item_id' => Yii::t('app/models', 'Menu Item ID'),
            'block_id' => Yii::t('app/models', 'Block ID'),
            'template' => Yii::t('app/models', 'Template'),
            'position' => Yii::t('app/models', 'Position'),
            'active' => Yii::t('app/models', 'Active'),
            'show_header' => Yii::t('app/models', 'Show Header'),
            'show_description' => Yii::t('app/models', 'Show Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuItem()
    {
        return $this->hasOne(MenuItem::className(), ['id' => 'menu_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlock()
    {
        return $this->hasOne(Block::className(), ['id' => 'block_id']);
    }
}
