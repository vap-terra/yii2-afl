<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "unit".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 *
 * @property Shop[] $shops
 * @property Shop[] $shops0
 * @property Shop[] $shops1
 * @property ShopItem[] $shopItems
 * @property ShopItem[] $shopItems0
 * @property ShopItem[] $shopItems1
 */
class Unit extends \app\models\BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'unit';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/unit', 'ID'),
            'name' => Yii::t('app/unit', 'Name'),
            'description' => Yii::t('app/unit', 'Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShops()
    {
        return $this->hasMany(Shop::className(), ['weight_unit_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShops0()
    {
        return $this->hasMany(Shop::className(), ['lwh_unit_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShops1()
    {
        return $this->hasMany(Shop::className(), ['quantity_unit_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShopItems()
    {
        return $this->hasMany(ShopItem::className(), ['weight_unit_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShopItems0()
    {
        return $this->hasMany(ShopItem::className(), ['lwh_unit_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShopItems1()
    {
        return $this->hasMany(ShopItem::className(), ['quantity_unit_id' => 'id']);
    }
}
