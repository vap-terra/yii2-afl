<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "information_item_group_property".
 *
 * @property integer $id
 * @property integer $information_id
 * @property integer $information_group_id
 * @property integer $information_property_id
 * @property integer $recursive
 *
 * @property InformationItem $information
 * @property InformationGroup $informationGroup
 * @property InformationProperty $informationProperty
 */
class InformationItemGroupProperty extends BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'information_item_group_property';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['information_id', 'information_group_id', 'information_property_id'], 'required'],
            [['information_id', 'information_group_id', 'information_property_id', 'recursive'], 'integer'],
            [['information_id'], 'exist', 'skipOnError' => true, 'targetClass' => InformationItem::className(), 'targetAttribute' => ['information_id' => 'id']],
            [['information_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => InformationGroup::className(), 'targetAttribute' => ['information_group_id' => 'id']],
            [['information_property_id'], 'exist', 'skipOnError' => true, 'targetClass' => InformationProperty::className(), 'targetAttribute' => ['information_property_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/information-item-group-property', 'ID'),
            'information_id' => Yii::t('app/information-item-group-property', 'Information ID'),
            'information_group_id' => Yii::t('app/information-item-group-property', 'Information Group ID'),
            'information_property_id' => Yii::t('app/information-item-group-property', 'Information Property ID'),
            'recursive' => Yii::t('app/information-item-group-property', 'Recursive'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformation()
    {
        return $this->hasOne(InformationItem::className(), ['id' => 'information_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformationGroup()
    {
        return $this->hasOne(InformationGroup::className(), ['id' => 'information_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformationProperty()
    {
        return $this->hasOne(InformationProperty::className(), ['id' => 'information_property_id']);
    }
}
