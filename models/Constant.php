<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "constant".
 *
 * @property integer $id
 * @property string $tid
 * @property string $value
 * @property string $name
 * @property string $description
 * @property integer $system
 */
class Constant extends \app\models\BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'constant';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tid', 'name'], 'required'],
            [['value'], 'string'],
            [['system'], 'integer'],
            [['tid', 'name'], 'string', 'max' => 100],
            [['description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/constant', 'ID'),
            'tid' => Yii::t('app/constant', 'Tid'),
            'value' => Yii::t('app/constant', 'Value'),
            'name' => Yii::t('app/constant', 'Name'),
            'description' => Yii::t('app/constant', 'Description'),
            'system' => Yii::t('app/constant', 'System'),
        ];
    }
}
