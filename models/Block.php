<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "block".
 *
 * @property integer $id
 * @property string $type
 * @property string $widget
 * @property string $template
 * @property string $parameters
 * @property string $name
 * @property string $header
 * @property string $description
 * @property string $content
 * @property integer $position
 * @property integer $active
 *
 * @property MenuItemBlock[] $menuItemBlocks
 */
class Block extends BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'block';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'parameters', 'description', 'content'], 'string'],
            [['parameters', 'name'], 'required'],
            [['position', 'active'], 'integer'],
            [['widget'], 'string', 'max' => 100],
            [['template'], 'string', 'max' => 50],
            [['name', 'header'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/block', 'ID'),
            'type' => Yii::t('app/block', 'Type'),
            'widget' => Yii::t('app/block', 'Widget'),
            'template' => Yii::t('app/block', 'Template'),
            'parameters' => Yii::t('app/block', 'Parameters'),
            'name' => Yii::t('app/block', 'Name'),
            'header' => Yii::t('app/block', 'Header'),
            'description' => Yii::t('app/block', 'Description'),
            'content' => Yii::t('app/block', 'Content'),
            'position' => Yii::t('app/block', 'Position'),
            'active' => Yii::t('app/block', 'Active'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuItemBlocks()
    {
        return $this->hasMany(MenuItemBlock::className(), ['block_id' => 'id']);
    }
}
