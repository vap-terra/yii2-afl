<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "geo_region".
 *
 * @property integer $id
 * @property string $name
 * @property integer $geo_country_id
 *
 * @property GeoLocality[] $geoLocalities
 * @property GeoLocalityArea[] $geoLocalityAreas
 * @property GeoCountry $geoCountry
 * @property GeoRegionArea[] $geoRegionAreas
 */
class GeoRegion extends BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'geo_region';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'geo_country_id'], 'required'],
            [['geo_country_id'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/models', 'ID'),
            'name' => Yii::t('app/models', 'Name'),
            'geo_country_id' => Yii::t('app/models', 'Geo Country ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGeoLocalities()
    {
        return $this->hasMany(GeoLocality::className(), ['geo_region_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGeoLocalityAreas()
    {
        return $this->hasMany(GeoLocalityArea::className(), ['geo_region_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGeoCountry()
    {
        return $this->hasOne(GeoCountry::className(), ['id' => 'geo_country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGeoRegionAreas()
    {
        return $this->hasMany(GeoRegionArea::className(), ['geo_region_id' => 'id']);
    }
}
