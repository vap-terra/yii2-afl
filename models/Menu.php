<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "menu".
 *
 * @property integer $id
 * @property string $tid
 * @property string $title
 * @property integer $active
 * @property integer $position
 * @property string $css_icon
 *
 * @property MenuItem[] $menuItems
 */
class Menu extends BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tid', 'title'], 'required'],
            [['active', 'position'], 'integer'],
            [['tid'], 'string', 'max' => 50],
            [['title'], 'string', 'max' => 255],
            [['css_icon'], 'string', 'max' => 100],
            [['tid'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/menu', 'ID'),
            'tid' => Yii::t('app/menu', 'Tid'),
            'title' => Yii::t('app/menu', 'Title'),
            'active' => Yii::t('app/menu', 'Active'),
            'position' => Yii::t('app/menu', 'Position'),
            'css_icon' => Yii::t('app/menu', 'Css Icon'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuItems()
    {
        return $this->hasMany(MenuItem::className(), ['menu_id' => 'id']);
    }
}
