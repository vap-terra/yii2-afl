<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "block_group_link".
 *
 * @property integer $block_id
 * @property integer $block_group_id
 * @property integer $position
 * @property integer $active
 *
 * @property Block $block
 * @property BlockGroup $blockGroup
 */
class BlockGroupLink extends BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'block_group_link';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['block_id', 'block_group_id', 'position', 'active'], 'required'],
            [['block_id', 'block_group_id', 'position', 'active'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'block_id' => Yii::t('app/models', 'Block ID'),
            'block_group_id' => Yii::t('app/models', 'Block Group ID'),
            'position' => Yii::t('app/models', 'Position'),
            'active' => Yii::t('app/models', 'Active'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlock()
    {
        return $this->hasOne(Block::className(), ['id' => 'block_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlockGroup()
    {
        return $this->hasOne(BlockGroup::className(), ['id' => 'block_group_id']);
    }
}
