<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "content".
 *
 * @property integer $id
 * @property integer $menu_item_id
 * @property string $name
 * @property string $content
 *
 * @property MenuItem $menuItem
 * @property MenuItemContent[] $menuItemContents
 */
class Content extends \app\models\BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'content';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['menu_item_id'], 'integer'],
            [['content'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['menu_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => MenuItem::className(), 'targetAttribute' => ['menu_item_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/content', 'ID'),
            'menu_item_id' => Yii::t('app/content', 'Menu Item ID'),
            'name' => Yii::t('app/content', 'Name'),
            'content' => Yii::t('app/content', 'Content'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuItem()
    {
        return $this->hasOne(MenuItem::className(), ['id' => 'menu_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuItemContents()
    {
        return $this->hasMany(MenuItemContent::className(), ['content_id' => 'id']);
    }
}
