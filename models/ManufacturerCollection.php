<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "manufacturer_collection".
 *
 * @property integer $id
 * @property string $tid
 * @property integer $manufacturer_id
 * @property string $name
 * @property string $image
 * @property integer $image_width
 * @property integer $image_height
 * @property string $year
 *
 * @property Manufacturer $manufacturer
 * @property ShopItem[] $shopItems
 */
class ManufacturerCollection extends \app\models\BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'manufacturer_collection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['manufacturer_id', 'name'], 'required'],
            [['manufacturer_id', 'image_width', 'image_height'], 'integer'],
            [['year'], 'safe'],
            [['tid', 'name', 'image'], 'string', 'max' => 255],
            [['manufacturer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Manufacturer::className(), 'targetAttribute' => ['manufacturer_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/manufacturer-collection', 'ID'),
            'tid' => Yii::t('app/manufacturer-collection', 'Tid'),
            'manufacturer_id' => Yii::t('app/manufacturer-collection', 'Manufacturer ID'),
            'name' => Yii::t('app/manufacturer-collection', 'Name'),
            'image' => Yii::t('app/manufacturer-collection', 'Image'),
            'image_width' => Yii::t('app/manufacturer-collection', 'Image Width'),
            'image_height' => Yii::t('app/manufacturer-collection', 'Image Height'),
            'year' => Yii::t('app/manufacturer-collection', 'Year'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManufacturer()
    {
        return $this->hasOne(Manufacturer::className(), ['id' => 'manufacturer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShopItems()
    {
        return $this->hasMany(ShopItem::className(), ['manufacturer_collection_id' => 'id']);
    }
}
