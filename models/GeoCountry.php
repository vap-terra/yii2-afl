<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "geo_country".
 *
 * @property integer $id
 * @property string $name
 * @property string $tid
 * @property string $tid2
 * @property string $iso
 * @property string $phone_code
 *
 * @property BoardItem[] $boardItems
 * @property GeoLocality[] $geoLocalities
 */
class GeoCountry extends BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'geo_country';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'tid', 'phone_code'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['tid'], 'string', 'max' => 2],
            [['tid2'], 'string', 'max' => 3],
            [['iso'], 'string', 'max' => 4],
            [['phone_code'], 'string', 'max' => 1]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/models', 'ID'),
            'name' => Yii::t('app/models', 'Name'),
            'tid' => Yii::t('app/models', 'Tid'),
            'tid2' => Yii::t('app/models', 'Tid2'),
            'iso' => Yii::t('app/models', 'Iso'),
            'phone_code' => Yii::t('app/models', 'Phone Code'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGeoRegions()
    {
        return $this->hasMany(GeoRegion::className(), ['geo_country_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGeoRegionAreas()
    {
        return $this->hasMany(GeoRegionArea::className(), ['geo_country_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGeoLocalities()
    {
        return $this->hasMany(GeoLocality::className(), ['geo_country_id' => 'id']);
    }
}
