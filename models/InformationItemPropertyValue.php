<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "information_item_property_value".
 *
 * @property integer $id
 * @property integer $information_item_id
 * @property integer $information_property_id
 * @property integer $information_property_value_id
 *
 * @property InformationItem $informationItem
 * @property InformationProperty $informationProperty
 */
class InformationItemPropertyValue extends BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'information_item_property_value';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['information_item_id', 'information_property_id', 'information_property_value_id'], 'required'],
            [['information_item_id', 'information_property_id', 'information_property_value_id'], 'integer'],
            [['information_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => InformationItem::className(), 'targetAttribute' => ['information_item_id' => 'id']],
            [['information_property_id'], 'exist', 'skipOnError' => true, 'targetClass' => InformationProperty::className(), 'targetAttribute' => ['information_property_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/information-item-property', 'ID'),
            'information_item_id' => Yii::t('app/information-item-property', 'Information Item ID'),
            'information_property_id' => Yii::t('app/information-item-property', 'Information Property ID'),
            'information_property_value_id' => Yii::t('app/information-item-property', 'Information Property Value ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformationItem()
    {
        return $this->hasOne(InformationItem::className(), ['id' => 'information_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformationProperty()
    {
        return $this->hasOne(InformationProperty::className(), ['id' => 'information_property_id']);
    }
}
