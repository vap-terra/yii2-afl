<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form".
 *
 * @property integer $id
 * @property string $tid
 * @property string $name
 * @property string $header
 * @property string $header_hide
 * @property string $mark_hide
 * @property string $button_title
 * @property string $description
 * @property integer $notify_user_email
 * @property integer $notify_admin_email
 * @property integer $notify_user_sms
 * @property integer $notify_admin_sms
 * @property string $action
 * @property string $css_class
 * @property string $css_icon
 * @property string $email
 * @property string $phone
 * @property string $subject_admin
 * @property string $template_admin
 * @property string $subject_user
 * @property string $template_user
 * @property string $sms_admin_text
 * @property string $sms_user_text
 * @property string $field_phone
 * @property string $field_email
 * @property integer $transliteration_sms
 * @property integer $use_yandex_target_success
 * @property integer $use_yandex_target_button
 * @property integer $use_google_target_success
 * @property integer $use_google_target_button
 * @property integer $target_separately
 * @property string $prefix_target_success
 * @property string $prefix_target_button
 * @property integer $active
 * @property integer $collect_data
 * @property integer $position
 *
 * @property FormCompleted[] $formCompleteds
 * @property FormElement[] $formElements
 */
class Form extends \app\models\BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'form';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'button_title'], 'required'],
            [['description'], 'string'],
            [['header_hide', 'mark_hide', 'notify_user_email', 'notify_admin_email', 'notify_user_sms', 'notify_admin_sms', 'transliteration_sms', 'use_yandex_target_success', 'use_yandex_target_button', 'use_google_target_success', 'use_google_target_button', 'target_separately', 'active', 'collect_data', 'position'], 'integer'],
            [['tid', 'name', 'header', 'action', 'css_class', 'email', 'phone', 'template_admin', 'subject_user', 'template_user', 'sms_admin_text', 'sms_user_text', 'field_phone', 'field_email'], 'string', 'max' => 255],
            [['button_title'], 'string', 'max' => 150],
            [['css_icon', 'prefix_target_success', 'prefix_target_button'], 'string', 'max' => 100],
            [['subject_admin'], 'string', 'max' => 205],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/form', 'ID'),
            'tid' => Yii::t('app/form', 'Tid'),
            'name' => Yii::t('app/form', 'Name'),
            'header' => Yii::t('app/form', 'Header'),
            'header_hide' => Yii::t('app/form', 'Header Hide'),
            'mark_hide' => Yii::t('app/form', 'Mark Hide'),
            'button_title' => Yii::t('app/form', 'Button Title'),
            'description' => Yii::t('app/form', 'Description'),
            'notify_user_email' => Yii::t('app/form', 'Notify User Email'),
            'notify_admin_email' => Yii::t('app/form', 'Notify Admin Email'),
            'notify_user_sms' => Yii::t('app/form', 'Notify User Sms'),
            'notify_admin_sms' => Yii::t('app/form', 'Notify Admin Sms'),
            'action' => Yii::t('app/form', 'Action'),
            'css_class' => Yii::t('app/form', 'Css Class'),
            'css_icon' => Yii::t('app/form', 'Css Icon'),
            'email' => Yii::t('app/form', 'Email'),
            'phone' => Yii::t('app/form', 'Phone'),
            'subject_admin' => Yii::t('app/form', 'Subject Admin'),
            'template_admin' => Yii::t('app/form', 'Template Admin'),
            'subject_user' => Yii::t('app/form', 'Subject User'),
            'template_user' => Yii::t('app/form', 'Template User'),
            'sms_admin_text' => Yii::t('app/form', 'Sms Admin Text'),
            'sms_user_text' => Yii::t('app/form', 'Sms User Text'),
            'field_phone' => Yii::t('app/form', 'Field Phone'),
            'field_email' => Yii::t('app/form', 'Field Email'),
            'transliteration_sms' => Yii::t('app/form', 'Transliteration Sms'),
            'use_yandex_target_success' => Yii::t('app/form', 'Use Yandex Target Success'),
            'use_yandex_target_button' => Yii::t('app/form', 'Use Yandex Target Button'),
            'use_google_target_success' => Yii::t('app/form', 'Use Google Target Success'),
            'use_google_target_button' => Yii::t('app/form', 'Use Google Target Button'),
            'target_separately' => Yii::t('app/form', 'Target Separately'),
            'prefix_target_success' => Yii::t('app/form', 'Prefix Target Success'),
            'prefix_target_button' => Yii::t('app/form', 'Prefix Target Button'),
            'active' => Yii::t('app/form', 'Active'),
            'collect_data' => Yii::t('app/form', 'Collect Data'),
            'position' => Yii::t('app/form', 'Position'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFormCompleteds()
    {
        return $this->hasMany(FormCompleted::className(), ['form_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFormElements()
    {
        return $this->hasMany(FormElement::className(), ['form_id' => 'id']);
    }
}
