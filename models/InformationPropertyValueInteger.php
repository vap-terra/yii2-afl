<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "information_property_value_integer".
 *
 * @property integer $id
 * @property integer $information_property_id
 * @property integer $information_item_id
 * @property integer $value
 * @property string $description
 *
 * @property InformationItem $informationItem
 * @property InformationProperty $informationProperty
 */
class InformationPropertyValueInteger extends BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'information_property_value_integer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['information_property_id', 'information_item_id', 'value'], 'required'],
            [['information_property_id', 'information_item_id', 'value'], 'integer'],
            [['description'], 'string', 'max' => 255],
            [['information_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => InformationItem::className(), 'targetAttribute' => ['information_item_id' => 'id']],
            [['information_property_id'], 'exist', 'skipOnError' => true, 'targetClass' => InformationProperty::className(), 'targetAttribute' => ['information_property_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/information-property-value', 'ID'),
            'information_property_id' => Yii::t('app/information-property-value', 'Information Property ID'),
            'information_item_id' => Yii::t('app/information-property-value', 'Information Item ID'),
            'value' => Yii::t('app/information-property-value', 'Value'),
            'description' => Yii::t('app/information-property-value', 'Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformationItem()
    {
        return $this->hasOne(InformationItem::className(), ['id' => 'information_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformationProperty()
    {
        return $this->hasOne(InformationProperty::className(), ['id' => 'information_property_id']);
    }
}
