<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "block_group".
 *
 * @property integer $id
 * @property string $tid
 * @property string $name
 * @property integer $position
 * @property integer $active
 */
class BlockGroup extends BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'block_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tid', 'name', 'position'], 'required'],
            [['position', 'active'], 'integer'],
            [['tid'], 'string', 'max' => 50],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/models', 'ID'),
            'tid' => Yii::t('app/models', 'Tid'),
            'name' => Yii::t('app/models', 'Name'),
            'position' => Yii::t('app/models', 'Position'),
            'active' => Yii::t('app/models', 'Active'),
        ];
    }
}
