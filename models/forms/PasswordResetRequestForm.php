<?php
namespace app\models\forms;

use app\models\ext\UserExt;
use yii\base\Model;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => '\app\models\ext\UserExt',
                'filter' => [ 'is_blocked' => 0 ],
                'message' => 'There is no user with such email.'
            ],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     */
    public function sendEmail()
    {
        /* @var $oUser UserExt */
        $oUser = UserExt::findOne([
            'is_blocked' => 0,
            'email' => $this->email,
        ]);

        if ($oUser) {
            if (!UserExt::isPasswordResetTokenValid($oUser->password_reset_token)) {
                $oUser->generatePasswordResetToken();
            }

            if ($oUser->save()) {
                return \Yii::$app->mailer->compose(['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'], ['user' => $oUser])
                    ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' robot'])
                    ->setTo($this->email)
                    ->setSubject('Password reset for ' . \Yii::$app->name)
                    ->send();
            }
        }

        return false;
    }
}
