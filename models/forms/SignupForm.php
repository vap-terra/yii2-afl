<?php
namespace app\models\forms;

use app\models\ext\UserExt;
use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;

/**
 * Registration form
 *
 * @property string $login
 * @property string $surname
 * @property string $name
 * @property string $patronymic
 * @property integer $gender
 * @property string $phone
 * @property string $email
 * @property string $photo
 * @property integer $language_id
 * @property integer $geo_country_id
 * @property integer $geo_locality_id
 *
 */
class SignupForm extends Model
{
    public $login;

    public $surname;
    public $name;
    public $patronymic;
    public $gender;
    public $phone;
    public $email;
    public $photo;
    public $language_id;
    public $geo_country_id;
    public $geo_locality_id;

    public $password;
    public $repeat_password;

    private $_user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language_id','geo_country_id','geo_locality_id'], 'default', 'value' => 0],

            [['photo'], 'file', 'extensions' => 'gif, jpg, jpe, jpeg, png'],

            ['login', 'filter', 'filter' => 'trim'],
            ['login', 'required'],
            ['login', 'unique', 'targetClass' => '\app\models\ext\UserExt', 'message' => 'This login has already been taken.'],
            ['login', 'string', 'min' => 2, 'max' => 255],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\app\models\ext\UserExt', 'message' => 'This email address has already been taken.'],

            ['phone', 'filter', 'filter' => 'trim'],
            ['phone', 'required'],
            ['phone', 'unique', 'targetClass' => '\app\models\ext\UserExt', 'message' => 'This phone number has already been taken.'],
            ['phone', 'string', 'min' => 2, 'max' => 255],

            ['password', 'required', 'on'=>['changePassword','insert']],
            ['password', 'string', 'min' => 6, 'on'=>['changePassword','insert']],
            ['password', 'validatePassword', 'on'=>['changePassword','insert']],

            ['repeat_password', 'required', 'on'=>['changePassword','insert']],
            ['repeat_password', 'string', 'min' => 6, 'on'=>['changePassword','insert']],

            ['repeat_password', 'compare', 'compareAttribute'=>'password', 'on'=>['changePassword','insert']],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'surname' => Yii::t('app/user', 'Surname'),
            'name' => Yii::t('app/user', 'Name'),
            'patronymic' => Yii::t('app/user', 'Patronymic'),
            'gender' => Yii::t('app/user', 'Gender'),
            'phone' => Yii::t('app/user', 'Phone'),
            'email' => Yii::t('app/user', 'Email'),
            'photo' => Yii::t('app/user', 'Photo'),
            'language_id' => Yii::t('app/user', 'Language ID'),
            'geo_country_id' => Yii::t('app/user', 'Geo Country ID'),
            'geo_locality_id' => Yii::t('app/user', 'Geo Locality ID'),
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'phone'=>[
                'class' => 'yii\behaviors\BlameableBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT =>['phone'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['phone'],
                ],
                'value' => function() { return strtr( $this->phone, ['+'=>'','('=>'',')'=>'',' '=>'','-'=>''] ); },
            ]
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    /**
     * Signs user up.
     *
     * @return UserExt|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = new UserExt();
            $user->login = $this->login;
            $user->email = $this->email;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            if ( $user->save() ) {
                return $user;
            }
        }

        return null;
    }

    /**
     * Finds user by [[username]]
     *
     * @return UserExt|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = UserExt::findByLogin($this->login);
        }

        return $this->_user;
    }
}
