<?php

namespace app\models\forms;

use app\models\ext\UserExt;
use Yii;
use yii\base\Model;
use yii\helpers\BaseFileHelper;
use yii\web\UploadedFile;

class ProfileForm extends Model
{
    public $photo;
    public $login;
    public $password;
    public $new_password;
    public $repeat_password;
    public $email;
    public $phone;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['login', 'filter', 'filter' => 'trim'],
            ['login', 'required', 'on'=>'changeData'],
            ['login', 'checkUnique', 'on'=>'changeData'],
            ['login', 'string', 'min' => 2, 'max' => 255],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required', 'on'=>'changeData'],
            ['email', 'email'],
            ['email', 'checkUnique', 'on'=>'changeData'],

            ['phone', 'filter', 'filter' => 'trim'],
            ['phone', 'required', 'on'=>'changeData'],
            ['phone', 'match', 'on'=>'changeData', 'pattern'=>'/^\+?[0-9]{0,1}\({0,1}[0-9]{3}\){0,1}[ |-]{0,1}[0-9]{3}[ |-]{0,1}[0-9]{2}[ |-]{0,1}[0-9]{2}$/' ],
            ['phone', 'filter', 'filter' => function(){ return strtr( $this->phone, ['+'=>'','('=>'',')'=>'',' '=>'','-'=>''] );} ],
            ['phone', 'checkUnique', 'on'=>'changeData' ],

            ['photo', 'required', 'skipOnEmpty' => true, 'on'=>'loadPhoto'],
            ['photo', 'image', 'maxWidth'=>2048,'maxHeight'=>2048, 'on'=>'loadPhoto'],

            ['password', 'required', 'on'=>'changePassword'],
            ['password', 'string', 'min' => 6, 'on'=>'changePassword'],
            ['password', 'validatePassword', 'on'=>'changePassword'],
            ['new_password', 'required', 'on'=>'changePassword'],
            ['new_password', 'string', 'min' => 6, 'on'=>'changePassword'],
            ['repeat_password', 'required', 'on'=>'changePassword'],
            ['repeat_password', 'string', 'min' => 6, 'on'=>'changePassword'],

            ['repeat_password', 'compare', 'compareAttribute'=>'new_password', 'on'=>'changePassword']
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'photo' => Yii::t('app\models','your photo'),
            'login' => Yii::t('app\models','login'),
            'password' => Yii::t('app\models','password'),
            'new_password' => Yii::t('app\models','new password'),
            'repeat_password' => Yii::t('app\models','repeat new password'),
            'email' => Yii::t('app\models','e-mail'),
            'phone' => Yii::t('app\models','phone number'),

        ];
    }


    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function checkUnique($attribute, $params)
    {
        /* @var $user UserExt */
        /* @var $mUser UserExt */
        /* @var $extUser UserExt */
        $user = Yii::$app->user->identity;
        //$mUser = new UserExt();
        $extUser = $user->find()->where(['=',$attribute,$this->{$attribute}])->one();
        if( !empty( $extUser ) && $extUser->id != $user->id ) {
            $this->addError(
                $attribute,
                Yii::t(
                    'lk/layout',
                    'This {attribute} has already been taken.',
                    [
                        'attribute'=>Yii::t('app\models',$attribute)
                    ]
                )
            );
        }
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            /* @var $user UserExt */
            $user = Yii::$app->user->identity;
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect password.');
            }
        }
    }

    public function changeData()
    {
        /* @var $user UserExt */
        $user = Yii::$app->user->identity;

        $upload = true;
        if( $this->scenario == 'loadPhoto' ) {
            $this->photo = UploadedFile::getInstance($this, 'photo');
            if( empty($this->photo) && !empty($photoTmp = Yii::$app->request->post('photo-tmp')) ) {
                $photoTmpPath = $user->getStorageTmpPath().$photoTmp;
                if( is_file($photoTmpPath) ) {
                    $this->photo = new UploadedFile([
                        'name' => $photoTmp,
                        'tempName' => $photoTmpPath,
                        'type' => BaseFileHelper::getMimeType($photoTmpPath),
                        'size' => filesize($photoTmpPath),
                        'error' => 0,
                    ]);
                    $upload = false;
                    //var_dump( $this->photo );
                }
            }
        }

        $status = -1;
        if( $this->validate() ){
            if( $this->scenario == 'loadPhoto' ) {
                $user->uploadPhoto( $this->photo, $upload);
            } else if( $this->scenario == 'changePassword' ) {
                $user->setPassword($this->new_password);
                Yii::$app->getUser()->login($user);
                $this->password = '';
                $this->new_password = '';
                $this->new_password = '';
            } else {
                $login = $user->login;
                $user->login = $this->login;
                $user->email = $this->email;
                $user->phone = $this->phone;
                if( $login != $user->login ) {
                    Yii::$app->getUser()->login($user);
                }
            }
            $status = $user->save();
            if( $user->hasErrors() ) {
                foreach( $user->errors as $attribute=>$errors ) {
                    foreach( $errors as $error ) {
                        $this->addError($attribute,$error);
                    }
                }
            }
        }
        return $status;
    }

}
