<?php
namespace app\models\forms;

use app\models\ext\LanguageExt;
use app\models\ext\UserExt;
use yii\base\Model;
use Yii;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

/**
 * Signup form
 */
class SignUpForm extends Model
{
    public $photo;
    //public $referral_id;
    public $name;
    public $surname;
    public $patronymic;
    public $gender;
    public $language_id;
    public $geo_country_id;
    public $geo_locality_id;
    public $self_geo_locality;
    public $login;
    public $email;
    public $phone_code;
    public $phone;
    public $password;
    public $repeat_password;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //['partner_id', 'required'],
            //['partner_id', 'string', 'min' => 11, 'max'=>11 ],

            ['login', 'filter', 'filter' => 'trim'],
            ['login', 'required'],
            ['login', 'unique', 'targetClass' => '\app\models\ext\UserExt', 'message' => Yii::t('app\models','This login has already been taken.')],
            ['login', 'string', 'min' => 2, 'max' => 255],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => '\app\models\ext\UserExt', 'message' => Yii::t('app\models','This email address has already been taken.')],

            ['name', 'filter', 'filter' => 'trim'],
            ['name', 'required'],
            ['surname', 'filter', 'filter' => 'trim'],
            ['surname', 'required'],
            ['patronymic', 'filter', 'filter' => 'trim'],
            [['surname','name','patronymic'], 'string', 'max' => 100],

            ['gender', 'in', 'range' =>[0,1,2]],
            ['geo_country_id', 'required'],
            ['geo_locality_id', 'required'],
            ['self_geo_locality', 'string', 'min' => 2, 'max' => 255],

            ['phone', 'filter', 'filter' => 'trim'],
            ['phone', 'required'],
            ['phone', 'match', 'pattern'=>'/^\+?[0-9]{0,1}\({0,1}[0-9]{3}\){0,1}[ |-]{0,1}[0-9]{3}[ |-]{0,1}[0-9]{2}[ |-]{0,1}[0-9]{2}$/' ],
            ['phone', 'unique', 'targetClass' => '\app\models\ext\UserExt', 'message' => Yii::t('app\models','This phone number has already been taken.'), 'filter' => function(){ return $this->phone_code . strtr( $this->phone, ['+'=>'','('=>'',')'=>'',' '=>'','-'=>''] );}],
            ['phone', 'filter', 'filter' => function(){ return $this->phone_code . strtr( $this->phone, ['+'=>'','('=>'',')'=>'',' '=>'','-'=>''] );} ],

            ['phone_code', 'filter', 'filter' => 'trim'],
            ['phone_code', 'required'],
            //['phone_code', 'int', 'min' => 1, 'max' => 1],

            ['photo', 'image', 'skipOnEmpty' => true, 'maxWidth'=>1600,'maxHeight'=>1600],//'extensions' => 'png, jpg, jpeg, gif']

            ['password', 'required'],
            ['password', 'string', 'min' => 6],

            ['repeat_password', 'required'],
            ['repeat_password', 'string', 'min' => 6],
            ['repeat_password', 'compare', 'compareAttribute'=>'password']
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'partner_id' => Yii::t('app\models','partner ID'),
            'login' => Yii::t('app\models','login'),
            'password' => Yii::t('app\models','password'),
            'repeat_password' => Yii::t('app\models','repeat password'),
            'email' => Yii::t('app\models','e-mail'),
            'phone' => Yii::t('app\models','phone'),
            'phone_code' => Yii::t('app\models','phone code'),
            'photo' => Yii::t('app\models','photo'),
            'name' => Yii::t('app\models','name'),
            'surname' => Yii::t('app\models','surname'),
            'patronymic' => Yii::t('app\models','patronymic'),
            'gender' => Yii::t('app\models','gender'),
            'language_id' => Yii::t('app\models','language'),
            'country_id' => Yii::t('app\models','country'),
            'locality_id' => Yii::t('app\models','locality'),
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'phone'=>[
                'class' => 'yii\behaviors\BlameableBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT =>['phone'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['phone'],
                ],
                'value' => function() { return strtr( $this->phone, ['+'=>'','('=>'',')'=>'',' '=>'','-'=>''] ); },
            ]
        ];
    }

    /**
     * Signs user up.
     *
     * @return UserExt|null the saved model or null if saving fails
     */
    public function signUp()
    {
        $this->photo = UploadedFile::getInstance($this, 'photo');
        if ( $this->validate() ) {
            $user = new UserExt();
            $user->name = $this->name;
            $user->surname = $this->surname;
            $user->patronymic = $this->patronymic;
            $user->login = $this->login;
            $user->email = $this->email;
            $user->phone = $this->phone;
            $user->gender = $this->gender;
            $user->geo_locality_id = $this->geo_locality_id;
            //$user->language_id = LanguageExt::getCurrent()->id;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            if ( $user->saveAsUser( intval($this->referral_id) ) ) {
                $user->uploadPhoto( $this->photo );
                return $user;
            } else {
                //var_dump( $user->errors );
                /*foreach( $user->getErrors() as $error ) {
                    Yii::$app->session->setFlash('error',$error);
                }*/
            }
        }
        if( !empty( $user ) && $user->hasErrors() ){
            $this->addErrors($user->getErrors());
        }
        return null;
    }
}
