<?php
namespace app\models\forms;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class CallBackForm extends Model
{
    public $name;
    public $email;
    public $phone;
    public $comment;
    //public $verifyCode;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'phone'], 'required'],
            ['email', 'email'],
            //['verifyCode', 'captcha'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t( 'frontend\frontend', 'Full Name'),
            'email' => Yii::t( 'frontend\frontend', 'E-mail Address'),
            'phone' => Yii::t( 'frontend\frontend', 'Phone'),
            'comment' => Yii::t( 'frontend\frontend', 'Comment'),
            'verifyCode' => Yii::t( 'frontend\frontend','Verification Code'),
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param  string  $email the target email address
     * @return boolean whether the email was sent
     */
    public function sendEmail($email)
    {
        $datetime = date('d.m.Y H:i:s');
        $subject = Yii::t( 'frontend\frontend', 'New message from feed-back form {datetime}', ['{datetime}'=>$datetime] );

        return Yii::$app->mailer->compose()
            ->setTo($email)
            ->setFrom([$this->email => $this->name])
            ->setSubject($subject)
            ->setTextBody(
                $this->name . "\n". $this->phone . "\n". $this->comment
            )
            ->send();
    }
}
