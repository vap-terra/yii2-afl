<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "list_value_item".
 *
 * @property integer $id
 * @property integer $list_value_id
 * @property string $name
 * @property integer $position
 * @property integer $active
 *
 * @property ListValue $listValue
 */
class ListValueItem extends BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'list_value_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['list_value_id', 'name'], 'required'],
            [['list_value_id', 'position', 'active'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/models', 'ID'),
            'list_value_id' => Yii::t('app/models', 'List Value ID'),
            'name' => Yii::t('app/models', 'Name'),
            'position' => Yii::t('app/models', 'Position'),
            'active' => Yii::t('app/models', 'Active'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getListValue()
    {
        return $this->hasOne(ListValue::className(), ['id' => 'list_value_id']);
    }
}
