<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 23.07.16
 * Time: 21:59
 */

namespace app\models;


use yii\db\ActiveQuery;

class BaseActiveQuery extends ActiveQuery
{

    const ALL = -1;
    const INACTIVE = 0;
    const ACTIVE = 1;

    public function active( $status=self::ACTIVE )
    {
        if( $status != self::ALL ) {
            return $this->andWhere(['=', 'active', $status]);
        }
        return $this;
    }

    public function orderByPosition($sort=SORT_ASC)
    {
        return $this->orderBy(['position'=>$sort]);
    }

    public function andOrderByPosition($sort=SORT_ASC)
    {
        return $this->addOrderBy(['position'=>$sort]);
    }
}