<?php
/**
 * Created by PhpStorm.
 * FontAwesomeAsset.php
 * @package /
 * @author Polyankin Semen Vicktorovich Vol][v, http://vap-terra.ru
 * @date 06.08.15
 * @time 22:54
 * @version 0.0.1
 * Copyright © 2014 VAP-TERRA, http://vap-terra.ru
 */

namespace app\assets;

use yii\web\AssetBundle;
class Select2Asset extends AssetBundle
{
    public $sourcePath = '@app/views/src';
    public $css = [
        'vendor/select2/css/select2.css',
        'vendor/select2-bootstrap-theme/select2-bootstrap.css',
        'vendor/bootstrap-multiselect/bootstrap-multiselect.css',
    ];
    public $js = [
        'vendor/select2/js/select2.js',
        'vendor/bootstrap-multiselect/bootstrap-multiselect.js',
    ];
    public $depends = [
        //'backend\assets\SimpleVendorAsset',
        'yii\web\JqueryAsset',
    ];
}