<?php
namespace app\assets;

use yii\web\AssetBundle;

class BaseAsset extends AssetBundle
{
    public $sourcePath = '@app/views/src';
    public $css = [
        'css/base.css',
    ];
    public $js = [
        'javascripts/base.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'app\assets\PnotifyAsset',
    ];
}
