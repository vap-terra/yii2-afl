<?php
/**
 * Created by PhpStorm.
 * FontAwesomeAsset.php
 * @package /
 * @author Polyankin Semen Vicktorovich Vol][v, http://vap-terra.ru
 * @date 06.08.15
 * @time 22:54
 * @version 0.0.1
 * Copyright © 2014 VAP-TERRA, http://vap-terra.ru
 */

namespace app\assets\jquery;

use yii\web\AssetBundle;
class UIAsset extends AssetBundle
{
    public $sourcePath = '@vendor/components/jqueryui';
    public $css = [
        'themes/base/jquery-ui.min.css',
    ];
    public $js = [
        'jquery-ui.min.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}