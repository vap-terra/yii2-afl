<?php
/**
 * Created by PhpStorm.
 * BlueimpLoadImageAsset.php
 * @package /
 * @author Polyankin Semen Vicktorovich Vol][v, http://vap-terra.ru
 * @date 08.08.15
 * @time 0:45
 * @version 0.0.1
 * Copyright © 2014 VAP-TERRA, http://vap-terra.ru
 */

namespace app\assets\jquery\file_upload;


class BlueimpLoadImageAsset extends AssetBundle
{
    public $sourcePath = '@vendor/bower/blueimp-load-image';
    public $js = [
        'js/load-image.all.min.js',
    ];
}