<?php
/**
 * Created by PhpStorm.
 * JQueryFileUploadAsset.php
 * @package /
 * @author Polyankin Semen Vicktorovich Vol][v, http://vap-terra.ru
 * @date 08.08.15
 * @time 0:31
 * @version 0.0.1
 * Copyright © 2014 VAP-TERRA, http://vap-terra.ru
 */

namespace app\assets\jquery\file_upload;

use yii\web\AssetBundle;

class FileUploadPlusAsset extends AssetBundle
{
    public $sourcePath = '@vendor/bower/blueimp-file-upload';
    public $css = [
        'css/jquery.fileupload.css'
    ];
    public $js = [
        'js/jquery.iframe-transport.js',
        'js/jquery.fileupload-process.js',
        'js/jquery.fileupload-image.js',
        'js/jquery.fileupload-audio.js',
        'js/jquery.fileupload-video.js',
        'js/jquery.fileupload-validate.js'
    ];
    public $depends = [
        'app\assets\jquery\file_upload\FileUploadAsset',
        'app\assets\jquery\file_upload\BlueimpLoadImageAsset',
        'app\assets\jquery\file_upload\BlueimpCanvasToBlobAsset',
    ];
}