<?php
/**
 * Created by PhpStorm.
 * BlueimpTmplAsset.php
 * @package /
 * @author Polyankin Semen Vicktorovich Vol][v, http://vap-terra.ru
 * @date 08.08.15
 * @time 0:49
 * @version 0.0.1
 * Copyright © 2014 VAP-TERRA, http://vap-terra.ru
 */

namespace app\assets\jquery\file_upload;


use yii\web\AssetBundle;

class BlueimpTmplAsset extends AssetBundle
{
    public $sourcePath = '@vendor/bower/blueimp-tmpl';
    public $js = [
        'js/tmpl.min.js',
    ];
}