<?php
/**
 * Created by PhpStorm.
 * FileUploadUIAsset.php
 * @package /
 * @author Polyankin Semen Vicktorovich Vol][v, http://vap-terra.ru
 * @date 08.08.15
 * @time 0:41
 * @version 0.0.1
 * Copyright © 2014 VAP-TERRA, http://vap-terra.ru
 */

namespace app\assets\jquery\file_upload;


use yii\web\AssetBundle;

class FileUploadUIAsset extends AssetBundle
{
    public $sourcePath = '@vendor/bower/blueimp-file-upload';
    public $css = [
        'css/jquery.fileupload.css'
    ];
    public $js = [
        'js/vendor/jquery.ui.widget.js',
        'js/jquery.iframe-transport.js',
        'js/jquery.fileupload.js',
        'js/jquery.fileupload-process.js',
        'js/jquery.fileupload-image.js',
        'js/jquery.fileupload-audio.js',
        'js/jquery.fileupload-video.js',
        'js/jquery.fileupload-validate.js',
        'js/jquery.fileupload-ui.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'app\assets\jquery\file_upload\BlueimpLoadImageAsset',
        'app\assets\jquery\file_upload\BlueimpCanvasToBlobAsset',
        'app\assets\jquery\file_upload\BlueimpTmplAsset'
    ];
}