<?php
/**
 * Created by PhpStorm.
 * FontAwesomeAsset.php
 * @package /
 * @author Polyankin Semen Vicktorovich Vol][v, http://vap-terra.ru
 * @date 06.08.15
 * @time 22:54
 * @version 0.0.1
 * Copyright © 2014 VAP-TERRA, http://vap-terra.ru
 */

namespace app\assets;

use yii\web\AssetBundle;
class SimpleVendorAsset extends AssetBundle
{
    public $sourcePath = '@app/views/src';
    public $css = [
        'http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light',
        //'vendor/magnific-popup/magnific-popup.css',
        //'vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css',
        'vendor/jquery-ui/jquery-ui.css',
        'vendor/jquery-ui/jquery-ui.theme.css',
        //'vendor/bootstrap-multiselect/bootstrap-multiselect.css',
        //'vendor/morris.js/morris.css',
    ];
    public $js = [
        //'vendor/modernizr/modernizr.js',
        //'vendor/style-switcher/style.switcher.localstorage.js',

		//'vendor/jquery/jquery.js',
		'vendor/jquery-browser-mobile/jquery.browser.mobile.js',
		'vendor/jquery-cookie/jquery-cookie.js',
		//'vendor/style-switcher/style.switcher.js',
		'vendor/bootstrap/js/bootstrap.js',
		'vendor/nanoscroller/bin/javascripts/jquery.nanoscroller.min.js',
		//'vendor/bootstrap-datepicker/js/bootstrap-datepicker.js',
		//'vendor/magnific-popup/jquery.magnific-popup.js',
		'vendor/jquery-placeholder/jquery-placeholder.js',

		'vendor/jquery-ui/jquery-ui.js',
		'vendor/jqueryui-touch-punch/jqueryui-touch-punch.js',

    ];
	public $depends = [
		'yii\web\JqueryAsset'
	];
}