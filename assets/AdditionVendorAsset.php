<?php
/**
 * Created by PhpStorm.
 * FontAwesomeAsset.php
 * @package /
 * @author Polyankin Semen Vicktorovich Vol][v, http://vap-terra.ru
 * @date 06.08.15
 * @time 22:54
 * @version 0.0.1
 * Copyright © 2014 VAP-TERRA, http://vap-terra.ru
 */

namespace app\assets;

use yii\web\AssetBundle;
class AdditionVendorAsset extends AssetBundle
{
    public $sourcePath = '@app/views/src';
    public $css = [

    ];
    public $js = [

        //'vendor/jquery-ui/jquery-ui.js',
        'vendor/jqueryui-touch-punch/jqueryui-touch-punch.js',
        'vendor/jquery-appear/jquery-appear.js',
        'vendor/bootstrap-multiselect/bootstrap-multiselect.js',
        'vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.js',
        'vendor/flot/jquery.flot.js',
        'vendor/flot.tooltip/flot.tooltip.js',
        'vendor/flot/jquery.flot.pie.js',
        'vendor/flot/jquery.flot.categories.js',
        'vendor/flot/jquery.flot.resize.js',
        'vendor/jquery-sparkline/jquery-sparkline.js',
        'vendor/raphael/raphael.js',
        'vendor/morris.js/morris.js',
        'vendor/gauge/gauge.js',
        'vendor/snap.svg/snap.svg.js',
        'vendor/liquid-meter/liquid.meter.js',
        //'vendor/jqvmap/jquery.vmap.js',
        //'vendor/jqvmap/data/jquery.vmap.sampledata.js',
        //'vendor/jqvmap/maps/jquery.vmap.world.js',
        //'vendor/jqvmap/maps/continents/jquery.vmap.africa.js',
        //'vendor/jqvmap/maps/continents/jquery.vmap.asia.js',
        //'vendor/jqvmap/maps/continents/jquery.vmap.australia.js',
        //'vendor/jqvmap/maps/continents/jquery.vmap.europe.js',
        //'vendor/jqvmap/maps/continents/jquery.vmap.north-america.js',
        //'vendor/jqvmap/maps/continents/jquery.vmap.south-america.js',

    ];
}