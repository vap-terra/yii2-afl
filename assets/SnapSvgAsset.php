<?php
/**
 * Created by PhpStorm.
 * FontAwesomeAsset.php
 * @package /
 * @author Polyankin Semen Vicktorovich Vol][v, http://vap-terra.ru
 * @date 06.08.15
 * @time 22:54
 * @version 0.0.1
 * Copyright © 2014 VAP-TERRA, http://vap-terra.ru
 */

namespace app\assets;

use yii\web\AssetBundle;
class SnapSvgAsset extends AssetBundle
{
    public $sourcePath = '@app/views/src';
    public $css = [

    ];
    public $js = [
        'vendor/snap.svg/0.4.1/dist/snap.svg-min.js',
    ];
    public $depends = [
    ];
}