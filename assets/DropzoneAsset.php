<?php
/**
 * Created by PhpStorm.
 * FontAwesomeAsset.php
 * @package /
 * @author Polyankin Semen Vicktorovich Vol][v, http://vap-terra.ru
 * @date 06.08.15
 * @time 22:54
 * @version 0.0.1
 * Copyright © 2014 VAP-TERRA, http://vap-terra.ru
 */

namespace app\assets;

use yii\web\AssetBundle;
class DropzoneAsset extends AssetBundle
{
    public $sourcePath = '@app/views/src';
    public $css = [
        'vendor/dropzone/basic.css',
        'vendor/dropzone/dropzone.css'
    ];
    public $js = [
        'vendor/dropzone/dropzone.js',
    ];
    public $depends = [
        //'app\assets\SimpleVendorAsset',
    ];
}