<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 07.08.16
 * Time: 21:58
 */

namespace app\assets;


use yii\web\AssetBundle;

class FancyboxAsset extends AssetBundle
{
    public $sourcePath = '@app/views/src';
    public $css = [
        'vendor/fancyapps-fancyBox/2.1.5/source/jquery.fancybox.css'
    ];
    public $js = [
        'vendor/fancyapps-fancyBox/2.1.5/source/jquery.fancybox.pack.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset'
    ];
}