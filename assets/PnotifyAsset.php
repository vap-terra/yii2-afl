<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 07.08.16
 * Time: 21:58
 */

namespace app\assets;


use yii\web\AssetBundle;

class PnotifyAsset extends AssetBundle
{
    public $sourcePath = '@app/views/src';
    public $css = [
        'vendor/pnotify/pnotify.custom.css'
    ];
    public $js = [
        'vendor/pnotify/pnotify.custom.js',
    ];
    public $depends = [
    ];
}