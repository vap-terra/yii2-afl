<?php
/**
 * Created by PhpStorm.
 * FontAwesomeAsset.php
 * @package /
 * @author Polyankin Semen Vicktorovich Vol][v, http://vap-terra.ru
 * @date 06.08.15
 * @time 22:54
 * @version 0.0.1
 * Copyright © 2014 VAP-TERRA, http://vap-terra.ru
 */

namespace app\assets;

use yii\web\AssetBundle;
use yii\web\View;

class YandexMapAsset extends AssetBundle
{
    public $js = [
        'https://api-maps.yandex.ru/2.1/?lang=ru_RU',
    ];
    public $jsOptions = [
        'position'=>View::POS_HEAD
    ];
}