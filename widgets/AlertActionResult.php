<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 12.10.16
 * Time: 23:06
 */

namespace app\widgets;


use app\components\BaseController;

class AlertActionResult extends \yii\bootstrap\Widget
{
    /**
     * @var array the alert types configuration for the flash messages.
     * This array is setup as $key => $value, where:
     * - $key is the name of the session flash variable
     * - $value is the bootstrap alert type (i.e. danger, success, info, warning)
     */
    public $alertTypes = [
        'error'   => 'alert-danger',
        'danger'  => 'alert-danger',
        'success' => 'alert-success',
        'info'    => 'alert-info',
        'warning' => 'alert-warning'
    ];


    public $closeButton = [
        'tag' => 'button',
        'label' => '&times;',
    ];

    public $result;

    public function run()
    {

        if( !empty( $this->result ) && isset($this->result['message']) ) {
            $appendCss = isset($this->options['class']) ? ' ' . $this->options['class'] : '';
            $this->options['class'] = $this->alertTypes[$this->result['status']] . $appendCss;
            $this->options['id'] = $this->getId() . '-' . $this->result['status'];
            $message = '<p>'.$this->result['message'].'</p>';
            if( !empty( $this->result['errors'] ) ){
                foreach( $this->result['errors'] as $errors ){
                    $message .= '<p>'.implode('</p><p>',(array) $errors).'</p>';
                }
            }
            echo \yii\bootstrap\Alert::widget([
                'body' => $message,
                'closeButton' => $this->closeButton,
                'options' => $this->options,
            ]);
        }
    }
}