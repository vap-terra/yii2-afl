<?php
/**
 * Created by PhpStorm.
 *
 * @author Polyankin Semen Vicktorovich Vol][v, volhv@vap-terra.ru
 * @date 30.01.16
 * @time 1:36
 * @version 0.0.1
 * Copyright © 2014-2016 VAP-TERRA, http://vap-terra.ru
 */
use yii\bootstrap\Nav;

/* @var $oUser app\models\ext\UserExt */

$noPhotoMale = Yii::$app->storage->getFileHref( 'no-photo-male.jpg' );
$noPhotoFemale = Yii::$app->storage->getFileHref( 'no-photo-female.jpg' );

?>
<a href="#" class="mobile-close visible-xs">
	Collapse <i class="fa fa-chevron-right"></i>
</a>

<div class="sidebar-right-wrapper">

	<div class="sidebar-widget widget-calendar">
		<?php /* <h6>Upcoming Tasks</h6> */ ?>

		<div data-plugin-datepicker data-plugin-skin="dark"></div>

		<ul>
			<li>
				<time datetime="<?php echo Yii::$app->formatter->asDatetime(time(),'full')?>"><?php echo Yii::$app->formatter->asDate(time())?></time>
				<?php /* <span>Company Meeting</span> */ ?>
			</li>
		</ul>
	</div>

	<div class="sidebar-widget widget-friends">
		<h6>Employees</h6>
		<?php /*<ul>
								<li class="status-online">
									<figure class="profile-picture">
										<img src="images/!sample-user.jpg" alt="Joseph Doe" class="img-circle">
									</figure>
									<div class="profile-info">
										<span class="name">Joseph Doe Junior</span>
										<span class="title">Hey, how are you?</span>
									</div>
								</li>
								<li class="status-online">
									<figure class="profile-picture">
										<img src="images/!sample-user.jpg" alt="Joseph Doe" class="img-circle">
									</figure>
									<div class="profile-info">
										<span class="name">Joseph Doe Junior</span>
										<span class="title">Hey, how are you?</span>
									</div>
								</li>
								<li class="status-offline">
									<figure class="profile-picture">
										<img src="images/!sample-user.jpg" alt="Joseph Doe" class="img-circle">
									</figure>
									<div class="profile-info">
										<span class="name">Joseph Doe Junior</span>
										<span class="title">Hey, how are you?</span>
									</div>
								</li>
								<li class="status-offline">
									<figure class="profile-picture">
										<img src="images/!sample-user.jpg" alt="Joseph Doe" class="img-circle">
									</figure>
									<div class="profile-info">
										<span class="name">Joseph Doe Junior</span>
										<span class="title">Hey, how are you?</span>
									</div>
								</li>
							</ul>*/ ?>
	</div>

</div>
