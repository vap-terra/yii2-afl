<?php
/**
 * Created by PhpStorm.
 *
 * @author Polyankin Semen Vicktorovich Vol][v, volhv@vap-terra.ru
 * @date 30.01.16
 * @time 1:36
 * @version 0.0.1
 * Copyright © 2014-2016 VAP-TERRA, http://vap-terra.ru
 */
use app\models\ext\UserExt;
use yii\bootstrap\Nav;

/* @var $oUser UserExt */

$noPhotoMale = Yii::$app->storage->getFileHref( 'no-photo-male.jpg' );
$noPhotoFemale = Yii::$app->storage->getFileHref( 'no-photo-female.jpg' );

?>
<div id="userbox" class="userbox navbar-right">

	<a href="#" data-toggle="dropdown">
		<figure class="profile-picture">

			<?php if( $oUser->photo ) { ?>
				<img class="img-responsive img-circle" src="<?php echo $oUser->getImageHref();?>" alt="<?php echo $oUser->getFullName(); ?>"
				     title="<?php echo $oUser->getFullName(); ?>" class="img-circle"
				     data-lock-picture="<?php echo $oUser->getImageHref(); ?>"/>
			<?php } else { ?>
				<?php if( $oUser->gender == 1 ){?>
					<img class="img-responsive img-circle" src="<?php echo $noPhotoMale;?>" alt="<?php echo $oUser->getFullName(); ?>"
					     title="<?php echo $oUser->getFullName(); ?>" class="img-circle"
					     data-lock-picture="<?php echo $noPhotoMale; ?>"/>
				<?php } else { ?>
					<img class="img-responsive img-circle" src="<?php echo $noPhotoFemale;?>" alt="<?php echo $oUser->getFullName(); ?>"
					     title="<?php echo $oUser->getFullName(); ?>" class="img-circle"
					     data-lock-picture="<?php echo $noPhotoFemale; ?>"/>
				<?php } ?>
			<?php } ?>

		</figure>
		<div class="profile-info" data-lock-name="<?php echo $oUser->getFullName(); ?>"
		     data-lock-email="<?php echo $oUser->email; ?>">
			<span class="name"><?php echo $oUser->getFullName(); ?></span>
			<span class="role"><?php echo $oUser->role; ?></span>
		</div>

		<i class="fa custom-caret"></i>
	</a>

	<div class="dropdown-menu">
		<?php
		$menuItems = [];
		if (Yii::$app->user->isGuest) {
			$menuItems[] = ['label' => 'Signup', 'url' => ['/backend/user/signup']];
			$menuItems[] = ['label' => 'Login', 'url' => ['/backend/user/login']];
		} else {
			$menuItems[] = [
				'label'       => Yii::t('backend/layout','Logout ({name})',['name'=>Yii::$app->user->identity->login]),
				'url'         => ['/backend/user/logout'],
				'linkOptions' => ['data-method' => 'post'],
			];
			$menuItems[] = [
				'label'       => Yii::t('backend/layout','Profile ({name})',['name'=>Yii::$app->user->identity->login]),
				'url'         => ['/backend/user/security'],
				'linkOptions' => [],
			];
		}
		echo Nav::widget([
			'options' => ['class' => 'list-unstyled'],
			'items'   => $menuItems,
		]);
		?>
		<?php /* <ul class="list-unstyled">
                        <li class="divider"></li>
                        <li>
                            <a role="menuitem" tabindex="-1" href="pages-user-profile.html"><i class="fa fa-user"></i> My Profile</a>
                        </li>
                        <li>
                            <a role="menuitem" tabindex="-1" href="index.html#" data-lock-screen="true"><i class="fa fa-lock"></i> Lock Screen</a>
                        </li>
                        <li>
                            <a role="menuitem" tabindex="-1" href="pages-signin.html"><i class="fa fa-power-off"></i> Logout</a>
                        </li>
                    </ul> */ ?>
	</div>
</div>
