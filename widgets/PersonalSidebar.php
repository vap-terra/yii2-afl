<?php
/**
 * Created by PhpStorm.
 * PersonalBlock.php
 * @package /
 * @author Polyankin Semen Vicktorovich Vol][v, http://vap-terra.ru
 * @date 15.07.15
 * @time 11:10
 * @version 0.0.1
 * Copyright © 2014 VAP-TERRA, http://vap-terra.ru
 */

namespace app\widgets;


use yii\bootstrap\Widget;

class PersonalSidebar extends Widget
{
    public $template = 'personal-sidebar/default';
    public function init(){}

    public function run()
    {
        return $this->render(
            $this->template,
            [
                'oUser' => (!\Yii::$app->user->isGuest)?\Yii::$app->user->identity:null
            ]
        );
    }
}