<?php
/**
 * Created by PhpStorm.
 * layout.php
 * @package /
 * @author Polyankin Semen Vicktorovich Vol][v, http://vap-terra.ru
 * @date 14.07.15
 * @time 16:15
 * @version 0.0.1
 * Copyright © 2014 VAP-TERRA, http://vap-terra.ru
 */
return [
    'Home' => 'Главная',
    'Sing In' => 'Авторизация',
    'Sing in' => 'Авторизация',
    'Login' => 'Вход',
    'Logout' => 'Выход',
    'Sing Up' => 'Регистрация',
    'Sing up' => 'Регистрация',
    'Thank you for contacting us. We will respond to you as soon as possible.' => 'Спасибо! Вскоре менеджер ответит Вам',
    'There was an error sending email.' => 'При отправлении, произошла ошибка. Попробуйте позже.',
    'Fill in all required fields'=>'Заполните все обязательные поля',
    'Logout ({name})' => 'Выйти ({name})',
    'Profile ({name})' => 'Профиль ({name})',
    'Your personal area' => 'Вход в личный кабинет',
];