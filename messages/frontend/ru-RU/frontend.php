<?php
/**
 * Created by PhpStorm.
 * backend.php
 * @package /
 * @author Polyankin Semen Vicktorovich Vol][v, http://vap-terra.ru
 * @date 14.07.15
 * @time 16:15
 * @version 0.0.1
 * Copyright © 2014 VAP-TERRA, http://vap-terra.ru
 */
return [

	'Verification Code' => 'Проверочный код',

	'login' => 'логин',
	'password' => 'пароль',
	'Login' => 'Логин',
	'Password' => 'Пароль',
	'rememberMe' => 'запомнить меня',
	'remember me' => 'запомнить меня',
	'Remember me' => 'Запомнить меня',

	'Full Name' => 'Ф.И.О.',
	'E-mail Address' => 'Электронный адрес',
	'Phone' => 'Телефон',
	'Question' => 'Вопрос',
	'Comment For Order' => 'Комментарий к заказу',
];