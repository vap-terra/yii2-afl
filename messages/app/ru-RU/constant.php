<?php
/**
 * Created by PhpStorm.
 * models.php
 * @package /
 * @author Polyankin Semen Vicktorovich Vol][v, http://vap-terra.ru
 * @date 14.07.15
 * @time 16:15
 * @version 0.0.1
 * Copyright © 2014 VAP-TERRA, http://vap-terra.ru
 */
return [
    'ID' => 'ID',
    'Tid' => 'Тег(только латиница)',
    'Value' => 'Значение константы',
    'Name' => 'Название константы',
    'Description' => 'Описание константы',
    'System' => 'Системная константа(удалить может только супер пользователь)',
];