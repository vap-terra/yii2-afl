<?php
/**
 * Created by PhpStorm.
 * models.php
 * @package /
 * @author Polyankin Semen Vicktorovich Vol][v, http://vap-terra.ru
 * @date 14.07.15
 * @time 16:15
 * @version 0.0.1
 * Copyright © 2014 VAP-TERRA, http://vap-terra.ru
 */
return [
    'ID' => 'ID',
    'Tid' => 'Адрес страницы(URL)',
    'Information ID' => 'Информационный тип',
    'Information Group ID' => 'Группа',
    'Name' => 'Название',
    'Header' => 'Заголовок страницы',
    'Sub Header' => 'Подзаголовок страницы',
    'Description' => 'Краткое описание',
    'Content' => 'Описание',
    'Title' => 'Title для группы',
    'Meta Description' => 'Мета описание для группы',
    'Meta Keywords' => 'Мета ключевые слова для группы',
    'Title Item' => 'Title для элемента',
    'Meta Description Item' => 'Мета описание для элемента',
    'Meta Keywords Item' => 'Мета ключевые слова для элемента',
    'Position' => 'Позиция',
    'Active' => 'Активность',
    'Depth' => 'Глубина',
    'Rb' => 'Правая граница',
    'Lb' => 'Левая граница',
    'Image' => 'Изображение',
    'Image Width' => 'Ширина изображения',
    'Image Height' => 'Высота изображения',
];