<?php
/**
 * Created by PhpStorm.
 * app.php
 * @package /
 * @author Polyankin Semen Vicktorovich Vol][v, http://vap-terra.ru
 * @date 14.07.15
 * @time 16:15
 * @version 0.0.1
 * Copyright © 2014 VAP-TERRA, http://vap-terra.ru
 */
return [
    '[404] Not Found board item {id}' => '[404] Не найдена доска объявлений {id}',
    '[404] Not Found board group {id}' => '[404] Не найдена группа {id} доски объявлений',
    '[404] Not Found board item {id}' => '[404] Не найден элемент {id} доски объявлений',
    '[404] Not Found information group {id}' => '[404] Не найден информационная группа {id}',
    '[404] Not Found information item {id}' => '[404] Не найден информационный элемент {id}',
    '[404] Not Found menu {id}' => '[404] Не найдено меню {id}',
    '[404] Not Found menu item {id}' => '[404] Не найдена страница {id}',

];