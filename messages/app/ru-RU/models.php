<?php
/**
 * Created by PhpStorm.
 * models.php
 * @package /
 * @author Polyankin Semen Vicktorovich Vol][v, http://vap-terra.ru
 * @date 14.07.15
 * @time 16:15
 * @version 0.0.1
 * Copyright © 2014 VAP-TERRA, http://vap-terra.ru
 */
return [
    'login' => 'логин',
    'password' => 'пароль',
    'Login' => 'Логин',
    'Password' => 'Пароль',
    'rememberMe' => 'запомнить меня',
    'remember me' => 'запомнить меня',
    'Remember me' => 'Запомнить меня',

    'verification code' => 'проверочный код',
    'name' => 'имя',
    'e-mail' => 'адрес электронной почты',
    'subject' => 'тема',
    'question' => 'вопрос',

    'message' => 'сообщение',
    'image' => 'изображение',

    'load your photo' => 'загрузите Ваше фото',
    'your photo' => 'Ваше фото',
    'new password' => 'новый пароль',
    'repeat new password' => 'повторите новый пароль',
    'phone number' => 'номер телефона',

    'repeat password'=>'повторите пароль',
    'phone' => 'телефон',
    'phone code' => 'код телефона',
    'photo' => 'фотография',
    'sername' => 'фамилия',
    'patronymic' => 'отчество',
    'gender' => 'пол',
    'male' => 'мужчина',
    'female' => 'женщина',
    'language' => 'язык',
    'country' => 'страна',
    'locality' => 'город',

    'This phone number has already been taken.' => 'Этот номер телефона занят.',
    'This email address has already been taken.' => 'Этот электронный адрес занят',
    'This login has already been taken.' => 'Этот логин занят',


];