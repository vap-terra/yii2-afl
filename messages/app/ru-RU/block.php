<?php
/**
 * Created by PhpStorm.
 * models.php
 * @package /
 * @author Polyankin Semen Vicktorovich Vol][v, http://vap-terra.ru
 * @date 14.07.15
 * @time 16:15
 * @version 0.0.1
 * Copyright © 2014 VAP-TERRA, http://vap-terra.ru
 */
return [
    'ID' => 'ID',
    'Type' => 'Тип блока',
    'Widget' => 'Класса виджета',
    'Template' => 'Шаблон',
    'Parameters' => 'Параметры',
    'Name' => 'Название',
    'Header' => 'Заголовок',
    'Description' => 'Краткое описание',
    'Content' => 'Описание',
    'Position' => 'Позиция',
    'Active' => 'Активность',

    'text' => 'текст',
    'code' => 'код',
    'widget' => 'виджет',
];