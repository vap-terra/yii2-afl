<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 26.08.16
 * Time: 16:19
 */
return [
    'ID'=>'Идентификатор',
    'Banner Group ID'=>'Группа',
    'Geo Country ID'=>'Страна для отображения',
    'Geo Region ID'=>'Регион для отображения',
    'Geo Region Area ID'=>'Район для отображения',
    'Geo Locality ID'=>'Город для отображения',
    'Name'=>'Наименование',
    'Link'=>'Ссылка',
    'Type'=>'Тип',
    'Content'=>'Текст',
    'Image'=>'Изображение',
    'Click Count'=>'Количество кликов',
    'Active'=>'Активность',
    'Position'=>'Позиция'
];