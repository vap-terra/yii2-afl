<?php
/**
 * Created by PhpStorm.
 * models.php
 * @package /
 * @author Polyankin Semen Vicktorovich Vol][v, http://vap-terra.ru
 * @date 14.07.15
 * @time 16:15
 * @version 0.0.1
 * Copyright © 2014 VAP-TERRA, http://vap-terra.ru
 */
return [
    'ID' => 'Идентификатор',
    'Menu Item ID' => 'Элемент меню',
    'Type' => 'Тип',
    'Item ID' => 'Элемент',
    'Position' => 'Позиция',
    'Active' => 'Активность',
    'Main' => 'Основной',
    'Params' => 'Параметры',

    'Content' => 'Текст',
    'Form' => 'Форма',
    'Information' => 'Информация',
    'Block' => 'Блок',
    'Block group' => 'Группа блоков',
    'Comment' => 'Комментарии',
    'Shop' => 'Магазин',
    'Question' => 'Вопрос-ответ',
    'Banner'=>'Баннер',
    'Widget'=>'Виджет',
];