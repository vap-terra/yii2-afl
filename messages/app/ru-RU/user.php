<?php
/**
 * Created by PhpStorm.
 * models.php
 * @package /
 * @author Polyankin Semen Vicktorovich Vol][v, http://vap-terra.ru
 * @date 14.07.15
 * @time 16:15
 * @version 0.0.1
 * Copyright © 2014 VAP-TERRA, http://vap-terra.ru
 */
return [
    'ID' => 'ID',
    'Repeat Password' => 'Пароль повторно',
    'Password' => 'Пароль',
    'Role' => 'Роль пользователя',
    'Group' => 'Группа пользователя',
    'Group ID' => 'Группа пользователя',
    'Login' => 'Логин',
    'Auth Key' => 'Код авторизации',
    'Security Key' => 'Секретный ключ',
    'Password Hash' => 'Неш пароля',
    'Password Reset Token' => 'Код востановления пароля',
    'Activation Code' => 'Код активации',
    'Surname' => 'Фамилия',
    'Name' => 'Имя',
    'Patronymic' => 'Отчество',
    'Gender' => 'Пол',
    'Phone' => 'Телефон',
    'Email' => 'Электронный адрес',
    'Photo' => 'Фотография',
    'Photo Width' => 'Ширина фотографии',
    'Photo Height' => 'Высота фотографии',
    'Language ID' => 'Язык',
    'Geo Locality ID' => 'Город',
    'Dt Last' => 'Дата последнего захода',
    'Ip Last' => 'IP с которого был произведен последняя авторизация',
    'Is Blocked' => 'Заблокирован',
    'Blocker User ID' => 'Пользователь, который заблокировал',
    'Reason Blocking' => 'Причина блокировки',
    'Dt Blocked' => 'Дата блокировки',
    'Ip Blocked' => 'IP с которого произведена блокировка',
    'Creator User ID' => 'Пользователь, который создал',
    'Dt Creation' => 'Дата добавления',
    'Ip Creation' => 'IP с которого добавили',
    'Updater User ID' => 'Пользователь который обновил',
    'Dt Update' => 'Дата обновления',
    'Ip Update' => 'IP с которого произведено обновление',

    'repeat password'=>'повторите пароль',
    'phone' => 'телефон',
    'phone code' => 'код телефона',
    'photo' => 'фотография',
    'sername' => 'фамилия',
    'patronymic' => 'отчество',

    'male' => 'Мужской',
    'female' => 'Женский',

    'root' => 'Супер пользователь',
    'admin' => 'Администратор',
    'employee' => 'Сотрудник',
    'user' => 'Пользователь',
];