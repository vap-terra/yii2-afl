<?php
/**
 * Created by PhpStorm.
 * models.php
 * @package /
 * @author Polyankin Semen Vicktorovich Vol][v, http://vap-terra.ru
 * @date 14.07.15
 * @time 16:15
 * @version 0.0.1
 * Copyright © 2014 VAP-TERRA, http://vap-terra.ru
 */
return [
    'ID' => 'Id',
    'Tid' => 'Адрес страницы(URL)',
    'Menu ID' => 'Меню',
    'Menu Item ID' => 'Родительская страница',
    'Type' => 'Тип генератора контента',
    'Type Name' => 'Наименование генератора контента',
    'Layout' => 'Шаблон страницы',
    'Template' => 'Шаблон контента страницы',
    'Name' => 'Название страницы',
    'Header' => 'Заголовок страницы(H1)',
    'Sub Header' => 'Подзаголовок страницы',
    'Title' => 'Title',
    'Meta Keywords' => 'Meta Keywords',
    'Meta Description' => 'Meta Description',
    'Link' => 'Ссылка на другую страницу',
    'Position' => 'Позиция',
    'Active' => 'Активность',
    'Depth' => 'Уровень',
    'Rb' => 'Правая граница',
    'Lb' => 'Левая граница',
    'Image' => 'Изображение',
    'Image Width' => 'Ширина изображения',
    'Image Height' => 'Высота изображения',
];