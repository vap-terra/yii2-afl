<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 24.07.16
 * Time: 13:45
 */
return [
    'ID' => 'Id',
    'Tid' => 'Тег(только латиница)',
    'Form ID' => 'Форма',
    'Required' => 'Обязательное',
    'Active' => 'Активность',
    'Type' => 'Тип',
    'Name' => 'Название',
    'Hint' => 'Подсказка',
    'Description' => 'Описание',
    'Params' => 'Параметры',
    'Position' => 'Позиция',
];