<?php
/**
 * Created by PhpStorm.
 * models.php
 * @package /
 * @author Polyankin Semen Vicktorovich Vol][v, http://vap-terra.ru
 * @date 14.07.15
 * @time 16:15
 * @version 0.0.1
 * Copyright © 2014 VAP-TERRA, http://vap-terra.ru
 */
return [
    'ID' => 'ID',
    'Menu Item ID' => 'Пункт меню для отображения доски объявления',
    'Name' => 'Наименование',
    'Description' => 'Краткое описание',
    'Title Group' => 'Title для группы',
    'Meta Description Group' => 'Meta Description для группы',
    'Meta Keywords Group' => 'Meta Keywords для группы',
    'Title Item' => 'Title для элемента',
    'Meta Description Item' => 'Meta Description для элемента',
    'Meta Keywords Item' => 'Meta Keywords для элемента',
    'Active' => 'Активность',
    'Allow Comment' => 'Разрешить комментарии к элементам',
    'Items On Page' => 'Количество элементов на странице',
    'Format Date' => 'Формат даты',
];