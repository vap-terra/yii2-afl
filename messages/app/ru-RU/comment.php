<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 27.07.16
 * Time: 14:14
 */
return [
    'ID' => 'Идентификатор',
    'Author Name' => 'Имя автора',
    'Author Photo' => 'Фото автора',
    'Autor Post' => 'Должность автора',
    'Autor Company'  => 'Сомпания автора' ,
    'Content'  => 'Сообщение',
    'Dt Creation'  => 'Дата время создания',
    'Active'  => 'Активность',
    'Position'  => 'Позиция',
];