<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 24.07.16
 * Time: 13:44
 */
return [
    'ID'=>'Идентификатор',
    'Form ID' => 'Форма',
    'Elements' => 'Список полей и значений',
    'Dt Creation' => 'Дата создания',
    'Dt View' => 'Дата просмотра администратором',
    'Processed' => 'Обработана',
    'Dt Processed' => 'Дата обработки',
    'Ip' => 'IP пользователя',
    'Notify User Email' => 'Пользователь попросил оповестить по эл. адресу',
    'Notify User Sms' => 'Пользователь попросил оповестить по SMS',
];