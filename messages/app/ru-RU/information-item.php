<?php
/**
 * Created by PhpStorm.
 * models.php
 * @package /
 * @author Polyankin Semen Vicktorovich Vol][v, http://vap-terra.ru
 * @date 14.07.15
 * @time 16:15
 * @version 0.0.1
 * Copyright © 2014 VAP-TERRA, http://vap-terra.ru
 */
return [
    'ID' => 'ID',
    'Tid' => 'Адрес страницы(URL)',
    'Information ID' => 'Информационный тип',
    'Information Group ID' => 'Группа',
    'Name' => 'Название',
    'Description' => 'Краткое описание',
    'Content' => 'Описание',
    'Header' => 'Заголовок страницы',
    'Sub Header' => 'Подзаголовок страницы',
    'Title' => 'Title',
    'Meta Keywords' => 'Meta Keywords',
    'Meta Description' => 'Meta Description',
    'Image' => 'Изображение',
    'Image Width' => 'Ширина изображения',
    'Image Height' => 'Высота изображения',
    'Position' => 'Позиция',
    'Active' => 'Активность',
    'Dt Beginning' => 'Дата начала отображения',
    'Dt Ending' => 'Дата окончания отображения',
    'Creator User ID' => 'Пользователь, который создал',
    'Dt Creation' => 'Дата добавления',
    'Ip Creation' => 'IP добавления',
    'Updater User ID' => 'Пользователь, который обновил',
    'Dt Update' => 'Дата обновления',
    'Ip Update' => 'IP обновления',
];