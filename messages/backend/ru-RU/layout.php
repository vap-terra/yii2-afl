<?php
/**
 * Created by PhpStorm.
 * layout.php
 * @package /
 * @author Polyankin Semen Vicktorovich Vol][v, http://vap-terra.ru
 * @date 14.07.15
 * @time 16:15
 * @version 0.0.1
 * Copyright © 2014 VAP-TERRA, http://vap-terra.ru
 */
return [
    'Navigation' => 'Меню',
    'Dashboard' => 'Рабочий стол',
    'Sing In' => 'Авторизоваться',
    'Lost password' => 'Потеряли пароль',

    'Content' => 'Контент',
    'Page' => 'Страница',
    'Pages' => 'Страницы',
    'Constant' => 'Константа',
    'Constants' => 'Константы',
    'Block' => 'Блок',
    'Blocks' => 'Блоки',
    'Menu' => 'Меню',
    'Information' => 'Информация',
    'Board' => 'Доска объявлений',
    'Boards' => 'Доски объявлений',
    'Location' => 'Местоположение',
    'Currency' => 'Валюта',
    'Forms' => 'Формы',
    'Completed forms' => 'Заполненные формы',
    'General information' => 'Общая информация',
    'Instruction' => 'Инструкция',
    'Security' => 'Безопасность',
    'Reviews' => 'Отзывы',
    'Comments' => 'Комментарии',
    'Comment' => 'Комментарий',
    'Payroll information' => 'Списочная информация',
    'Settings' => 'Настройки',
    'Notifications' => 'Оповещения',

    'Profile' => 'Профиль',
    'Logout ({name})' => 'Выйти ({name})',
    'Profile ({name})' => 'Профиль ({name})',

    'List' => 'Список',
    'Lists' => 'Списки',
    'Lists of values' => 'Списки',
    'List of values <{name}>' => 'Элементы списка <{name}>',
    'Edit list <{name}>' => 'Ректировать список <{name}>',
    'Edit list value <{name}>' => 'Ректировать элемент <{name}>',

    'Form' => 'Форма',
    'form' => 'форма',

    'Banner' => 'Баннер',
    'Banners' => 'Баннеры',

    'Unit' => 'Еденица измерения',
    'Units' => 'Еденицы измерения',

    'Group' => 'Группа',
    'Groups' => 'Группы',

    'Select None' => 'Отменить отметить все',
    'Select All' => 'Отметить все',
    'Edit' => 'Редактировать',
    'Delete' => 'Удалить',
    'Filter' => 'Фильтр',
    'All' => 'Все',
    'Documents' => 'Документы',
    'Images' => 'Изображения',
    'Videos' => 'Видео',
    'Show Bar' => 'Показать',
    'Hide Bar' => 'Скрыть',
    'Download' => 'Скачать',
    'Apply' => 'Выбрать',
    'Select' => 'Отметить',
    'Only my' => 'Мои',
    'All users' => 'Всех',

    'Add group' => 'Добавить группу',
    'Add recipe' => 'Добавить рецепт',

    'Add' => 'Добавить',
    'Add menu item' => 'Добавить элемент в меню',
    'Add menu' => 'Добавить меню',
    'Add block' => 'Добавить блок',
    'Add list' => 'Добавить список',
    'Add list value' => 'Добавить элемент',
    'Add list value item' => 'Добавить элемент',
    'Add form' => 'Добавить форму',
    'Add constant' => 'Добавить константу',

    'Informations' => 'Инфосистемы',
    'Information <{name}>' => 'Инфосистема <{name}>',
    'Information Properties' => 'Доп. свойства',
    'Information properties' => 'Доп. свойства',
    'Add information item' => 'Добавить элемент',
    'Add information group' => 'Добавить группу',
    'Add information' => 'Добавить инфосистему',
    'Add information property' => 'Добавить доп. свойство',
    'Edit information property <{name}>' => 'Редактировать доп. свойство <{name}>',
    'Edit information <{name}>' => 'Редактировать инфосистему <{name}>',
    'Edit information group <{name}>' => 'Редактировать группу <{name}>',
    'Edit information item <{name}>' => 'Редактировать элемент <{name}>',

    'Add relation' => 'Добавить связь',
    'Add comment' => 'Добавить отзыв',

    'Edit comment <{name}>' => 'Ректировать комментарий <{name}>',

    'Banner group' => 'Группа',
    'Banner groups' => 'Группы',
    'Banner group <{name}>' => 'Группа баннеров <{name}>',
    'Add banner' => 'Добавить баннер',
    'Add banner group' => 'Добавить группу',
    'Edit banner <{name}>' => 'Редактировать баннер <{name}>',

    'Menu items' => 'Элементы меню',
    'Menus' => 'Меню',

    'Dictionaries' => 'Словари',

    'Countries' => 'Страны',
    'Country' => 'Страна',
    'Add country' => 'Добавить страну',
    'Regions' => 'Регионы',
    'Region' => 'Регион',
    'Add region' => 'Добавить регион',
    'Add country region' => 'Добавить регион',
    'Region areas' => 'Области/Округа',
    'Region area' => 'Облать/Край/Округ',
    'Add region area' => 'Добавить облать/край/округ',
    'Localities' => 'Города',
    'Locality' => 'Город',
    'Add locality' => 'Добавить город',
    'Locality areas' => 'Районы города',
    'Locality area' => 'Район город',
    'Add locality area' => 'Добавить район город',


    'User' => 'Пользователь',
    'Users' => 'Пользователи',
    'Add user'=>'Добавить пользователи',
    'add user'=>'добавить пользователи',
    'User group'=>'Группа пользователя',
    'User groups'=>'Группы пользователя',
    'Add user group'=>'Добавить группу',
    'add user group'=>'добавить группу',

    'Save' => 'Сохранить',
    'save' => 'сохранить',

    'Update' => 'Обновить',
    'update' => 'обновить',

    'Change' => 'Изменить',
    'Select file' => 'Выбрать файл',
    'Remove' => 'Удалить',

    'Yes' => 'Да',
    'es' => 'да',
    'No' => 'Нет',
    'no' => 'нет',

    'Generate Sitemap XML' => 'XML карта сайта',

    'Add item' => 'Добавить элемент',
    'Edit item <{name}>' => 'Редактировать элемент <{name}>',

    'Item {name} deleted successfully' => 'Элемент {name} успешно удален',
    'Failed to remove an item {name}' => 'Ошибка при сохранении элемента {name}',
    'Element {name} successfully saved!' => 'Элемент {name} успешно сохранен.',
    'Element {id} {name} successfully saved!' => 'Элемент {id} {name} успешно сохранен.',
    'Please fill in all required fields' => 'Пожалуйста заполните все обязательные поля',
    'Error saving element {name}' => 'Ошибка при сохранении элемента {name}',
    'Error saving element {id} {name}' => 'Ошибка при сохранении элемента {id} {name}',
    'You do not have permission to remove the item {name}' => 'У вас недостаточно прав для удаления элемента {name}',
];