/**
 * Created by volhv on 22.07.16.
 */
var __instanceNameForm = 'FormController';
(function($){

    'use strict';
    var FormController = function( container, options )
    {
        return this.initialize( container, options );
    };

    FormController.prototype = {
        options:{},
        xslResponder:null,
        $container:null,
        $form:null,
        $submitButton:null,
        defaults:{},
        initialize: function( container, options )
        {
            this.$container = $(container);
            if (typeof( container ) === 'object'
                && typeof( container.nodeName ) !== 'undefined'
                && container.nodeName == 'FORM'// instanceof HTMLFormElement
            ) {
                this.$form = $(container);
            } else {
                this.$form = $('form', container);
            }
            this.$submitButton = this.$form.find('input[type="submit"]');
            this
                .setData()
                .setOptions( options )
                .setVars()
                .build()
                .events();
            return this;
        },
        setData: function() {
            this.$container.data(__instanceNameForm, this);
            return this;
        },
        setVars: function()
        {
            return this;
        },
        setOptions: function(opts)
        {
            this.options = $.extend(true, {}, this.defaults, opts);
            return this;
        },
        build: function()
        {
            var self = this;
            return this;
        },
        events: function()
        {
            var self = this;
            if (typeof( $.fn.validate ) !== 'undefined') {
                this.$form.validate(
                    {
                        errorPlacement: function (error, element) {},
                        submitHandler: function (form) {
                            try {
                                self.send();
                            } catch (e){
                                console.log(e);
                            }
                            return false;
                        }
                    }
                );
            } else {
                this.$form.on('submit',function(){
                    try {
                        self.send();
                    } catch (e){
                        console.log(e);
                    }
                    return false;
                });
            }
            this.$submitButton.on('click', function () {
                //$form3.submit();
                if( self.$form.valid() ){
                    try {
                        self.send();
                    } catch (e){
                        console.log(e);
                    }
                }
                return false;
            });

            // маска
            if (typeof( $.fn.mask ) !== 'undefined') {
                self.$form.find("input[name='phone']").mask("+7 999 999 99 99");
            }

            return this;
        },
        send:function() {
            var self = this;
            var data = {};
            var inputs = $(this.$form).serializeArray();
            $.each(inputs,function (key,item) {
                data[item.name] = item.value;
            });
            data['guid'] = '';
            var options = {
                title: '',
                text: '',
                type: '',
                addclass: '',
                icon: '',
                opacity: 0.6,
                nonblock: {
                    /*nonblock: true*/
                }
            };
            $.post(
                $(this.$form).attr('action'),
                data,
                function (data) {
                    if (data.status == 'success') {
                        options.title = 'Успешно!';
                        options.text = data.response.message;
                        options.addclass = 'alert alert-success';
                        options.icon = 'glyphicon glyphicon-ok';
                        self.$form.get(0).reset();
                    } else {
                        var message = data.response.message;
                        if( typeof(data.response.errors) !== 'undefined' && !$.isEmptyObject( data.response.errors )  ){
                            for( var k in data.response.errors ){
                                if( $.isArray( data.response.errors[k] ) || !$.isEmptyObject( data.response.errors[k] ) ){
                                    for( var m in data.response.errors[k] ){
                                        message += '<p>'+data.response.errors[k][m]+'</p>';
                                    }
                                } else {
                                    message += '<p>'+data.response.errors[k]+'</p>';
                                }
                            }
                        }
                        options.title = 'Ошибка!';
                        options.text = message;
                        options.type = data.status;
                        options.addclass = 'alert alert-danger';
                        options.icon = 'glyphicon glyphicon-alert';
                    }

                    $(window).trigger('form-send-'+data.status,[self.$form,self.$container,options]);

                    if (typeof( PNotify ) !== 'undefined' ) {
                        new PNotify(options);
                    } else if( typeof( $.fn.fancybox ) !== 'undefined' ){
                        $.fancybox('<div class="'+options.addclass+'">' +
                            '<h1><i class="'+options.icon+'"></i> '+options.title+'</h1>'+
                            options.text+'</div>'
                        );
                    }
                },
                'json'
            );
        }
    }

    if (typeof( $.fn.FormController ) == 'undefined') {
        $.fn.FormController = function () {
            var options = {};
            if (
                typeof(arguments[0]) !== 'undefined'
                && arguments[0] != null
                && arguments[0] instanceof Object != false
            ) {
                $.extend(options, arguments[0]);
            }

            return $(this).each(
                function ()
                {
                    var $this = $(this);
                    if ($this.data(__instanceNameForm)) {
                        return $this.data(__instanceNameForm);
                    } else {
                        return new FormController(this,options);
                    }
                }
            );
        }
    }

})(jQuery);