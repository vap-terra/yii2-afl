/**
 * Created by volhv on 07.08.16.
 */
(function($) {
    $(document).ready(function () {
        if( typeof $.fn.fancybox !== 'undefined' ) {
            $('.ajax-load-form-link').fancybox({});
        }
    });
})(jQuery);
