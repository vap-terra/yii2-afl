/**
 * Created by volhv on 27.06.16.
 */
var __instanceNameFormBackend = 'FormControllerBackend';
(function ($) {

    'use strict';

    var FormControllerBackend = function ($container, options) {
        return this.initialize($container, options);
    };

    FormControllerBackend.prototype = {
        options: {},
        xslResponder: null,
        $container: null,
        $list: null,
        $addButton: null,
        defaults: {},
        initialize: function ($container, options) {
            this.$container = $container;
            this
                .setData()
                .setOptions(options)
                .setVars()
                .build()
                .events();
            return this;
        },
        setData: function () {
            this.$container.data(__instanceNameFormBackend, this);
            return this;
        },
        setVars: function () {
            this.$list = this.$container.find('.form-fields');
            this.$addButton = this.$container.find('.add-form-field');
            return this;
        },
        setOptions: function (opts) {
            this.options = $.extend(true, {}, this.defaults, opts);
            return this;
        },
        build: function () {
            var self = this;
            return this;
        },
        events: function () {
            var self = this;
            this.$container.find('.form-element').each(
                function () {
                    self.eventsField($(this));
                }
            );
            this.$addButton.on(
                'click',
                function () {
                    try {
                        self.addField($(this));
                    } catch (e) {
                        console.log(e);
                    }
                    return false;
                }
            );
            this.$list.sortable({
                revert: true,
                delay: 300,
                cursor: "move",
                items: ".form-element",
                stop: function (event, ui) {
                    var position = self.$list.find('.form-element').index(ui.item);
                    //console.log( position );
                    ui.item.on(
                        'errorReposition',
                        function (event) {
                            self.$list.sortable("cancel");
                        }
                    ).on(
                        'alwaysReposition',
                        function (event) {
                            $(this).off('errorReposition');
                            $(this).off('alwaysReposition');
                        }
                    );

                    self.reposition(ui.item, position);
                },
                activate: function (event, ui) {
                }
            });
            return this;
        },
        eventsField: function ($element) {
            var self = this;
            $element.find('.remove-form-element').on(
                'click',
                function () {
                    try {
                        self.removeField($element);
                    } catch (e) {
                        console.log(e);
                    }
                    return false;
                }
            );
            $element.find('.save-form-element').on(
                'click',
                function () {
                    try {
                        self.saveField($element);
                    } catch (e) {
                        console.log(e);
                    }
                    return false;
                }
            );
            $element.find('select[name="FormElement[type]"]').on(
                'change',
                function () {
                    try {
                        self.changeType($element);
                    } catch (e) {
                        console.log(e);
                    }
                    return false;
                }
            );
        },
        themeField: function ($element)
        {
            var $inputs = $element.find('input[type="hidden"],input[type="text"],input[type="checkbox"],input[type="radio"],input[type="tel"],input[type="email"],input[type="url"],textarea,select');
            $inputs.each(
                function ()
                {
                    if(  typeof( $(this).attr('data-plugin-selectTwo') ) !== 'undefined' ) {
                        var select2 = $(this).themePluginSelect2({placeholder: ""}).data('__select2');
                    }
                    if ( typeof( $(this).attr('data-plugin-tinymce') ) !== 'undefined' && typeof tinymce == 'object' ) {
                        tinyMCE.execCommand("mceRemoveEditor",false, this.id);
                        tinyMCE.execCommand("mceAddEditor",false, this.id);
                    }
                    if( typeof( $(this).attr('data-plugin-ios-switch') ) !== 'undefined' ) {
                        var switcher = $(this).themePluginIOS7Switch().data('__IOS7Switch');
                    }
                }
            );
            return this;
        },
        addField: function ($actionElement) {
            var self = this;
            var url = $actionElement.attr('href');
            if (typeof( url ) == 'undefined') {
                url = $actionElement.attr('data-href');
            }
            $.ajax(
                {
                    url: url,
                    data: {},
                    type: 'get',
                }
            ).done(
                function (response) {
                    var $element = $(response);
                    self.$list.append($element);
                    self.eventsField($element);
                    self.themeField($element);
                }
            ).always(
                function () {

                }
            ).fail(
                function () {

                }
            );
        },
        saveField: function ($element) {
            var self = this;
            var url = $element.find('.save-form-element').attr('href');
            if (typeof( url ) == 'undefined') {
                url = $element.find('.save-form-element').attr('data-href');
            }
            var data = {};
            var $inputs = $element.find('input[type="hidden"],input[type="text"],input[type="checkbox"]:checked,input[type="radio"]:checked,input[type="tel"],input[type="email"],input[type="url"],textarea,select');
            $inputs.each(function () {
                if ($(this).hasClass('ckeditor')) {
                    CKEDITOR.instances[$(this).attr('id')].updateElement();
                }
                data[$(this).attr('name')] = $(this).val();
            });
            $.ajax(
                {
                    url: url,
                    data: data,
                    type: 'post',
                }
            ).done(
                function (response) {
                    if (response.status && response.status == 'success') {

                    }
                }
            ).always(
                function () {
                }
            ).fail(
                function () {
                }
            );
        },
        removeField: function ($element) {
            var self = this;
            var url = $element.find('.remove-form-element').attr('href');
            if (typeof( url ) == 'undefined') {
                url = $element.find('.remove-form-element').attr('data-href');
            }
            $.ajax(
                {
                    url: url,
                    data: {},
                    type: 'get',
                }
            ).done(
                function (response) {
                    $element.remove();
                }
            ).always(
                function () {

                }
            ).fail(
                function () {

                }
            );
        },
        reposition: function ($element, newPosition) {
            var self = this;
            var url = $element.find('.move-form-element').attr('href');
            if (typeof( url ) == 'undefined') {
                url = $element.find('.move-form-element').attr('data-href');
            }
            var elementId = parseInt($element.attr('data-id'));
            $.ajax(
                {
                    url: url,
                    data: {number: newPosition},
                    type: 'get',
                }
            ).done(
                function (response) {
                    self.$container.trigger('doneReposition', $element);
                    $element.trigger('doneReposition');
                }
            ).fail(
                function () {
                    self.$container.trigger('failReposition', $element);
                    $element.trigger('failReposition');
                }
            ).always(
                function () {
                    self.$container.trigger('alwaysReposition', $element);
                    $element.trigger('alwaysReposition');
                }
            );
        },
        changeType: function ($element) {
            self.themeField($element);
        },
    };


    if (typeof( $.fn.FormControllerBackend ) == 'undefined') {
        $.fn.FormControllerBackend = function () {
            var options = {};
            if (
                typeof(arguments[0]) !== 'undefined'
                && arguments[0] != null
                && arguments[0] instanceof Object != false
            ) {
                $.extend(options, arguments[0]);
            }

            return $(this).each(
                function () {
                    var $this = $(this);
                    if ($this.data(__instanceNameFormBackend)) {
                        return $this.data(__instanceNameFormBackend);
                    } else {
                        return new FormControllerBackend($this,options);
                    }
                }
            );
        }
    }

})(jQuery);
