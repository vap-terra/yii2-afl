/**
 * Created by volhv on 14.03.16.
 */
(function($) {

    $.extend({
        filterTop:null,
        filterTopHeight:52,
        filterTopChangeHeight: function(){
            $.filterTop.css( { height : 'auto' } );
        },
        filterTopResetHeight: function(){
            $.filterTop.css( { height : 'auto' } );
        },
        initDateRangeFilter: function ($element) {
            var $trigger = $element.find('a.trigger');
            var elementSelector = $trigger.attr('data-selector');
            var $container = $(elementSelector);

            //console.log( $trigger );
            //console.log( elementSelector );

            $trigger.on(
                'click',
                function(){

                    if($container.is(':visible')) {

                        $container.slideUp(0);
                        $.filterTopResetHeight();
                    } else {

                        $container.slideDown(0);
                        $.filterTopChangeHeight();
                    }
                    return false;
                }
            );
        },
        initFilterPanel:function( $container ) {
            $.filterTop = $container;
            $.filterTopHeight = $container.outerHeight(true);
            $.initDateRangeFilter( $container.find('li.date-range') );
        }
    });
    $.initFilterPanel( $( '.media-gallery .inner-toolbar' ) );
}(jQuery));

