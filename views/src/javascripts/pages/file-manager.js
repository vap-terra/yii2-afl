var fileManagerController = function( $object ) {

	var _csrf = $object.attr('data-csrf');
	var $filesWrapper = $object.find('.mg-files');

	this.editor = $filesWrapper.attr('data-editor');
	this.type = $filesWrapper.attr('data-type');
	this.folder = $filesWrapper.attr('data-folder');

	var fileManager = this;

	this.loadFiles = function() {

	};

	this.loadFolders = function() {

	};

	this.applyFile = function($file) {
		var window_parent = window.parent ? window.parent : window.opener;
		var editor = $filesWrapper.attr('data-editor');
		var type = $filesWrapper.attr('data-type');
		var url = $file.parents('.isotope-item').attr('data-url');
		//console.log( type );
		//console.log( editor );

		if( typeof( parent.$.fancybox ) !== 'undefined' ) {

		} else {
			var $input, $wrapperInput;
			if (type == 1) {
				$wrapperInput = $('.mce-link_' + editor, window_parent.document);
			} else if (type == 2) {
				$wrapperInput = $('.mce-img_' + editor, window_parent.document);
			} else if (type == 3 ) {
				$wrapperInput = $('.mce-video' + type + '_' + editor, window_parent.document);
			}

			if( $wrapperInput.get(0).tagName !== 'INPUT' ) {
				$input = $('input',$wrapperInput);
			} else {
				$input = $wrapperInput;
			}

			$input.val(url);
		}

		this.closeWindow();
	};

	this.closeWindow = function() {
		if( typeof( parent.$.fancybox ) !== 'undefined' ) {
			parent.$.fancybox.close();
		} else {
			//window.close();
			parent.tinyMCE.activeEditor.windowManager.close(window);
		}
	}

	this.generateFileView = function() {

	}

	this.initItem = function( $item ){
		/*
		 Thumbnail: Select
		 */
		$item.find('.mg-option input[type=checkbox]').on('change', function( ev ) {
			var wrapper = $(this).parents('.thumbnail');
			if($(this).is(':checked')) {
				wrapper.addClass('thumbnail-selected');
			} else {
				wrapper.removeClass('thumbnail-selected');
			}
		});

		$item.find('.mg-option input[type=checkbox]:checked').trigger('change');

		//mg-download-item
		$item.find('.mg-delete-item').on('click', function( ev ) {
			ev.preventDefault();
			//var $destination = $('[data-sort-destination][data-sort-id="file-manager"]');
			var $this = $(this);
			var data = {'_csrf':_csrf,'file':[]};

			$.get(
				$this.attr('href'),
				data
			).done(
				function( response ) {
					for( var k in response.result ) {
						var result = response.result[k];
						var $fileCheck = $filesWrapper.find('.mg-option input[type=checkbox][value="'+k+'"]');
						var $file = $fileCheck.parents('.isotope-item');
						if( result.status == 'success' ) {
							//$file.remove();
							$filesWrapper.isotope('remove', $file).isotope('layout');
						} else {
							$file.addClass('error');
						}
					}
				}
			);
		});

		/*
		 */
		$item.find('.thumbnail a.apply-file-action').on(
			'click',
			function() {
				fileManager.applyFile( $(this) );
				return false;
			}
		);


		/*
		 Image Preview: Lightbox
		 */
		$item.find('.thumb-preview a[href].thumb-image').magnificPopup({
			type: 'image',
			closeOnContentClick: true,
			mainClass: 'mfp-img-mobile',
			image: {
				verticalFit: true
			}
		});
		$item.find('.thumb-preview .mg-zoom').on('click.lightbox', function( ev ) {
			ev.preventDefault();
			$(this).closest('.thumb-preview').find('a.thumb-image').triggerHandler('click');
		});

		/*
		 Thumnail: Dropdown Options
		 */
		$item.find('.thumbnail .mg-toggle').parent()
			.on('show.bs.dropdown', function( ev ) {
				$(this).closest('.mg-thumb-options').css('overflow', 'visible');
			})
			.on('hidden.bs.dropdown', function( ev ) {
				$(this).closest('.mg-thumb-options').css('overflow', '');
			});

		$item.find('.thumbnail').on('mouseenter', function() {
			var toggle = $(this).find('.mg-toggle');
			if ( toggle.parent().hasClass('open') ) {
				toggle.dropdown('toggle');
			}
		});
	}

	this.init = function() {
		var $items = $object.find('.file-item');
		$items.each( function() {
			fileManager.initItem( $(this) );
		});
	}

	return this;
};

(function($) {

	var $fileManager = $('#file_manager');
	var fileManager = new fileManagerController($fileManager);
	fileManager.init();
	var _csrf = $fileManager.attr('data-csrf');
	var $destination = $('[data-sort-destination][data-sort-id="file-manager"]');

	Dropzone.options.dropzoneFileManager = {
		dictDefaultMessage : 'Загрузить файлы',
		accept: function(file, done)
		{
			var extension=file.name.split('.').pop().toLowerCase();
			/*if ($.inArray(extension, allowed_ext) > -1) {
			 done();
			 } else {
			 done("File extension is not allowed.");
			 }*/
			done();
		},
		success: function( file, response )
		{
			//console.log( arguments );
			try {
				var obj = response;//$.parseJSON(response);
				//var $destination = $('[data-sort-destination][data-sort-id="file-manager"]');
				//console.log($destination);
				var $item = $(obj.view);
				//console.log( fileManager.folder + '==' + obj.info.relativePath);
				var $fileCheck = $destination.find('.mg-option input[type=checkbox][value="'+obj.info.name+'"]');

				if( fileManager.folder == obj.info.relativePath.replace(/\//gi,'') ) {
					if( $fileCheck.length == 0 ) {
						$destination.append($item);
						$destination.isotope().isotope('appended', $item)
							.isotope('layout');
					} else {

					}
				} else {

				}
				fileManager.initItem( $item );
			} catch ( e ){
				console.log( e );
			}
			var dropZone = this;
			window.setTimeout(
				function(){
					dropZone.removeFile(file)
				},
				5000
			);
			return true;
		},
		complete : function( file ){
			if( file.status == 'error' )
			{
				var dropZone = this;
				window.setTimeout(
					function () {
						dropZone.removeFile(file)
					},
					5000
				);
			}
		}

	};

	$('.mg-folders li').each( function(){
		var $link = $(this).find('> a');
		$link.on('click',function(){

			//return false;
		});
	} );

	/*
	 Toolbar: Select All
	 */
	$('#mgSelectAll').on('click', function( ev ) {
		ev.preventDefault();
		var $this = $(this),
			$label = $this.find('> span'),
			$checks = $('.mg-option input[type=checkbox]');

		if($this.attr('data-all-selected')) {
			$this.removeAttr('data-all-selected');
			$checks.prop('checked', false).trigger('change');
			$label.html($label.data('all-text'));
		} else {
			$this.attr('data-all-selected', 'true');
			$checks.prop('checked', true).trigger('change');
			$label.html($label.data('none-text'));
		}
	});



	/*
	 Toolbar: Delete selected
	 */
	$('#mgDeleteSelected').on('click', function( ev ) {
		ev.preventDefault();
		//var $destination = $('[data-sort-destination][data-sort-id="file-manager"]');
		var $this = $(this),
			$checks = $('.mg-option input[type=checkbox]:checked');

		var data = {'_csrf':_csrf,'file':[]};
		$checks.each( function(){
			data.file[data.file.length] = this.value;
		} );

		$.get(
			$this.attr('href'),
			data

		).done(
			function( response ) {
				for( var k in response.result ) {
					var result = response.result[k];
					var $fileCheck = $checks.filter('[value="'+k+'"]');
					var $file = $fileCheck.parents('.isotope-item');
					if( result.status == 'success' ) {
						//$file.remove();
						$destination.isotope('remove', $file).isotope('layout');
					} else {
						$file.addClass('error');
					}
				}
			}
		);
	});

	/*
	 Isotope: Sort Thumbnails
	 */
	$("[data-sort-source]").each(function() {

		var source = $(this);

		//console.log( source );

		var destination = $("[data-sort-destination][data-sort-id=" + $(this).attr("data-sort-id") + "]");

		if(destination.get(0)) {

			$(window).load(function() {

				var defaultSortBy = 'name';
				destination.isotope({
					itemSelector: ".isotope-item",
					layoutMode: 'fitRows',
					getSortData: {
						'name': '.name',
						'type': function( itemElem ) { // function
							return $( itemElem ).find('.name > small').text();
						},
						'date': '.date',
					}
				});

				var $mainContainer = $('html.file-manager .inner-body.mg-main'),
					wMainContainer = $mainContainer.width();

				$(window).on('sidebar-left-toggle inner-menu-toggle', function() {
					if( $('html.file-manager').hasClass('inner-menu-opened') ) {
						$mainContainer.width(wMainContainer - 200);
					} else {
						$mainContainer.width(wMainContainer);
					}
					var $link = source.find("a[data-option-value].active"),
						sort = $link.attr("data-option-value");
					destination.isotope({
						sortBy : sort
					});
				});

				source.find("a[data-option-value]").click(function(e) {

					e.preventDefault();

					var $this = $(this),
						sort = $this.attr("data-option-value");

					source.find(".active").removeClass("active");
					$this.closest("li").addClass("active");

					destination.isotope({
						//filter: filter
						sortBy : sort
					});

					/*if(window.location.hash != "" || filter.replace(".","") != "*") {
					 window.location.hash = filter.replace(".","");
					 }*/

					return false;

				});

				$(window).bind("hashchange", function(e) {

					var hashSort = location.hash.replace("#",""),
						sort = hashSort;

					source.find(".active").removeClass("active");
					source.find("[data-option-value='" + sort + "']").closest("li").addClass("active");

					destination.isotope({
						sortBy: sort
					});

				});

				var hashSort = (location.hash.replace("#","") || defaultSortBy );
				var initFilterEl = source.find("a[data-option-value='" + hashSort + "']");

				if(initFilterEl.get(0)) {
					source.find("[data-option-value='" + hashSort + "']").click();
				} else {
					source.find(".active a").click();
				}

			});

		}

	});

}(jQuery));