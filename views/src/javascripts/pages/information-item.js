/**
 * Created by volhv on 14.03.16.
 */
(function($) {
    $.extend({
        loadPropertiesFormByGroup: function ($element) {
            //подгружаем регионы
            var parameter = 'information_group_id';
            var url = $element.attr('data-url');
            var $dependent = $('#addition-parameters .panel-body > .properties');
            var data = {};
            data[parameter] = $element.val();
            $.ajax(
                {
                    url: url,
                    data: data
                }
            ).done(
                function (response) {
                    if (response.length > 0) {
                        $dependent.html(response);
                        $.initProperties( $dependent );
                    } else {
                        $dependent.html('');
                    }
                }
            ).fail(
                function () {
                }
            ).always(
                function () {
                }
            );
        },
        addPropertyValue:function( $property ) {

        },
        deletePropertyValue:function( $property ) {

        },
        propertyTypeValueHtml: {},
        initProperties:function( $container ) {
            $container.find('.property').each( function(){
                var $property = $(this);
                var type = $property.attr('data-type');
                var propertyId = $property.attr('id');
                var $valuesContainer = $property.find('.property-values');
                var $values = $valuesContainer.find('[data-property-value-id]');

                $values.each( function(){
                    var $value = $(this);
                    var id = $value.attr('data-property-value-id');
                    if( !id && typeof( $.propertyTypeValueHtml[type] ) === 'undefined' ) {
                        $.propertyTypeValueHtml[propertyId] = $value.clone();
                    }
                    $value.find('.delete-btn').on('click',function(){
                        $value.remove();
                    });
                } );

                $property.find('.add-btn').on('click',function(){
                    var $value = $.propertyTypeValueHtml[propertyId].clone();
                    $valuesContainer.append( $value );
                    $value.find('.delete-btn').on('click',function(){
                        $value.remove();
                    });

                    var $values = $valuesContainer.find('[data-property-value-id]');
                    var cnt = $values.length;
                    if( cnt > 1 ) {
                        $values.each( function(n,e){
                           if( ( cnt - 1 ) > n ) {
                               $(e).find('.input-group-btn').removeClass('hidden').show();
                           }
                        });
                    }
                });

            } );
        }
    });
    $.initProperties( $( '#addition-parameters .panel-body > .properties' ) );
    $.initProperties( $( '#media .panel-body' ) );
}(jQuery));

