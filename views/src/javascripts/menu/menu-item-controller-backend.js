/**
 * Created by volhv on 27.06.16.
 */
(function ($) {

    'use strict';

    var instanceName = 'MenuItemControllerBackend';

    var MenuItemControllerBackend = function ($container, options) {
        return this.initialize($container, options);
    };

    MenuItemControllerBackend.prototype =
    {
        options: {},
        xslResponder: null,
        $container: null,
        $list: null,
        $addButton: null,
        defaults: {},
        countRelations: 0,
        initialize: function ($container, options)
        {
            this.$container = $container;
            this
                .setData()
                .setOptions(options)
                .setVars()
                .build()
                .events();
            return this;
        },
        setData: function ()
        {
            this.$container.data(instanceName, this);
            return this;
        },
        setVars: function ()
        {
            this.$list = this.$container.find('.menu-item-relation-list');
            this.$addButton = this.$container.find('.add-menu-item-relation');
            return this;
        },
        setOptions: function (opts)
        {
            this.options = $.extend(true, {}, this.defaults, opts);
            return this;
        },
        build: function ()
        {
            var self = this;
            return this;
        },
        loadData: function(eventName,$element,params)
        {
            var self = this;
            $.ajax(
                params
            ).done(
                function (response, textStatus, jqXHR )
                {
                    var status = 'done';
                    try {
                       response = $.parseJSON(response);
                    } catch( e ){
                        //console.log( e );
                    }
                    if( typeof( response.status ) !== 'undefined' && response.status == 'error' ){
                        status = 'fail';
                        console.log(status + ' ' + eventName);
                    }
                    self.$container.trigger(status+eventName, [$element, response, params]);
                    //$element.trigger(status+eventName, [response, params]);
                }
            ).fail(
                function ( jqXHR, textStatus, errorThrown )
                {
                    self.$container.trigger('fail'+eventName, [$element, jqXHR, params]);
                    //$element.trigger('fail'+eventName);
                }
            ).always(
                function ( data, textStatus, errorThrown ) //data|jqXHR, textStatus, jqXHR|errorThrown
                {
                    self.$container.trigger('always'+eventName, [$element, data, params]);
                    //$element.trigger('always'+eventName, [params]);
                }
            );
        },
        events: function ()
        {
            var self = this;
            this.$container.find('.menu-item-relation').each(
                function () {
                    self.eventsItem($(this));
                }
            );
            this.$addButton.on(
                'click',
                function () {
                    try {
                        self.addItemAction($(this));
                    } catch (e) {
                        console.log(e);
                    }
                    return false;
                }
            );
            this.$list.sortable({
                revert: true,
                delay: 300,
                cursor: "move",
                items: ".menu-item-relation",
                stop: function (event, ui) {
                    var position = self.$list.find('.menu-item-relation').index(ui.item);
                    self.repositionAction(ui.item, position);
                },
                activate: function (event, ui) {}
            });
            self.$container.on(
                'errorReposition',
                function (event)
                {
                    self.$list.sortable("cancel");
                }
            );
            self.$container.on('doneAddItem',self.addItem);
            self.$container.on('doneChangeType',self.changeType);
            self.$container.on('doneChangeContent',self.changeContent);
            self.$container.on('doneRemoveItem',self.removeItem);
            self.$container.on('doneSaveItem',self.saveItem);
            return this;
        },
        eventsItem: function ($element)
        {
            var self = this;
            $element.find('.remove-menu-item-relation').on(
                'click',
                function () {
                    try {
                        self.removeItemAction($element);
                    } catch (e) {
                        console.log(e);
                    }
                    return false;
                }
            );
            $element.find('.save-menu-item-relation').on(
                'click',
                function () {
                    try {
                        self.saveItemAction($element);
                    } catch (e) {
                        console.log(e);
                    }
                    return false;
                }
            );
            $element.find('select.MenuItemRelationExt-type').on(
                'change',
                function () {
                    try {
                        self.changeTypeAction($element);
                    } catch (e) {
                        console.log(e);
                    }
                    return false;
                }
            );
            $element.find('select.MenuItemRelationExt-contentext-id').on(
                'change',
                function () {
                    try {
                        self.changeContentAction($element);
                    } catch (e) {
                        console.log(e);
                    }
                    return false;
                }
            );
            return this;
        },
        themeItem: function ($element)
        {
            var $inputs = $element.find('input[type="hidden"],input[type="text"],input[type="checkbox"],input[type="radio"],input[type="tel"],input[type="email"],input[type="url"],textarea,select');
            $inputs.each(
                function ()
                {
                    if(  typeof( $(this).attr('data-plugin-selectTwo') ) !== 'undefined' ) {
                        var select2 = $(this).themePluginSelect2({placeholder: ""}).data('__select2');
                    }
                    if ( typeof( $(this).attr('data-plugin-tinymce') ) !== 'undefined' && typeof tinymce == 'object' ) {
                        tinyMCE.execCommand("mceRemoveEditor",false, this.id);
                        tinyMCE.execCommand("mceAddEditor",false, this.id);
                    }
                    if( typeof( $(this).attr('data-plugin-ios-switch') ) !== 'undefined' ) {
                        var switcher = $(this).themePluginIOS7Switch().data('__IOS7Switch');
                    }
                }
            );
            return this;
        },
        addItem: function (event, $button, data, params)
        {
            var self = $(this).data(instanceName);
            var $item = $(data);
            self.$list.append($item);
            self.countRelations++;
            self.eventsItem($item)
                .themeItem($item)
            ;
        },
        addItemAction: function ($button)
        {
            var self = this;
            var url = $button.attr('href');
            if (typeof( url ) == 'undefined') {
                url = $button.attr('data-href');
            }
            var params = {
                url: url,
                data: {num:self.countRelations+1},
                type: 'get',
            };
            self.loadData('AddItem',$button,params);
        },
        saveItem: function(event, $element, data, params)
        {
            var self = $(this).data(instanceName);
        },
        saveItemAction: function ($element)
        {
            var self = this;
            var url = $element.find('.save-menu-item-relation').attr('href');
            if (typeof( url ) == 'undefined') {
                url = $element.find('.save-menu-item-relation').attr('data-href');
            }
            var data = {};
            var $inputs = $element.find('input[type="hidden"],input[type="text"],input[type="checkbox"]:checked,input[type="radio"]:checked,input[type="tel"],input[type="email"],input[type="url"],textarea,select');
            $inputs.each(function () {
                if ($(this).hasClass('textarea-editor-tinymce')) {
                    tinyMCE.triggerSave();
                    //tinyMCE.updateContent($(this).attr('id'));
                }
                data[$(this).attr('name')] = $(this).val();
            });
            var params = {
                url: url,
                data: data,
                type: 'post',
            };
            this.loadData('SaveItem',$element,params);
        },
        removeItem: function(event, $element, data, params)
        {
            var self = $(this).data(instanceName);
            $element.remove();
        },
        removeItemAction: function ($element)
        {
            var self = this;
            var url = $element.find('.remove-menu-item-relation').attr('href');
            if (typeof( url ) == 'undefined') {
                url = $element.find('.remove-menu-item-relation').attr('data-href');
            }
            var params = {
                url: url,
                data: {},
                type: 'get',
            };
            this.loadData('RemoveItem',$element,params);
        },
        reposition:function(event, $element, data, params)
        {
            var self = $(this).data(instanceName);
        },
        repositionAction: function ($element, newPosition)
        {
            var self = this;
            var url = $element.find('.move-menu-item-relation').attr('href');
            if (typeof( url ) == 'undefined') {
                url = $element.find('.move-menu-item-relation').attr('data-href');
            }
            var elementId = parseInt($element.attr('data-id'));
            var params = {
                url: url,
                data: {number: newPosition},
                type: 'get',
            };
            this.loadData('Reposition',$element,params);
        },
        changeType:function(event, $element, data, params){
            var self = $(this).data(instanceName);
            // Загружаем часть формы для типа
            // со скриптами для обработки этой части формы
            $element.find('.item-parameter').html(data);
            self.themeItem($element);
        },
        changeTypeAction: function ($element)
        {
            var url = $element.find('select.MenuItemRelationExt-type').attr('href');
            if (typeof( url ) == 'undefined') {
                url = $element.find('select.MenuItemRelationExt-type').attr('data-href');
            }
            var params = {
                url: url,
                data: {
                    type: $element.find('select.MenuItemRelationExt-type').val()
                },
                type: 'get',
            };
            this.loadData('ChangeType',$element,params);
        },
        changeContent:function(event, $element, data, params){
            var self = $(this).data(instanceName);
            // Загружаем часть формы для типа
            // со скриптами для обработки этой части формы
            $element.find('.item-parameter-content-data').html(data);
            self.themeItem($element);
        },
        changeContentAction: function ($element)
        {
            var url = $element.find('select.MenuItemRelationExt-contentext-id').attr('href');
            if (typeof( url ) == 'undefined') {
                url = $element.find('select.MenuItemRelationExt-contentext-id').attr('data-href');
            }
            var params = {
                url: url,
                data: {
                    contentId: $element.find('select.MenuItemRelationExt-contentext-id').val()
                },
                type: 'get',
            };
            this.loadData('ChangeContent',$element,params);
        },
    };


    if (typeof( $.fn.MenuItemControllerBackend ) == 'undefined')
    {
        $.fn.MenuItemControllerBackend = function ()
        {
            var options = {};
            if (
                typeof(arguments[0]) !== 'undefined'
                && arguments[0] != null
                && arguments[0] instanceof Object != false
            ) {
                $.extend(options, arguments[0]);
            }

            return $(this).each(
                function ()
                {
                    var $this = $(this);
                    if ($this.data(instanceName)) {
                        return $this.data(instanceName);
                    } else {
                        return new MenuItemControllerBackend($this,options);
                    }
                }
            );
        }
    }

})(jQuery);
