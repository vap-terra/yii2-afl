<?php

/* @var $this \yii\web\View */
/* @var $content string */


use app\widgets\Alert;
use yii\helpers\Html;

app\views\backend\themes\protoadmin\assets\BackendAsset::register($this);

$queryParams = Yii::$app->request->getQueryParams();

$collapsedMenu = false;
?><?php $this->beginPage() ?><!DOCTYPE html>
	<html lang="<?php echo Yii::$app->language ?>" class="fixed sidebar-light sidebar-left-xs inner-menu-opened<?php if( $collapsedMenu ){?> sidebar-left-collapsed sidebar-left-with-menu<?php }?> file-manager" data-style-switcher-options="{'sidebarColor': 'light','sidebarSize': 'xs'}">
	<head>
		<meta charset="<?php echo Yii::$app->charset ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>

		<meta name="description" content="">
		<meta name="keywords" content=""/>
		<meta name="author" content="">

		<?php echo Html::csrfMetaTags() ?>
		<title><?php echo Html::encode($this->title) ?></title>

		<?php $this->head() ?>

	</head>
	<body>

	<?php $this->beginBody() ?>

	<section class="body">


		<!-- end: header -->

		<div class="inner-wrapper">

			<section role="main" class="content-body">

				<?php echo Alert::widget() ?>

				<?php echo $content ?>

			</section>
		</div>

	</section>


	<?php $this->endBody() ?>
	</body>
	</html>
<?php $this->endPage() ?>