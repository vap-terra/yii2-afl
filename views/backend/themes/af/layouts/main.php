<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\models\ext\ConstantExt;
use app\modules\BackendModule;
use app\widgets\Alert;
use app\widgets\PersonalBlock;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

app\views\backend\themes\af\assets\BackendAsset::register($this);

$queryParams = Yii::$app->request->getQueryParams();

?><?php $this->beginPage() ?><!DOCTYPE html>
<html lang="<?php echo Yii::$app->language ?>" class="sidebar-light sidebar-left-xs">
    <head>
        <meta charset="<?php echo Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/> -->

        <?php echo Html::csrfMetaTags() ?>
        <title><?php echo Html::encode($this->title) ?></title>

        <?php $this->head() ?>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body>
    <?php $this->beginBody() ?>

    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/" target="_blank"><i
                        class="fa fa-eye fa-fw"></i><?php echo ConstantExt::getValueByTid('site_name'); ?></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <?php
                $items = BackendModule::generateMenuItems();
                echo app\components\bootstrap\Nav::widget([
                    'options' => ['class' => 'nav navbar-nav'],
                    'items' => $items,
                    'activateItems' => true,
                    'activateParents' => true,
                ]);
                ?>
                
                <?php echo PersonalBlock::widget(['options'=>['class'=>'nav navbar-nav navbar-right']]); ?>

                <?php
                $items = BackendModule::generateRightMenuItems();
                echo app\components\bootstrap\Nav::widget([
                    'options' => ['class' => 'nav navbar-nav navbar-right'],
                    'items' => $items,
                    'activateItems' => true,
                    'activateParents' => true,
                ]);
                ?>



            </div>
        </div>
    </nav>

    <div class="inner-wrapper">


    <section role="main" class="content-body">

        <header class="page-header">
            <h2><?php echo $this->title ?></h2>

            <div class="right-wrapper pull-right">
                <?php echo Breadcrumbs::widget([
                    'tag' => 'ol',
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    'homeLink' => ['url' => '/', 'label' => '<i class="fa fa-home"></i>', 'encode' => false,],
                    'activeItemTemplate' => "<li class=\"active\"><span>{link}</span></li>\n",
                    'options' => ['class' => 'breadcrumbs',],
                ]) ?>
                <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
            </div>
        </header>

        <?php echo Alert::widget() ?>

        <!-- start: page -->
        <?php echo $content ?>
        <!-- end: page -->

        <p class="text-center"><?php echo \app\components\web\Application::getName(); ?> v <?php echo \app\components\web\Application::getVersion(); ?></p>
    </section>

    </div>


    <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>