<?php
/**
 * Created by PhpStorm.
 * FontAwesomeAsset.php
 * @package /
 * @author Polyankin Semen Vicktorovich Vol][v, http://vap-terra.ru
 * @date 06.08.15
 * @time 22:54
 * @version 0.0.1
 * Copyright © 2014 VAP-TERRA, http://vap-terra.ru
 */

namespace app\views\backend\themes\af\assets;

use yii\web\AssetBundle;
class FileManagerAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $sourcePath = '@app/views/backend/themes/af/src';
    public $css = [
    ];

    public $js = [
        'js/pages/file-manager.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
        'app\assets\IsotopeAsset',
        'app\assets\DropzoneAsset',
        'app\assets\MagnificPopupAsset',
    ];
}