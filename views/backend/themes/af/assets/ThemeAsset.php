<?php
/**
 * Created by PhpStorm.
 * FontAwesomeAsset.php
 * @package /
 * @author Polyankin Semen Vicktorovich Vol][v, http://vap-terra.ru
 * @date 06.08.15
 * @time 22:54
 * @version 0.0.1
 * Copyright © 2014 VAP-TERRA, http://vap-terra.ru
 */

namespace app\views\backend\themes\af\assets;

use yii\web\AssetBundle;
class ThemeAsset extends AssetBundle
{
    public $sourcePath = '@app/views/backend/themes/af/src';
    public $css = [
        //'css/theme.css',
        'css/_theme.css',
        //'css/theme-custom.css',
    ];
    public $js = [
        'js/theme.js',
        'js/theme.custom.js',
        'js/theme.init.js',
    ];
    public $depends = [
    ];
}