<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\views\backend\themes\protoadmin\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class BackendAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $sourcePath = '@app/views/backend/themes/protoadmin/src';
    public $css = [
    ];
    public $js = [
    ];
    public $depends = [
        'app\assets\ModernizrAsset',
        'yii\web\JqueryAsset',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'app\assets\FontAwesomeAsset',
        'app\assets\PnotifyAsset',
        'app\assets\SimpleVendorAsset',
        'app\views\backend\themes\protoadmin\assets\ThemeAsset',
    ];
}
