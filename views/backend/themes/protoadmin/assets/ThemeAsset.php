<?php
/**
 * Created by PhpStorm.
 * FontAwesomeAsset.php
 * @package /
 * @author Polyankin Semen Vicktorovich Vol][v, http://vap-terra.ru
 * @date 06.08.15
 * @time 22:54
 * @version 0.0.1
 * Copyright © 2014 VAP-TERRA, http://vap-terra.ru
 */

namespace app\views\backend\themes\protoadmin\assets;

use yii\web\AssetBundle;
class ThemeAsset extends AssetBundle
{
    public $sourcePath = '@app/views/backend/themes/protoadmin/src';
    public $css = [
        'css/stylesheets/theme.css',
        'css/stylesheets/theme-custom.css',
    ];
    public $js = [
        'javascripts/theme.js',
        'javascripts/theme.custom.js',
        'javascripts/theme.init.js',
    ];
}