// Tab click
(function($) {
    'use strict';
    $('a[data-toggle="tab"]').on('shown', function (e) {
        //YMaps.jQuery(function () {
            if ($(e.target).attr('href') == '#address' )
            {
                if( typeof( yandexMap ) !== 'undefined' ) {
                    yandexMap.redraw();
                }
            }
        //})
    });
}(jQuery));

// Navigation vertical
(function($) {

    'use strict';

    var $items = $( '.nav-vertical li.nav-parent' );

    function expand( $li ) {
        $li.children( 'ul.nav-children' ).slideDown( 'fast', function() {
            $li.addClass( 'nav-expanded' );
            $(this).css( 'display', '' );
            ensureVisible( $li );
        });
    }

    function collapse( $li ) {
        $li.children('ul.nav-children' ).slideUp( 'fast', function() {
            $(this).css( 'display', '' );
            $li.removeClass( 'nav-expanded' );
        });
    }

    function ensureVisible( $li ) {
        var scroller = $li.offsetParent();
        if ( !scroller.get(0) ) {
            return false;
        }

        var top = $li.position().top;
        if ( top < 0 ) {
            scroller.animate({
                scrollTop: scroller.scrollTop() + top
            }, 'fast');
        }
    }

    $items.find('> a').on('click', function( ev ) {

        var $caret = $( this).find('.caret-dropdown');
        var $anchor = $( this),//$caret.parent(),
            $prev = $anchor.closest('ul.nav').find('> li.nav-expanded' ),
            $next = $anchor.closest('li');

        if ( $anchor.prop('href') ) {
            var arrowWidth = $caret.width();
            //console.log( ev.offsetX + ' ' + ($anchor.offset().left - arrowWidth) );
            if (ev.offsetX > $anchor.offset().left - arrowWidth) {
                //ev.preventDefault();
                document.location = $anchor.prop('href');
                return true;
            }
        }

        if ( $prev.get( 0 ) !== $next.get( 0 ) ) {
            collapse( $prev );
            expand( $next );
        } else {
            collapse( $prev );
        }

    });


}).apply(this, [jQuery]);

// Navigation horizontal
(function($) {

    'use strict';

    var $items = $( '.nav-horizontal li.nav-parent' );

    function expand( $li ) {
        $li.children( 'ul.nav-children' ).slideDown( 'fast', function() {
            $li.addClass( 'nav-expanded' );
            $(this).css( 'display', '' );
            ensureVisible( $li );
        });
    }

    function collapse( $li ) {
        $li.children('ul.nav-children' ).slideUp( 'fast', function() {
            $(this).css( 'display', '' );
            $li.removeClass( 'nav-expanded' );
        });
    }

    function ensureVisible( $li ) {
        var scroller = $li.offsetParent();
        if ( !scroller.get(0) ) {
            return false;
        }

        var top = $li.position().top;
        if ( top < 0 ) {
            scroller.animate({
                scrollTop: scroller.scrollTop() + top
            }, 'fast');
        }
    }

    $items.find('> a').on('click', function( ev ) {

        var $caret = $( this).find('.caret-dropdown');
        var $anchor = $( this),//$caret.parent(),
            $prev = $anchor.closest('ul.nav').find('> li.nav-expanded' ),
            $next = $anchor.closest('li');

        if ( $anchor.prop('href') ) {
            var arrowWidth = $caret.width();
            //console.log( ev.offsetX + ' ' + ($anchor.offset().left - arrowWidth) );
            if (ev.offsetX > $anchor.offset().left - arrowWidth) {
                //ev.preventDefault();
                document.location = $anchor.prop('href');
                return true;
            }
        }

        if ( $prev.get( 0 ) !== $next.get( 0 ) ) {
            collapse( $prev );
            expand( $next );
        } else {
            collapse( $prev );
        }

    });


}).apply(this, [jQuery]);

/* menu item sortable */
(function ($) {

    'use strict';

    $(function () {

        if( typeof( $.ui.sortable ) !== 'undefined' ) {
            $('ul.sortable-menu').sortable(
                {
                    items: 'li',
                    //containment: 'parent',
                    //revert: true,
                    cursor: 'move',
                    tolerance: 'pointer',
                    distance: 0,
                    handle: '> .btn-group .draggable-handler',
                    //helper: 'original',//'clone',
                    helper: "clone",
                    opacity: 0.5,
                    connectWith: "li > ul",
                    out: function (event, ui) {},
                    over: function (event, ui) {},
                    update: function (event, ui) {
                        //console.log(ui.item)
                        //console.log(ui.item.prev());
                        //console.log( $(event.target).attr('data-url') );
                    },
                    change: function (event, ui) {},
                }
            );

            if( typeof( $.fn.nestedSortable ) !== 'undefined' ) {
                var ns = $('ul.sortable-groups').nestedSortable({
                    forcePlaceholderSize: true,
                    handle: '> div',
                    helper: 'clone',
                    listType: 'ul',
                    items: 'li',
                    opacity: .6,
                    placeholder: 'placeholder',
                    revert: 250,
                    tabSize: 25,
                    tolerance: 'pointer',
                    toleranceElement: '> div',
                    maxLevels: 4,
                    isTree: true,
                    expandOnHover: 700,
                    startCollapsed: false,
                    change: function () {
                        console.log('Relocated item');
                    }
                });
            }
            /*.draggable(
             {
             containment:'parent',
             handle: 'button.draggable-handler',
             revert: true,
             refreshPositions: true,
             snap: 'true',
             drag: function( event, ui ) {},
             start: function( event, ui ) {},
             stop: function( event, ui ) {}
             }
             );*/

            /*var groupStart = false;
            var listGroup = $('ul.sortable-groups')[0];
            $('ul.sortable-groups').sortable(
                {
                    items: 'li',
                    //containment: 'parent',
                    //revert: true,
                    cursor: 'move',
                    tolerance: 'intersect',
                    distance: 0,
                    handle: '> .item-options .draggable-handler',
                    helper: 'original',//'clone',
                    //helper: "clone",
                    opacity: 0.5,
                    axis: "y",
                    //connectWith: "ul",
                    appendTo:"ul",
                    forceHelperSize: true,
                    placeholder: "highlight",
                    start: function(event, ui) {
                        //jQuery.data( listGroup, "start", true );
                    },
                    stop: function(event, ui) {

                    },
                    out: function (event, ui) {},
                    over: function (event, ui) {
                        //console.log(ui.item);
                        //console.log(ui.sender);
                    },
                    update: function (event, ui) {
                        //console.log(ui.item)
                        //console.log(ui.item.prev());
                        //console.log( $(event.target).attr('data-url') );
                        //jQuery.data( listGroup, "start", false );
                    },
                    change: function (event, ui) {
                    },
                    remove: function( event, ui ) {
                    },
                    sort: function( event, ui ) {

                        //console.log( ui.position );
                        $('ul.sortable-groups li').each(function(){
                            if( $(this).position().top <= ui.position.top ){
                                $('>ul', this).css({'min-height': '50px', 'display': 'block'});
                            } else {
                                $('>ul',this).css({'min-height':'auto','display':'hide'});
                            }
                        });

                    },
                    receive: function( event, ui ) {
                        console.log(ui.item);
                        console.log(ui.sender);
                    }
                }
            );*/
            /*$('ul.sortable-groups li').on('mouseover',function(){
                console.log( jQuery.data( listGroup, "start" ) );
                if( jQuery.data( listGroup, "start" ) ) {
                    $('>ul', this).css({'min-height': '50px', 'display': 'block'});
                }
            }).on('mouseout',function(){
                if( jQuery.data( listGroup, "start" ) ) {
                    $('>ul',this).css({'min-height':'auto','display':'hidden'});
                }
            });*/
        }
    });
}).apply(this, [jQuery]);

(function ($) {

    'use strict';

    /*
     Update Output
     */
    var updateOutput = function (e) {
        var list = e.length ? e : $(e.target),
            output = list.data('output');

        if (window.JSON) {
            output.val(window.JSON.stringify(list.nestable('serialize')));
        } else {
            output.val('JSON browser support required for this demo.');
        }
    };
    if( typeof( $.nestable ) !== 'undefined' ) {
        $('#nestable').nestable({
            group: 1
        }).on('change', updateOutput);

        /*
         Output Initial Serialised Data
         */
        $(function () {
            updateOutput($('#nestable').data('output', $('#nestable-output')));
        });
    }
}).apply(this, [jQuery]);

(function ($) {
    /*
     Bootstrap Confirmation - BASIC
     */
    $('[data-toggle="confirmation"]').each(function () {
        var $this = $(this),
            opts = {};

        var pluginOptions = $this.data('data-plugin-options');
        if (pluginOptions)
            opts = $.extend(pluginOptions, opts);

        $this.confirmation(opts);
    });

    /*
     Bootstrap Confirmation - CALLBACK
     */
    $('.confirmation-callback').each(function () {
        var $this = $(this),
            opts = {
                onConfirm: function () {

                },
                onCancel: function () {

                }
            };

        var pluginOptions = $this.data('data-plugin-options');
        if (pluginOptions)
            opts = $.extend(pluginOptions, opts);

        $this.confirmation(opts);
    });

}(jQuery));


(function($) {
    'use strict';
    /* load select another select */



    $.fn.leadingDependentElement = function() {

        $(this).each(
            function () {


                var $element = $(this);
                var url = $element.attr('data-url');
                var parameter = $element.attr('data-parameter');
                if( typeof( parameter )==='undefined' ) {
                    parameter = 'id';
                }
                var dataRequest = $element.attr('data-request');
                var $dependent =  $( $element.attr('data-dependent-element') );
                var $dependentContainer = $dependent.parents('.form-group');
                $element.on(
                    'change',
                    function() {

                        var data = {};
                        data[parameter] = $element.val();

                        $dependentContainer.hide();

                        $.ajax(
                            { url:url, data:data }
                        ).done(
                            function( response ){

                                $dependentContainer.show().addClass('form-group');
                                $dependent.val(0);
                                var select2 = $dependent.themePluginSelect2({placeholder: ""}).data('__select2');
                                try {
                                    var element = $dependent.get(0);
                                    $dependent.find('option').remove();
                                    for( var key in element.options ) {
                                        element.remove( key );
                                    }

                                    var options = '';
                                    if( typeof( response ) === 'object' && !$.isEmptyObject( response ) ) {
                                        for (var key in response) {
                                            var item = response[key];
                                            options += '<option value="' + key + '">' + item + '</option>';
                                        }
                                    }
                                    $dependent.html(options);
                                } catch ( e ) {
                                    console.log( e );
                                }
                                select2.resetData();
                                select2.resetValue('');
                            }
                        ).fail(
                            function(){}
                        ).always(
                            function(){}
                        );
                    }
                );


            }
        );

    }

    $('[data-leading-dependent-element]').leadingDependentElement();


}).apply(this, [jQuery]);