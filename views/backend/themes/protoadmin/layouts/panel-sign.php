<?php

/* @var $this \yii\web\View */
/* @var $content string */


use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

app\views\backend\themes\protoadmin\assets\BackendAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?php echo Yii::$app->language ?>" class="fixed">
<head>
    <meta charset="<?php echo Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <meta name="description" content="">
    <meta name="keywords" content="" />
    <meta name="author" content="">

    <?= Html::csrfMetaTags() ?>
    <title><?php echo Html::encode($this->title) ?></title>

    <?php $this->head() ?>

</head>
    <body>

<?php $this->beginBody() ?>


		<section class="body-sign">


			<div class="center-sign">
				<a href="/" class="logo pull-left">
					<?php /* <img src="./images/logo.png" height="54" alt="Porto Admin" /> */ ?>
				</a>

                <?php echo $content ?>

				<p class="text-center text-muted mt-md mb-md">&copy; Авторские права <?php echo date('Y')?>. Все права защищены.</p>

                <?php echo Breadcrumbs::widget([
                    'tag' => 'ol',
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    'homeLink' => [ 'url'=> '/','label'=>'<i class="fa fa-home"></i>','encode' => false, ],
                    'activeItemTemplate' => "<li class=\"active\"><span>{link}</span></li>\n",
                    'options' => ['class'=>'breadcrumb',],
                ]) ?>

			</div>
		</section>

<?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>