<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\models\ext\ConstantExt;use app\modules\BackendModule;
use app\widgets\PersonalBlock;
use app\widgets\PersonalSidebar;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\widgets\Alert;

app\views\backend\themes\protoadmin\assets\BackendAsset::register($this);

$queryParams = Yii::$app->request->getQueryParams();

$collapsedMenu = false;
if( !empty($queryParams['r']) ) {
	if( strpos($queryParams['r'],'page/') !== false ) {
		$collapsedMenu = true;
	}
	if( strpos($queryParams['r'],'information/') !== false ) {
		$collapsedMenu = true;
	}
}
?><?php $this->beginPage() ?><!DOCTYPE html>
	<html lang="<?php echo Yii::$app->language ?>" class="fixed sidebar-light sidebar-left-xs<?php if( $collapsedMenu ){?> sidebar-left-collapsed sidebar-left-with-menu<?php }?>" data-style-switcher-options="{'sidebarColor': 'light','sidebarSize': 'xs'}">
	<head>
		<meta charset="<?php echo Yii::$app->charset ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>

		<?php /* <meta name="description" content="">
		<meta name="keywords" content=""/>
		<meta name="author" content=""> */ ?>

		<?php echo Html::csrfMetaTags() ?>
		<title><?php echo Html::encode($this->title) ?></title>

		<?php $this->head() ?>

	</head>
	<body>

	<?php $this->beginBody() ?>

	<section class="body">

		<!-- start: header -->
		<header class="header">
			<div class="logo-container">
				<a href="/" class="logo">
					<?php /*<img src="images/logo.png" height="35" alt=""/> */ ?>
				</a>
				<a class="navbar-brand" href="/" target="_blank"><i class="fa fa-eye fa-fw"></i><?php echo ConstantExt::getValueByTid('site_name');?></a>

				<div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html"
				     data-fire-event="sidebar-left-opened">
					<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
				</div>
			</div>

			<!-- start: search & user box -->
			<div class="header-right">

				<?php /*<form action="pages-search-results.html" class="search nav-form">
					<div class="input-group input-search">
						<input type="text" class="form-control" name="q" id="q" placeholder="Search...">
							<span class="input-group-btn">
								<button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
							</span>
					</div>
				</form>

				<span class="separator"></span>

				<ul class="notifications">
					<li>
						<a href="index.html#" class="dropdown-toggle notification-icon" data-toggle="dropdown">
							<i class="fa fa-tasks"></i>
							<span class="badge">3</span>
						</a>

						<div class="dropdown-menu notification-menu large">
							<div class="notification-title">
								<span class="pull-right label label-default">3</span>
								Tasks
							</div>

							<div class="content">
								<ul>
									<li>
										<p class="clearfix mb-xs">
											<span class="message pull-left">Generating Sales Report</span>
											<span class="message pull-right text-dark">60%</span>
										</p>

										<div class="progress progress-xs light">
											<div class="progress-bar" role="progressbar" aria-valuenow="60"
											     aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
										</div>
									</li>

									<li>
										<p class="clearfix mb-xs">
											<span class="message pull-left">Importing Contacts</span>
											<span class="message pull-right text-dark">98%</span>
										</p>

										<div class="progress progress-xs light">
											<div class="progress-bar" role="progressbar" aria-valuenow="98"
											     aria-valuemin="0" aria-valuemax="100" style="width: 98%;"></div>
										</div>
									</li>

									<li>
										<p class="clearfix mb-xs">
											<span class="message pull-left">Uploading something big</span>
											<span class="message pull-right text-dark">33%</span>
										</p>

										<div class="progress progress-xs light mb-xs">
											<div class="progress-bar" role="progressbar" aria-valuenow="33"
											     aria-valuemin="0" aria-valuemax="100" style="width: 33%;"></div>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</li>
					<li>
						<a href="index.html#" class="dropdown-toggle notification-icon" data-toggle="dropdown">
							<i class="fa fa-envelope"></i>
							<span class="badge">4</span>
						</a>

						<div class="dropdown-menu notification-menu">
							<div class="notification-title">
								<span class="pull-right label label-default">230</span>
								Messages
							</div>

							<div class="content">
								<ul>
									<li>
										<a href="index.html#" class="clearfix">
											<figure class="image">
												<img src="images/!sample-user.jpg" alt="Joseph Doe Junior"
												     class="img-circle"/>
											</figure>
											<span class="title">Joseph Doe</span>
											<span class="message">Lorem ipsum dolor sit.</span>
										</a>
									</li>
									<li>
										<a href="index.html#" class="clearfix">
											<figure class="image">
												<img src="images/!sample-user.jpg" alt="Joseph Junior"
												     class="img-circle"/>
											</figure>
											<span class="title">Joseph Junior</span>
											<span class="message truncate">Truncated message. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sit amet lacinia orci. Proin vestibulum eget risus non luctus. Nunc cursus lacinia lacinia. Nulla molestie malesuada est ac tincidunt. Quisque eget convallis diam, nec venenatis risus. Vestibulum blandit faucibus est et malesuada. Sed interdum cursus dui nec venenatis. Pellentesque non nisi lobortis, rutrum eros ut, convallis nisi. Sed tellus turpis, dignissim sit amet tristique quis, pretium id est. Sed aliquam diam diam, sit amet faucibus tellus ultricies eu. Aliquam lacinia nibh a metus bibendum, eu commodo eros commodo. Sed commodo molestie elit, a molestie lacus porttitor id. Donec facilisis varius sapien, ac fringilla velit porttitor et. Nam tincidunt gravida dui, sed pharetra odio pharetra nec. Duis consectetur venenatis pharetra. Vestibulum egestas nisi quis elementum elementum.</span>
										</a>
									</li>
									<li>
										<a href="index.html#" class="clearfix">
											<figure class="image">
												<img src="images/!sample-user.jpg" alt="Joe Junior" class="img-circle"/>
											</figure>
											<span class="title">Joe Junior</span>
											<span class="message">Lorem ipsum dolor sit.</span>
										</a>
									</li>
									<li>
										<a href="index.html#" class="clearfix">
											<figure class="image">
												<img src="images/!sample-user.jpg" alt="Joseph Junior"
												     class="img-circle"/>
											</figure>
											<span class="title">Joseph Junior</span>
											<span class="message">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sit amet lacinia orci. Proin vestibulum eget risus non luctus. Nunc cursus lacinia lacinia. Nulla molestie malesuada est ac tincidunt. Quisque eget convallis diam.</span>
										</a>
									</li>
								</ul>

								<hr/>

								<div class="text-right">
									<a href="index.html#" class="view-more">View All</a>
								</div>
							</div>
						</div>
					</li>
					<li>
						<a href="index.html#" class="dropdown-toggle notification-icon" data-toggle="dropdown">
							<i class="fa fa-bell"></i>
							<span class="badge">3</span>
						</a>

						<div class="dropdown-menu notification-menu">
							<div class="notification-title">
								<span class="pull-right label label-default">3</span>
								Alerts
							</div>

							<div class="content">
								<ul>
									<li>
										<a href="index.html#" class="clearfix">
											<div class="image">
												<i class="fa fa-thumbs-down bg-danger"></i>
											</div>
											<span class="title">Server is Down!</span>
											<span class="message">Just now</span>
										</a>
									</li>
									<li>
										<a href="index.html#" class="clearfix">
											<div class="image">
												<i class="fa fa-lock bg-warning"></i>
											</div>
											<span class="title">User Locked</span>
											<span class="message">15 minutes ago</span>
										</a>
									</li>
									<li>
										<a href="index.html#" class="clearfix">
											<div class="image">
												<i class="fa fa-signal bg-success"></i>
											</div>
											<span class="title">Connection Restaured</span>
											<span class="message">10/10/2014</span>
										</a>
									</li>
								</ul>

								<hr/>

								<div class="text-right">
									<a href="index.html#" class="view-more">View All</a>
								</div>
							</div>
						</div>
					</li>
				</ul>

				<span class="separator"></span>*/ ?>

				<?php echo PersonalBlock::widget();?>

			</div>
			<!-- end: search & user box -->
		</header> */ ?>
		<!-- end: header -->

		<div class="inner-wrapper">
			<?php
			if (!Yii::$app->user->isGuest) {

				?>
				<!-- start: sidebar -->
				<aside id="sidebar-left" class="sidebar-left">

					<div class="sidebar-header">
						<div class="sidebar-title">
							<?php echo Yii::t('backend/layout', 'Navigation'); ?>
						</div>
						<div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed"
						     data-target="html" data-fire-event="sidebar-left-toggle">
							<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
						</div>
					</div>

					<div class="nano">
						<div class="nano-content">
							<nav id="menu" class="nav-main" role="navigation">
								<?php
									$items = BackendModule::generateMenuItems();
									echo  app\components\bootstrap\Nav::widget([
										'options' => ['class' => 'nav nav-main'],
										'items' => $items,
										'activateItems' => true,
										'activateParents' => true,
									]);
								?>
								<?php /* <ul class="nav nav-main">

									<li class="nav-active">
										<a href="<?php echo Yii::$app->urlManager->createUrl('site/index'); ?>">
											<i class="fa fa-home" aria-hidden="true"></i>
											<span><?php echo Yii::t('backend/layout', 'Dashboard'); ?></span>
										</a>
									</li>

 									<li class="nav-parent<?php if( !empty($queryParams['r']) && strpos($queryParams['r'],'page/') !== false ){ ?> nav-expanded nav-active<?php } ?>">
										<a>
											<i class="fa fa-book"></i>
											<span><?php echo Yii::t('backend/layout', 'Content'); ?></span>
										</a>

										<ul class="nav nav-children">
											<li>
												<a href="<?php echo Yii::$app->urlManager->createUrl('page/menu-item/index'); ?>">
													<i class="fa fa-book"></i>
													<span><?php echo Yii::t('backend/layout', 'Pages'); ?></span>
												</a>
											</li>
											<!-- <li>
												<a href="<?php echo Yii::$app->urlManager->createUrl('page/block/index'); ?>">
													<i class="fa fa-th"></i>
													<span><?php echo Yii::t('backend/layout', 'Blocks'); ?></span>
												</a>
											</li> -->
											<li>
												<a href="<?php echo Yii::$app->urlManager->createUrl('page/constant/index'); ?>">
													<i class="fa fa-circle"></i>
													<span><?php echo Yii::t('backend/layout', 'Constants'); ?></span>
												</a>
											</li>
											<li>
												<a href="<?php echo Yii::$app->urlManager->createUrl('page/list/index'); ?>">
													<i class="fa fa-list"></i>
													<span><?php echo Yii::t('backend/layout', 'Lists'); ?></span>
												</a>
											</li>
											<li>
												<a href="<?php echo Yii::$app->urlManager->createUrl('page/banner-group/index'); ?>">
													<i class="fa fa-picture-o"></i>
													<span><?php echo Yii::t('backend/layout', 'Banners'); ?></span>
												</a>
											</li>
										</ul>

									</li>

									<li>
										<a href="<?php echo Yii::$app->urlManager->createUrl('information/default/index'); ?>">
											<i class="fa fa-info-circle"></i>
											<span><?php echo Yii::t('backend/layout', 'Information'); ?></span>
										</a>
									</li>

									<li>
										<a href="<?php echo Yii::$app->urlManager->createUrl(['user/default/index']); ?>">
											<i class="fa fa-users"></i>
											<span><?php echo Yii::t('backend/layout', 'Users'); ?></span>
										</a>
									</li>

									<li>
										<a href="<?php echo Yii::$app->urlManager->createUrl('geo/default/index'); ?>">
											<i class="fa fa-compass"></i>
											<span><?php echo Yii::t('backend/layout', 'Location'); ?></span>
										</a>
									</li>

									<li>
										<a href="<?php echo Yii::$app->urlManager->createUrl('currency/default/index'); ?>">
											<i class="fa fa-money"></i>
											<span><?php echo Yii::t('backend/layout', 'Currency'); ?></span>
										</a>
									</li>

								</ul> */ ?>

							</nav>

						</div>

					</div>

				</aside>
				<!-- end: sidebar -->
				<?php
			}
			?>

			<section role="main" class="content-body">

				<header class="page-header">
					<h2><?php echo $this->title ?></h2>

					<div class="right-wrapper pull-right">
						<?php echo Breadcrumbs::widget([
							'tag' => 'ol',
							'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
							'homeLink' => [ 'url'=> '/','label'=>'<i class="fa fa-home"></i>','encode' => false, ],
							'activeItemTemplate' => "<li class=\"active\"><span>{link}</span></li>\n",
							'options' => ['class'=>'breadcrumbs',],
						]) ?>
						<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
					</div>
				</header>

				<?php echo Alert::widget() ?>

				<!-- start: page -->
				<?php echo $content ?>
				<!-- end: page -->

				<p class="text-center"><?php echo \app\components\web\Application::getName();?> v <?php echo \app\components\web\Application::getVersion();?></p>
			</section>
		</div>

		<aside id="sidebar-right" class="sidebar-right">

			<div class="nano">
				<div class="nano-content">

					<?php echo PersonalSidebar::widget() ?>

				</div>
			</div>
		</aside>
		<div class="clearfix"></div>
	</section>


	<?php $this->endBody() ?>
	</body>
	</html>
<?php $this->endPage() ?>