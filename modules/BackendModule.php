<?php
namespace app\modules;
use app\components\Theme;
use app\models\ext\FormExt;
use app\models\ext\MenuExt;
use Yii;

/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 27.06.16
 * Time: 23:14
 */
class BackendModule extends \yii\base\Module
{
    public function init()
    {
        if( strlen( trim( $themeName = Yii::$app->session->get('backend_theme') ) ) == 0 ) {
            $themeName = 'af';
        }
        $theme = new Theme(['type'=>Theme::TYPE_BACKEND]);
        $theme->setBasePath('@app/views/backend/themes/'.$themeName.'/');
        $theme->setBaseUrl('/views/backend/themes/'.$themeName.'/');
        $theme->pathMap = [
            '@app/modules/backend/views/layouts/' => '@app/views/backend/themes/'.$themeName.'/layouts',
            '@app/modules/backend/widgets/views/' => '@app/views/backend/themes/'.$themeName.'/widgets',
            '@app/views' => '@app/views/backend/themes/'.$themeName,
        ];
        Yii::$app->view->theme = $theme;
        $this->controllerNamespace = 'app\modules\backend\controllers';
        $this->setViewPath('@app/modules/backend/views');
        //$this->setLayoutPath($theme->getBasePath().'/layouts');
        Yii::$app->user->loginUrl = Yii::$app->urlManager->createUrl(['/backend/user/login']);
        parent::init();
    }

    public static function generateMenuItems()
    {
        $mainItems = [];
        $items = [];
        $menus = MenuExt::find()->where(['=','active','1'])->orderBy(['position'=>SORT_ASC])->all();
        if( !empty( $menus ) ) {
            foreach( $menus as $menu ) {
                $items[] = [
                    'icon' => !empty($menu->css_icon)?$menu->css_icon:'fa fa-pagelines fa-fw fa-lg',
                    'label' => $menu->title,
                    'url' => ['/backend/menu-item/index','menu_id'=>$menu->id],
                ];
            }
        }
        $items[] = [
            'icon' => 'fa fa-info-circle fa-fw fa-lg',
            'label' => Yii::t('backend/layout', 'Information'),//новости/статьи
            'url' => ['/backend/information/index'],
        ];
        $items[] = [
            'icon' => 'fa fa-picture-o fa-fw fa-lg',
            'label' => Yii::t('backend/layout', 'Banners'),
            'url' => ['/backend/banner-group/index'],
        ];
        $items[] = [
            'icon' => 'fa fa-comments-o fa-fw fa-lg',
            'label' => Yii::t('backend/layout', 'Reviews'),//Отзывы
            'url' => ['/backend/comment/index'],
        ];
        $mainItems[] = [
            'icon' => 'fa fa-file fa-fw fa-lg',
            'label' => Yii::t('backend/layout', 'Content'),
            //'url' =>'',
            'items' => $items,
        ];


        $items = [];
        //Заполненные формы
        $forms = FormExt::find()->active()->orderByPosition()->all();
        if( !empty( $forms ) ) {
            //$subItems = [];
            foreach( $forms as $form ) {
                /* @var $form FormExt */
                //$subItems[]
                $countCompleteds = $form->getFormCompleteds()->count();
                $countCompletedsNotView = $form->getFormCompleteds()->andWhere(['=','dt_view','0000-00-00 00:00:00'])->orWhere(['is','dt_view',NULL])->count();
                $countCompletedsNotProcessed = $form->getFormCompleteds()->andWhere(['=','processed','1'])->count();
                $items[] = [
                    'icon' => !empty($form->css_icon)?$form->css_icon:'fa fa-bell fa-fw fa-lg',
                    'label' => $form->name,
                    'numbers' => [
                        $countCompleteds,
                        $countCompletedsNotView,
                        $countCompletedsNotProcessed
                    ],
                    'url' => [
                        '/backend/form-completed/index',
                        'form_id'=>$form->id
                    ],
                ];

            }
            /*$items[] = [
                'icon' => 'fa fa-bell fa-fw fa-lg',
                'label' => Yii::t('backend/layout','Completed forms'),
                'items' => $subItems
            ];*/
        }
        $mainItems[] = [
            'icon' => 'fa fa-bell fa-fw fa-lg',
            'label' => Yii::t('backend/layout', 'Notifications'),
            //'url' =>'',
            'items' => $items,
        ];
        return $mainItems;
    }

    public function generateRightMenuItems()
    {
        $mainItems = [];
        $items = [];
        $items[] = [
            'icon' => 'fa fa-object-group fa-fw fa-lg',
            'label' => Yii::t('backend/layout','Forms'),
            'url' => ['/backend/form/index']
        ];
        $items[] = [
            'icon' => 'fa fa-list-ol fa-fw fa-lg',
            'label' => Yii::t('backend/layout', 'Payroll information'),//Списочная информация
            'url' => ['/backend/list/index'],
        ];
        $items[] = [
            'icon' => 'fa fa-circle fa-fw fa-lg',
            'label' => Yii::t('backend/layout', 'Configuration'),//общая информация
            'url' => ['/backend/constant/index'],
        ];

        $mainItems[] = [
            'icon' => 'fa fa-sun-o fa-fw fa-lg',
            'label' => Yii::t('backend/layout', 'Settings'),
            //'url' =>'',
            'items' => $items,
        ];
        $mainItems[] = [
            'icon' => 'fa fa-question-circle fa-fw fa-lg',
            'label' => '',//Yii::t('backend/layout', 'Instruction'),//инструкция
            'url' => ['/backend/default/instruction'],
        ];
        return $mainItems;
    }
}