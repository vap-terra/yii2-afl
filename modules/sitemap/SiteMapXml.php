<?php
namespace app\modules\sitemap;
use app\models\BaseActiveQuery;
use app\models\ext\MenuExt;
use app\models\ext\MenuItemExt;
use Yii;
use yii\base\InvalidConfigException;
use yii\caching\Cache;

/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 27.11.16
 * Time: 13:12
 */
class SiteMapXml extends \yii\base\Module
{
    /** @var int */
    public $cacheExpire = 86400;

    /** @var Cache|string */
    public $cacheProvider = 'cache';

    /** @var string */
    public $cacheKey = 'SiteMapXml';

    /** @var boolean Use php's gzip compressing. */
    public $enableGzip = false;

    public function init()
    {
        parent::init();

        if (is_string($this->cacheProvider)) {
            $this->cacheProvider = Yii::$app->{$this->cacheProvider};
        }

        if (!$this->cacheProvider instanceof Cache) {
            throw new InvalidConfigException('Invalid `cacheKey` parameter was specified.');
        }
    }

    /**
     * Build and cache a site map xml
     * @return string
     */
    public function buildXml()
    {
        $xmlData = '<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
        $arMenus = MenuExt::find()->active(BaseActiveQuery::ACTIVE)->all();
        foreach( $arMenus as $oMenu ){
            /* @var $oMenu MenuExt */
            $oMenuItems = $oMenu->getMenuItems()->active(BaseActiveQuery::ACTIVE)->andWhere(['is not','menu_item_id',null])->all();
            if( !empty( $oMenuItems ) ) {
                foreach( $oMenuItems as $oMenuItem ) {
                    /* @var $oMenuItem MenuItemExt */
                    $xmlData .= '
   <url>
      <loc>'.$oMenuItem->getUrl().'</loc>
      <lastmod></lastmod>
      <changefreq>weekly</changefreq>
      <priority>0.5</priority>
   </url>
            ';
                }
            }
        }
        $xmlData .= '
</urlset>';
        $this->cacheProvider->set($this->cacheKey, $xmlData, $this->cacheExpire);
        return $xmlData;
    }

}