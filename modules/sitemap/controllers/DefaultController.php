<?php
namespace app\modules\sitemap\controllers;
use Yii;
use yii\base\Controller;
use yii\web\Response;

/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 27.11.16
 * Time: 13:15
 */
class DefaultController extends Controller
{
    public function actionIndex()
    {
        $module = $this->module;

        if (!$xmlData = $module->cacheProvider->get($module->cacheKey)) {
            $xmlData = $module->buildXml();
        }

        Yii::$app->response->format = Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/xml');
        if ($module->enableGzip) {
            $xmlData = gzencode($xmlData);
            $headers->add('Content-Encoding', 'gzip');
            $headers->add('Content-Length', strlen($xmlData));
        }
        return $xmlData;
    }
}