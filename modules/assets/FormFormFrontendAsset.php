<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 25.07.16
 * Time: 9:38
 */

namespace app\modules\assets;


use yii\web\AssetBundle;

class FormFormFrontendAsset extends AssetBundle
{
    public $sourcePath = '@app/views/src';
    public $css = [
    ];

    public $js = [
        'javascripts/forms/form-controller-frontend.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
        'app\assets\jquery\ValidationAsset',
        'app\assets\jquery\MaskedInputAssets'
        //'app\assets\jquery\UIAsset',
    ];
}