<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 25.07.16
 * Time: 9:38
 */

namespace app\modules\assets;


use yii\web\AssetBundle;

class MenuItemFormBackendAsset extends AssetBundle
{
    public $sourcePath = '@app/views/src';
    public $css = [
    ];

    public $js = [
        'javascripts/menu/menu-item-controller-backend.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
        'app\assets\jquery\ValidationAsset',
        'app\assets\jquery\MaskedInputAssets',
        //'app\assets\jquery\UIAsset',
    ];
}