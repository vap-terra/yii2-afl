<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 25.07.16
 * Time: 9:38
 */

namespace app\modules\assets;


use yii\web\AssetBundle;

class FormFormBackendAsset extends AssetBundle
{
    public $sourcePath = '@app/views/src';
    public $css = [
    ];

    public $js = [
        'javascripts/forms/form-controller-backend.js',
    ];

    public $depends = [
        'app\assets\jquery\UIAsset'
    ];
}