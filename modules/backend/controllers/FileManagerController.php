<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 06.07.16
 * Time: 22:10
 */

namespace app\modules\backend\controllers;


use Yii;
use yii\base\Theme;

class FileManagerController extends \app\components\FileManagerController
{
    public function init()
    {
        parent::init();
        $this->layout = '@app/views/backend/themes/protoadmin/layouts/main';
    }

    public function beforeAction($action)
    {
        //print_r( $this->layout );
        //die();
        if( parent::beforeAction($action) ){

            $theme = new Theme();
            $theme->pathMap = [
                '@app/views' => '@app/views/backend/themes/protoadmin',
                '@app/layouts' => '@app/views/backend/themes/protoadmin'
            ];
            //$theme->basePath = '@app/views/backend/themes/protoadmin';
            //$theme->baseUrl = '@web/views/backend/themes/protoadmin';

            $this->view->theme = $theme;

            return true;
        }
        return false;
    }
}