<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 28.02.16
 * Time: 12:10
 */

namespace app\modules\backend\controllers;


use app\components\BackendController;
use app\models\ext\GeoCountryExt;
use Yii;

class GeoCountryController extends BackendController
{
    protected $_defaultRoute = ['/backend/geo-country/index'];
    protected $_modelClass = 'app\models\ext\GeoCountryExt';

    public function actions()
    {
        return array_merge(
            parent::actions(),
            [
                /*'index' => [
                    'class' => 'app\components\actions\ListAction',
                    'modelClass' => $this->_modelClass,
                    'view' => 'geo-country-list',
                ],*/
                'edit' => [
                    'class'=>'app\components\actions\EditAction',
                    'modelClass'=>$this->_modelClass,
                    'view' => 'geo-country-edit',
                ],
                'add' => [
                    'class'=>'app\components\actions\AddAction',
                    'modelClass'=>$this->_modelClass,
                    'view' => 'geo-country-add',
                ],
                'delete' => [
                    'class'=>'app\components\actions\DeleteAction',
                    'modelClass'=>$this->_modelClass,
                    'modelMethod' => 'delete',
                    'titleAttribute' => 'name',
                    'redirect'=>$this->_defaultRoute,
                ],
                'save' => [
                    'class'=>'app\components\actions\SaveAction',
                    'modelClass'=>$this->_modelClass,
                    'modelMethod' => 'save',
                    'view' => 'geo-country-edit',
                    'viewUpdate' => 'geo-country-add',
                    'titleAttribute' => 'name',
                    'fileAttribute' => '',
                    'modelUploadMethod' => 'uploadImage',
                    'redirect'=>$this->_defaultRoute,
                ]
            ]
        );
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $qGeoCountry = GeoCountryExt::find();
        return $this->render(
            'index',
            [
                'query' => $qGeoCountry
            ]
        );
    }


}