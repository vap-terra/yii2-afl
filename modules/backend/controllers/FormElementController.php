<?php

namespace app\modules\backend\controllers;


use app\components\BackendController;
use app\models\ext\FormElementExt;
use app\models\ext\FormExt;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\ActiveFormElement;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class FormElementController extends BackendController
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        /* @var $qFormElement ActiveQuery */
        $qFormElement = FormElementExt::find();
        $formElementDataProvider = new ActiveDataProvider(
            [
                'query' => $qFormElement
            ]
        );
        return $this->render(
            'index',
            [
                'formElementDataProvider' => $formElementDataProvider,
            ]
        );
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionEdit( $id ) {
        /* @var $oFormElement FormElementExt */
        $oFormElement = FormElementExt::getByTid( $id );
        return $this->render(
            'formElement-edit',
            [
                'model' => $oFormElement,
                'result' => []
            ]
        );
    }

    /**
     * @return string
     */
    public function actionAdd() {
        /* @var $oFormElement FormElementExt */
        $oFormElement = new FormElementExt();
        return $this->render(
            'formElement-add',
            [
                'model' => $oFormElement,
                'result' => []
            ]
        );
    }

    /**
     * @param $id
     * @throws NotFoundHttpException
     */
    public function actionDelete( $id )
    {
        /* @var $oFormElement FormElementExt */
        $oFormElement = FormElementExt::getByTid( $id );
        if( $oFormElement->delete() ) {
            $this->setFlashSuccess(Yii::t('backend/layout','Item {name} deleted successfully',['name'=>$oFormElement->name]));
        } else {
            $this->setFlashError(Yii::t('backend/layout','Failed to remove an item {name}',['name' => $oFormElement->name]));
        }
        $this->redirect( ['/backend/form/edit','id'=>$oFormElement->form_id] );
    }

    public function actionReposition($id,$number)
    {
        /* @var $oFormElement FormElementExt */
        $oFormElement = FormElementExt::getByTid($id);

        $oFormElement->reposition($number);

        $status = 'success';
        $message = 'Успешно изменена';
        $errors = [];

        if ($oFormElement->hasErrors()) {
            $errors = $oFormElement->getErrors();
            $status = 'error';
            $message = 'Не удалось изменить';
        }

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'status' => $status,
                'message' => $message,
                'errors' => $errors,
                'id' => $oFormElement->id,
                'position' => $oFormElement->position,
            ];
        }

        $this->setFlashSuccess( Yii::t('backend/layout','Element {name} successfully saved!',['name'=>$oFormElement->name] ) );
        $this->redirect(['/backend/form/edit','id'=>$oFormElement->form_id]);
    }
    
    public function actionCreate($id)
    {
        $form = FormExt::getById($id);
        $model = new FormElementExt();
        $model->tid = '-';
        $model->form_id = $id;
        $model->required = 0;
        $model->active = 0;
        $model->type = FormElementExt::TYPE_INPUT;
        $model->name = '-';
        $model->hint = '';
        $model->description = '';
        $model->params = '';
        $model->position = FormElementExt::getMax('position', 0,['=','form_id',$id]) + 10;
        $model->save();

        if ($model->load($_POST)) {
            $save = $model->save();

            $status = 'success';
            $message = 'Успешно добавлена';
            $errors = [];

            if ($model->hasErrors()) {
                $errors = $model->getErrors();
                $status = 'error';
                $message = 'Не удалось добавить.';
            }

            if( Yii::$app->request->isAjax ) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'status' => $status,
                    'message' => $message,
                    'errors' => $errors,
                ];
            } else if ($save) {
                $this->redirect(['/backend/form/edit','id'=>$form->id]);
            }
        }


        $view = 'form-element-add';
        $context = array(
            'model' => $model,
            'form' => $form,
        );
        if (Yii::$app->request->isAjax) {
            $view = 'form-element-form-inner';
            $form = new ActiveForm();
            $form->requiredCssClass = 'has-required';
            $form->options = [
                'enctype' => 'multipart/form-data',
                'class' => 'form-horizontal form-bordered'
            ];
            $form->fieldConfig = [
                'template' => "

                                    {label}\n
                                    <div class=\"col-md-6\">
                    {input}\n
                    {hint}\n
                    {error}\n
                                    </div>

",
                'labelOptions' => [
                    'class' => 'col-md-3 control-label'
                ],
            ];
            $context['activeForm'] = $form;
            return $this->renderPartial($view, $context);
        }

        return $this->render($view, $context);
    }

    /**
     * @param int $id
     * @return array|string
     * @throws NotFoundHttpException
     */
    public function actionSave( $id = 0 )
    {
        /* @var $oFormElement FormElementExt */
        if( $id ) {
            $oFormElement = FormElementExt::getByTid($id);
        } else {
            $oFormElement = new FormElementExt();
        }
        /* @var $result array */
        $result = [];
        $arValues = Yii::$app->request->post();
        if( $oFormElement->load( $arValues ) ) {

            if( $oFormElement->isNewRecord ){
                $oFormElement->position = FormElementExt::getMax('position', 0,($oFormElement->form_id)?['=','form_id',$oFormElement->form_id]:null) + 10;
            }
            if( Yii::$app->request->isAjax && Yii::$app->request->get('validate','') == 'true' ) {
                return ActiveForm::validate( $oFormElement );
            }

            $validErrors = ActiveForm::validate($oFormElement);

            if( !empty( $validErrors ) ) {
                $result = [
                    'status' => self::ERROR,
                    'message' => Yii::t('backend/layout','Please fill in all required fields'),
                    'errors' => $validErrors
                ];
            } else if( $oFormElement->save() ) {
                $result = [
                    'status'=>self::SUCCESS,
                    'message'=>Yii::t('backend/layout','Element {name} successfully saved!',['name'=>$oFormElement->name])
                ];
                if( !Yii::$app->request->isAjax ) {
                    $this->setFlashSuccess( Yii::t('backend/layout','Element {name} successfully saved!',['name'=>$oFormElement->name] ) );
                    //return $this->refresh();
                    $this->redirect( ['/backend/form-element/index'] );
                    Yii::$app->end();
                }
            }

            if( $oFormElement->hasErrors() ) {
                $result = [
                    'status'=>self::ERROR,
                    'message'=>Yii::t('backend/layout','Error saving element {name}',['name'=>$oFormElement->name]),
                    'errors' => $oFormElement->getErrors()
                ];
            }

            if( Yii::$app->request->isAjax ) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $result;
            }

        }

        $view = 'form-element-add';
        if( !$oFormElement->isNewRecord ) {
            $view = 'form-element-edit';
        }
        return $this->render(
            $view,
            [
                'model' => $oFormElement,
                'result' => $result
            ]
        );
    }
}