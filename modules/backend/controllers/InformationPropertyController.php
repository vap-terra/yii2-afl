<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 28.02.16
 * Time: 13:58
 */

namespace app\modules\backend\controllers;



use app\components\BackendController;
use app\components\data\AdjacencyListDataProvider;
use app\models\ext\InformationExt;
use app\models\ext\InformationGroupExt;
use app\models\ext\InformationItemGroupPropertyExt;
use app\models\ext\InformationPropertyExt;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\db\ActiveQuery;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class InformationPropertyController extends BackendController
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge_recursive(
            parent::behaviors(),
            [
                'access' => [
                    'rules' => [
                        [
                            'actions' => ['change-recursive','change-select-group'],
                            'allow' => true,
                            'roles' => ['@'],
                        ],
                    ],
                ],
            ]
        );
    }

    /**
     * @param int $id
     * @return string
     */
    public function actionIndex( $id )
    {

        /* @var $oInformationProperty InformationPropertyExt */
        /* @var $qInformationGroup ActiveQuery */

        $oInformationProperty = InformationPropertyExt::getById( $id );
        $oInformation = $oInformationProperty->information;
        $qInformationGroup = $oInformation->getInformationGroups();
        $countQuery = clone $qInformationGroup;
        $paginationInformationGroupList = new Pagination(
            [
                'totalCount' => $countQuery->count()
            ]
        );
        $dataProviderInformationGroupList = new AdjacencyListDataProvider(
            [
                'parentKey' => 'information_group_id',
                'query' => $qInformationGroup
            ]
        );

        $dataProviderInformationPropertyList = null;
        $qInformationProperty = $oInformation->getInformationProperties();
        $dataProviderInformationPropertyList = new ActiveDataProvider(
            [
                'query' => $qInformationProperty
            ]
        );

        return $this->render(
            'index',
            [
                'oInformation' => $oInformation,
                'oInformationProperty' => $oInformationProperty,
                'dataProviderInformationGroupList' => $dataProviderInformationGroupList,
                'paginationInformationGroupList' => $paginationInformationGroupList,
                'dataProviderInformationPropertyList' => $dataProviderInformationPropertyList,
            ]
        );
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionEdit( $id ) {
        /* @var $oInformationProperty InformationPropertyExt */
        $oInformationProperty = InformationPropertyExt::getById( $id );
        return $this->render(
            'information-property-edit',
            [
                'model' => $oInformationProperty,
                'oInformation' => $oInformationProperty->information,
                'result' => []
            ]
        );
    }

    /**
     * @param int $information_id
     * @return string
     */
    public function actionAdd( $information_id ) {
        /* @var $oInformationProperty InformationPropertyExt */
        $oInformationProperty = new InformationPropertyExt();
        $oInformation = InformationExt::getById( $information_id );
        return $this->render(
            'information-property-add',
            [
                'model' => $oInformationProperty,
                'oInformation' => $oInformation,
                'result' => []
            ]
        );
    }

    /**
     * @param $id
     * @throws NotFoundHttpException
     */
    public function actionDelete( $id )
    {
        /* @var $oInformationProperty InformationPropertyExt */
        $oInformationProperty = InformationPropertyExt::getById( $id );
        if( $oInformationProperty->delete() ) {
            Yii::$app->getSession()->setFlash('success', Yii::t('backend/layout','Item {name} deleted successfully',['name'=>$oInformationProperty->name]));
        } else {
            Yii::$app->getSession()->setFlash('error', Yii::t('backend/layout','Failed to remove an item {name}',['name' => $oInformationProperty->name]));
        }
        $this->redirect( ['/backend/information/index'] );
    }


    /**
     * @param $id
     * @param $information_group_id
     */
    public function actionChangeSelectGroup( $id, $information_group_id )
    {
        /* @var $oInformationGroup InformationGroupExt */
        /* @var $oInformationProperty InformationPropertyExt */
        /* @var $oInformationItemGroupProperty InformationItemGroupPropertyExt */
        $oInformationGroup = InformationGroupExt::getByTid($id);
        $oInformationProperty = InformationPropertyExt::getById( $id );
        $oInformationItemGroupProperty = InformationItemGroupPropertyExt::findOne(
            [
                'information_group_id' => $information_group_id,
                'information_property_id' => $id,
            ]
        );
        if( $oInformationItemGroupProperty ) {
            if ( $oInformationItemGroupProperty->delete() ) {
                $this->setFlashSuccess(Yii::t('backend/layout', 'Connection between property {name} and group {group_name} successfully delete',
                    ['name' => $oInformationProperty->name,'group_name'=>$oInformationGroup->name])
                );
            } else {
                $this->setFlashError( Yii::t('backend/layout', 'Failed to delete connection between property {name} and group {group_name}',
                    ['name' => $oInformationProperty->name,'group_name'=>$oInformationGroup->name])
                );
            }
        } else {
            // create link
            $oInformationItemGroupProperty = new InformationItemGroupPropertyExt(
                [
                    'information_id' => $oInformationGroup->information_id,
                    'information_group_id' => $information_group_id,
                    'information_property_id' => $id,
                    'recursive' => 0,
                ]
            );
            if( $oInformationItemGroupProperty->save() ){
                $this->setFlashSuccess(Yii::t('backend/layout', 'Connection between property {name} and group {group_name} successfully create',
                    ['name' => $oInformationProperty->name,'group_name'=>$oInformationGroup->name])
                );
            } else {
                $this->setFlashError( Yii::t('backend/layout', 'Failed to create connection between property {name} and group {group_name}',
                    ['name' => $oInformationProperty->name,'group_name'=>$oInformationGroup->name])
                );
            }
        }
        $this->redirect( ['/backend/information-property/index','id'=>$oInformationProperty->id] );
    }

    /**
     * @param $id
     * @param $information_group_id
     */
    public function actionChangeRecursive( $id, $information_group_id )
    {
        /* @var $oInformationGroup InformationGroupExt */
        /* @var $oInformationProperty InformationPropertyExt */
        /* @var $oInformationItemGroupProperty InformationItemGroupPropertyExt */
        $oInformationGroup = InformationGroupExt::getByTid($id);
        $oInformationProperty = InformationPropertyExt::getById( $id );
        $oInformationItemGroupProperty = InformationItemGroupPropertyExt::findOne(
            [
                'information_group_id' => $information_group_id,
                'information_property_id' => $id,
            ]
        );
        if( $oInformationItemGroupProperty ) {
            $oInformationItemGroupProperty->recursive = 1 - $oInformationItemGroupProperty->recursive;
            if ($oInformationItemGroupProperty->save()) {
                $this->setFlashSuccess(Yii::t('backend/layout', 'For property {name} and group {group_name} change recursive successfully',
                    ['name' => $oInformationProperty->name,'group_name'=>$oInformationGroup->name])
                );
            } else {
                $this->setFlashError( Yii::t('backend/layout', 'Failed to change active for property {name} and group {group_name}',
                    ['name' => $oInformationProperty->name,'group_name'=>$oInformationGroup->name])
                );
            }
        } else {
            $this->setFlashError( Yii::t('backend/layout', 'Connection between property {name} and group {group_name} has not been created',
                ['name' => $oInformationProperty->name,'group_name'=>$oInformationGroup->name])
            );
        }

        $this->redirect( ['/backend/information-property/index','id'=>$oInformationProperty->id] );
    }

    /**
     * @param int $id
     * @return array|string
     * @throws NotFoundHttpException
     */
    public function actionSave( $id = 0 )
    {
        /* @var $oInformationProperty InformationPropertyExt */
        if( $id ) {
            $oInformationProperty = InformationPropertyExt::getById($id);
        } else {
            $oInformationProperty = new InformationPropertyExt();
        }
        /* @var $result array */
        $result = [];
        $arValues = Yii::$app->request->post();
        if( $oInformationProperty->load( $arValues ) ) {

            if( Yii::$app->request->isAjax && Yii::$app->request->get('validate','') == 'true' ) {
                return ActiveForm::validate( $oInformationProperty );
            }

            $validErrors = ActiveForm::validate($oInformationProperty);

            if( !empty( $validErrors ) ) {
                $result = [
                    'status' => self::ERROR,
                    'message' => Yii::t('backend/layout','Please fill in all required fields'),
                    'errors' => $validErrors
                ];
            } else if( $oInformationProperty->save() ) {
                $result = [
                    'status'=>self::SUCCESS,
                    'message'=>Yii::t('backend/layout','Element {name} successfully saved!',['name'=>$oInformationProperty->name])
                ];
                if( !Yii::$app->request->isAjax ) {
                    $this->setFlashSuccess( Yii::t('backend/layout','Element {name} successfully saved!',['name'=>$oInformationProperty->name] ) );
                    //return $this->refresh();
                    $this->redirect( ['/backend/information/index'] );
                    Yii::$app->end();
                }
            }

            if( $oInformationProperty->hasErrors() ) {
                $result = [
                    'status'=>self::ERROR,
                    'message'=>Yii::t('backend/layout','Error saving element {name}',['name'=>$oInformationProperty->name]),
                    'errors' => $oInformationProperty->getErrors()
                ];
            }

            if( Yii::$app->request->isAjax ) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $result;
            }

        }

        $view = 'information-property-add';
        if( !$oInformationProperty->isNewRecord ) {
            $view = 'information-property-edit';
        }
        return $this->render(
            $view,
            [
                'model' => $oInformationProperty,
                'oInformation' => $oInformationProperty->information,
                'result' => $result
            ]
        );
    }
}