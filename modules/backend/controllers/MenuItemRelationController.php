<?php

namespace app\modules\backend\controllers;


use app\components\BackendController;
use app\models\ext\ContentExt;
use app\models\ext\MenuExt;
use app\models\ext\MenuItemExt;
use app\models\ext\MenuItemRelationExt;
use Exception;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class MenuItemRelationController extends BackendController
{
    public function actionChangeType($type,$id=0)
    {
        /* @var $oMenuItemRelation MenuItemRelationExt */
        try {
            $oMenuItemRelation = MenuItemRelationExt::getByTid($id);
        } catch( \yii\base\Exception $e ){
            $oMenuItemRelation = new MenuItemRelationExt();
        }


        if( $oMenuItemRelation->main ){
            $oMenuItemRelation->getItem(false);
        }
        //$oldType = $oMenuItemRelation->type;
        if( $oMenuItemRelation->type != $type ){
            $oMenuItemRelation->item_id = null;
        }
        $oMenuItemRelation->type = $type;
        /*if(!$oMenuItemRelation->save()){
            $oMenuItemRelation->type = $oMenuItemRelation->getOldAttribute('type');
        }*/
        $view = 'type/type-'.$oMenuItemRelation->type;
        $form = new ActiveForm();
        $form->requiredCssClass = 'has-required';
        $form->options = [
            'enctype' => 'multipart/form-data',
            'class' => 'form-horizontal form-bordered'
        ];
        $form->fieldConfig = [
            'template' => "

                                    {label}\n
                                    <div class=\"col-md-6\">
                    {input}\n
                    {hint}\n
                    {error}\n
                                    </div>

",
            'labelOptions' => [
                'class' => 'col-md-3 control-label'
            ],
        ];
        $context = [
            'num' => $id,
            'oMenuRelation' => $oMenuItemRelation,
            'oMenu' => $oMenuItemRelation->menuItem,
            'activeForm' => $form,
        ];
        return $this->renderPartial($view,$context);
    }

    public function actionChangeContent($id=0,$contentId=0)
    {
        /* @var $oMenuItemRelation MenuItemRelationExt */
        if( $id ) {
            $oMenuItemRelation = MenuItemRelationExt::getByTid($id);
        } else {
            $oMenuItemRelation = new MenuItemRelationExt();
            $oMenuItemRelation->type = MenuItemRelationExt::TYPE_CONTENT;
        }

        if( $contentId ){
            $oContent = ContentExt::getById($contentId);
        } else {
            $oContent = new ContentExt();
        }
        $view = 'type/type-'.$oMenuItemRelation->type.'-inner';
        $form = new ActiveForm();
        $form->requiredCssClass = 'has-required';
        $form->options = [
            'enctype' => 'multipart/form-data',
            'class' => 'form-horizontal form-bordered'
        ];
        $form->fieldConfig = [
            'template' => "

                                    {label}\n
                                    <div class=\"col-md-6\">
                    {input}\n
                    {hint}\n
                    {error}\n
                                    </div>

",
            'labelOptions' => [
                'class' => 'col-md-3 control-label'
            ],
        ];
        $context = [
            'num' => $id,
            'oMenuRelation' => $oMenuItemRelation,
            'oMenu' => $oMenuItemRelation->menuItem,
            'activeForm' => $form,
            'oContent' => $oContent
        ];
        return $this->renderPartial($view,$context);
    }

    public function actionReposition($id,$number)
    {
        /* @var $oMenuItemRelation MenuItemRelationExt */
        $oMenuItemRelation = MenuItemRelationExt::getByTid($id);

        $oMenuItemRelation->reposition($number);

        $status = 'success';
        $message = 'Успешно изменена';
        $errors = [];

        if ($oMenuItemRelation->hasErrors()) {
            $errors = $oMenuItemRelation->getErrors();
            $status = 'error';
            $message = 'Не удалось изменить';
        }

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'status' => $status,
                'message' => $message,
                'errors' => $errors,
                'id' => $oMenuItemRelation->id,
                'position' => $oMenuItemRelation->position,
            ];
        }

        $this->setFlashSuccess( Yii::t('backend/layout','Element {name} successfully saved!',['name'=>$oMenuItemRelation->id] ) );
        $this->redirect(['/backend/menu-item/edit','id'=>$oMenuItemRelation->menu_item_id]);
    }

    public function actionCreate($menu_item_id=0,$num=0)
    {
        if( $menu_item_id ) {
            $oMenuItem = MenuItemExt::getById($menu_item_id);
        } else {
            $oMenuItem = new MenuItemExt();
        }

        $oMenuItemRelation = new MenuItemRelationExt();
        $oMenuItemRelation->menu_item_id = $oMenuItem->id;
        $oMenuItemRelation->item_id = 0;
        $oMenuItemRelation->position = MenuItemRelationExt::getMax('position',0,['=','menu_item_id',$menu_item_id])+10;
        $oMenuItemRelation->main = 0;
        $oMenuItemRelation->active = 0;
        $oMenuItemRelation->type = MenuItemRelationExt::TYPE_CONTENT;
        $oMenuItemRelation->params = '';
        $oMenuItemRelation->save();

        if ($oMenuItemRelation->load($_POST)) {
            $save = $oMenuItemRelation->save();

            $status = 'success';
            $message = 'Успешно добавлена';
            $errors = [];

            if ($oMenuItemRelation->hasErrors()) {
                $errors = $oMenuItemRelation->getErrors();
                $status = 'error';
                $message = 'Не удалось добавить.';
            }

            if( Yii::$app->request->isAjax ) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'status' => $status,
                    'message' => $message,
                    'errors' => $errors,
                ];
            } else if ($save) {
                $this->redirect(['/backend/menu-item/edit','id'=>$oMenuItem->id]);
            }
        }


        $view = 'menu-item-relation-form';
        $context = array(
            'oMenuRelation' => $oMenuItemRelation,
            'oMenu' => $oMenuItem,
        );
        if (Yii::$app->request->isAjax) {
            $view = 'menu-item-relation-form-inner';
            $context['num'] = $num;
            $form = new ActiveForm();
            $form->requiredCssClass = 'has-required';
            $form->options = [
                'enctype' => 'multipart/form-data',
                'class' => 'form-horizontal form-bordered'
            ];
            $form->fieldConfig = [
                'template' => "

                                    {label}\n
                                    <div class=\"col-md-6\">
                    {input}\n
                    {hint}\n
                    {error}\n
                                    </div>

",
                'labelOptions' => [
                    'class' => 'col-md-3 control-label'
                ],
            ];
            $context['activeForm'] = $form;
            return $this->renderPartial($view, $context);
        }
        return $this->render($view, $context);
    }

    /**
     * @param int $id
     * @return array|string
     * @throws NotFoundHttpException
     */
    public function actionSave( $id )
    {
        /* @var $oMenuItemRelation MenuItemRelationExt */
        $oMenuItemRelation = MenuItemRelationExt::getByTid($id);
        /* @var $result array */
        $result = [];
        $arValues = ArrayHelper::getValue(Yii::$app->request->post('MenuItemRelationExt',[]),$id,[]);
        if( $oMenuItemRelation->load( $arValues, '' ) ) {

            if( Yii::$app->request->isAjax && Yii::$app->request->get('validate','') == 'true' ) {
                return ActiveForm::validate( $oMenuItemRelation );
            }

            if( $oMenuItemRelation->isNewRecord ){
                $oMenuItemRelation->position = MenuItemRelationExt::getMax('position', 0,($oMenuItemRelation->menu_item_id)?['=','menu_item_id',$oMenuItemRelation->menu_item_id]:null) + 10;
            }

            $validErrors = ActiveForm::validate($oMenuItemRelation);

            if( !empty( $validErrors ) ) {
                $result = [
                    'status' => self::ERROR,
                    'message' => Yii::t('backend/layout','Please fill in all required fields'),
                    'errors' => $validErrors
                ];
            } else if( $oMenuItemRelation->save() ) {

                //сохранить связанные элементы
                if( isset( $_POST['ContentExt'][$oMenuItemRelation->id] ) ) {
                    $contentData = $_POST['ContentExt'][$oMenuItemRelation->id];
                    if( !empty( $contentData['id'] ) ) {
                        $oContent = ContentExt::getById($contentData['id']);
                    } else {
                        $oContent = new ContentExt();
                    }

                    $oContent->content = $contentData['content'];
                    $oContent->name = $contentData['name'];

                    if( $oMenuItemRelation->main ) {
                        $oContent->menu_item_id = $oMenuItemRelation->menu_item_id;
                        $oContent->name = $oMenuItemRelation->menuItem->name;
                    }
                    if( $oContent->save() ) {
                        $oMenuItemRelation->item_id = $oContent->id;
                    } else {
                        if( $oMenuItemRelation->type !== $oMenuItemRelation->getOldAttribute('type') ){
                            $oMenuItemRelation->type = $oMenuItemRelation->getOldAttribute('type');
                        }
                    }
                }

                $result = [
                    'status'=>self::SUCCESS,
                    'message'=>Yii::t('backend/layout','Element {name} successfully saved!',['name'=>$oMenuItemRelation->id])
                ];
                if( !Yii::$app->request->isAjax ) {
                    $this->setFlashSuccess( Yii::t('backend/layout','Element {name} successfully saved!',['name'=>$oMenuItemRelation->id] ) );
                    //return $this->refresh();
                    $this->redirect( ['/backend/menu-item/edit','id'=>$oMenuItemRelation->menu_item_id] );
                    Yii::$app->end();
                }
            }

            if( $oMenuItemRelation->hasErrors() ) {
                $result = [
                    'status'=>self::ERROR,
                    'message'=>Yii::t('backend/layout','Error saving element {name}',['name'=>$oMenuItemRelation->id]),
                    'errors' => $oMenuItemRelation->getErrors()
                ];
            }

            if( Yii::$app->request->isAjax ) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $result;
            }

        }

        $view = 'menu-item-relation-form';
        $context = array(
            'oMenuRelation' => $oMenuItemRelation,
            'oMenu' => $oMenuItemRelation->menuItem,
        );
        if (Yii::$app->request->isAjax) {
            $view = 'menu-item-relation-form-inner';
            $form = new ActiveForm();
            $form->requiredCssClass = 'has-required';
            $form->options = [
                'enctype' => 'multipart/form-data',
                'class' => 'form-horizontal form-bordered'
            ];
            $form->fieldConfig = [
                'template' => "

                                    {label}\n
                                    <div class=\"col-md-6\">
                    {input}\n
                    {hint}\n
                    {error}\n
                                    </div>

",
                'labelOptions' => [
                    'class' => 'col-md-3 control-label'
                ],
            ];
            $context['activeForm'] = $form;
            return  $this->renderPartial($view, $context);
        }
        return $this->render($view, $context);
    }

    /**
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionDelete( $id )
    {
        /* @var $oMenuItemRelation MenuItemRelationExt */
        $oMenuItemRelation = MenuItemRelationExt::getByTid( $id );
        $result = [
            'status'=>self::SUCCESS,
            'message'=>Yii::t('backend/layout','Element {name} successfully deletes!',['name'=>$oMenuItemRelation->id])
        ];
        try {
            $oMenuItemRelation->delete();
            if( !Yii::$app->request->isAjax ) {
                $this->setFlashSuccess(Yii::t('backend/layout', 'Item {name} deleted successfully', ['name' => $oMenuItemRelation->id]));
            }
        } catch( Exception $e ) {
            $result = [
                'status'=>self::ERROR,
                'message'=>Yii::t('backend/layout','Error delete element {name}',['name'=>$oMenuItemRelation->id]),
                'errors' => $oMenuItemRelation->getErrors()
            ];
            if( !Yii::$app->request->isAjax ) {
                $this->setFlashError(Yii::t('backend/layout', 'Failed to remove an item {name}', ['name' => $oMenuItemRelation->id]));
            }
        }

        if( Yii::$app->request->isAjax ) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $result;
        }

        $this->redirect( ['/backend/menu-item/edit','id'=>$oMenuItemRelation->menu_item_id] );
    }
}