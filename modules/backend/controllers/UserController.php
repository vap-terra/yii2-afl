<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 22.02.16
 * Time: 14:44
 */

namespace app\modules\backend\controllers;


use app\models\forms\LoginForm;
use app\models\forms\ResetPasswordForm;
use app\models\forms\SignupForm;
use app\models\forms\PasswordResetRequestForm;
use Yii;
use yii\base\InvalidParamException;
use yii\bootstrap\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

use app\components\BackendController;
use app\models\BaseActiveRecord;
use app\models\ext\UserExt;

class UserController extends BackendController
{
    public function actionIndex()
    {
        $model = new UserExt();
        $model->setScenario('changePassword');
        $arRoles = UserExt::getOptionsRoleList();

        $arUserDataProviders = [];
        foreach( $arRoles as $key => $name ) {
            $arUserDataProviders[ $key ] = new ActiveDataProvider(
                [
                    'query' => UserExt::find()->where([ '=', 'role', $key ])
                ]
            );
        }

        return $this->render(
            'index',
            [
                'arUserDataProviders' => $arUserDataProviders,
                'model' => $model,
                'arRoles' => $arRoles,
            ]
        );
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionEdit( $id ) {
        /* @var $oUser UserExt */
        $oUser = UserExt::getById( $id );
        return $this->render(
            'user-edit',
            [
                'model' => $oUser,
                'result' => []
            ]
        );
    }

    public function actionAdd() {
        /* @var $oUser UserExt */
        $oUser = new UserExt();
        $oUser->setScenario('changePassword');
        return $this->render(
            'user-add',
            [
                'model' => $oUser,
                'result' => []
            ]
        );
    }

    /**
     * @param $id
     * @throws NotFoundHttpException
     */
    public function actionDelete( $id )
    {
        /* @var $oUser UserExt */
        $oUser = UserExt::getById( $id );
        if( $id != 1 ) {
            if( $oUser->delete() ) {
                Yii::$app->getSession()->setFlash('success', Yii::t('backend/layout','Item {name} deleted successfully',['name'=>$oUser->name]));
            } else {
                Yii::$app->getSession()->setFlash('error', Yii::t('backend/layout','Failed to remove an item {name}',['name' => $oUser->name]));
            }
        } else {
            $this->setFlashInfo(Yii::t('backend/layout','You do not have permission to remove the item {name}',['name' => $oUser->name]));
        }
        $this->redirect( ['/backend/user/edit','id'=>$oUser->id] );
    }

    /**
     * @return array|string
     */
    public function actionSave( $id = 0 )
    {
        /* @var $oUser UserExt */
        if( $id ) {
            $oUser = UserExt::getById($id);
        } else {
            $oUser = new UserExt();
            $oUser->setScenario('changePassword');
        }
        /* @var $result array */
        $result = [];
        $arValues = Yii::$app->request->post();
        if( ( $password = ArrayHelper::getValue( ArrayHelper::getValue($arValues,$oUser->formName() ), 'password' ) )  ){
            $oUser->setScenario('changePassword');
        }
        if( $oUser->load( $arValues ) ) {
            if( Yii::$app->request->isAjax && Yii::$app->request->get('validate','') == 'true' ) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate( $oUser );
            }

            if( !empty( $password ) ) {
                $oUser->setPassword($password);
            }

            /*var_dump($oUser->getOldAttribute('password_hash'));
            var_dump($oUser->password_hash);
            die();*/
            $validErrors = ActiveForm::validate($oUser);

            if( !empty( $validErrors ) ) {
                $result = [
                    'status' => self::ERROR,
                    'message' => Yii::t('backend/layout','Please fill in all required fields'),
                    'errors' => $validErrors
                ];
            } else if( $oUser->save() ) {
                $result = [
                    'status'=>self::SUCCESS,
                    'message'=>Yii::t('backend/layout','Element {name} successfully saved!',['name'=>$oUser->name])
                ];

                $file = UploadedFile::getInstance($oUser, 'photo');
                if ($file) {
                    if ($file->error == 0) {
                        if (!$oUser->uploadImage($file, true, true)) {
                            $result['errors'] = [
                                BaseActiveRecord::getErrorUploadMessage(8, $file->name)
                            ];
                        }
                    } else if ($file->error != 4) {
                        $result['errors'] = [
                            BaseActiveRecord::getErrorUploadMessage($file->error, $file->name)
                        ];
                    }
                }

                if( !Yii::$app->request->isAjax ) {
                    $this->setFlashSuccess( Yii::t('backend/layout','Element {name} successfully saved!',['name'=>$oUser->name] ) );
                    //return $this->refresh();
                    $this->redirect( ['/backend/user/edit','id'=>$oUser->id] );
                    Yii::$app->end();
                }
            }

            if( $oUser->hasErrors() ) {
                $result = [
                    'status'=>self::ERROR,
                    'message'=>Yii::t('backend/layout','Error saving element {name}',['name'=>$oUser->name]),
                    'errors' => $oUser->getErrors()
                ];
            }

            if( Yii::$app->request->isAjax ) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $result;
            }

        }

        $view = 'user-add';
        if( !$oUser->isNewRecord ) {
            $view = 'user-edit';
        }
        return $this->render(
            $view,
            [
                'model' => $oUser,
                'result' => $result
            ]
        );
    }

    public function actionSecurity()
    {
        /* @var $oUser UserExt */
        $oUser = UserExt::getById(1);
        $oUser->setScenario('changePassword');
        return $this->render(
            'user-change',
            [
                'model' => $oUser,
                'result' => []
            ]
        );
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render(
                'user-login',
                [
                    'model' => $model,
                ]
            );
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render(
            'user-signup',
            [
                'model' => $model,
            ]
        );
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                $this->setFlashSuccess(Yii::t('backend/layout','Check your email for further instructions.'));
                return $this->goHome();
            } else {
                $this->setFlashError(Yii::t('backend/layout','Sorry, we are unable to reset password for email provided.'));
            }
        }

        return $this->render(
            'user-request-password-reset-token',
            [
                'model' => $model,
            ]
        );
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            $this->setFlashSuccess(Yii::t('backend/layout','New password was saved.'));

            return $this->goHome();
        }

        return $this->render(
            'user-reset-password',
            [
                'model' => $model,
            ]
        );
    }
}