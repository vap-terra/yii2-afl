<?php

namespace app\modules\backend\controllers;


use app\components\BackendController;
use Yii;

class ConstantController extends BackendController
{
    protected $_defaultRoute = ['/backend/constant/index'];
    protected $_modelClass = 'app\models\ext\ConstantExt';

    public function actions()
    {
        return array_merge(
            parent::actions(),
            [
                'index' => [
                    'class' => 'app\components\actions\ListAction',
                    'modelClass' => $this->_modelClass,
                    'view' => 'constant-list',
                ],
                'edit' => [
                    'class'=>'app\components\actions\EditAction',
                    'modelClass'=>$this->_modelClass,
                    'view' => 'constant-edit',
                ],
                'add' => [
                    'class'=>'app\components\actions\AddAction',
                    'modelClass'=>$this->_modelClass,
                    'view' => 'constant-add',
                ],
                'delete' => [
                    'class'=>'app\components\actions\DeleteAction',
                    'modelClass'=>$this->_modelClass,
                    'modelMethod' => 'delete',
                    'titleAttribute' => 'name',
                    'redirect'=>$this->_defaultRoute,
                ],
                'save' => [
                    'class'=>'app\components\actions\SaveAction',
                    'modelClass'=>$this->_modelClass,
                    'modelMethod' => 'save',
                    'view' => 'constant-edit',
                    'viewUpdate' => 'constant-add',
                    'titleAttribute' => 'name',
                    'redirect'=>$this->_defaultRoute,
                ]
            ]
        );
    }
}