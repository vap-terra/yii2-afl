<?php

namespace app\modules\backend\controllers;

use app\components\BackendController;
use app\models\ext\BlockExt;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class BlockController extends BackendController
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        /* @var $qBlock ActiveQuery */
        $qBlock = BlockExt::find();
        $dataProvider = new ActiveDataProvider(
            [
                'query' => $qBlock
            ]
        );
        return $this->render(
            'index',
            [
                'dataProviderBlocks' => $dataProvider,
            ]
        );
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionEdit( $id ) {
        /* @var $oBlockExt BlockExt */
        $oBlockExt = BlockExt::getById( $id );
        return $this->render(
            'block-edit',
            [
                'model' => $oBlockExt,
                'result' => []
            ]
        );
    }

    /**
     * @return string
     */
    public function actionAdd() {
        /* @var $oBlockExt BlockExt */
        $oBlockExt = new BlockExt();
        return $this->render(
            'block-add',
            [
                'model' => $oBlockExt,
                'result' => []
            ]
        );
    }

    /**
     * @param $id
     * @throws NotFoundHttpException
     */
    public function actionDelete( $id )
    {
        /* @var $oBlockExt BlockExt */
        $oBlockExt = BlockExt::getById( $id );
        if( Yii::$app->user->identity->isRoot || !$oBlockExt->system  ) {
            if( $oBlockExt->delete() ) {
                $this->setFlashSuccess(Yii::t('backend/layout','Item {name} deleted successfully',['name'=>$oBlockExt->name]));
            } else {
                $this->setFlashError(Yii::t('backend/layout','Failed to remove an item {name}',['name' => $oBlockExt->name]));
            }
        } else {
            $this->setFlashInfo(Yii::t('backend/layout','You do not have permission to remove the item {name}',['name' => $oBlockExt->name]));
        }
        $this->redirect( ['/page/block/index'] );
    }

    /**
     * @param int $id
     * @return array|string
     * @throws NotFoundHttpException
     */
    public function actionSave( $id = 0 )
    {
        /* @var $oBlockExt BlockExt */
        if( $id ) {
            $oBlockExt = BlockExt::getById($id);
        } else {
            $oBlockExt = new BlockExt();
        }
        /* @var $result array */
        $result = [];
        $arValues = Yii::$app->request->post();
        if( $oBlockExt->load( $arValues ) ) {

            if( Yii::$app->request->isAjax && Yii::$app->request->get('validate','') == 'true' ) {
                return ActiveForm::validate( $oBlockExt );
            }

            $validErrors = ActiveForm::validate($oBlockExt);

            if( !empty( $validErrors ) ) {
                $result = [
                    'status' => self::ERROR,
                    'message' => Yii::t('backend/layout','Please fill in all required fields'),
                    'errors' => $validErrors
                ];
            } else if( $oBlockExt->save() ) {
                $result = [
                    'status'=>self::SUCCESS,
                    'message'=>Yii::t('backend/layout','Element {name} successfully saved!',['name'=>$oBlockExt->name])
                ];
                if( !Yii::$app->request->isAjax ) {
                    $this->setFlashSuccess( Yii::t('backend/layout','Element {name} successfully saved!',['name'=>$oBlockExt->name] ) );
                    //return $this->refresh();
                    $this->redirect( ['/page/block/index'] );
                    Yii::$app->end();
                }
            }

            if( $oBlockExt->hasErrors() ) {
                $result = [
                    'status'=>self::ERROR,
                    'message'=>Yii::t('backend/layout','Error saving element {name}',['name'=>$oBlockExt->name]),
                    'errors' => $oBlockExt->getErrors()
                ];
            }

            if( Yii::$app->request->isAjax ) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $result;
            }

        }

        $view = 'block-add';
        if( !$oBlockExt->isNewRecord ) {
            $view = 'block-edit';
        }
        return $this->render(
            $view,
            [
                'model' => $oBlockExt,
                'result' => $result
            ]
        );
    }
}