<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 28.02.16
 * Time: 12:10
 */

namespace app\modules\backend\controllers;

use app\components\BackendController;
use app\models\ext\GeoCountryExt;
use app\models\ext\GeoRegionExt;
use Yii;

class GeoRegionController extends BackendController
{
    protected $_defaultRoute = ['/backend/geo-region/index','geo_country_id'];
    protected $_modelClass = 'app\models\ext\GeoRegionExt';

    public function actions()
    {
        return array_merge(
            parent::actions(),
            [
                /*'index' => [
                    'class' => 'app\components\actions\ListAction',
                    'modelClass' => $this->_modelClass,
                    'view' => 'geo-region-list',
                ],*/
                'edit' => [
                    'class'=>'app\components\actions\EditAction',
                    'modelClass'=>$this->_modelClass,
                    'view' => 'geo-region-edit',
                ],
                'add' => [
                    'class'=>'app\components\actions\AddAction',
                    'modelClass'=>$this->_modelClass,
                    'view' => 'geo-region-add',
                    'presetAttributesFromQuery' => ['geo_country_id'=>'geo_country_id'],
                ],
                'delete' => [
                    'class'=>'app\components\actions\DeleteAction',
                    'modelClass'=>$this->_modelClass,
                    'modelMethod' => 'delete',
                    'titleAttribute' => 'name',
                    'redirect'=>$this->_defaultRoute,
                ],
                'save' => [
                    'class'=>'app\components\actions\SaveAction',
                    'modelClass'=>$this->_modelClass,
                    'modelMethod' => 'save',
                    'view' => 'geo-region-edit',
                    'viewUpdate' => 'geo-region-add',
                    'titleAttribute' => 'name',
                    'fileAttribute' => '',
                    'modelUploadMethod' => 'uploadImage',
                    'redirect'=>$this->_defaultRoute,
                ]
            ]
        );
    }

    /**
     * @return string
     */
    public function actionIndex($geo_country_id)
    {
        $qGeoRegion = GeoRegionExt::find();
        return $this->render(
            'index',
            [
                'query' => $qGeoRegion,
                'oGeoCountry' => GeoCountryExt::getById($geo_country_id),
            ]
        );
    }


}