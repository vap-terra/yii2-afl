<?php

namespace app\modules\backend\controllers;


use app\components\BackendController;

use Yii;

class UnitController extends BackendController
{
    protected $_defaultRoute = ['/backend/unit/index'];
    protected $_modelClass = 'app\models\ext\UnitExt';

    public function actions()
    {
        return array_merge(
            parent::actions(),
            [
                'index' => [
                    'class' => 'app\components\actions\ListAction',
                    'modelClass' => $this->_modelClass,
                    'view' => 'unit-list',
                ],
                'edit' => [
                    'class'=>'app\components\actions\EditAction',
                    'modelClass'=>$this->_modelClass,
                    'view' => 'unit-edit',
                ],
                'add' => [
                    'class'=>'app\components\actions\AddAction',
                    'modelClass'=>$this->_modelClass,
                    'view' => 'unit-add',
                ],
                'delete' => [
                    'class'=>'app\components\actions\DeleteAction',
                    'modelClass'=>$this->_modelClass,
                    'modelMethod' => 'delete',
                    'titleAttribute' => 'name',
                    'redirect'=>$this->_defaultRoute,
                ],
                'change-active'=>[
                    'class'=>'app\components\actions\SwitchAction',
                    'modelClass'=>$this->_modelClass,
                    'modelMethod' => 'switchBoolean',
                    'modelAttribute' => 'active',
                    'titleAttribute' => 'name',
                    'redirect'=>$this->_defaultRoute,
                ],
                'save' => [
                    'class'=>'app\components\actions\SaveAction',
                    'modelClass'=>$this->_modelClass,
                    'modelMethod' => 'save',
                    'view' => 'unit-edit',
                    'viewUpdate' => 'unit-add',
                    'titleAttribute' => 'name',
                    'fileAttribute' => '',
                    'modelUploadMethod' => '',
                    'redirect'=>$this->_defaultRoute,
                ]
            ]
        );
    }

}