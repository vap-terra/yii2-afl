<?php

namespace app\modules\backend\controllers;


use app\components\BackendController;
use app\models\ext\MenuExt;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class MenuController extends BackendController
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        /* @var $qMenu ActiveQuery */
        $qMenu = MenuExt::find();
        $dataProviderMenus = new ActiveDataProvider(
            [
                'query' => $qMenu
            ]
        );
        return $this->render(
            'index',
            [
                'dataProviderMenus' => $dataProviderMenus,
            ]
        );
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionEdit( $id ) {
        /* @var $oMenu MenuExt */
        $oMenu = MenuExt::getByTid( $id );
        return $this->render(
            'menu-edit',
            [
                'model' => $oMenu,
                'result' => []
            ]
        );
    }

    /**
     * @return string
     */
    public function actionAdd() {
        /* @var $oMenu MenuExt */
        $oMenu = new MenuExt();
        return $this->render(
            'menu-add',
            [
                'model' => $oMenu,
                'result' => []
            ]
        );
    }

    /**
     * @param $id
     * @throws NotFoundHttpException
     */
    public function actionChangeActive( $id )
    {
        /* @var $oBanner MenuExt */
        $oMenu = MenuExt::getByTid( $id );
        $oMenu->active = 1 - $oMenu->active;
        if( $oMenu->save() ) {
            Yii::$app->getSession()->setFlash('success', Yii::t('backend/layout','Item {name} change active successfully',['name'=>$oMenu->title]));
        } else {
            Yii::$app->getSession()->setFlash('error', Yii::t('backend/layout','Failed to change active an item {name}',['name' => $oMenu->title]));
        }
        $this->redirect( ['/backend/menu/index'] );
    }

    /**
     * @param $id
     * @throws NotFoundHttpException
     */
    public function actionDelete( $id )
    {
        /* @var $oMenu MenuExt */
        $oMenu = MenuExt::getByTid( $id );
        if( $oMenu->delete() ) {
            $this->setFlashSuccess(Yii::t('backend/layout','Item {name} deleted successfully',['name'=>$oMenu->title]));
        } else {
            $this->setFlashError(Yii::t('backend/layout','Failed to remove an item {name}',['name' => $oMenu->title]));
        }
        $this->redirect( ['/backend/menu/index'] );
    }

    /**
     * @param int $id
     * @return array|string
     * @throws NotFoundHttpException
     */
    public function actionSave( $id = 0 )
    {
        /* @var $oMenu MenuExt */
        if( $id ) {
            $oMenu = MenuExt::getByTid($id);
        } else {
            $oMenu = new MenuExt();
        }
        /* @var $result array */
        $result = [];
        $arValues = Yii::$app->request->post();
        if( $oMenu->load( $arValues ) ) {

            if( Yii::$app->request->isAjax && Yii::$app->request->get('validate','') == 'true' ) {
                return ActiveForm::validate( $oMenu );
            }

            $validErrors = ActiveForm::validate($oMenu);

            if( !empty( $validErrors ) ) {
                $result = [
                    'status' => self::ERROR,
                    'message' => Yii::t('backend/layout','Please fill in all required fields'),
                    'errors' => $validErrors
                ];
            } else if( $oMenu->save() ) {
                $result = [
                    'status'=>self::SUCCESS,
                    'message'=>Yii::t('backend/layout','Element {name} successfully saved!',['name'=>$oMenu->title])
                ];
                if( !Yii::$app->request->isAjax ) {
                    $this->setFlashSuccess( Yii::t('backend/layout','Element {name} successfully saved!',['name'=>$oMenu->title] ) );
                    //return $this->refresh();
                    $this->redirect( ['/backend/menu/index'] );
                    Yii::$app->end();
                }
            }

            if( $oMenu->hasErrors() ) {
                $result = [
                    'status'=>self::ERROR,
                    'message'=>Yii::t('backend/layout','Error saving element {name}',['name'=>$oMenu->title]),
                    'errors' => $oMenu->getErrors()
                ];
            }

            if( Yii::$app->request->isAjax ) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $result;
            }

        }

        $view = 'menu-add';
        if( !$oMenu->isNewRecord ) {
            $view = 'menu-edit';
        }
        return $this->render(
            $view,
            [
                'model' => $oMenu,
                'result' => $result
            ]
        );
    }
}