<?php

namespace app\modules\backend\controllers;



use app\components\BackendController;
use app\models\ext\ListValueItemExt;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class ListItemController extends BackendController
{

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionEdit( $id ) {
        /* @var $oListValueItem ListValueItemExt */
        $oListValueItem = ListValueItemExt::getById( $id );
        return $this->render(
            'list-value-item-edit',
            [
                'model' => $oListValueItem,
                'result' => []
            ]
        );
    }

    public function actionAdd() {
        /* @var $oListValueItem ListValueItemExt */
        $oListValueItem = new ListValueItemExt();
        return $this->render(
            'list-value-item-add',
            [
                'model' => $oListValueItem,
                'result' => []
            ]
        );
    }

    /**
     * @param $id
     * @throws NotFoundHttpException
     */
    public function actionDelete( $id )
    {
        /* @var $oListValueItem ListValueItemExt */
        $oListValueItem = ListValueItemExt::getById( $id );
        if( $oListValueItem->delete() ) {
            Yii::$app->getSession()->setFlash('success', Yii::t('backend/layout','Item {name} deleted successfully',['name'=>$oListValueItem->name]));
        } else {
            Yii::$app->getSession()->setFlash('error', Yii::t('backend/layout','Failed to remove an item {name}',['name' => $oListValueItem->name]));
        }
        $this->redirect( ['/page/list/index'] );
    }

    /**
     * @param $id
     * @throws NotFoundHttpException
     */
    public function actionChangeActive( $id )
    {
        /* @var $oListValueItem ListValueItemExt */
        $oListValueItem = ListValueItemExt::getById( $id );
        $oListValueItem->active = 1 - $oListValueItem->active;
        if( $oListValueItem->save() ) {
            Yii::$app->getSession()->setFlash('success', Yii::t('backend/layout','Item {name} change active successfully',['name'=>$oListValueItem->name]));
        } else {
            Yii::$app->getSession()->setFlash('error', Yii::t('backend/layout','Failed to change active an item {name}',['name' => $oListValueItem->name]));
        }
        $this->redirect( ['/backend/list/index'] );
    }

    /**
     * @param int $id
     * @return array|string
     * @throws NotFoundHttpException
     */
    public function actionSave( $id = 0 )
    {
        /* @var $oListValueItem ListValueItemExt */
        if( $id ) {
            $oListValueItem = ListValueItemExt::getById($id);
        } else {
            $oListValueItem = new ListValueItemExt();
        }
        /* @var $result array */
        $result = [];
        $arValues = Yii::$app->request->post();
        if( $oListValueItem->load( $arValues ) ) {

            if( Yii::$app->request->isAjax && Yii::$app->request->get('validate','') == 'true' ) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate( $oListValueItem );
            }

            $validErrors = ActiveForm::validate($oListValueItem);

            if( !empty( $validErrors ) ) {
                $result = [
                    'status' => self::ERROR,
                    'message' => Yii::t('backend/layout','Please fill in all required fields'),
                    'errors' => $validErrors
                ];
            } else if( $oListValueItem->save() ) {
                $result = [
                    'status'=>self::SUCCESS,
                    'message'=>Yii::t('backend/layout','Element {name} successfully saved!',['name'=>$oListValueItem->name])
                ];
                if( !Yii::$app->request->isAjax ) {
                    $this->setFlashSuccess( Yii::t('backend/layout','Element {name} successfully saved!',['name'=>$oListValueItem->name] ) );
                    //return $this->refresh();
                    $this->redirect( ['/backend/list/index'] );
                    Yii::$app->end();
                }
            }

            if( $oListValueItem->hasErrors() ) {
                $result = [
                    'status'=>self::ERROR,
                    'message'=>Yii::t('backend/layout','Error saving element {name}',['name'=>$oListValueItem->name]),
                    'errors' => $oListValueItem->getErrors()
                ];
            }

            if( Yii::$app->request->isAjax ) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $result;
            }

        }

        $view = 'list-value-item-add';
        if( !$oListValueItem->isNewRecord ) {
            $view = 'list-value-item-edit';
        }
        return $this->render(
            $view,
            [
                'model' => $oListValueItem,
                'result' => $result
            ]
        );
    }
}