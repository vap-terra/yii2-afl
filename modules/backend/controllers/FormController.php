<?php

namespace app\modules\backend\controllers;


use app\components\BackendController;
use app\models\ext\FormExt;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class FormController extends BackendController
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        /* @var $qForm ActiveQuery */
        $qForm = FormExt::find();
        $formDataProvider = new ActiveDataProvider(
            [
                'query' => $qForm
            ]
        );
        return $this->render(
            'index',
            [
                'formDataProvider' => $formDataProvider,
            ]
        );
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionEdit( $id )
    {
        /* @var $oForm FormExt */
        $oForm = FormExt::getByTid( $id );
        return $this->render(
            'form-edit',
            [
                'model' => $oForm,
                'result' => []
            ]
        );
    }

    /**
     * @return string
     */
    public function actionAdd()
    {
        /* @var $oForm FormExt */
        $oForm = new FormExt();
        return $this->render(
            'form-add',
            [
                'model' => $oForm,
                'result' => []
            ]
        );
    }

    /**
     * @param $id
     * @throws NotFoundHttpException
     */
    public function actionDelete( $id )
    {
        /* @var $oForm FormExt */
        $oForm = FormExt::getByTid( $id );
        if( Yii::$app->user->identity->isRoot || !$oForm->system  ) {
            if( $oForm->delete() ) {
                $this->setFlashSuccess(Yii::t('backend/layout','Item {name} deleted successfully',['name'=>$oForm->name]));
            } else {
                $this->setFlashError(Yii::t('backend/layout','Failed to remove an item {name}',['name' => $oForm->name]));
            }
        } else {
            $this->setFlashInfo(Yii::t('backend/layout','You do not have permission to remove the item {name}',['name' => $oForm->name]));
        }
        $this->redirect( ['/backend/form/index'] );
    }

    /**
     * @param int $id
     * @return array|string
     * @throws NotFoundHttpException
     */
    public function actionSave( $id = 0 )
    {
        /* @var $oForm FormExt */
        if( $id ) {
            $oForm = FormExt::getByTid($id);
        } else {
            $oForm = new FormExt();
        }
        /* @var $result array */
        $result = [];
        $arValues = Yii::$app->request->post();
        if( $oForm->load( $arValues ) ) {

            if( Yii::$app->request->isAjax && Yii::$app->request->get('validate','') == 'true' ) {
                return ActiveForm::validate( $oForm );
            }

            $validErrors = ActiveForm::validate($oForm);

            if( !empty( $validErrors ) ) {
                $result = [
                    'status' => self::ERROR,
                    'message' => Yii::t('backend/layout','Please fill in all required fields'),
                    'errors' => $validErrors
                ];
            } else if( $oForm->save() ) {
                $result = [
                    'status'=>self::SUCCESS,
                    'message'=>Yii::t('backend/layout','Element {name} successfully saved!',['name'=>$oForm->name])
                ];
                if( !Yii::$app->request->isAjax ) {
                    $this->setFlashSuccess( Yii::t('backend/layout','Element {name} successfully saved!',['name'=>$oForm->name] ) );
                    //return $this->refresh();
                    $this->redirect( ['/backend/form/index'] );
                    Yii::$app->end();
                }
            }

            if( $oForm->hasErrors() ) {
                $result = [
                    'status'=>self::ERROR,
                    'message'=>Yii::t('backend/layout','Error saving element {name}',['name'=>$oForm->name]),
                    'errors' => $oForm->getErrors()
                ];
            }

            if( Yii::$app->request->isAjax ) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $result;
            }

        }

        $view = 'form-add';
        if( !$oForm->isNewRecord ) {
            $view = 'form-edit';
        }
        return $this->render(
            $view,
            [
                'model' => $oForm,
                'result' => $result
            ]
        );
    }
}