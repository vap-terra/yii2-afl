<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 28.02.16
 * Time: 13:58
 */

namespace app\modules\backend\controllers;


use app\components\BackendController;
use app\models\BaseActiveRecord;
use app\models\ext\InformationExt;
use app\models\ext\InformationGroupExt;
use dosamigos\transliterator\TransliteratorHelper;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\helpers\Inflector;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

class InformationGroupController extends BackendController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge_recursive(
            parent::behaviors(),
            [
                'access' => [
                    'rules' => [
                        [
                            'actions' => ['load-properties-form'],
                            'allow' => true,
                            'roles' => ['@'],
                        ],
                    ],
                ],
            ]
        );
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        /* @var $qInformationGroup ActiveQuery */
        $qInformationGroup = InformationGroupExt::find();
        $constantDataProvider = new ActiveDataProvider(
            [
                'query' => $qInformationGroup
            ]
        );
        return $this->render(
            'index',
            [
                'constantDataProvider' => $constantDataProvider,
            ]
        );
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionEdit( $id ) {
        /* @var $oInformationGroup InformationGroupExt */
        $oInformationGroup = InformationGroupExt::getByTid( $id );
        return $this->render(
            'information-group-edit',
            [
                'model' => $oInformationGroup,
                'oInformation' => $oInformationGroup->information,
                'result' => []
            ]
        );
    }

    /**
     * @param int $information_id
     * @return string
     */
    public function actionAdd( $information_id ) {
        /* @var $oInformationGroup InformationGroupExt */
        $oInformationGroup = new InformationGroupExt();
        $oInformation = InformationExt::getById( $information_id );
        return $this->render(
            'information-group-add',
            [
                'model' => $oInformationGroup,
                'oInformation' => $oInformation,
                'result' => []
            ]
        );
    }

    /**
     * @param $id
     * @param int $toId
     * @param int $beforeId
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionRelocation($id,$toId=0,$beforeId=0)
    {

        $result = [
            'status'=>self::ERROR,
            'message'=>'Error relocation element {name}',
            'errors' => []
        ];

        /* @var $oInformationGroup InformationGroupExt */
        $oInformationGroup = InformationGroupExt::getByTid( $id );

        if( $oInformationGroup->relocation($toId,$beforeId) ) {
            $result['message']='Item {name} relocation successfully';
            $result['status']=self::SUCCESS;
        } else {
            $result['message']='Failed to relocation an item {name}';
        }

        if( $oInformationGroup->hasErrors() ) {
            $result['errors'] = $oInformationGroup->getErrors();
        }

        if( Yii::$app->request->isAjax ) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $result['message'] = Yii::t('backend/layout',$result['message'],['name' => $oInformationGroup->name]);
            return $result;
        }

        $this->setFlash($result['status'],Yii::t('backend/layout',$result['message'],['name' => $oInformationGroup->name]));
        $this->redirect( ['/backend/information/index','id'=>$oInformationGroup->information_id] );
    }

    /**
     * @param $id
     * @throws NotFoundHttpException
     */
    public function actionDelete( $id )
    {
        /* @var $oInformationGroup InformationGroupExt */
        $oInformationGroup = InformationGroupExt::getByTid( $id );
        if( $oInformationGroup->delete() ) {
            Yii::$app->getSession()->setFlash('success', Yii::t('backend/layout','Item {name} deleted successfully',['name'=>$oInformationGroup->name]));
        } else {
            Yii::$app->getSession()->setFlash('error', Yii::t('backend/layout','Failed to remove an item {name}',['name' => $oInformationGroup->name]));
        }
        $this->redirect( ['/backend/information/index','id'=>$oInformationGroup->information_id] );
    }

    /**
     * @param int $id
     * @return array|string
     * @throws NotFoundHttpException
     */
    public function actionSave( $id = 0 )
    {
        /* @var $oInformationGroup InformationGroupExt */
        if( $id ) {
            $oInformationGroup = InformationGroupExt::getByTid($id);
        } else {
            $oInformationGroup = new InformationGroupExt();
        }
        /* @var $result array */
        $result = [];
        $arValues = Yii::$app->request->post();
        if( $oInformationGroup->load( $arValues ) ) {

            $oInformation = $oInformationGroup->information;

            if( $oInformationGroup->isNewRecord ) {
                $oInformationGroup->lb = 0;
                $oInformationGroup->rb = 0;
                $oInformationGroup->depth = 1;

                $count = $oInformation->getInformationGroups()->count();
                if( $count == 0 ) {
                    $oRootInformationGroup = new InformationGroupExt();
                    $oRootInformationGroup->name = 'Корневая группа для '.$oInformation->name;
                    $oRootInformationGroup->tid = 'root-'.Inflector::slug( TransliteratorHelper::process( $oInformation->name ), '-', true );
                    $oRootInformationGroup->information_id = $oInformation->id;
                    $oRootInformationGroup->information_group_id = null;
                    $oRootInformationGroup->position = InformationGroupExt::getMax('position',0,['information_group_id'=>null])+10;
                    $oRootInformationGroup->active = 1;
                    $oRootInformationGroup->lb = 1;
                    $oRootInformationGroup->rb = 2;
                    $oRootInformationGroup->depth = 1;
                    $oRootInformationGroup->makeRoot();
                    if( !$oRootInformationGroup->save() ){
                        var_dump($oRootInformationGroup->getErrors());
                        die();
                    }
                }

                if( $count == 0 || $oInformationGroup->information_group_id == 0 ) {
                    if( !empty( $oRootInformationGroup ) ){
                        $oInformationGroupBefore = $oRootInformationGroup;
                        $count = 1;
                    } else {
                        $oInformationGroupBefore = $oInformation->getInformationGroups()->where(['=', 'depth', 2])->orderBy(['lb' => SORT_DESC])->one();
                    }
                    $oInformationGroup->information_group_id = $oInformationGroupBefore->id;
                    if( $count == 1 ) {
                        if( !$oInformationGroup->position ) {
                            $oInformationGroup->position = 10;
                        }
                        $oInformationGroup->appendTo($oInformationGroupBefore);
                    } else {
                        if( !$oInformationGroup->position ){
                            $oInformationGroup->position = $oInformationGroupBefore->position + 10;
                        }
                        $oInformationGroup->insertAfter($oInformationGroupBefore);
                    }

                } else {
                    $oInformationGroupParent = $oInformationGroup->getInformationGroup()->one();
                    if( !$oInformationGroup->position ) {
                        $oInformationGroupBrother = $oInformationGroupParent->getInformationGroups()->orderBy(['position' => SORT_DESC])->one();
                        if( $oInformationGroupBrother ) {
                            $oInformationGroup->position = $oInformationGroupBrother->position + 10;
                        } else {
                            $oInformationGroup->position = 10;
                        }
                    }
                    $oInformationGroup->appendTo($oInformationGroupParent);
                }
            }

            if( Yii::$app->request->isAjax && Yii::$app->request->get('validate','') == 'true' ) {
                return ActiveForm::validate( $oInformationGroup );
            }

            $validErrors = ActiveForm::validate($oInformationGroup);

            if( !empty( $validErrors ) ) {
                $result = [
                    'status' => self::ERROR,
                    'message' => Yii::t('backend/layout','Please fill in all required fields'),
                    'errors' => $validErrors
                ];
            } else if( $oInformationGroup->save() ) {
                $result = [
                    'status'=>self::SUCCESS,
                    'message'=>Yii::t('backend/layout','Element {name} successfully saved!',['name'=>$oInformationGroup->name])
                ];


                $file = UploadedFile::getInstance($oInformationGroup, 'image');
                if ($file) {
                    if ($file->error == 0) {
                        if (!$oInformationGroup->uploadImage($file, true, true)) {
                            $result['errors'] = [
                                BaseActiveRecord::getErrorUploadMessage(8, $file->name)
                            ];
                        }
                    } else if ($file->error != 4) {
                        $result['errors'] = [
                            BaseActiveRecord::getErrorUploadMessage($file->error, $file->name)
                        ];
                    }
                }


                if( !Yii::$app->request->isAjax ) {
                    $this->setFlashSuccess( Yii::t('backend/layout','Element {name} successfully saved!',['name'=>$oInformationGroup->name] ) );
                    //return $this->refresh();
                    $this->redirect( ['/backend/information/index'] );
                    Yii::$app->end();
                }
            }

            if( $oInformationGroup->hasErrors() ) {
                $result = [
                    'status'=>self::ERROR,
                    'message'=>Yii::t('backend/layout','Error saving element {name}',['name'=>$oInformationGroup->name]),
                    'errors' => $oInformationGroup->getErrors()
                ];
            }

            if( Yii::$app->request->isAjax ) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $result;
            }

        }

        $view = 'information-group-add';
        if( !$oInformationGroup->isNewRecord ) {
            $view = 'information-group-edit';
        }
        return $this->render(
            $view,
            [
                'model' => $oInformationGroup,
                'oInformation' => $oInformationGroup->information,
                'result' => $result
            ]
        );
    }

    /**
     * @param int $information_group_id
     * @return string
     */
    public function actionLoadPropertiesForm( $information_group_id )
    {
        /* @var $oInformationGroup InformationGroupExt */
        $oInformationGroup = InformationGroupExt::getByTid($information_group_id);

        $arProperties = $oInformationGroup->getInformationProperties([],[1])->all();

        $view = '@module_views/information-item/information-item-properties-form';
        $context = [
            'oInformation' => $oInformationGroup->information,
            'oInformationGroup' => $oInformationGroup,
            'oInformationItem' => null,
            'arProperties' => $arProperties
        ];
        if( Yii::$app->request->isAjax ) {
            return $this->renderAjax(
                $view,
                $context
            );
        }

        return $this->render(
            $view,
            $context
        );
    }
}