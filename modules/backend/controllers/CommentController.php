<?php

namespace app\modules\backend\controllers;


use app\components\BackendController;

use Yii;

class CommentController extends BackendController
{
    protected $_defaultRoute = ['/backend/comment/index'];
    protected $_modelClass = 'app\models\ext\CommentExt';

    public function actions()
    {
        return array_merge(
            parent::actions(),
            [
                'index' => [
                    'class' => 'app\components\actions\ListAction',
                    'modelClass' => $this->_modelClass,
                    'view' => 'comment-list',
                ],
                'edit' => [
                    'class'=>'app\components\actions\EditAction',
                    'modelClass'=>$this->_modelClass,
                    'view' => 'comment-edit',
                ],
                'add' => [
                    'class'=>'app\components\actions\AddAction',
                    'modelClass'=>$this->_modelClass,
                    'view' => 'comment-add',
                ],
                'delete' => [
                    'class'=>'app\components\actions\DeleteAction',
                    'modelClass'=>$this->_modelClass,
                    'modelMethod' => 'delete',
                    'titleAttribute' => 'name',
                    'redirect'=>$this->_defaultRoute,
                ],
                'change-active'=>[
                    'class'=>'app\components\actions\SwitchAction',
                    'modelClass'=>$this->_modelClass,
                    'modelMethod' => 'switchBoolean',
                    'modelAttribute' => 'active',
                    'titleAttribute' => 'name',
                    'redirect'=>$this->_defaultRoute,
                ],
                'save' => [
                    'class'=>'app\components\actions\SaveAction',
                    'modelClass'=>$this->_modelClass,
                    'modelMethod' => 'save',
                    'view' => 'comment-edit',
                    'viewUpdate' => 'comment-add',
                    'titleAttribute' => 'name',
                    'fileAttribute' => 'author_photo',
                    'modelUploadMethod' => 'uploadImage',
                    'redirect'=>$this->_defaultRoute,
                ]
            ]
        );
    }

}