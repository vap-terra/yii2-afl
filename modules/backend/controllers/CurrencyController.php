<?php

namespace app\modules\backend\controllers;


use app\components\BackendController;

use Yii;

class CurrencyController extends BackendController
{
    protected $_defaultRoute = ['/backend/currency/index'];
    protected $_modelClass = 'app\models\ext\CurrencyExt';

    public function actions()
    {
        return array_merge(
            parent::actions(),
            [
                'index' => [
                    'class' => 'app\components\actions\ListAction',
                    'modelClass' => $this->_modelClass,
                    'view' => 'currency-list',
                ],
                'edit' => [
                    'class'=>'app\components\actions\EditAction',
                    'modelClass'=>$this->_modelClass,
                    'view' => 'currency-edit',
                ],
                'add' => [
                    'class'=>'app\components\actions\AddAction',
                    'modelClass'=>$this->_modelClass,
                    'view' => 'currency-add',
                ],
                'delete' => [
                    'class'=>'app\components\actions\DeleteAction',
                    'modelClass'=>$this->_modelClass,
                    'modelMethod' => 'delete',
                    'titleAttribute' => 'name',
                    'redirect'=>$this->_defaultRoute,
                ],
                'change-active'=>[
                    'class'=>'app\components\actions\SwitchAction',
                    'modelClass'=>$this->_modelClass,
                    'modelMethod' => 'switchBoolean',
                    'modelAttribute' => 'active',
                    'titleAttribute' => 'name',
                    'redirect'=>$this->_defaultRoute,
                ],
                'save' => [
                    'class'=>'app\components\actions\SaveAction',
                    'modelClass'=>$this->_modelClass,
                    'modelMethod' => 'save',
                    'view' => 'currency-edit',
                    'viewUpdate' => 'currency-add',
                    'titleAttribute' => 'name',
                    'fileAttribute' => '',
                    'modelUploadMethod' => '',
                    'redirect'=>$this->_defaultRoute,
                ]
            ]
        );
    }

}