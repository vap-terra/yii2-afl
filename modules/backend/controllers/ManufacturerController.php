<?php

namespace app\modules\backend\controllers;


use app\components\BackendController;
use Yii;

class ManufacturerController extends BackendController
{
    protected $_defaultRoute = ['/backend/manufacturer/index'];
    protected $_modelClass = 'app\models\ext\ManufacturerExt';

    public function actions()
    {
        return array_merge(
            parent::actions(),
            [
                'index' => [
                    'class' => 'app\components\actions\ListAction',
                    'modelClass' => $this->_modelClass,
                    'view' => 'manufacturer-list',
                ],
                'edit' => [
                    'class'=>'app\components\actions\EditAction',
                    'modelClass'=>$this->_modelClass,
                    'view' => 'manufacturer-edit',
                ],
                'add' => [
                    'class'=>'app\components\actions\AddAction',
                    'modelClass'=>$this->_modelClass,
                    'view' => 'manufacturer-add',
                ],
                'delete' => [
                    'class'=>'app\components\actions\DeleteAction',
                    'modelClass'=>$this->_modelClass,
                    'modelMethod' => 'delete',
                    'titleAttribute' => 'name',
                    'redirect'=>$this->_defaultRoute,
                ],
                'save' => [
                    'class'=>'app\components\actions\SaveAction',
                    'modelClass'=>$this->_modelClass,
                    'modelMethod' => 'save',
                    'view' => 'manufacturer-edit',
                    'viewUpdate' => 'manufacturer-add',
                    'titleAttribute' => 'name',
                    'fileAttribute' => 'image',
                    'modelUploadMethod' => 'uploadImage',
                    'redirect'=>$this->_defaultRoute,
                ]
            ]
        );
    }
}