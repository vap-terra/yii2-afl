<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 28.02.16
 * Time: 13:20
 */

namespace app\modules\backend\controllers;


use app\components\BackendController;
use app\components\FileStorageManager;
use app\models\BaseActiveRecord;
use app\models\ext\InformationExt;
use app\models\ext\InformationItemExt;
use app\models\ext\InformationPropertyExt;
use app\models\ext\InformationPropertyValueFileExt;
use app\models\ext\InformationPropertyValueIntegerExt;
use app\models\ext\InformationPropertyValueStringExt;
use app\models\ext\InformationPropertyValueTextExt;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

class InformationItemController extends BackendController
{
    public function actionEdit( $id )
    {
        /* @var $oInformationItem InformationItemExt */
        $oInformationItem = InformationItemExt::getByTid( $id );
        return $this->render(
            'information-item-edit',
            [
                'model' => $oInformationItem,
                'oInformation' => $oInformationItem->information,
                'result' => []
            ]
        );
    }

    /**
     * @param int $information_id
     * @return string
     */
    public function actionAdd( $information_id ) {
        /* @var $oInformationItem InformationItemExt */
        $oInformationItem = new InformationItemExt();
        $oInformation = InformationExt::getById($information_id);
        return $this->render(
            'information-item-add',
            [
                'model' => $oInformationItem,
                'oInformation' => $oInformation,
                'result' => []
            ]
        );
    }

    public function actionDelete( $id )
    {
        /* @var $oInformationItem InformationItemExt */
        $oInformationItem = InformationItemExt::getByTid( $id );
        if( $oInformationItem->delete() ) {
            Yii::$app->getSession()->setFlash('success', Yii::t('backend/layout','Item {name} deleted successfully',['name'=>$oInformationItem->name]));
        } else {
            Yii::$app->getSession()->setFlash('error', Yii::t('backend/layout','Failed to remove an item {name}',[ 'name' => $oInformationItem->name]));
        }
        $this->redirect( ['/backend/information/index','id'=>$oInformationItem->information_id] );
    }

    /**
     * @param int $id
     * @return array|string
     * @throws NotFoundHttpException
     */
    public function actionSave( $id = 0 )
    {
        /* @var $oInformationItem InformationItemExt */
        if( $id ) {
            $oInformationItem = InformationItemExt::getByTid($id);
        } else {
            $oInformationItem = new InformationItemExt();
        }
        /* @var $result array */
        $result = [];
        $arValues = Yii::$app->request->post();
        if( $oInformationItem->load( $arValues ) ) {

            if( Yii::$app->request->isAjax && Yii::$app->request->get('validate','') == 'true' ) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate( $oInformationItem );
            }

            $validErrors = ActiveForm::validate($oInformationItem);

            if( !empty( $validErrors ) ) {
                $result = [
                    'status' => self::ERROR,
                    'message' => Yii::t('backend/layout','Please fill in all required fields'),
                    'errors' => $validErrors
                ];
            } else if( $oInformationItem->save() ) {
                $result = [
                    'status'=>self::SUCCESS,
                    'message'=>Yii::t('backend/layout','Element {name} successfully saved!',['name'=>$oInformationItem->name])
                ];

                $file = UploadedFile::getInstance($oInformationItem, 'image');
                if ($file) {
                    if ($file->error == 0) {
                        if (!$oInformationItem->uploadImage($file, true, true)) {
                            $result['errors'] = [
                                BaseActiveRecord::getErrorUploadMessage(8, $file->name)
                            ];
                        }
                    } else if ($file->error != 4) {
                        $result['errors'] = [
                            BaseActiveRecord::getErrorUploadMessage($file->error, $file->name)
                        ];
                    }
                }
                

                $_files = ArrayHelper::getValue($_FILES,'property');
                $files = [];
                if( !empty( $_files ) ) {
                    foreach( $_files as $key=>$items ) {
                        foreach ( $items as $propertyId => $values ) {
                            if (empty($files[$propertyId])) {
                                $files[$propertyId] = [];
                            }
                            foreach( $values as $valueId => $value ) {
                                if (empty($files[$propertyId][$valueId])) {
                                    $files[$propertyId][$valueId] = [];
                                }
                                $files[$propertyId][$valueId][$key] = $value;
                            }
                        }
                    }
                }
                //var_dump($files);

                $properties = Yii::$app->request->post( 'property' );
                //var_dump($properties);
                /* @var $oProperty InformationPropertyExt */
                /* @var $arProperties InformationPropertyExt[] */
                $arProperties = $oInformationItem->getInformationProperties([],[])->all();
                $itemPath = $oInformationItem->getPath();

                foreach( $arProperties as $oProperty ) {
                    //var_dump($oProperty->id, $oProperty->formName(), $oProperty->type);

                    $arPropertyValues = $oInformationItem->getInformationPropertyValues($oProperty->id);
                    if( $oProperty->type == InformationPropertyExt::TYPE_FILE  ) {
                        $propValues = ArrayHelper::getValue( $files, $oProperty->id );
                    } else {
                        $propValues = ArrayHelper::getValue( $properties, $oProperty->id );
                    }
                    foreach( $arPropertyValues as $oPropertyValue ) {
                        switch( $oProperty->type ) {
                            case InformationPropertyExt::TYPE_FILE:
                                /* @var $oPropertyValue InformationPropertyValueFileExt */
                                if( isset( $propValues[$oPropertyValue->id] ) ) {
                                    $file = $propValues[$oPropertyValue->id];
                                    if ($file['error'] == 0 && is_uploaded_file($file['tmp_name'])) {
                                        $oPropertyValue->file = $oPropertyValue->generateFileName(FileStorageManager::getFileExtension( $file['name'] ));
                                        if (!move_uploaded_file($file['tmp_name'],$itemPath.$oPropertyValue->file)) {
                                            $result['errors'] = [
                                                BaseActiveRecord::getErrorUploadMessage(8, $file['name'])
                                            ];
                                        } else {
                                            $oPropertyValue->save();
                                        }
                                    } else if ($file['error'] != 4) {
                                        $result['errors'] = [
                                            BaseActiveRecord::getErrorUploadMessage($file->error, $file['name'])
                                        ];
                                    }
                                    unset($propValues[$oPropertyValue->id]);
                                } else {
                                    //$oPropertyValue->delete();
                                }
                                break;
                            case InformationPropertyExt::TYPE_BOOLEAN:
                                /* @var $oPropertyValue InformationPropertyValueIntegerExt */
                                if( isset( $propValues[$oPropertyValue->id] ) ) {
                                    $oPropertyValue->value = 1;
                                    $oPropertyValue->save();
                                    unset($propValues[$oPropertyValue->id]);
                                } else {
                                    $oPropertyValue->value = 0;
                                }
                                break;
                            case InformationPropertyExt::TYPE_INTEGER:
                                /* @var $oPropertyValue InformationPropertyValueIntegerExt */
                                if( isset( $propValues[$oPropertyValue->id] ) ) {
                                    $oPropertyValue->value = $propValues[$oPropertyValue->id];
                                    $oPropertyValue->save();
                                    unset($propValues[$oPropertyValue->id]);
                                } else {
                                    //$oPropertyValue->delete();
                                }
                                break;
                            case InformationPropertyExt::TYPE_STRING:
                                /* @var $oPropertyValue InformationPropertyValueStringExt */
                                if( isset( $propValues[$oPropertyValue->id] ) ) {
                                    $oPropertyValue->value = $propValues[$oPropertyValue->id];
                                    $oPropertyValue->save();
                                    unset($propValues[$oPropertyValue->id]);
                                } else {
                                    //$oPropertyValue->delete();
                                }
                                break;
                            case InformationPropertyExt::TYPE_TEXT:
                                /* @var $oPropertyValue InformationPropertyValueTextExt */
                                if( isset( $propValues[$oPropertyValue->id] ) ) {
                                    $oPropertyValue->value = $propValues[$oPropertyValue->id];
                                    $oPropertyValue->save();
                                    unset($propValues[$oPropertyValue->id]);
                                } else {
                                    //$oPropertyValue->delete();
                                }
                                break;
                            case InformationPropertyExt::TYPE_LIST_VALUE:
                                /* @var $oPropertyValue InformationPropertyValueIntegerExt */
                                if( isset( $propValues[$oPropertyValue->id] ) ) {
                                    $oPropertyValue->value = $propValues[$oPropertyValue->id];
                                    $oPropertyValue->save();
                                    unset($propValues[$oPropertyValue->id]);
                                } else {
                                    //$oPropertyValue->delete();
                                }
                                break;
                        }
                    }

                    /*echo '<pre>';
                    print_r( $propValues );
                    echo '</pre>';*/
                    /*$oPropertyValue = new InformationPropertyValueFileExt();
                    $oPropertyValue->file = '';
                    $oPropertyValue->information_item_id = $oInformationItem->id;
                    $oPropertyValue->information_property_id = 1;
                    if (!$oPropertyValue->save()) {
                        var_dump($oPropertyValue->getErrors());
                    }*/
                    if( !empty( $propValues ) ) {
                        foreach ($propValues as $num => $value) {
                            switch ($oProperty->type) {
                                case InformationPropertyExt::TYPE_FILE:
                                    /* @var $oPropertyValue InformationPropertyValueFileExt */
                                    $file = $value;
                                    if ($file['error'] == 0 && is_uploaded_file($file['tmp_name'])) {
                                        $oPropertyValue = new InformationPropertyValueFileExt();
                                        $oPropertyValue->file = '';
                                        $oPropertyValue->information_item_id = $oInformationItem->id;
                                        $oPropertyValue->information_property_id = $oProperty->id;
                                        $oPropertyValue->save();
                                        $oPropertyValue->file = $oPropertyValue->generateFileName(FileStorageManager::getFileExtension($file['name']));
                                        if (!move_uploaded_file($file['tmp_name'], $itemPath . $oPropertyValue->file)) {
                                            $oPropertyValue->delete();
                                            $result['errors'] = [
                                                BaseActiveRecord::getErrorUploadMessage(8, $file['name'])
                                            ];
                                        } else {
                                            $oPropertyValue->save();
                                        }
                                    } else if ($file['error'] != 4) {
                                        $result['errors'] = [
                                            BaseActiveRecord::getErrorUploadMessage($file->error, $file['name'])
                                        ];
                                    }
                                    //unset($propValues[$num]);
                                    break;
                                case InformationPropertyExt::TYPE_BOOLEAN:

                                    /* @var $oPropertyValue InformationPropertyValueIntegerExt */
                                    $oPropertyValue = new InformationPropertyValueIntegerExt();
                                    $oPropertyValue->information_item_id = $oInformationItem->id;
                                    $oPropertyValue->information_property_id = $oProperty->id;
                                    $oPropertyValue->value = 1;
                                    $oPropertyValue->save();
                                    //unset($propValues[$num]);
                                    break;
                                case InformationPropertyExt::TYPE_INTEGER:
                                    if( $value ) {
                                        /* @var $oPropertyValue InformationPropertyValueIntegerExt */
                                        $oPropertyValue = new InformationPropertyValueIntegerExt();
                                        $oPropertyValue->information_item_id = $oInformationItem->id;
                                        $oPropertyValue->information_property_id = $oProperty->id;
                                        $oPropertyValue->value = $value;
                                        $oPropertyValue->save();
                                        //unset($propValues[$num]);
                                    }
                                    break;
                                case InformationPropertyExt::TYPE_STRING:
                                    if( $value ) {
                                        /* @var $oPropertyValue InformationPropertyValueStringExt */
                                        $oPropertyValue = new InformationPropertyValueStringExt();
                                        $oPropertyValue->information_item_id = $oInformationItem->id;
                                        $oPropertyValue->information_property_id = $oProperty->id;
                                        $oPropertyValue->value = $value;
                                        $oPropertyValue->save();
                                        //unset($propValues[$num]);
                                    }
                                    break;
                                case InformationPropertyExt::TYPE_TEXT:
                                    if( $value ) {
                                        /* @var $oPropertyValue InformationPropertyValueTextExt */
                                        $oPropertyValue = new InformationPropertyValueTextExt();
                                        $oPropertyValue->information_item_id = $oInformationItem->id;
                                        $oPropertyValue->information_property_id = $oProperty->id;
                                        $oPropertyValue->value = $value;
                                        $oPropertyValue->save();
                                        //unset($propValues[$num]);
                                    }
                                    break;
                                case InformationPropertyExt::TYPE_LIST_VALUE:
                                    if( $value ) {
                                        /* @var $oPropertyValue InformationPropertyValueIntegerExt */
                                        $oPropertyValue = new InformationPropertyValueIntegerExt();
                                        $oPropertyValue->information_item_id = $oInformationItem->id;
                                        $oPropertyValue->information_property_id = $oProperty->id;
                                        $oPropertyValue->value = $value;
                                        $oPropertyValue->save();
                                        //unset($propValues[$num]);
                                    }
                                    break;
                            }
                        }
                    }
                }
                
                
                
                
                
                
                
                
                
                
                
                if( !Yii::$app->request->isAjax ) {
                    $this->setFlashSuccess( Yii::t('backend/layout','Element {name} successfully saved!',['name'=>$oInformationItem->name] ) );
                    //return $this->refresh();
                    $this->redirect( ['/backend/information/index'] );
                    Yii::$app->end();
                }
            }

            if( $oInformationItem->hasErrors() ) {
                $result = [
                    'status'=>self::ERROR,
                    'message'=>Yii::t('backend/layout','Error saving element {name}',['name'=>$oInformationItem->name]),
                    'errors' => $oInformationItem->getErrors()
                ];
            }

            if( Yii::$app->request->isAjax ) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $result;
            }

        }

        $view = 'information-item-add';
        if( !$oInformationItem->isNewRecord ) {
            $view = 'information-item-edit';
        }
        return $this->render(
            $view,
            [
                'model' => $oInformationItem,
                'oInformation' => $oInformationItem->information,
                'result' => $result
            ]
        );
    }
}