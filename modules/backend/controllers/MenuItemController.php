<?php

namespace app\modules\backend\controllers;

use app\behaviors\Transliteration;
use app\components\BackendController;
use app\models\BaseActiveRecord;
use app\models\ext\BlockExt;
use app\models\ext\ConstantExt;
use app\models\ext\ContentExt;
use app\models\ext\MenuExt;
use app\models\ext\MenuItemContentExt;
use app\models\ext\MenuItemExt;
use app\models\ext\MenuItemRelationExt;
use dosamigos\transliterator\TransliteratorHelper;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\helpers\Inflector;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;

class MenuItemController extends BackendController
{
    public function actionIndex( $menu_id = 0)
    {
        /* @var $oMenu MenuExt */
        /* @var $oMenuItem MenuItemExt */

        $qMenu = MenuExt::find();
        $countQuery = clone $qMenu;
        $paginationMenuList = new Pagination(
            [
                //'route' => $route,
                'totalCount' => $countQuery->count(),
                'pageSize' => 10,
                'pageParam' => 'page-menu',
            ]
        );
        $dataProviderMenuList = new ActiveDataProvider(
            [
                'query'=>$qMenu,
                'pagination'=>$paginationMenuList
            ]
        );

        $oMenu = null;
        if( $menu_id ){
            $oMenu = MenuExt::getByTid($menu_id);
        }
        $oMenuItem = new MenuItemExt();
        if( !empty( $oMenu ) ) {
            $oMenuItem->menu_id = $oMenu->id;
        }
        $arMenuItemContentBlocks = null;


        $oMenuItemRelation = new MenuItemRelationExt();
        $oMenuItemRelation->menu_item_id = $oMenuItem->id;
        $oMenuItemRelation->type = MenuItemRelationExt::TYPE_CONTENT;
        $oMenuItemRelation->main = 1;
        $oMenuItemRelation->active = 1;
        $oMenuItemRelation->item_id = 0;
        $arMenuItemRelations = [$oMenuItemRelation];

        return $this->render(
            'index',
            [
                'dataProviderMenuList' => $dataProviderMenuList,
                'oMenu' => $oMenu,
                'oMenuItem' => $oMenuItem,
                'arMenuItemRelations' => $arMenuItemRelations,
                //'arMenuItemContentBlocks'=>$arMenuItemContentBlocks,
            ]
        );
    }

    public function actionEdit( $id, $menu_id = 0 )
    {
        /* @var $oMenu MenuExt */
        /* @var $oMenuItem MenuItemExt */
        $qMenu = MenuExt::find();
        $countQuery = clone $qMenu;
        $paginationMenuList = new Pagination(
            [
                //'route' => $route,
                'totalCount' => $countQuery->count(),
                'pageSize' => 10,
                'pageParam' => 'page-menu',
            ]
        );
        $dataProviderMenuList = new ActiveDataProvider(
            [
                'query'=>$qMenu,
                'pagination'=>$paginationMenuList
            ]
        );

        $oMenu = null;
        $oMenuItem = new MenuItemExt();

        if( $id ) {
            $oMenuItem = MenuItemExt::getByTid($id);
            $oMenu = $oMenuItem->menu;
            $arMenuItemRelations = $oMenuItem->getMenuItemRelations()->orderBy(['position'=>SORT_ASC])->all();
        } else if( $menu_id ){
            $oMenu = MenuExt::getByTid($menu_id);
        }

        if (empty($arMenuItemRelations)) {
            $oMenuItemRelation = new MenuItemRelationExt();
            $oMenuItemRelation->menu_item_id = $oMenuItem->id;
            $oMenuItemRelation->type = MenuItemRelationExt::TYPE_CONTENT;
            $oMenuItemRelation->item_id = 0;
            $oMenuItemRelation->main = 1;
            $oMenuItemRelation->active = 1;
            $arMenuItemRelations = [$oMenuItemRelation];
        }

        return $this->render(
            'index',
            [
                'dataProviderMenuList' => $dataProviderMenuList,
                'oMenu' => $oMenu,
                'oMenuItem' => $oMenuItem,
                'arMenuItemRelations' => $arMenuItemRelations,
            ]
        );
    }

    /**
     * @param $id
     * @throws NotFoundHttpException
     */
    public function actionDelete( $id )
    {
        /* @var $oMenuItem MenuItemExt */
        $oMenuItem = MenuItemExt::getByTid( $id );

        if( $oMenuItem->delete() ) {
            $this->setFlashSuccess(Yii::t('backend/layout','Item {name} deleted successfully',['name'=>$oMenuItem->name]));
        } else {
            $this->setFlashError(Yii::t('backend/layout','Failed to remove an item {name}',['name' => $oMenuItem->name]));
        }

        $this->redirect( ['/backend/menu-item/index','menu_id'=>$oMenuItem->menu_id] );
    }

    /**
     * @param $id
     * @param int $toId
     * @param int $beforeId
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionRelocation($id,$toId=0,$beforeId=0)
    {

        $result = [
            'status'=>self::ERROR,
            'message'=>'Error relocation element {name}',
            'errors' => []
        ];

        /* @var $oMenuItem MenuItemExt */
        $oMenuItem = MenuItemExt::getByTid( $id );

        if( $oMenuItem->relocation($toId,$beforeId) ) {
            $result['message']='Item {name} relocation successfully';
            $result['status']=self::SUCCESS;
        } else {
            $result['message']='Failed to relocation an item {name}';
        }

        if( $oMenuItem->hasErrors() ) {
            $result['errors'] = $oMenuItem->getErrors();
        }

        if( Yii::$app->request->isAjax ) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $result['message'] = Yii::t('backend/layout',$result['message'],['name' => $oMenuItem->name]);
            return $result;
        }

        $this->setFlash($result['status'],Yii::t('backend/layout',$result['message'],['name' => $oMenuItem->name]));
        $this->redirect( ['/backend/menu-item/index','menu_id'=>$oMenuItem->menu_id] );
    }

    /**
     * @param int $id
     * @param int $menu_id
     * @return array|string
     * @throws NotFoundHttpException
     */
    public function actionSave( $id = 0, $menu_id = 0 )
    {
        /* @var $oMenuItem MenuItemExt */
        /* @var $oMenu MenuExt */
        $oMenu = null;
        if( $id ) {
            $oMenuItem = MenuItemExt::getByTid($id);
        } else {
            $oMenuItem = new MenuItemExt();
        }

        /* @var $result array */
        $result = [];
        $arValues = Yii::$app->request->post();


        if( $oMenuItem->load( $arValues ) ) {

            $oMenu = $oMenuItem->menu;

            if( $oMenuItem->isNewRecord ) {
                $oMenuItem->lb = 0;
                $oMenuItem->rb = 0;
                $oMenuItem->depth = 0;

                $count = $oMenu->getMenuItems()->count();
                if( $count == 0 ) {
                    $oRootMenuItem = new MenuItemExt();
                    $oRootMenuItem->name = 'Корневой элемент для '.$oMenu->title;
                    $oRootMenuItem->tid = 'root-'.Inflector::slug( TransliteratorHelper::process( $oMenu->title ), '-', true );
                    //$oRootMenuItem->position = MenuItemExt::getMax('position',10,['menu_item_id'=>null]);
                    $oRootMenuItem->active = 1;
                    $oRootMenuItem->menu_id = $oMenu->id;
                    $oRootMenuItem->menu_item_id = null;
                    $oRootMenuItem->lb = 0;
                    $oRootMenuItem->rb = 0;
                    $oRootMenuItem->depth = 0;
                    $oRootMenuItem->makeRoot();
                    $oRootMenuItem->save();
                }

                if( $count == 0 || !$oMenuItem->menu_item_id ) {
                    if( !empty( $oRootMenuItem ) ){
                        $oMenuItemBefore = $oRootMenuItem;
                        $count = 1;
                    } else {
                        $oMenuItemBefore = $oMenu->getMenuItems()->where(['=', 'depth', 1])->orderBy(['lb' => SORT_DESC])->one();
                    }

                    if( $count == 1 ) {
                       /* if( !$oMenuItem->position ) {
                            $oMenuItem->position = 10;
                        }*/
                        $oMenuItem->appendTo($oMenuItemBefore);
                    } else {
                        /*if( !$oMenuItem->position ){
                            $oMenuItem->position = $oMenuItemBefore->position + 10;
                        }*/
                        $oMenuItem->insertAfter($oMenuItemBefore);
                    }

                } else {
                    $oMenuItemParent = $oMenuItem->getMenuItem()->one();
                    /*if( !$oMenuItem->position ) {
                        $oMenuItemBrother = $oMenuItemParent->getMenuItems()->orderBy(['position' => SORT_DESC])->one();
                        if( $oMenuItemBrother ) {
                            $oMenuItem->position = $oMenuItemBrother->position + 10;
                        } else {
                            $oMenuItem->position = 10;
                        }
                    }*/
                    $oMenuItem->appendTo($oMenuItemParent);
                }
            }


            if(
                Yii::$app->request->isAjax
                && Yii::$app->request->get('validate','') == 'true'
            ) {
                return ActiveForm::validate( $oMenuItem );
            }

            $validErrors = ActiveForm::validate($oMenuItem);


            if( !empty( $validErrors ) ) {
                $result = [
                    'status' => self::ERROR,
                    'message' => Yii::t('backend/layout','Please fill in all required fields'),
                    'errors' => $validErrors
                ];
            } else if( $oMenuItem->save() ) {

                $oMenuItem->saveRelations( $arValues );

                $result = [
                    'status'=>self::SUCCESS,
                    'message'=>Yii::t('backend/layout','Element {name} successfully saved!',['name'=>$oMenuItem->name])
                ];

                $file = UploadedFile::getInstance($oMenuItem, 'image');
                if ($file) {
                    if ($file->error == 0) {
                        if (!$oMenuItem->uploadImage($file, true, true)) {
                            $result['errors'] = [
                                BaseActiveRecord::getErrorUploadMessage(8, $file->name)
                            ];
                        }
                    } else if ($file->error != 4) {
                        $result['errors'] = [
                            BaseActiveRecord::getErrorUploadMessage($file->error, $file->name)
                        ];
                    }
                }


                if( !Yii::$app->request->isAjax ) {
                    Yii::$app->getSession()->setFlash('success', Yii::t('backend/layout','Element {name} successfully saved!',['name'=>$oMenuItem->name] ) );
                    //return $this->refresh();
                    $this->redirect( ['/backend/menu-item/index','menu_id'=>$oMenuItem->menu_id] );
                    Yii::$app->end();
                }
            }

            if( $oMenuItem->hasErrors() ) {
                $result = [
                    'status'=>self::ERROR,
                    'message'=>Yii::t('backend/layout','Error saving element {name}',['name'=>$oMenuItem->name]),
                    'errors' => $oMenuItem->getErrors()
                ];
            }

            if( Yii::$app->request->isAjax ) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $result;
            }

        }

        if( !$oMenuItem->isNewRecord ) {
            $arMenuItemRelations = $oMenuItem->getContents()->all();
            $arMenuItemContentBlocks = $oMenuItem->getQueryBlocks('content',SORT_ASC,MenuExt::ALL);
        }

        if( empty( $arMenuItemContentBlocks ) ) {
            $arMenuItemContentBlocks = [ new BlockExt() ];
        }

        if (empty($arMenuItemRelations)) {
            $arMenuItemRelations = [new ContentExt()];
        }

        /* @var $arMenuItems MenuItemExt[] */
        /* @var $oMenuItem MenuItemExt */
        /* @var $arMenuItemRelations ContentExt[] */
        $view = 'menu-item-add';
        if( !$oMenuItem->isNewRecord ) {
            $view = 'menu-item-edit';
        }
        return $this->render(
            $view,
            [
                'oMenuItem' => $oMenuItem,
                'oMenu' => $oMenu,
                'arMenuItemRelations' => $arMenuItemRelations,
                //'arMenuItemContentBlocks'=>$arMenuItemContentBlocks,
                'result' => $result
            ]
        );
    }

    public function actionGenerateTid($text)
    {

        $oMenuItem = new MenuItemExt();
        $oMenuItem->name = $text;
        $translit = new Transliteration(['owner'=>$oMenuItem]);
        $oMenuItem->validate();
        if( Yii::$app->request->isAjax ) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'status' => self::SUCCESS,
                'tid' => $oMenuItem->tid
            ];
        }
        return $oMenuItem->tid;
    }

}