<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 28.02.16
 * Time: 12:10
 */

namespace app\modules\backend\controllers;


use app\components\BackendController;
use app\models\ext\InformationExt;
use app\models\ext\InformationGroupExt;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class InformationController extends BackendController
{
    /**
     * @param int $id
     * @param null $information_group_id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionIndex( $id = 0, $information_group_id=null )
    {

        /* @var $oInformation InformationExt | null*/
        $oInformation = null;
        if( $id ) {
            $oInformation = InformationExt::getById($id);
        } else {
            $oInformation = InformationExt::find()->orderBy('id')->one();
        }

        $oInformationGroup = $information_group_id?InformationGroupExt::getById($information_group_id):null;

        $qInformation = InformationExt::find();

        $countQuery = clone $qInformation;
        $paginationInformationList = new Pagination(['totalCount' => $countQuery->count()]);
        $dataProviderInformationList = new ActiveDataProvider(
            [
                'query'=>$qInformation
            ]
        );

        $dataProviderInformationItemList = null;
        $paginationInformationItemList = null;
        if( $oInformation ) {
            $qInformationItem = $oInformation->getInformationItems();
            if( $oInformationGroup ) {
                $ids = $oInformationGroup->getChildren()->select('id')->all();
                $qInformationItem->andWhere(['information_group_id' => $ids]);
            }
            $countQuery = clone $qInformationItem;
            $paginationInformationItemList = new Pagination(['totalCount' => $countQuery->count()]);
            $dataProviderInformationItemList = new ActiveDataProvider(
                [
                    'query' => $qInformationItem,
                    'pagination' => [
                        'pageSize' => 30,
                        'pageParam' => 'page'
                    ],
                ]
            );

            $qInformationProperties = $oInformation->getInformationProperties();
            $countQuery = clone $qInformationProperties;
            $paginationInformationPropertyList = new Pagination(['totalCount' => $countQuery->count()]);

            $dataProviderInformationPropertyList = new ActiveDataProvider(
                [
                    'query' => $qInformationProperties,
                    'pagination' => [
                        'pageSize' => 30,
                        'pageParam' => 'page-property'
                    ],
                ]
            );
        }

        return $this->render(
            'index',
            [
                'oInformation' => $oInformation,
                'oInformationGroup' => $oInformationGroup,
                'dataProviderInformationList' => $dataProviderInformationList,
                'paginationInformationList' => $paginationInformationList,
                'dataProviderInformationItemList'=>$dataProviderInformationItemList,
                'paginationInformationItemList'=>$paginationInformationItemList,
                'dataProviderInformationPropertyList' => $dataProviderInformationPropertyList,
                'paginationInformationPropertyList' => $paginationInformationPropertyList,
            ]
        );
    }


    /**
     * @return string
     */
    public function actionAdd( )
    {
        $oInformation = new InformationExt();
        return $this->render(
            'information-add',
            [
                'model' => $oInformation
            ]
        );
    }

    /**
     * @param int $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionEdit( $id )
    {
        $oInformation = InformationExt::getById($id);

        return $this->render(
            'information-edit',
            [
                'model' => $oInformation
            ]
        );
    }

    public function actionDelete( $id )
    {
        /* @var $oInformation InformationExt */
        $oInformation = InformationExt::getById( $id );
        if( $oInformation->delete() ) {
            Yii::$app->getSession()->setFlash('success', Yii::t('backend/layout','Item {name} deleted successfully',['name'=>$oInformation->name]));
        } else {
            Yii::$app->getSession()->setFlash('error', Yii::t('backend/layout','Failed to remove an item {name}',['name' => $oInformation->name]));
        }
        $this->redirect(['/backend/information/index']);
    }

    /**
     * @param int $id
     * @return array|string
     * @throws NotFoundHttpException
     */
    public function actionSave( $id = 0 )
    {
        /* @var $oInformation InformationExt */
        if( $id ) {
            $oInformation = InformationExt::getById( $id );
        } else {
            $oInformation = new InformationExt();
        }
        /* @var $result array */
        $result = [];
        $arValues = Yii::$app->request->post();
        if( $oInformation->load( $arValues ) ) {

            if( Yii::$app->request->isAjax && Yii::$app->request->get('validate','') == 'true' ) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate( $oInformation );
            }

            $validErrors = ActiveForm::validate($oInformation);

            if( !empty( $validErrors ) ) {
                $result = [
                    'status' => self::ERROR,
                    'message' => Yii::t('backend/layout','Please fill in all required fields'),
                    'errors' => $validErrors
                ];
            } else if( $oInformation->save() ) {
                $result = [
                    'status'=>self::SUCCESS,
                    'message'=>Yii::t('backend/layout','Information {name} successfully saved!',['name'=>$oInformation->name])
                ];
                if( !Yii::$app->request->isAjax ) {
                    $this->setFlashSuccess( Yii::t('backend/layout','Information {name} successfully saved!',['name'=>$oInformation->name] ) );
                    //return $this->refresh();
                    $this->redirect( ['/backend/information/index'] );
                    Yii::$app->end();
                }
            }

            if( $oInformation->hasErrors() ) {
                $result = [
                    'status'=>self::ERROR,
                    'message'=>Yii::t('backend/layout','Error saving information {name}',['name'=>$oInformation->name]),
                    'errors' => $oInformation->getErrors()
                ];
            }

            if( Yii::$app->request->isAjax ) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $result;
            }

        }

        $view = 'information-add';
        if( !$oInformation->isNewRecord ) {
            $view = 'information-edit';
        }
        return $this->render(
            $view,
            [
                'model' => $oInformation,
                'result' => $result
            ]
        );
    }
}