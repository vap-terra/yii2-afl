<?php

namespace app\modules\backend\controllers;


use app\components\BackendController;
use app\models\ext\FormCompletedExt;
use app\models\ext\FormExt;
use app\models\FormCompleted;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class FormCompletedController extends BackendController
{
    /**
     * @param int $form_id
     * @return string
     */
    public function actionIndex($form_id=0)
    {
        $oForm = null;
        if( $form_id ){
            $oForm = FormExt::getById($form_id);
        }
        /* @var $qFormCompleted ActiveQuery */

        if( !empty( $oForm ) ){
            $qFormCompleted = $oForm->getFormCompleteds();
        } else {
            $qFormCompleted = FormCompletedExt::find();
        }
        $formCompletedDataProvider = new ActiveDataProvider(
            [
                'query' => $qFormCompleted
            ]
        );
        return $this->render(
            'index',
            [
                'form' => $oForm,
                'formCompletedDataProvider' => $formCompletedDataProvider,
            ]
        );
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionEdit( $id )
    {
        /* @var $oFormCompleted FormCompletedExt */
        $oFormCompleted = FormCompletedExt::getByTid( $id );
        return $this->render(
            'form-completed-edit',
            [
                'model' => $oFormCompleted,
                'result' => []
            ]
        );
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView( $id )
    {
        /* @var $oFormCompleted FormCompletedExt */
        $oFormCompleted = FormCompletedExt::getByTid( $id );
        return $this->render(
            'form-completed-view',
            [
                'model' => $oFormCompleted,
                'result' => []
            ]
        );
    }

    /**
     * @return string
     */
    public function actionAdd()
    {
        /* @var $oFormCompleted FormCompletedExt */
        $oFormCompleted = new FormCompletedExt();
        return $this->render(
            'form-completed-add',
            [
                'model' => $oFormCompleted,
                'result' => []
            ]
        );
    }

    /**
     * @param $id
     * @throws NotFoundHttpException
     */
    public function actionDelete( $id )
    {
        /* @var $oFormCompleted FormCompletedExt */
        $oFormCompleted = FormCompletedExt::getByTid( $id );
        if( $oFormCompleted->delete() ) {
            $this->setFlashSuccess(Yii::t('backend/layout','Item {name} deleted successfully',['name'=>$oFormCompleted->id]));
        } else {
            $this->setFlashError(Yii::t('backend/layout','Failed to remove an item {name}',['name' => $oFormCompleted->id]));
        }
        $this->redirect( ['/backend/form-completed/index','form_id'=>$oFormCompleted->form->id] );
    }

    /**
     * @param int $id
     * @return array|string
     * @throws NotFoundHttpException
     */
    public function actionSave( $id = 0 )
    {
        /* @var $oFormCompleted FormCompletedExt */
        if( $id ) {
            $oFormCompleted = FormCompletedExt::getByTid($id);
        } else {
            $oFormCompleted = new FormCompletedExt();
        }
        /* @var $result array */
        $result = [];
        $arValues = Yii::$app->request->post();
        if( $oFormCompleted->load( $arValues ) ) {

            if( Yii::$app->request->isAjax && Yii::$app->request->get('validate','') == 'true' ) {
                return ActiveForm::validate( $oFormCompleted );
            }

            $validErrors = ActiveForm::validate($oFormCompleted);

            if( !empty( $validErrors ) ) {
                $result = [
                    'status' => self::ERROR,
                    'message' => Yii::t('backend/layout','Please fill in all required fields'),
                    'errors' => $validErrors
                ];
            } else if( $oFormCompleted->save() ) {
                $result = [
                    'status'=>self::SUCCESS,
                    'message'=>Yii::t('backend/layout','Element {name} successfully saved!',['name'=>$oFormCompleted->dt_creation])
                ];
                if( !Yii::$app->request->isAjax ) {
                    $this->setFlashSuccess( Yii::t('backend/layout','Element {name} successfully saved!',['name'=>$oFormCompleted->dt_creation] ) );
                    //return $this->refresh();
                    $this->redirect( ['/backend/form-completed/index','form_id'=>$oFormCompleted->form->id] );
                    Yii::$app->end();
                }
            }

            if( $oFormCompleted->hasErrors() ) {
                $result = [
                    'status'=>self::ERROR,
                    'message'=>Yii::t('backend/layout','Error saving element {name}',['name'=>$oFormCompleted->dt_creation]),
                    'errors' => $oFormCompleted->getErrors()
                ];
            }

            if( Yii::$app->request->isAjax ) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $result;
            }

        }

        $view = 'form-completed-add';
        if( !$oFormCompleted->isNewRecord ) {
            $view = 'form-completed-edit';
        }
        return $this->render(
            $view,
            [
                'model' => $oFormCompleted,
                'result' => $result
            ]
        );
    }
}