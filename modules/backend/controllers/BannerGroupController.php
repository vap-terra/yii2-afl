<?php

namespace app\modules\backend\controllers;


use app\components\BackendController;
use app\models\ext\BannerGroupExt;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class BannerGroupController extends BackendController
{
    /**
     * @param int $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionIndex($id=0)
    {
        /* @var $oBannerGroup BannerGroupExt */
        /* @var $dataProviderBannerGroupList \yii\data\ActiveDataProvider */
        /* @var $paginationBannerGroupList \yii\data\Pagination */
        /* @var $dataProviderBannerList \yii\data\ActiveDataProvider */
        /* @var $paginationBannerList \yii\data\Pagination */
        $oBannerGroup = null;
        if( $id ) {
            $oBannerGroup = BannerGroupExt::getById($id);
        } else {
            $oBannerGroup = BannerGroupExt::find()->orderBy('id')->one();
        }

        $qBannerGroup = BannerGroupExt::find();
        $countQuery = clone $qBannerGroup;
        $paginationBannerGroupList = new Pagination(['totalCount' => $countQuery->count()]);
        $dataProviderBannerGroupList = new ActiveDataProvider(
            [
                'query'=>$qBannerGroup
            ]
        );

        $dataProviderBannerList = null;
        $paginationBannerList = null;
        if( $oBannerGroup ) {
            $qBanner = $oBannerGroup->getBanners();
            $countQuery = clone $qBanner;
            $paginationBannerList = new Pagination(['totalCount' => $countQuery->count()]);
            $dataProviderBannerList = new ActiveDataProvider(
                [
                    'query' => $qBanner
                ]
            );
        }

        return $this->render(
            'index',
            [
                'oBannerGroup' => $oBannerGroup,
                'dataProviderBannerGroupList' => $dataProviderBannerGroupList,
                'paginationBannerGroupList' => $paginationBannerGroupList,
                'dataProviderBannerList' => $dataProviderBannerList,
                'paginationBannerList' => $paginationBannerList,
            ]
        );
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionEdit( $id ) {
        /* @var $oBannerGroup BannerGroupExt */
        $oBannerGroup = BannerGroupExt::getById( $id );
        return $this->render(
            'banner-group-edit',
            [
                'model' => $oBannerGroup,
                'result' => []
            ]
        );
    }

    public function actionAdd() {
        /* @var $oBannerGroup BannerGroupExt */
        $oBannerGroup = new BannerGroupExt();
        return $this->render(
            'banner-group-add',
            [
                'model' => $oBannerGroup,
                'result' => []
            ]
        );
    }

    /**
     * @param $id
     * @throws NotFoundHttpException
     */
    public function actionDelete( $id )
    {
        /* @var $oBannerGroup BannerGroupExt */
        $oBannerGroup = BannerGroupExt::getById( $id );
        if( $oBannerGroup->delete() ) {
            Yii::$app->getSession()->setFlash('success', Yii::t('backend/layout','Item {name} deleted successfully',['name'=>$oBannerGroup->name]));
        } else {
            Yii::$app->getSession()->setFlash('error', Yii::t('backend/layout','Failed to remove an item {name}',['name' => $oBannerGroup->name]));
        }
        $this->redirect( ['/backend/banner-group/index'] );
    }

    /**
     * @param int $id
     * @return array|string
     * @throws NotFoundHttpException
     */
    public function actionSave( $id = 0 )
    {
        /* @var $oBannerGroup BannerGroupExt */
        if( $id ) {
            $oBannerGroup = BannerGroupExt::getById($id);
        } else {
            $oBannerGroup = new BannerGroupExt();
        }
        /* @var $result array */
        $result = [];
        $arValues = Yii::$app->request->post();
        if( $oBannerGroup->load( $arValues ) ) {

            if( Yii::$app->request->isAjax && Yii::$app->request->get('validate','') == 'true' ) {
                return ActiveForm::validate( $oBannerGroup );
            }

            $validErrors = ActiveForm::validate($oBannerGroup);

            if( !empty( $validErrors ) ) {
                $result = [
                    'status' => self::ERROR,
                    'message' => Yii::t('backend/layout','Please fill in all required fields'),
                    'errors' => $validErrors
                ];
            } else if( $oBannerGroup->save() ) {
                $result = [
                    'status'=>self::SUCCESS,
                    'message'=>Yii::t('backend/layout','Element {name} successfully saved!',['name'=>$oBannerGroup->name])
                ];
                if( !Yii::$app->request->isAjax ) {
                    $this->setFlashSuccess( Yii::t('backend/layout','Element {name} successfully saved!',['name'=>$oBannerGroup->name] ) );
                    //return $this->refresh();
                    $this->redirect( ['/backend/banner-group/index'] );
                    Yii::$app->end();
                }
            }

            if( $oBannerGroup->hasErrors() ) {
                $result = [
                    'status'=>self::ERROR,
                    'message'=>Yii::t('backend/layout','Error saving element {name}',['name'=>$oBannerGroup->name]),
                    'errors' => $oBannerGroup->getErrors()
                ];
            }

            if( Yii::$app->request->isAjax ) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $result;
            }

        }

        $view = 'banner-group-add';
        if( !$oBannerGroup->isNewRecord ) {
            $view = 'banner-group-edit';
        }
        return $this->render(
            $view,
            [
                'model' => $oBannerGroup,
                'result' => $result
            ]
        );
    }
}