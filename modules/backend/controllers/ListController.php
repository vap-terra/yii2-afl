<?php

namespace app\modules\backend\controllers;


use app\components\BackendController;
use app\models\ext\ListValueExt;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class ListController extends BackendController
{
    /**
     * @param int $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionIndex($id=0)
    {
        /* @var $oListValue ListValueExt */
        /* @var $paginationListValueList \yii\data\Pagination */
        /* @var $paginationListValueItemList \yii\data\Pagination */
        $oListValue = null;
        if( $id ) {
            $oListValue = ListValueExt::getById($id);
        } else {
            $oListValue = ListValueExt::find()->orderBy('id')->one();
        }

        $qListValue = ListValueExt::find();
        $countQuery = clone $qListValue;
        $dataProviderListValueList = new ActiveDataProvider(
            [
                'query'=>$qListValue,
                'pagination' => [
                    'pageSize' => 30,
                    'pageParam' => 'page-list',
                    'totalCount' => $countQuery->count()
                ],
            ]
        );

        $dataProviderListValueItemList = null;
        $paginationListValueItemList = null;
        if( $oListValue ) {
            $qListValueItem = $oListValue->getListValueItems();
            $countQuery = clone $qListValueItem;
            $dataProviderListValueItemList = new ActiveDataProvider(
                [
                    'query' => $qListValueItem,
                    'pagination' => [
                        'pageSize' => 30,
                        'pageParam' => 'page',
                        'totalCount' => $countQuery->count()
                    ],
                ]
            );
        }

        return $this->render(
            'index',
            [
                'oListValue' => $oListValue,
                'dataProviderListValueList' => $dataProviderListValueList,
                'dataProviderListValueItemList' => $dataProviderListValueItemList,
            ]
        );
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionEdit( $id ) {
        /* @var $oListValue ListValueExt */
        $oListValue = ListValueExt::getById( $id );
        return $this->render(
            'list-value-edit',
            [
                'model' => $oListValue,
                'result' => []
            ]
        );
    }

    public function actionAdd() {
        /* @var $oListValue ListValueExt */
        $oListValue = new ListValueExt();
        return $this->render(
            'list-value-add',
            [
                'model' => $oListValue,
                'result' => []
            ]
        );
    }

    /**
     * @param $id
     * @throws NotFoundHttpException
     */
    public function actionDelete( $id )
    {
        /* @var $oListValue ListValueExt */
        $oListValue = ListValueExt::getById( $id );
        if( $oListValue->delete() ) {
            Yii::$app->getSession()->setFlash('success', Yii::t('backend/layout','Item {name} deleted successfully',['name'=>$oListValue->name]));
        } else {
            Yii::$app->getSession()->setFlash('error', Yii::t('backend/layout','Failed to remove an item {name}',['name' => $oListValue->name]));
        }
        $this->redirect( ['/backend/list/index'] );
    }

    /**
     * @param int $id
     * @return array|string
     * @throws NotFoundHttpException
     */
    public function actionSave( $id = 0 )
    {
        /* @var $oListValue ListValueExt */
        if( $id ) {
            $oListValue = ListValueExt::getById($id);
        } else {
            $oListValue = new ListValueExt();
        }
        /* @var $result array */
        $result = [];
        $arValues = Yii::$app->request->post();
        if( $oListValue->load( $arValues ) ) {

            if( Yii::$app->request->isAjax && Yii::$app->request->get('validate','') == 'true' ) {
                return ActiveForm::validate( $oListValue );
            }

            $validErrors = ActiveForm::validate($oListValue);

            if( !empty( $validErrors ) ) {
                $result = [
                    'status' => self::ERROR,
                    'message' => Yii::t('backend/layout','Please fill in all required fields'),
                    'errors' => $validErrors
                ];
            } else if( $oListValue->save() ) {
                $result = [
                    'status'=>self::SUCCESS,
                    'message'=>Yii::t('backend/layout','Element {name} successfully saved!',['name'=>$oListValue->name])
                ];
                if( !Yii::$app->request->isAjax ) {
                    $this->setFlashSuccess( Yii::t('backend/layout','Element {name} successfully saved!',['name'=>$oListValue->name] ) );
                    //return $this->refresh();
                    $this->redirect( ['/backend/list/index'] );
                    Yii::$app->end();
                }
            }

            if( $oListValue->hasErrors() ) {
                $result = [
                    'status'=>self::ERROR,
                    'message'=>Yii::t('backend/layout','Error saving element {name}',['name'=>$oListValue->name]),
                    'errors' => $oListValue->getErrors()
                ];
            }

            if( Yii::$app->request->isAjax ) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $result;
            }

        }

        $view = 'list-value-add';
        if( !$oListValue->isNewRecord ) {
            $view = 'list-value-edit';
        }
        return $this->render(
            $view,
            [
                'model' => $oListValue,
                'result' => $result
            ]
        );
    }
}