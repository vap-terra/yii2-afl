<?php

namespace app\modules\backend\controllers;



use app\components\BackendController;
use app\models\BaseActiveRecord;
use app\models\ext\BannerExt;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

class BannerController extends BackendController
{
    protected $_defaultRoute = ['/backend/banner-group/index'];
    protected $_modelClass = 'app\models\ext\BannerExt';

    public function actions()
    {
        return array_merge(
            parent::actions(),
            [
                'index' => [
                    'class' => 'app\components\actions\ListAction',
                    'modelClass' => $this->_modelClass,
                    'view' => 'banner-list',
                ],
                'edit' => [
                    'class'=>'app\components\actions\EditAction',
                    'modelClass'=>$this->_modelClass,
                    'view' => 'banner-edit',
                ],
                'add' => [
                    'class'=>'app\components\actions\AddAction',
                    'modelClass'=>$this->_modelClass,
                    'view' => 'banner-add',
                ],
                'delete' => [
                    'class'=>'app\components\actions\DeleteAction',
                    'modelClass'=>$this->_modelClass,
                    'modelMethod' => 'delete',
                    'titleAttribute' => 'name',
                    'redirect'=>$this->_defaultRoute,
                ],
                'change-active'=>[
                    'class'=>'app\components\actions\SwitchAction',
                    'modelClass'=>$this->_modelClass,
                    'modelMethod' => 'switchBoolean',
                    'modelAttribute' => 'active',
                    'titleAttribute' => 'name',
                    'redirect'=>$this->_defaultRoute,
                ],
                'save' => [
                    'class'=>'app\components\actions\SaveAction',
                    'modelClass'=>$this->_modelClass,
                    'modelMethod' => 'save',
                    'view' => 'banner-edit',
                    'viewUpdate' => 'banner-add',
                    'titleAttribute' => 'name',
                    'fileAttribute' => 'image',
                    'modelUploadMethod' => 'uploadImage',
                    'redirect'=>$this->_defaultRoute,
                ]
            ]
        );
    }

}