<?php
namespace app\modules\backend\controllers;
use app\components\BackendController;

/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 27.06.16
 * Time: 23:16
 */
class DefaultController extends BackendController
{
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionInstruction()
    {
        return $this->render('instruction');
    }
}