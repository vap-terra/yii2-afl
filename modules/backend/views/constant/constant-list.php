<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 12.02.16
 * Time: 13:22
 */
use yii\helpers\Html;
use yii\grid\GridView;

?>

<div class="inner-toolbar clearfix">
    <ul>
        <li>
            <a href="<?php echo Yii::$app->urlManager->createUrl(['/backend/constant/add'])?>">
                <i class="fa fa-plus"></i>
                <span><?php echo Yii::t('backend/layout','Add constant' );?></span>
            </a>
        </li>
    </ul>
</div>

<?php
echo GridView::widget([
    'dataProvider' => $dataProviderList,
    //'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'tid',
        'id',
        'name',
        'value',
        [
            'class' => 'yii\grid\ActionColumn',
            'headerOptions' => ['width' => '80'],
            'template' => '{edit} {delete}',
            'buttons' => [
                'edit' => function ($url, $model, $key) {
                    $link = Html::a('<span class="glyphicon glyphicon-pencil"></span>',$url,[]);
                    return $link;
                },
                'delete' => function ($url, $model, $key) {
                    if( !Yii::$app->user->identity->isRoot && $model->system ) {
                        return '';
                    }
                    $link = Html::a('<span class="glyphicon glyphicon-trash"></span>',$url,[]);
                    return $link;
                },
            ]
        ],
    ]
]);
?>

