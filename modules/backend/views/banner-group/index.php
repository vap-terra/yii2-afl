<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 12.02.16
 * Time: 13:22
 */

use app\models\ext\BannerGroupExt;
use app\widgets\Alert;
use yii\widgets\ListView;

/* @var $this \yii\base\View */
/* @var $oBannerGroup BannerGroupExt */
/* @var $dataProviderBannerGroupList \yii\data\ActiveDataProvider */
/* @var $paginationBannerGroupList \yii\data\Pagination */
/* @var $dataProviderBannerList \yii\data\ActiveDataProvider */
/* @var $paginationBannerList \yii\data\Pagination */

$this->title = Yii::t('backend/layout', 'Banner group').(!empty($oBannerGroup)?' "'.$oBannerGroup->name.'"':'');
$this->params['breadcrumbs'][] = Yii::t('backend/layout', 'Banner group');
if( !empty($oBannerGroup) ) {
    $this->params['breadcrumbs'][] = $oBannerGroup->name;
}
?>

<section class="">
    <div class="row">
        <div class="inner-body mg-main col-md-9">


            <div class="inner-toolbar clearfix">
                <?php /*<ul>
                    <li>
                        <a href="#" id="mgSelectAll"><i class="fa fa-check-square"></i> <span data-all-text="<?php echo Yii::t('backend/layout','Select All'); ?>" data-none-text="<?php echo Yii::t('backend/layout','Select None'); ?>"><?php echo Yii::t('backend/layout','Select All'); ?></span></a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-pencil"></i> <?php echo Yii::t('backend/layout','Edit'); ?></a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-trash-o"></i> <?php echo Yii::t('backend/layout','Delete'); ?></a>
                    </li>
                    <li class="right" data-sort-source data-sort-id="media-gallery">
                        <ul class="nav nav-pills nav-pills-primary">
                            <li>
                                <label><?php echo Yii::t('backend/layout','Filter'); ?>:</label>
                            </li>
                            <li class="active">
                                <a data-option-value="*" href="#all"><?php echo Yii::t('backend/layout','All'); ?></a>
                            </li>
                            <li>
                                <a data-option-value=".document" href="#document"><?php echo Yii::t('backend/layout','Documents');?></a>
                            </li>
                            <li>
                                <a data-option-value=".image" href="#image"><?php echo Yii::t('backend/layout','Images');?></a>
                            </li>
                            <li>
                                <a data-option-value=".video" href="#video"><?php echo Yii::t('backend/layout','Videos');?></a>
                            </li>
                        </ul>
                    </li>
                </ul>*/ ?>
            </div>

            <?= Alert::widget() ?>

            <div class="row mg-files" data-sort-destination data-sort-id="media-gallery">
                <?php
                if( !empty( $dataProviderBannerList ) ) {
                    echo $this->render(
                        '../banner/banner-list',
                        [
                            'dataProviderList' => $dataProviderBannerList,
                            'paginationBannerList' => $paginationBannerList
                        ]
                    );
                }
                ?>
            </div>

        </div>
        <menu id="content-menu" class="inner-menu col-md-3" role="menu">
            <div class="nano">
                <div class="nano-content">

                    <div class="inner-menu-content">

                        <?php if( !empty( $oBannerGroup) ){ ?>
                            <a href="<?php echo Yii::$app->urlManager->createUrl(['/backend/banner/add','banner_group_id'=>$oBannerGroup->id])?>" class="btn btn-block btn-primary btn-md pt-sm pb-sm text-md">
                                <i class="fa fa-plus mr-xs"></i>
                                <?php echo Yii::t('backend/layout','Add banner' );?>
                            </a>
                        <?php } ?>

                        <hr class="separator" />
                        <div class="sidebar-widget m-none">
                            <div class="widget-header clearfix">
                                <h6 class="title pull-left mt-xs"><?php echo Yii::t('backend/layout','Banner groups' );?></h6>
                                <div class="pull-right">
                                    <a href="<?php echo Yii::$app->urlManager->createUrl(['/backend/banner-group/add'])?>" class="btn btn-dark btn-sm btn-widget-act">
                                        <i class="fa fa-plus mr-xs"></i>
                                        <?php echo Yii::t('backend/layout','Add banner group' );?>
                                    </a>
                                </div>
                            </div>
                            <div class="widget-content">
                                <?php
                                echo ListView::widget(
                                    [
                                        'dataProvider' => $dataProviderBannerGroupList,
                                        'itemOptions' => [ 'tag'=>'ul', 'class' => 'mg-folders' ],
                                        'itemView' => 'banner-group-list-item',
                                        'options' => ['class' => ''],
                                    ]
                                );
                                ?>
                            </div>
                        </div>

                        <hr class="separator" />

                    </div>
                </div>
            </div>
        </menu>
    </div>
</section>


