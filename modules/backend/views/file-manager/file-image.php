<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 10.03.16
 * Time: 17:57
 */

/* @var $file array */
/* @var $folder string */
?>

<div class="isotope-item file-item <?php echo $file['group']?> col-sm-3 col-md-2 col-lg-2" data-url="<?php echo $file['url'];?>">
    <div class="thumbnail">
        <div class="thumb-preview">
            <a class="thumb-image mg-view-wrapper" href="<?php echo $file['url'];?>">
                <img src="<?php echo $file['url'];?>" class="img-responsive preview" alt="<?php echo $file['label'];?>">
                <?php
                $icon = 'file_extension_'.$file['ext'].'.png';
                $iconFile = Yii::$app->storage->getIconPath('file_extension_'.$file['ext'].'.png',null,'file-types/v2/big');
                if( !is_file( $iconFile ) ) {
                    $icon = 'unknown.png';
                }
                ?>
                <img src="<?php echo Yii::$app->storage->getIconHref($icon,null,'file-types/v2/big')?>" class="icon" />
            </a>
            <div class="mg-thumb-options">
                <div class="mg-zoom"><i class="fa fa-search"></i></div>
                <div class="mg-toolbar">
                    <div class="mg-option checkbox-custom checkbox-inline">
                        <input type="checkbox" id="file_<?php echo $file['num'];?>" name="file[]" value="<?php echo $file['name'];?>" data-relative-path="<?php echo $file['relativePath'];?>">
                        <label for="file_<?php echo $file['num'];?>"><?php echo Yii::t('backend/layout','Select');?></label>
                    </div>
                    <div class="mg-group pull-right">
                        <a href="#" class="apply-file-action"><?php echo Yii::t('backend/layout','Apply');?></a>
                        <button class="dropdown-toggle mg-toggle" type="button" data-toggle="dropdown">
                            <i class="fa fa-caret-up"></i>
                        </button>
                        <ul class="dropdown-menu mg-menu" role="menu">
                            <li><a href="<?php echo Yii::$app->urlManager->createUrl(['/backend/file-manager/download-file','folder'=>$folder, 'file'=>$file['name']]);?>" class="mg-download-item"><i class="fa fa-download"></i> <?php echo Yii::t('backend/layout','Download');?></a></li>
                            <li><a href="<?php echo Yii::$app->urlManager->createUrl(['/backend/file-manager/delete-file','folder'=>$folder, 'file'=>$file['name']]);?>" class="mg-delete-item"><i class="fa fa-trash-o"></i> <?php echo Yii::t('backend/layout','Delete');?></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <h5 class="mg-title name text-weight-semibold"><?php echo $file['label'];?><small>.<?php echo $file['ext'];?></small></h5>
        <div class="mg-description">
            <?php /*<small class="pull-left text-muted"><?php echo $folder;?></small> */ ?>
            <small class="pull-right date text-muted"><?php echo $file['date_update'];?></small>
        </div>
    </div>
</div>
