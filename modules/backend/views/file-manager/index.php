<?php

/* @var $this yii\web\View */
/* @var $folders array */
/* @var $files array */
/* @var $folder string */

$this->title = $folder . ' - ' . Yii::t('backend/layout','File Manager');

\app\assets\FileManagerAsset::register($this);
?>
<script>
    var ext_img=new Array('<?php echo implode("','", $imgExtensions)?>');
    var allowed_ext=new Array('<?php echo implode("','", $allowedExtensions)?>');
</script>
        <!-- start: page -->
        <section class="content-with-menu content-with-menu-has-toolbar media-gallery" id="file_manager" data-csrf="<?php echo Yii::$app->request->getCsrfToken();?>">
            <div class="content-with-menu-container">
                <div class="inner-menu-toggle">
                    <a href="#" class="inner-menu-expand" data-open="inner-menu">
<?php echo Yii::t('backend/layout','Show Bar');?> <i class="fa fa-chevron-right"></i>
                    </a>
                </div>

                <menu id="content-menu" class="inner-menu" role="menu">
                    <div class="nano">
                        <div class="nano-content">

                            <div class="inner-menu-toggle-inside">
                                <a href="#" class="inner-menu-collapse">
                                    <i class="fa fa-chevron-up visible-xs-inline"></i><i class="fa fa-chevron-left hidden-xs-inline"></i> <?php echo Yii::t('backend/layout','Hide Bar');?>
                                </a>
                                <a href="#" class="inner-menu-expand" data-open="inner-menu">
<?php echo Yii::t('backend/layout','Show Bar');?> <i class="fa fa-chevron-down"></i>
                                </a>
                            </div>

                            <div class="inner-menu-content">

                                <hr class="separator" />

                                <div class="sidebar-widget m-none">
                                    <?php if($canUploadFiles){ ?>
                                        <form action="<?php echo Yii::$app->urlManager->createUrl(['/backend/file-manager/upload','folder'=>$folder]);?>" class="dropzone dz-square" id="dropzone-file-manager">
                                            <input type="hidden" name="_csrf" value="<?php echo Yii::$app->request->getCsrfToken();?>" />
                                        </form>
                                    <?php }?>
                                </div>

                                <hr class="separator" />

                                <div class="sidebar-widget m-none">
                                    <div class="widget-header clearfix">
                                        <h6 class="title pull-left mt-xs"><?php echo Yii::t('backend/layout','Folders');?></h6>
                                        <?php /*<div class="pull-right">
                                            <a href="#" class="btn btn-dark btn-sm btn-widget-act"><?php echo Yii::t('backend/layout','Add Folder');?></a>
                                        </div>*/ ?>
                                    </div>
                                    <div class="widget-content">
                                        <ul class="mg-folders">
                                            <?php if( !empty( $folders ) ){ ?>
                                                <?php foreach( $folders as $item ){ ?>
                                                <li>
                                                    <a href="<?php echo Yii::$app->urlManager->createUrl(['/backend/file-manager/index','folder'=>$item['label'],'editor'=>$editor,'type'=>$type,'lang'=>$lang]);?>" class="menu-item<?php if( $item['label'] == $folder ){;?> active<?php }?>"><i class="fa fa-folder"></i> <?php echo $item['label'];?></a>
                                                    <?php /*<div class="item-options">
                                                        <a href="#">
                                                            <i class="fa fa-edit"></i>
                                                        </a>
                                                        <a href="#" class="text-danger">
                                                            <i class="fa fa-times"></i>
                                                        </a>
                                                    </div>*/ ?>
                                                </li>
                                                <?php } ?>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </div>

                                <?php /*<hr class="separator" />

                                <div class="sidebar-widget m-none">
                                    <div class="widget-header">
                                        <h6 class="title">Labels</h6>
                                        <span class="widget-toggle">+</span>
                                    </div>
                                    <div class="widget-content">
                                        <ul class="mg-tags">
                                            <li><a href="#">Design</a></li>
                                            <li><a href="#">Projects</a></li>
                                            <li><a href="#">Photos</a></li>
                                            <li><a href="#">Websites</a></li>
                                            <li><a href="#">Documentation</a></li>
                                            <li><a href="#">Download</a></li>
                                            <li><a href="#">Images</a></li>
                                            <li><a href="#">Vacation</a></li>
                                        </ul>
                                    </div>
                                </div>*/ ?>

                            </div>
                        </div>
                    </div>
                </menu>
                <div class="inner-body mg-main">

                    <div class="inner-toolbar clearfix">
                        <ul>
                            <li>
                                <a href="#" id="mgSelectAll"><i class="fa fa-check-square"></i> <span data-all-text="<?php echo Yii::t('backend/layout','Select All');?>" data-none-text="<?php echo Yii::t('backend/layout','Select None');?>"><?php echo Yii::t('backend/layout','Select All');?></span></a>
                            </li>
                            <?php /*<li>
                                <a href="#"><i class="fa fa-pencil"></i> <?php echo Yii::t('backend/layout','Edit');?></a>
                            </li>*/ ?>
                            <li>
                                <a href="<?php echo Yii::$app->urlManager->createUrl(['/backend/file-manager/delete-file','folder'=>$folder]);?>" id="mgDeleteSelected"><i class="fa fa-trash-o"></i> <?php echo Yii::t('backend/layout','Delete');?></a>
                            </li>
                            <li class="right" data-sort-source data-sort-id="file-manager">
                                <ul class="nav nav-pills nav-pills-primary">
                                    <li>
                                        <label><?php echo Yii::t('backend/layout','Sort by');?>:</label>
                                    </li>
                                    <li class="active">
                                        <a data-option-value="name" href="#name"><?php echo Yii::t('backend/layout','Name');?></a>
                                    </li>
                                    <li>
                                        <a data-option-value="type" href="#type"><?php echo Yii::t('backend/layout','Type');?></a>
                                    </li>
                                    <li>
                                        <a data-option-value="date" href="#date"><?php echo Yii::t('backend/layout','Date');?></a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="row mg-files" data-sort-destination data-sort-id="file-manager" data-editor="<?php echo $editor?>" data-type="<?php echo $type?>" data-folder="<?php echo $folder?>">

                        <?php if( !empty( $files ) ) {?>
                            <?php foreach( $files as $file ){ ?>
                                <?php
                                    echo $this->render(
                                        'file',
                                        [
                                            'file' => $file,
                                            'folder' => $folder
                                        ]
                                    );
                                ?>
                            <?php } ?>
                        <?php } ?>
                        <?php /*
                        <div class="isotope-item col-sm-6 col-md-4 col-lg-3">
                            <div class="thumbnail">
                                <div class="thumb-preview">
                                    <a class="thumb-image" href="assets/images/projects/project-2.jpg">
                                        <img src="assets/images/projects/project-2.jpg" class="img-responsive" alt="Project">
                                    </a>
                                    <div class="mg-thumb-options">
                                        <div class="mg-zoom"><i class="fa fa-search"></i></div>
                                        <div class="mg-toolbar">
                                            <div class="mg-option checkbox-custom checkbox-inline">
                                                <input type="checkbox" id="file_2" value="1">
                                                <label for="file_2">SELECT</label>
                                            </div>
                                            <div class="mg-group pull-right">
                                                <a href="#">EDIT</a>
                                                <button class="dropdown-toggle mg-toggle" type="button" data-toggle="dropdown">
                                                    <i class="fa fa-caret-up"></i>
                                                </button>
                                                <ul class="dropdown-menu mg-menu" role="menu">
                                                    <li><a href="#"><i class="fa fa-download"></i> Download</a></li>
                                                    <li><a href="#"><i class="fa fa-trash-o"></i> Delete</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <h5 class="mg-title text-weight-semibold">Blog<small>.png</small></h5>
                                <div class="mg-description">
                                    <small class="pull-left text-muted">PSDs, Projects</small>
                                    <small class="pull-right text-muted">07/10/2014</small>
                                </div>
                            </div>
                        </div>
                        <div class="isotope-item video col-sm-6 col-md-4 col-lg-3">
                            <div class="thumbnail">
                                <div class="thumb-preview">
                                    <a class="thumb-image" href="assets/images/projects/project-5.jpg">
                                        <img src="assets/images/projects/project-5.jpg" class="img-responsive" alt="Project">
                                    </a>
                                    <div class="mg-thumb-options">
                                        <div class="mg-zoom"><i class="fa fa-search"></i></div>
                                        <div class="mg-toolbar">
                                            <div class="mg-option checkbox-custom checkbox-inline">
                                                <input type="checkbox" id="file_3" value="1">
                                                <label for="file_3">SELECT</label>
                                            </div>
                                            <div class="mg-group pull-right">
                                                <a href="#">EDIT</a>
                                                <button class="dropdown-toggle mg-toggle" type="button" data-toggle="dropdown">
                                                    <i class="fa fa-caret-up"></i>
                                                </button>
                                                <ul class="dropdown-menu mg-menu" role="menu">
                                                    <li><a href="#"><i class="fa fa-download"></i> Download</a></li>
                                                    <li><a href="#"><i class="fa fa-trash-o"></i> Delete</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <h5 class="mg-title text-weight-semibold">Friends<small>.png</small></h5>
                                <div class="mg-description">
                                    <small class="pull-left text-muted">Projects, Vacation</small>
                                    <small class="pull-right text-muted">07/10/2014</small>
                                </div>
                            </div>
                        </div>
                        <div class="isotope-item image col-sm-6 col-md-4 col-lg-3">
                            <div class="thumbnail">
                                <div class="thumb-preview">
                                    <a class="thumb-image" href="assets/images/projects/project-4.jpg">
                                        <img src="assets/images/projects/project-4.jpg" class="img-responsive" alt="Project">
                                    </a>
                                    <div class="mg-thumb-options">
                                        <div class="mg-zoom"><i class="fa fa-search"></i></div>
                                        <div class="mg-toolbar">
                                            <div class="mg-option checkbox-custom checkbox-inline">
                                                <input type="checkbox" id="file_4" value="1">
                                                <label for="file_4">SELECT</label>
                                            </div>
                                            <div class="mg-group pull-right">
                                                <a href="#">EDIT</a>
                                                <button class="dropdown-toggle mg-toggle" type="button" data-toggle="dropdown">
                                                    <i class="fa fa-caret-up"></i>
                                                </button>
                                                <ul class="dropdown-menu mg-menu" role="menu">
                                                    <li><a href="#"><i class="fa fa-download"></i> Download</a></li>
                                                    <li><a href="#"><i class="fa fa-trash-o"></i> Delete</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <h5 class="mg-title text-weight-semibold">Life<small>.png</small></h5>
                                <div class="mg-description">
                                    <small class="pull-left text-muted">Images, Photos</small>
                                    <small class="pull-right text-muted">07/10/2014</small>
                                </div>
                            </div>
                        </div>
                        <div class="isotope-item video col-sm-6 col-md-4 col-lg-3">
                            <div class="thumbnail">
                                <div class="thumb-preview">
                                    <a class="thumb-image" href="assets/images/projects/project-5.jpg">
                                        <img src="assets/images/projects/project-5.jpg" class="img-responsive" alt="Project">
                                    </a>
                                    <div class="mg-thumb-options">
                                        <div class="mg-zoom"><i class="fa fa-search"></i></div>
                                        <div class="mg-toolbar">
                                            <div class="mg-option checkbox-custom checkbox-inline">
                                                <input type="checkbox" id="file_5" value="1">
                                                <label for="file_5">SELECT</label>
                                            </div>
                                            <div class="mg-group pull-right">
                                                <a href="#">EDIT</a>
                                                <button class="dropdown-toggle mg-toggle" type="button" data-toggle="dropdown">
                                                    <i class="fa fa-caret-up"></i>
                                                </button>
                                                <ul class="dropdown-menu mg-menu" role="menu">
                                                    <li><a href="#"><i class="fa fa-download"></i> Download</a></li>
                                                    <li><a href="#"><i class="fa fa-trash-o"></i> Delete</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <h5 class="mg-title text-weight-semibold">Poetry<small>.png</small></h5>
                                <div class="mg-description">
                                    <small class="pull-left text-muted">Websites</small>
                                    <small class="pull-right text-muted">07/10/2014</small>
                                </div>
                            </div>
                        </div>
                        <div class="isotope-item document col-sm-6 col-md-4 col-lg-3">
                            <div class="thumbnail">
                                <div class="thumb-preview">
                                    <a class="thumb-image" href="assets/images/projects/project-6.jpg">
                                        <img src="assets/images/projects/project-6.jpg" class="img-responsive" alt="Project">
                                    </a>
                                    <div class="mg-thumb-options">
                                        <div class="mg-zoom"><i class="fa fa-search"></i></div>
                                        <div class="mg-toolbar">
                                            <div class="mg-option checkbox-custom checkbox-inline">
                                                <input type="checkbox" id="file_6" value="1">
                                                <label for="file_6">SELECT</label>
                                            </div>
                                            <div class="mg-group pull-right">
                                                <a href="#">EDIT</a>
                                                <button class="dropdown-toggle mg-toggle" type="button" data-toggle="dropdown">
                                                    <i class="fa fa-caret-up"></i>
                                                </button>
                                                <ul class="dropdown-menu mg-menu" role="menu">
                                                    <li><a href="#"><i class="fa fa-download"></i> Download</a></li>
                                                    <li><a href="#"><i class="fa fa-trash-o"></i> Delete</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <h5 class="mg-title text-weight-semibold">Fun<small>.png</small></h5>
                                <div class="mg-description">
                                    <small class="pull-left text-muted">Documentation, Projects</small>
                                    <small class="pull-right text-muted">07/10/2014</small>
                                </div>
                            </div>
                        </div>
                        <div class="isotope-item col-sm-6 col-md-4 col-lg-3">
                            <div class="thumbnail">
                                <div class="thumb-preview">
                                    <a class="thumb-image" href="assets/images/projects/project-7.jpg">
                                        <img src="assets/images/projects/project-7.jpg" class="img-responsive" alt="Project">
                                    </a>
                                    <div class="mg-thumb-options">
                                        <div class="mg-zoom"><i class="fa fa-search"></i></div>
                                        <div class="mg-toolbar">
                                            <div class="mg-option checkbox-custom checkbox-inline">
                                                <input type="checkbox" id="file_7" value="1">
                                                <label for="file_7">SELECT</label>
                                            </div>
                                            <div class="mg-group pull-right">
                                                <a href="#">EDIT</a>
                                                <button class="dropdown-toggle mg-toggle" type="button" data-toggle="dropdown">
                                                    <i class="fa fa-caret-up"></i>
                                                </button>
                                                <ul class="dropdown-menu mg-menu" role="menu">
                                                    <li><a href="#"><i class="fa fa-download"></i> Download</a></li>
                                                    <li><a href="#"><i class="fa fa-trash-o"></i> Delete</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <h5 class="mg-title text-weight-semibold">Family<small>.png</small></h5>
                                <div class="mg-description">
                                    <small class="pull-left text-muted">Documentation</small>
                                    <small class="pull-right text-muted">07/10/2014</small>
                                </div>
                            </div>
                        </div>
                        <div class="isotope-item image col-sm-6 col-md-4 col-lg-3">
                            <div class="thumbnail">
                                <div class="thumb-preview">
                                    <a class="thumb-image" href="assets/images/projects/project-1.jpg">
                                        <img src="assets/images/projects/project-1.jpg" class="img-responsive" alt="Project">
                                    </a>
                                    <div class="mg-thumb-options">
                                        <div class="mg-zoom"><i class="fa fa-search"></i></div>
                                        <div class="mg-toolbar">
                                            <div class="mg-option checkbox-custom checkbox-inline">
                                                <input type="checkbox" id="file_8" value="1">
                                                <label for="file_8">SELECT</label>
                                            </div>
                                            <div class="mg-group pull-right">
                                                <a href="#">EDIT</a>
                                                <button class="dropdown-toggle mg-toggle" type="button" data-toggle="dropdown">
                                                    <i class="fa fa-caret-up"></i>
                                                </button>
                                                <ul class="dropdown-menu mg-menu" role="menu">
                                                    <li><a href="#"><i class="fa fa-download"></i> Download</a></li>
                                                    <li><a href="#"><i class="fa fa-trash-o"></i> Delete</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <h5 class="mg-title text-weight-semibold">Hapiness<small>.png</small></h5>
                                <div class="mg-description">
                                    <small class="pull-left text-muted">Websites</small>
                                    <small class="pull-right text-muted">07/10/2014</small>
                                </div>
                            </div>
                        </div>

                        */ ?>
                    </div>
                </div>
            </div>
        </section>
        <!-- end: page -->

