<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 10.03.16
 * Time: 17:57
 */
use app\components\FileStorageManager;

/* @var $file array */
/* @var $folder string */
?>

<?php
$view = 'file-simple';
switch( $file['group'] ) {
    case FileStorageManager::TYPE_IMAGE:
        $view = 'file-image';
        break;
    case FileStorageManager::TYPE_VIDEO:
        $view = 'file-video';
        break;
    case FileStorageManager::TYPE_DOC:
    case FileStorageManager::TYPE_SCRIPT:
    case FileStorageManager::TYPE_ARCHIVE:
    case FileStorageManager::TYPE_AUDIO:
    case FileStorageManager::TYPE_UNTRUSTED:
}
echo $this->render(
    $view,
    [
        'file' => $file,
        'folder' => $folder
    ]
);
?>
