<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 28.02.16
 * Time: 15:47
 */
use app\components\BaseController;
use app\models\ext\InformationExt;
use app\models\ext\InformationGroupExt;

/* @var $this \yii\web\View */
/* @var $model InformationGroupExt */
/* @var $oInformation InformationExt */
/* @var $result array */

$this->title = Yii::t('backend/layout','Add group');
$this->params['breadcrumbs'][] = [
    'url'=>Yii::$app->urlManager->createUrl(['/backend/information/index']),
    'label'=>Yii::t('backend/layout', 'Information')
];
if( !empty($oInformation) ) {
    $this->params['breadcrumbs'][] = [
        'url' => Yii::$app->urlManager->createUrl(['/backend/information/index', 'id' => $oInformation->id]),
        'label' => $oInformation->name
    ];
}
$this->params['breadcrumbs'][] = Yii::t('backend/layout', 'Add');

if( !empty( $result ) ) {
    echo \app\widgets\AlertActionResult::widget(['result'=>$result]);
}

echo $this->render(
    'information-group-form',
    [
        'model' => $model
    ]
);

