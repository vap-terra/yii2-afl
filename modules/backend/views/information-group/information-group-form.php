<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 28.02.16
 * Time: 16:00
 */

use app\models\ext\InformationExt;
use app\models\ext\InformationGroupExt;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $form ActiveForm */
/* @var $model InformationGroupExt */

\app\assets\Select2Asset::register($this);
\app\assets\IOS7SwitchAsset::register($this);
\app\assets\TinymceAsset::register($this);
//\app\assets\DropzoneAsset::register($this);
\app\assets\BootstrapFileuploadAsset::register($this);

$form = ActiveForm::begin(
    [
        'id' => 'information-form',
        'action' => Yii::$app->urlManager->createUrl(['/backend/information-group/save','id'=>$model->id]),
        'requiredCssClass' => 'has-required',
        'options' => [
            'enctype' => 'multipart/form-data',
            'class' => 'form-horizontal form-bordered',
        ],
        'fieldConfig' => [
            'template' => "

                                    {label}\n
                                    <div class=\"col-md-6\">
                    {input}\n
                    {hint}\n
                    {error}\n
                                    </div>

",
            'labelOptions' => [
                'class' => 'col-md-3 control-label'
            ],
        ],
    ]
);
?>

<section class="panel panel-featured panel-featured-primary panel-collapsed">
    <header class="panel-heading panel-heading-transparent">
        <div class="panel-actions">
            <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
            <!-- <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a> -->
        </div>

        <h2 class="panel-title">Мета теги</h2>
        <p class="panel-subtitle">SEO продвижение</p>
    </header>
    <div class="panel-body">

        <?php echo $form->field( $model, 'title'); ?>
        <?php echo $form->field( $model, 'meta_keywords')->textarea(['data-role'=>'tagsinput']); ?>
        <?php echo $form->field( $model, 'meta_description')->textarea(); ?>

        <?php echo $form->field( $model, 'title_item'); ?>
        <?php echo $form->field( $model, 'meta_keywords_item')->textarea(['data-role'=>'tagsinput']); ?>
        <?php echo $form->field( $model, 'meta_description_item')->textarea(); ?>

        <div class="text-center">
            <button class="btn btn-success btn-lg">
                <i class="fa <?php echo ($model->isNewRecord)?' fa-plus':' fa-cog';?>"></i> <?php echo Yii::t('backend/layout',($model->isNewRecord)?'Save':'Update'); ?>
            </button>
        </div>

    </div>
    <!-- <div class="panel-footer"></div> -->
</section>

<section class="panel panel-featured panel-featured-dark">
    <header class="panel-heading panel-heading-transparent">
        <div class="panel-actions">
            <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
            <!-- <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a> -->
        </div>

        <h2 class="panel-title">Основное</h2>
        <!-- <p class="panel-subtitle"></p> -->
    </header>
    <div class="panel-body">

        <?php
        echo $form->field( $model, 'information_id')->dropDownList(
            InformationExt::getArOptionList(),
            [
                'prompt' => ' ... ',
                'onchange'=>'',
                'class'=>'form-control populate',
                //'data-plugin-selectTwo' => '',
            ]
        );
        ?>

        <?php
        echo $form->field( $model, 'information_group_id')->dropDownList(
            InformationGroupExt::getArOptionListGroupByInformation(),
            [
                'prompt' => ' НЕТ ',
                'onchange'=>'',
                'class'=>'form-control populate',
                'data-plugin-selectTwo' => '',
                'groups' => InformationExt::getArOptionListOptgroup(),
            ]
        );
        ?>

        <?php
            echo $form->field($model, 'active', [
                    'template' => "
                {label}\n
                <div class=\"col-md-6\">
                    <div class=\"switch switch-sm switch-success\">
                        {input}\n
                    </div>
                    {hint}\n
                    {error}\n
                </div>",
                    'labelOptions' => [
                        'class' => 'col-md-3 control-label'
                    ],

                ]
            )->checkbox(
                [
                    'data-plugin-ios-switch' => ""

                ],
                false
            );
        ?>

        <?php echo $form->field( $model, 'name');?>

        <?php echo $form->field( $model, 'tid');?>

        <?php
        $imgLink = '';
        if( !empty( $model->image ) && is_file($model->getImagePath()))
        {
            $img = Html::img($model->getImageHref(), ['class'=>'img-responsive img-thumbnail', 'width'=>'100']);
            $imgLink = Html::a($img,$model->getImageHref(),['target'=>'_blank']);
            //$form->field($model,'del_img')->checkBox(['class'=>'span-1']);
        }
        ?>

        <?php
        echo $form->field( $model, 'image',
            [
                'template' => "
                        {label}\n
                        <div class=\"col-md-9\">
                            <div class=\"col-md-9\">
                            <div class=\"fileupload fileupload-new\" data-provides=\"fileupload\">
                                <div class=\"input-append\">

                                    <div class=\"uneditable-input\">
                                        <i class=\"fa fa-file fileupload-exists\"></i>
                                        <span class=\"fileupload-preview\"></span>
                                    </div>
                                    <span class=\"btn btn-default btn-file\">
                                        <span class=\"fileupload-exists\">".Yii::t('backend/layout','Change')."</span>
                                        <span class=\"fileupload-new\">".Yii::t('backend/layout','Select file')."</span>
                                        {input}\n
                                    </span>
                                    <a href=\"#\" class=\"btn btn-default fileupload-exists\" data-dismiss=\"fileupload\">".Yii::t('backend/layout','Remove')."</a>

                                </div>
                            </div>
                            {hint}\n
                            {error}\n
                            </div>
                            <div class=\"text-center col-md-3\">
                            {$imgLink}
                            </div>
                        </div>",
                'labelOptions' => [
                    'class' => 'col-md-3 control-label'
                ]
            ]
        )->fileInput([]);
        ?>

        <?php
        echo $form->field( $model, 'description',
            [
                'template' => "
                {label}\n
                <div class=\"col-md-12\">
                    {input}\n
                    {hint}\n
                    {error}\n
                </div>",
                'labelOptions' => [
                    'class' => 'col-md-12 control-label'
                ],
            ]
        )->textarea(
            [
                'rows' => 6,
                'style'=>'width:100%;height:150px;',
                'class'=>'textarea-editor-tinymce',
                'data-plugin-tinymce'=>'',
                'plugin-options' => '',
            ]
        );
        ?>

        <?php
        echo $form->field( $model, 'content',
            [
                'template' => "
                {label}\n
                <div class=\"col-md-12\">
                    {input}\n
                    {hint}\n
                    {error}\n
                </div>",
                'labelOptions' => [
                    'class' => 'col-md-12 control-label'
                ],
            ]
        )->textarea(
            [
                'rows' => 6,
                'style'=>'width:100%;height:150px;',
                'class'=>'textarea-editor-tinymce',
                'data-plugin-tinymce'=>'',
                'plugin-options' => '',
            ]
        );
        ?>

        <?php echo $form->field( $model, 'position');?>


        <div class="text-center">
            <button class="btn btn-success btn-lg">
                <i class="fa <?php echo ($model->isNewRecord)?' fa-plus':' fa-cog';?>"></i> <?php echo Yii::t('backend/layout',($model->isNewRecord)?'Save':'Update'); ?>
            </button>
        </div>

    </div>
</section>

<?php ActiveForm::end(); ?>
<!-- </div>
</section> -->

