<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 28.02.16
 * Time: 13:22
 */
use app\components\web\View;
use app\models\ext\InformationExt;
use app\models\ext\InformationGroupExt;
use app\models\ext\InformationItemExt;
use app\models\ext\InformationPropertyExt;

/* @var $this View */
/* @var $oInformation InformationExt */
/* @var $oInformationGroup InformationGroupExt */
/* @var $oInformationItem InformationItemExt */
/* @var $arProperties InformationPropertyExt */

foreach( $arProperties as $oProperty ) {
    /* @var $oProperty InformationPropertyExt */
    $arPropertyValues = [];
    if( !empty( $oInformationItem ) ) {
        $arPropertyValues = $oInformationItem->getInformationPropertyValues($oProperty->id);
    }
    ?>
    <div class="form-group">
        <label class=" col-md-3 control-label"><?php echo $oProperty->title;?></label>
        <div class="col-lg-6">
            <?php
            switch( $oProperty->type ) {
                case InformationPropertyExt::TYPE_FILE:
                    echo $this->render(
                        'properties/property-value-files',
                        [
                            'oProperty'=>$oProperty,
                            'arPropertyValues'=>$arPropertyValues
                        ]
                    );
                    break;
                case InformationPropertyExt::TYPE_BOOLEAN:
                    echo $this->render(
                        'properties/property-value-booleans',
                        [
                            'oProperty'=>$oProperty,
                            'arPropertyValues'=>$arPropertyValues
                        ]
                    );
                    break;
                case InformationPropertyExt::TYPE_STRING:
                    echo $this->render(
                        'properties/property-value-strings',
                        [
                            'oProperty'=>$oProperty,
                            'arPropertyValues'=>$arPropertyValues
                        ]
                    );
                    break;
                case InformationPropertyExt::TYPE_INTEGER:
                    echo $this->render(
                        'properties/property-value-integers',
                        [
                            'oProperty'=>$oProperty,
                            'arPropertyValues'=>$arPropertyValues
                        ]
                    );
                    break;
                case InformationPropertyExt::TYPE_TEXT:
                    echo $this->render(
                        'properties/property-value-texts',
                        [
                            'oProperty'=>$oProperty,
                            'arPropertyValues'=>$arPropertyValues
                        ]
                    );
                    break;
                case InformationPropertyExt::TYPE_LIST_VALUE:
                    echo $this->render(
                        'properties/property-value-list-values',
                        [
                            'oProperty'=>$oProperty,
                            'arPropertyValues'=>$arPropertyValues
                        ]
                    );
                    break;
            }
            ?>
        </div>
    </div>
    <?php
}
?>

