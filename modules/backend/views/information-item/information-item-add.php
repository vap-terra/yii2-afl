<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 28.02.16
 * Time: 15:47
 */
use app\components\BaseController;
use app\components\web\View;
use app\models\ext\InformationExt;
use app\models\ext\InformationItemExt;


/* @var $this View */
/* @var $oInformation InformationExt */
/* @var $model InformationItemExt */
/* @var $result array */

$this->title = Yii::t('backend/layout','Add item');
$this->params['breadcrumbs'][] = [
    'url'=>Yii::$app->urlManager->createUrl(['/backend/information/index']),
    'label'=>Yii::t('backend/layout', 'Information')
];
$this->params['breadcrumbs'][] = [
    'url'=>Yii::$app->urlManager->createUrl(['/backend/information/index','id'=>$oInformation->id]),
    'label'=>$oInformation->name
];
$this->params['breadcrumbs'][] = Yii::t('backend/layout', 'Add');

if( !empty( $result ) ) {
    echo \app\widgets\AlertActionResult::widget(['result'=>$result]);
}

echo $this->render(
    'information-item-form',
    [
        'model' => $model
    ]
);

