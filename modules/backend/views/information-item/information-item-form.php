<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 28.02.16
 * Time: 13:22
 */

use app\components\web\View;
use app\models\ext\InformationExt;
use app\models\ext\InformationGroupExt;
use app\models\ext\InformationItemExt;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;


/* @var $this View */
/* @var $form ActiveForm */
/* @var $model InformationItemExt */

\app\assets\Select2Asset::register($this);
\app\assets\IOS7SwitchAsset::register($this);
\app\assets\TinymceAsset::register($this);
//\app\assets\BootstrapDatetimepickerAsset::register($this);
//\app\assets\BootstrapDatepickerAsset::register($this);
//\app\assets\BootstrapTimepickerAsset::register($this);
\app\assets\BootstrapFileuploadAsset::register($this);
//\app\assets\DropzoneAsset::register($this);

$form = ActiveForm::begin(
    [
        'id' => 'information-item-form',
        'action' => Yii::$app->urlManager->createUrl(['/backend/information-item/save','id'=>$model->id]),
        'requiredCssClass' => 'has-required',
        'options' => [
            'enctype' => 'multipart/form-data',
            'class' => 'form-horizontal form-bordered'
        ],
        'fieldConfig' => [
            'template' => "

                                    {label}\n
                                    <div class=\"col-md-6\">
                    {input}\n
                    {hint}\n
                    {error}\n
                                    </div>

",
            'labelOptions' => [
                'class' => 'col-md-3 control-label'
            ],
        ],
    ]
);
?>


<section class="panel panel-featured panel-featured-primary panel-collapsed">
    <header class="panel-heading panel-heading-transparent">
        <div class="panel-actions">
            <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
            <!-- <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a> -->
        </div>

        <h2 class="panel-title">Мета теги</h2>
        <p class="panel-subtitle">SEO продвижение</p>
    </header>
    <div class="panel-body">

        <?php  echo $form->field( $model, 'title'); ?>
        <?php echo $form->field( $model, 'meta_keywords')->textarea(['data-role'=>'tagsinput']); ?>
        <?php echo $form->field( $model, 'meta_description')->textarea();  ?>

        <div class="text-center">
            <button class="btn btn-success btn-lg">
                <i class="fa <?php echo ($model->isNewRecord)?' fa-plus':' fa-cog';?>"></i> <?php echo Yii::t('backend/layout',($model->isNewRecord)?'Save':'Update'); ?>
            </button>
        </div>

    </div>
    <!-- <div class="panel-footer"></div> -->
</section>

<section class="panel panel-featured panel-featured-dark">
    <header class="panel-heading panel-heading-transparent">
        <div class="panel-actions">
            <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
            <!-- <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a> -->
        </div>

        <h2 class="panel-title">Основное</h2>
        <!-- <p class="panel-subtitle"></p> -->
    </header>
    <div class="panel-body">

        <?php echo $form->field( $model, 'information_id')->dropDownList(
            InformationExt::getArOptionList(),
            [
                'prompt' => ' ... ',
                'onchange'=>'',
                'class'=>'form-control populate',
                //'data-plugin-selectTwo' => '',
            ]
        );?>


        <?php
        echo $form->field( $model, 'information_group_id')->dropDownList(
            InformationGroupExt::getArOptionListGroupByInformation(),
            [
                'prompt' => ' НЕТ ',
                'onchange'=>'',
                'class'=>'form-control populate',
                'data-plugin-selectTwo' => '',
                'groups' => InformationExt::getArOptionListOptgroup(),
            ]
        );
        ?>

        <?php
        echo $form->field( $model, 'active',[
                'template' => "
                {label}\n
                <div class=\"col-md-6\">
                    <div class=\"switch switch-sm switch-success\">
                        {input}\n
                    </div>
                    {hint}\n
                    {error}\n
                </div>",
                'labelOptions' => [
                    'class' => 'col-md-3 control-label'
                ],

            ]
        )->checkbox(
            [
                'data-plugin-ios-switch'=>""
            ],
            false
        );
        ?>

        <!-- <div class="form-group">
            <label class="col-md-3 control-label">Default Datepicker</label>

            <div class="col-md-6">
                <div class="input-group input-append date" data-plugin-datetimepicker>
                    <span class="input-group-addon add-on">
                        <i class="fa fa-calendar" data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                    </span>
                    <input data-format="dd.MM.yyyy hh:mm:ss" type="text" class="form-control">
                </div>
            </div>
        </div> -->

        <?php echo $form->field( $model, 'name');?>

        <?php echo $form->field( $model, 'tid');?>

        <?php
        if( !empty( $model->image ) && is_file($model->getImagePath()))
        {
            $img = Html::img($model->getImageHref(), ['class'=>'img-responsive img-thumbnail', 'width'=>'100']);
            echo Html::tag('div',Html::a($img,$model->getImageHref(),['target'=>'_blank']),['class'=>'text-center']);
            echo $form->field($model,'del_img',
                [
                    'template' => "
                    {label}\n
                    <div class=\"col-md-6\">
                        <div class=\"\">
                            {input}\n
                        </div>
                        {hint}\n
                        {error}\n
                    </div>",
                    'labelOptions' => [
                        'class' => 'col-md-3 control-label'
                    ],
                ]
            )->checkBox([]);
        }
        ?>

        <?php
        echo $form->field( $model, 'image',
            [
                'template' => "
                        {label}\n
                        <div class=\"col-md-6\">
                            <div class=\"fileupload fileupload-new\" data-provides=\"fileupload\">
                                <div class=\"input-append\">
                                    <div class=\"uneditable-input\">
                                        <i class=\"fa fa-file fileupload-exists\"></i>
                                        <span class=\"fileupload-preview\"></span>
                                    </div>
                                    <span class=\"btn btn-default btn-file\">
                                        <span class=\"fileupload-exists\">".Yii::t('backend/layout','Change')."</span>
                                        <span class=\"fileupload-new\">".Yii::t('backend/layout','Select file')."</span>
                                        {input}\n
                                    </span>
                                    <a href=\"#\" class=\"btn btn-default fileupload-exists\" data-dismiss=\"fileupload\">".Yii::t('backend/layout','Remove')."</a>

                                </div>
                            </div>
                            {hint}\n
                            {error}\n
                        </div>",
                'labelOptions' => [
                    'class' => 'col-md-3 control-label'
                ]
            ]
        )->fileInput([]);
        ?>

        <?php

        echo $form->field( $model, 'description',
            [
                'template' => "
                {label}\n
                <div class=\"col-md-12\">
                    {input}\n
                    {hint}\n
                    {error}\n
                </div>",
                'labelOptions' => [
                    'class' => 'col-md-12 control-label'
                ],
            ]
        )->textarea(
            [
                'class'=>'textarea-editor-tinymce',
                'data-plugin-tinymce'=>'',
                'plugin-options' => '',
            ]
        );
        ?>

        <?php

        echo $form->field( $model, 'content',
            [
                'template' => "
                {label}\n
                <div class=\"col-md-12\">
                    {input}\n
                    {hint}\n
                    {error}\n
                </div>",
                'labelOptions' => [
                    'class' => 'col-md-12 control-label'
                ],
            ]
        )->textarea(
            [
                'class'=>'textarea-editor-tinymce',
                'data-plugin-tinymce'=>'',
                'plugin-options' => '',
            ]
        );
        ?>

        <div class="text-center">
            <button class="btn btn-success btn-lg">
                <i class="fa <?php echo ($model->isNewRecord)?' fa-plus':' fa-cog';?>"></i> <?php echo Yii::t('backend/layout',($model->isNewRecord)?'Save':'Update'); ?>
            </button>
        </div>

    </div>
</section>

<section class="panel panel-featured panel-featured-primary">
    <header class="panel-heading panel-heading-transparent">
        <div class="panel-actions">
            <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
            <!-- <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a> -->
        </div>

        <h2 class="panel-title">Доп. параметры</h2>
        <p class="panel-subtitle"></p>
    </header>
    <div class="panel-body">

        <div class="properties">

            <?php if( !$model->isNewRecord ){ ?>


                <?php
                $arProperties = $model->getInformationProperties([],[1])->all();
                echo $this->render(
                    'information-item-properties-form',
                    [
                        'oInformation' => $model->information,
                        'oInformationGroup' => $model->informationGroup,
                        'oInformationItem' => $model,
                        'arProperties'=>$arProperties
                    ]
                );
                ?>
            <?php } else {

            } ?>

        </div>

        <div class="text-center">
            <button class="btn btn-success btn-lg">
                <i class="fa <?php echo ($model->isNewRecord)?' fa-plus':' fa-cog';?>"></i> <?php echo Yii::t('backend/layout',($model->isNewRecord)?'Save':'Update'); ?>
            </button>
        </div>

    </div>
    <!-- <div class="panel-footer"></div> -->
</section>


<?php ActiveForm::end(); ?>
<!-- </div>
</section> -->
