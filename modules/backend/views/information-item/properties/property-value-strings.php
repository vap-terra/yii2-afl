<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 09.03.16
 * Time: 7:09
 */

use app\components\web\View;
use app\models\ext\InformationPropertyExt;
use app\models\ext\InformationPropertyValueStringExt;
use yii\helpers\Html;

/* @var $this View */
/* @var $oProperty InformationPropertyExt */
/* @var $arPropertyValues InformationPropertyValueStringExt */
?>
<div class="property" id="property_<?php echo $oProperty->id;?>" data-property-id="<?php echo $oProperty->id;?>" data-type="<?php echo $oProperty->type;?>">
    <div class="row property-values">

<?php
if (!empty($arPropertyValues)) {
    foreach ($arPropertyValues as $oStringValue) {
        /* @var $oStringValue InformationPropertyValueStringExt */
        ?>

            <div class="form-group" data-property-value-id="<?php echo $oStringValue->id;?>">
                <div class="col-md-12">
                    <div class="input-group">
                    <?php echo Html::textInput(
                        'property['.$oStringValue->information_property_id.']['.$oStringValue->id.']',
                        $oStringValue->value,
                        [
                            'class'=>'form-control'
                        ]
                    );?>
                        <span class="input-group-btn<?php if( !$oProperty->multiple ) {?> hidden<?php }?>">
                            <button type="button" data-href="" class="btn input-group-addon delete-btn"><i class="fa fa-minus"></i></button>
                        </span>
                    </div>
                    <p class="help-block help-block-error"></p>
                </div>
            </div>

        <?php
    }
}
?>

<?php

if( $oProperty->multiple || empty($arPropertyValues) ) {
    ?>
    <div class="form-group" data-property-value-id="">
        <div class="col-md-12">
            <div class="input-group">
            <?php echo Html::textInput(
                'property[' . $oProperty->id . '][]',
                '',
                [
                    'class'=>'form-control'
                ]
            ); ?>
                <span class="input-group-btn<?php if( empty($arPropertyValues) ) {?> hidden<?php }?>">
                    <button type="button" data-href="" class="btn input-group-addon delete-btn"><i class="fa fa-minus"></i></button>
                </span>
            </div>
            <p class="help-block help-block-error"></p>
        </div>
    </div>

    <?php
}
?>

    </div>
    <?php
    if( $oProperty->multiple ) {
        ?>
        <div class="text-center">
            <button type="button" data-href="" class="btn btn-sm add-btn"><i class="fa fa-plus"></i></button>
        </div>
        <?php
    }
    ?>
</div>