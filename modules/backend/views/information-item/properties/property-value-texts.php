<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 09.03.16
 * Time: 7:09
 */

use app\components\web\View;
use app\models\ext\InformationPropertyExt;
use app\models\ext\InformationPropertyValueTextExt;
use yii\helpers\Html;

/* @var $this View */
/* @var $oProperty InformationPropertyExt */
/* @var $arPropertyValues InformationPropertyValueTextExt */
?>
<div class="property" id="property_<?php echo $oProperty->id;?>" data-property-id="<?php echo $oProperty->id;?>" data-type="<?php echo $oProperty->type;?>">
    <div class="row property-values">
<?php
if (!empty($arPropertyValues)) {
    foreach ($arPropertyValues as $oTextValue) {
        /* @var $oTextValue InformationPropertyValueTextExt */
        ?>

        <div class="form-group" data-property-value-id="<?php echo $oTextValue->id;?>">
            <div class="col-md-12">
                <div class="input-group">
                <?php echo Html::textarea(
                    'property['.$oTextValue->information_property_id.']['.$oTextValue->id.']',
                    $oTextValue->value,
                    [
                        'class'=>'form-control',
                        'row'=>4,
                    ]
                );?>
                    <span class="input-group-btn<?php if( !$oProperty->multiple ) {?> hidden<?php }?>">
                        <button type="button" data-href="" class="btn input-group-addon delete-btn"><i class="fa fa-minus"></i></button>
                    </span>
                </div>
                <p class="help-block help-block-error"></p>
            </div>
        </div>

        <?php
    }
}
?>

<?php

if( $oProperty->multiple || empty($arPropertyValues) ) {
$oTextValue = new InformationPropertyValueTextExt();
?>
    <div class="form-group" data-property-value-id="">
        <div class="col-md-12">
            <div class="input-group">
            <?php echo Html::textarea(
                'property['.$oProperty->id.'][]',
                $oTextValue->value,
                [
                    'class'=>'form-control',
                    'row'=>4,
                ]
            );?>
                <span class="input-group-btn hidden">
                        <button type="button" data-href="" class="btn input-group-addon delete-btn"><i class="fa fa-minus"></i></button>
                    </span>
            </div>
            <p class="help-block help-block-error"></p>
        </div>
    </div>
<?php
}
?>
    </div>
    <?php
    if( $oProperty->multiple ) {
        ?>
        <div class="text-center">
            <button type="button" data-href="" class="btn btn-sm add-btn"><i class="fa fa-plus"></i></button>
        </div>
        <?php
    }
    ?>
</div>