<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 09.03.16
 * Time: 7:09
 */
use app\components\web\View;
use app\models\ext\InformationPropertyExt;
use app\models\ext\InformationPropertyValueIntegerExt;
use yii\helpers\Html;

/* @var $this View */
/* @var $oProperty InformationPropertyExt */
/* @var $arPropertyValues InformationPropertyValueIntegerExt[] */
?>
<div class="property" id="property_<?php echo $oProperty->id;?>" data-type="<?php echo $oProperty->type;?>" data-property-id="<?php echo $oProperty->id;?>">
    <div class="row property-values">
<?php
if (!empty($arPropertyValues)) {
    foreach ($arPropertyValues as $oIntegerValue) {
        /* @var $oIntegerValue InformationPropertyValueIntegerExt */
        ?>
        <div class="form-group" data-property-value-id="<?php echo $oIntegerValue->id;?>">
            <div class="col-md-12">

            <?php
            echo Html::checkbox(
                'property['.$oIntegerValue->information_property_id.']['.$oIntegerValue->id.']',
                ($oIntegerValue->value)?true:false,
                [
                    //'class'=>'form-control'
                ]
            );
            ?>

            <p class="help-block help-block-error"></p>
            </div>
        </div>

        <?php
    }
} else {
    //$oIntegerValue = new InformationPropertyValueIntegerExt();
?>

    <div class="form-group" data-property-value-id="">
        <div class="col-md-12">

            <?php
            echo Html::checkbox(
                'property['.$oProperty->id.'][]',
                false,
                [
                    //'class'=>'form-control'
                ]
            );
            ?>

            <p class="help-block help-block-error"></p>
        </div>
    </div>

<?php
}
?>
    </div>
    <?php
    /*if( $oProperty->multiple ) {
        ?>
        <div class="text-center">
            <button type="button" data-href="" class="btn btn-sm"><i class="fa fa-plus"></i></button>
        </div>
        <?php
    }*/
    ?>
</div>
