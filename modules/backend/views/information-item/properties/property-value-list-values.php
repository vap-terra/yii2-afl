<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 09.03.16
 * Time: 7:09
 */


use app\components\web\View;
use app\models\ext\InformationPropertyExt;
use app\models\ext\InformationPropertyValueIntegerExt;
use app\models\ext\ListValueExt;
use yii\helpers\Html;

/* @var $this View */
/* @var $oProperty InformationPropertyExt */
/* @var $arPropertyValues InformationPropertyValueIntegerExt[] */
?>
<div class="property" id="property_<?php echo $oProperty->id;?>" data-type="<?php echo $oProperty->type;?>" data-property-id="<?php echo $oProperty->id;?>">
    <div class="row property-values">
<?php
if (!empty($arPropertyValues)) {
    foreach ($arPropertyValues as $oIntegerValue) {
        /* @var $oIntegerValue InformationPropertyValueIntegerExt */
        ?>
        <?php
        //echo Html::dropDownList('list_value_id',$oProperty->list_value_id,ListValueExt::getArOptionList(),[]);
        ?>
        <div class="form-group" data-property-value-id="<?php echo $oIntegerValue->id;?>">
            <div class="col-md-12">
                <div class="input-group">

                    <?php
                    //$oProperty->value = $oIntegerValue->value;

                    echo Html::dropDownList(
                        'property['.$oIntegerValue->information_property_id.']['.$oIntegerValue->id.']',
                        $oIntegerValue->value,
                        [' ... ']+ListValueExt::getArItemsOptionList($oProperty->list_value_id),
                        ['class'=>'form-control']
                    );

                    ?>

                    <span class="input-group-btn<?php if( !$oProperty->multiple ) {?> hidden<?php }?>">
                        <button type="button" data-href="" class="btn input-group-addon delete-btn"><i class="fa fa-minus"></i></button>
                    </span>
                </div>
                <p class="help-block help-block-error"></p>
            </div>


        </div>
        <?php
    }
}
?>

<?php
$oProperty->value = '';
if( $oProperty->multiple || empty($arPropertyValues) ) {
?>
    <div class="form-group" data-property-value-id="">
        <div class="col-md-12">
            <div class="input-group">

                <?php
                //$oProperty->value = $oIntegerValue->value;

                echo Html::dropDownList(
                    'property['.$oProperty->id.'][]',
                    '',
                    [' ... ']+ListValueExt::getArItemsOptionList($oProperty->list_value_id),
                    ['class'=>'form-control']
                );

                ?>

                <span class="input-group-btn hidden">
                    <button type="button" data-href="" class="btn input-group-addon delete-btn"><i class="fa fa-minus"></i></button>
                </span>
            </div>
            <p class="help-block help-block-error"></p>
        </div>
    </div>
<?php
}
?>
    </div>
<?php
if( $oProperty->multiple ) {
?>
    <div class="text-center">
        <button type="button" data-href="" class="btn btn-sm add-btn"><i class="fa fa-plus"></i></button>
    </div>
<?php
}
?>
</div>
