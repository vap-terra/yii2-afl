<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 09.03.16
 * Time: 7:09
 */

use app\components\web\View;
use app\models\ext\InformationPropertyExt;
use app\models\ext\InformationPropertyValueFileExt;
use yii\helpers\Html;

/* @var $this View */
/* @var $oProperty InformationPropertyExt */
/* @var $arPropertyValues InformationPropertyValueFileExt[] */
?>
<div class="property" id="property_<?php echo $oProperty->id;?>" data-type="<?php echo $oProperty->type;?>" data-property-id="<?php echo $oProperty->id;?>">
    <div class="row property-values">
        <?php
if (!empty($arPropertyValues)) {
    foreach ($arPropertyValues as $oFileValue) {
        /* @var $oFileValue InformationPropertyValueFileExt */
?>
        <?php
        $imgLink = '';
        if (!empty($oFileValue->file) && is_file($oFileValue->getFilePath())) {
            $img = Html::img($oFileValue->getFileHref(), ['class' => 'img-responsive img-thumbnail', 'width' => '100']);
            $imgLink = Html::a($img, $oFileValue->getFileHref(), ['target' => '_blank']);
            //$form->field($oFileValue, 'del_img['.$oFileValue->information_property_id.']['.$oFileValue->id.']')->checkBox(['class' => 'span-1']);
        }
        ?>

        <div class="form-group" data-property-value-id="<?php echo $oFileValue->id;?>">
            <div class="col-md-12">

                <div class="col-md-3"></div>
                <div class="col-md-9">
                    <div class="col-md-9">
                        <div class="fileupload fileupload-new" data-provides="fileupload">
                        <div class="input-append">
                            <div class="uneditable-input">
                                <i class="fa fa-file fileupload-exists"></i>
                                <span class="fileupload-preview"></span>
                            </div>
                            <span class="btn btn-default btn-file">
                            <span class="fileupload-exists"><?php echo Yii::t('backend/layout', 'Change');?></span>
                            <span class="fileupload-new"><?php echo Yii::t('backend/layout', 'Select file');?></span>
                                <?php
                                echo Html::fileInput('property['.$oFileValue->information_property_id.']['.$oFileValue->id.']',$oFileValue->file,[]);
                                ?>
                            </span>
                            <a href="#" class="btn btn-default fileupload-exists delete-btn" data-dismiss="fileupload"><?php echo Yii::t('backend/layout', 'Remove');?></a>

                        </div>
                    </div>
                    <p class="help-block help-block-error"></p>
                </div>
                <div class="text-center col-md-3">
                    <?php echo $imgLink;?>
                    <button type="button" data-href="" class="btn delete-btn<?php if( !$oProperty->multiple ) {?> hidden<?php }?>"><i class="fa fa-minus"></i></button>
                </div>
            </div>


            </div>
        </div>

        <?php
    }
}
?>

<?php
if( $oProperty->multiple || empty($arPropertyValues) ) {
    $oFileValue = new InformationPropertyValueFileExt();
?>
    <div class="form-group" data-property-value-id="">
        <div class="col-md-12">

            <div class="col-md-3"></div>
            <div class="col-md-9">
                <div class="col-md-9">
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                        <div class="input-append">
                            <div class="uneditable-input">
                                <i class="fa fa-file fileupload-exists"></i>
                                <span class="fileupload-preview"></span>
                            </div>
                            <span class="btn btn-default btn-file">
                            <span class="fileupload-exists"><?php echo Yii::t('backend/layout', 'Change');?></span>
                            <span class="fileupload-new"><?php echo Yii::t('backend/layout', 'Select file');?></span>
                                <?php
                                echo Html::fileInput('property['.$oProperty->id.'][]','',[]);
                                ?>
                            </span>
                            <a href="#" class="btn btn-default fileupload-exists delete-btn" data-dismiss="fileupload"><?php echo Yii::t('backend/layout', 'Remove');?></a>

                        </div>
                    </div>
                    <p class="help-block help-block-error"></p>
                </div>
                <div class="text-center col-md-3">
                    <button type="button" data-href="" class="btn delete-btn hidden"><i class="fa fa-minus"></i></button>
                </div>
            </div>


        </div>
    </div>
<?php
}
?>
    </div>
<?php
if( $oProperty->multiple ) {
?>
    <div class="text-center">
        <button type="button" data-href="" class="btn btn-sm add-btn"><i class="fa fa-plus"></i></button>
    </div>
<?php
}
?>
</div>