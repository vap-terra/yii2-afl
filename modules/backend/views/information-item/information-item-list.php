<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 22.02.16
 * Time: 18:31
 */
use app\components\web\View;
use app\models\ext\InformationItemExt;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this View */
/* @var $dataProviderInformationItemList ActiveDataProvider */
/* @var $paginationInformationItemList Pagination */

$model = new InformationItemExt();
?>
<?php
    echo GridView::widget(
        [
            'dataProvider' => $dataProviderInformationItemList,
            //'filterModel' => $searchModelInformationItemList,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'id',
                'dt_creation',
                [
                    'class' => 'yii\grid\DataColumn',
                    'attribute' => 'image',
                    'headerOptions' => ['width' => '100'],
                    'value' => function ($data) {
                        /* @var $data InformationItemExt */
                        $src = Yii::$app->storage->getFileHref( 'no-photo-item.png' );
                        if( !empty( $data->image ) ) {
                            $src = $data->getImageHref();
                        }
                        return Html::img($src,['class'=>'img-thumbnail']);
                    },
                    //'label' => $model->getAttributeLabel('image'),
                    'format' => 'html',
                ],
                'name',
                [
                    'value'=>'description',
                    'attribute' => 'description',
                    //'label' => $model->getAttributeLabel('description'),
                    'format' => 'html',
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'headerOptions' => ['width' => '80'],
                    'template' => '{comments} {edit} {delete}',
                    'buttons' => [
                        'comments' => function ($url, $model, $key) {
                            if( $model->information->allow_comment ) {
                                $url = Yii::$app->urlManager->createUrl(['/backend/information-item-comment/index', 'information_item_id' => $model->id]);
                                return Html::a('<span class="glyphicon glyphicon-comment"></span>', $url, []);
                            }
                            return '';
                        },
                        'edit' => function ($url, $model, $key) {
                            $url = Yii::$app->urlManager->createUrl(['/backend/information-item/edit','id'=>$model->id]);
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>',$url,[]);
                        },
                        'delete' => function ($url, $model, $key) {
                            $url = Yii::$app->urlManager->createUrl(['/backend/information-item/delete','id'=>$model->id]);
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>',$url,[]);
                        },
                    ]
                ],
            ],
            'tableOptions' => ['class' => 'table table-striped table-hover'],
        ]
    );
?>
