<?php
use app\models\BaseActiveQuery;
use app\models\ext\FormExt;
use app\models\ext\InformationExt;
use app\models\ext\MenuItemExt;
use app\models\ext\MenuItemRelationExt;
use yii\bootstrap\ActiveForm;


/* @var $num integer */
/* @var $oMenuRelation MenuItemRelationExt */
/* @var $oMenu MenuItemExt */
/* @var $activeForm ActiveForm */

?>
<div class="sortable-item menu-item-relation" data-id="<?php echo $oMenuRelation->id ?>" data-position="<?php echo $oMenuRelation->position ?>">

    <?php if( !$oMenuRelation->isNewRecord ){?>
    <div class="panel-action">
        <span data-href="<?php echo Yii::$app->urlManager->createUrl(['/backend/menu-item-relation/reposition', 'id' => $oMenuRelation->id]) ?>"
              class="desc inline-block move-menu-item-relation" data-toggle="tooltip" title="Переместить">
            <i class="fa fa-arrows fa-fw"></i>
        </span>
        <a href="<?php echo Yii::$app->urlManager->createUrl(['/backend/menu-item-relation/save', 'id' => $oMenuRelation->id]) ?>"
           class="desc inline-block margin-left fs-12 save-menu-item-relation" data-toggle="tooltip" title="сохранить">
            <i class="fa fa-floppy-o fa-fw"></i>
        </a>
        <a href="<?php echo Yii::$app->urlManager->createUrl(['/backend/menu-item-relation/delete', 'id' => $oMenuRelation->id]) ?>"
           class="desc inline-block margin-left fs-12 remove-menu-item-relation" data-toggle="tooltip" title="удалить">
            <i class="fa fa-minus-circle fa-fw"></i>
        </a>
    </div>
    <?php } ?>

    <?php
    $modelItem = $oMenuRelation->getItem(true);
    $modelRelationName = 'MenuItemRelationExt';
    $modelRelationIndex = $oMenuRelation->id?$oMenuRelation->id:'new_'.$num;
    echo $activeForm->field( $oMenuRelation, 'type')->dropDownList(
        MenuItemRelationExt::getMenuItemTypes(),
        [
            //'prompt' => ' НЕТ ',
            'id' => 'menuitemrelationext-type-'.$oMenuRelation->id,
            'onchange'=>'',
            'class'=>'form-control populate '.$modelRelationName.'-type',
            'data-plugin-selectTwo' => '',
            //'groups' => MenuExt::getArOptionListOptgroup(),
            'name'=>$modelRelationName.'['.$modelRelationIndex.'][type]',
            'data-href'=>Yii::$app->urlManager->createUrl(['/backend/menu-item-relation/change-type', 'id' => $oMenuRelation->id]),
            'data-type'=>$oMenuRelation->type,
        ]
    );
    ?>

    <div class="panel-group" id="data_<?php echo $modelRelationIndex ?>" role="tablist" aria-multiselectable="true">

        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">

                    <a role="button" data-toggle="collapse" data-parent="#data_<?php echo $modelRelationIndex ?>"
                       href="#collapse_<?php echo $modelRelationIndex ?>" aria-expanded="true" aria-controls="collapseOne">
                        параметры
                    </a>

                </h4>
            </div>
            <div id="collapse_<?php echo $modelRelationIndex ?>" class="panel-collapse collapse" role="tabpanel"
                 aria-labelledby="headingOne">
                <div class="panel-body">


                    <div class="item-parameters">
                        <?php
                        echo $activeForm->field(
                            $oMenuRelation,
                            'position',
                            [
                                'template' => '{input}',
                                'options'=>['class'=>'']
                            ]
                        )->hiddenInput(
                            [
                                'name'=>$modelRelationName.'['.$modelRelationIndex.'][position]'
                            ]
                        );
                        echo $activeForm->field( $oMenuRelation, 'active',[
                                'template' => "
                {label}\n
                <div class=\"col-md-6\">
                    <div class=\"switch switch-sm switch-success\">
                        {input}\n
                    </div>
                    {hint}\n
                    {error}\n
                </div>",
                                'labelOptions' => [
                                    'class' => 'col-md-3 control-label'
                                ],

                            ]
                        )->checkbox(
                            [
                                'data-plugin-ios-switch'=>'',
                                'name'=>$modelRelationName.'['.$modelRelationIndex.'][active]',
                            ],
                            false
                        );
                        echo $activeForm->field( $oMenuRelation, 'main',[
                                'template' => "
                {label}\n
                <div class=\"col-md-6\">
                    <div class=\"switch switch-sm switch-success\">
                        {input}\n
                    </div>
                    {hint}\n
                    {error}\n
                </div>",
                                'labelOptions' => [
                                    'class' => 'col-md-3 control-label'
                                ],

                            ]
                        )->checkbox(
                            [
                                'data-plugin-ios-switch'=>'',
                                'name'=>$modelRelationName.'['.$modelRelationIndex.'][main]',
                            ],
                            false
                        );
                        ?>


                        <div class="item-parameter">

                            <?php

                            $view = 'type/type-'.$oMenuRelation->type;
                            $context = [
                                'num' => $num,
                                'oMenuRelation' => $oMenuRelation,
                                'oMenu' => $oMenu,
                                'activeForm' => $activeForm,
                            ];
                            echo $this->render($view,$context);

                            ?>
                        </div>

                    </div>



                </div>
            </div>
        </div>

    </div>


</div>