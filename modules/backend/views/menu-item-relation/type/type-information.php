<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 17.08.16
 * Time: 21:31
 */
use app\models\BaseActiveQuery;
use app\models\ext\InformationExt;
use app\models\ext\MenuItemExt;
use app\models\ext\MenuItemRelationExt;
use yii\widgets\ActiveForm;

/* @var $num integer */
/* @var $oMenuRelation MenuItemRelationExt */
/* @var $oMenu MenuItemExt */
/* @var $activeForm ActiveForm */
$modelRelationIndex = $oMenuRelation->id?$oMenuRelation->id:'new_'.$num;
$modelRelationName = 'MenuItemRelationExt';

echo $activeForm->field( $oMenuRelation, 'item_id')->dropDownList(
    InformationExt::getArOptionList(BaseActiveQuery::ALL),
    [
        //'prompt' => ' НЕТ ',
        'id' => 'menuitemrelationext-item_id-'.$oMenuRelation->id,
        'onchange'=>'',
        'class'=>'form-control populate '.$modelRelationName.'-item_id',
        'data-plugin-selectTwo' => '',
        //'groups' => MenuExt::getArOptionListOptgroup(),
        'name'=>$modelRelationName.'['.$modelRelationIndex.'][item_id]',
        'data-href'=>Yii::$app->urlManager->createUrl(['/backend/menu-item-relation/change-item', 'id' => $oMenuRelation->id]),
    ]
);