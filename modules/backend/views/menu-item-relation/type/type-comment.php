<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 17.08.16
 * Time: 21:31
 */
use app\models\ext\InformationExt;
use app\models\ext\MenuItemExt;
use app\models\ext\MenuItemRelationExt;
use yii\widgets\ActiveForm;

/* @var $num integer */
/* @var $oMenuRelation MenuItemRelationExt */
/* @var $oMenu MenuItemExt */
/* @var $activeForm ActiveForm */
$modelRelationIndex = $oMenuRelation->id?$oMenuRelation->id:'new_'.$num;
$modelRelationName = 'MenuItemRelationExt';