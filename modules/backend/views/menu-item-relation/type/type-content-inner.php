<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 17.08.16
 * Time: 21:31
 */
use app\components\web\View;
use app\models\ext\ContentExt;
use app\models\ext\MenuItemExt;
use app\models\ext\MenuItemRelationExt;
use yii\widgets\ActiveForm;

/* @var $this View */
/* @var $num integer */
/* @var $oMenuRelation MenuItemRelationExt */
/* @var $oMenu MenuItemExt */
/* @var $activeForm ActiveForm */
/* @var $oContent ContentExt */
$modelRelationIndex = $oMenuRelation->id?$oMenuRelation->id:'new_'.$num;
$modelRelationName = 'MenuItemRelationExt';

echo $activeForm->field(
    $oContent,
    'name'
)->textInput(
    [
        'name' => 'ContentExt['.$modelRelationIndex.'][name]',
        'id' => 'contentext_name_'.$modelRelationIndex,
    ]
);
echo $activeForm->field(
    $oContent,
    'content',
    [
        'template' => "
                                    {label}\n
                                    <div class=\"col-md-12\">
                                        {input}\n
                                        {hint}\n
                                        {error}\n
                                    </div>",
        'labelOptions' => [
            'class' => 'col-md-12 control-label'
        ],
    ]
)->textarea(
    [
        'name' => 'ContentExt['.$modelRelationIndex.'][content]',
        'id' => 'contentext_content_'.$modelRelationIndex,
        'rows' => 6,
        'style'=>'width:100%;height:150px;',
        'class'=>'textarea-editor-tinymce',
        'data-plugin-tinymce'=>'',
        'plugin-options' => '',
    ]
);
