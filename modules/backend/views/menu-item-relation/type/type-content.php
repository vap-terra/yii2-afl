<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 17.08.16
 * Time: 21:31
 */
use app\components\web\View;
use app\models\ext\ContentExt;
use app\models\ext\MenuItemExt;
use app\models\ext\MenuItemRelationExt;
use yii\widgets\ActiveForm;

/* @var $this View */
/* @var $num integer */
/* @var $oMenuRelation MenuItemRelationExt */
/* @var $oMenu MenuItemExt */
/* @var $activeForm ActiveForm */
$modelRelationIndex = $oMenuRelation->id?$oMenuRelation->id:'new_'.$num;
$modelRelationName = 'MenuItemRelationExt';

// TODO add list of exist content, prompt - create new content
// TODO add name of content if new
$oContent = $oMenuRelation->getItem(true);
echo $activeForm->field( $oContent, 'id')->dropDownList(
    [0=>' Новый текст ']+ContentExt::getArOptionList(),
    [
        'name' => 'ContentExt['.$modelRelationIndex.'][id]',
        'id' => 'contentext_id_'.$modelRelationIndex,
        //'prompt' => ' Новый текст ',
        'onchange'=>'',
        'class'=>'form-control populate '.$modelRelationName.'-contentext-id',
        'data-plugin-selectTwo' => '',
        'data-href'=>Yii::$app->urlManager->createUrl(['/backend/menu-item-relation/change-content', 'id' => $oMenuRelation->id]),
        //'groups' => [],
    ]
);
?>
<div class="item-parameter-content-data">
<?php
    echo $this->render(
        'type-content-inner',
        [
            'num'=>$num,
            'oMenuRelation'=>$oMenuRelation,
            'oMenu'=>$oMenu,
            'activeForm'=>$activeForm,
            'oContent'=>$oContent
        ]
    );
?>
</div>
