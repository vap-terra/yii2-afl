<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 28.02.16
 * Time: 16:00
 */

use app\models\ext\BannerExt;
use app\models\ext\BannerGroupExt;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $form ActiveForm */
/* @var $model BannerExt */

\app\assets\Select2Asset::register($this);
//\app\assets\IOS7SwitchAsset::register($this);
\app\assets\TinymceAsset::register($this);
//\app\assets\DropzoneAsset::register($this);
\app\assets\BootstrapFileuploadAsset::register($this);

$form = ActiveForm::begin(
    [
        'id' => 'board-form',
        'action' => Yii::$app->urlManager->createUrl(['/backend/banner/save','id'=>$model->id]),
        'requiredCssClass' => 'has-required',
        'options' => [
            'enctype' => 'multipart/form-data',
            'class' => 'form-horizontal form-bordered',
        ],
        'fieldConfig' => [
            'template' => "

                                    {label}\n
                                    <div class=\"col-md-6\">
                    {input}\n
                    {hint}\n
                    {error}\n
                                    </div>

",
            'labelOptions' => [
                'class' => 'col-md-3 control-label'
            ],
        ],
    ]
);
?>

<section class="panel panel-featured panel-featured-dark">
    <header class="panel-heading panel-heading-transparent">
        <div class="panel-actions">
            <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
            <!-- <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a> -->
        </div>

        <h2 class="panel-title"></h2>
        <!-- <p class="panel-subtitle"></p> -->
    </header>
    <div class="panel-body">

        <?php
        echo $form->field( $model, 'active',[
                'labelOptions' => [
                ],

            ]
        )->checkbox(
            [
            ],
            false
        );
        ?>

        <?php
        echo $form->field( $model, 'banner_group_id')->dropDownList(
            BannerGroupExt::getArOptionList(),
            [
                'prompt' => ' ... ',
                'onchange'=>'',
                'class'=>'form-control populate',
                'data-plugin-selectTwo' => '',
            ]
        );
        ?>

        <?php echo $form->field( $model, 'name');?>

        <?php echo $form->field( $model, 'link');?>

        <?php
        $imgLink = '';
        if( !empty( $model->image ) && is_file($model->getImagePath()))
        {
            $img = Html::img($model->getImageHref(), ['class'=>'img-responsive img-thumbnail', 'width'=>'100']);
            $imgLink = Html::a($img,$model->getImageHref(),['target'=>'_blank']);
            //$form->field($model,'del_img')->checkBox(['class'=>'span-1']);
        }
        ?>

        <?php
        echo $form->field( $model, 'image',
            [
                'template' => "
                        {label}\n
                        <div class=\"col-md-9\">
                            <div class=\"col-md-9\">
                            <div class=\"fileupload fileupload-new\" data-provides=\"fileupload\">
                                <div class=\"input-append\">

                                    <div class=\"uneditable-input\">
                                        <i class=\"fa fa-file fileupload-exists\"></i>
                                        <span class=\"fileupload-preview\"></span>
                                    </div>
                                    <span class=\"btn btn-default btn-file\">
                                        <span class=\"fileupload-exists\">".Yii::t('backend/layout','Change')."</span>
                                        <span class=\"fileupload-new\">".Yii::t('backend/layout','Select file')."</span>
                                        {input}\n
                                    </span>
                                    <a href=\"#\" class=\"btn btn-default fileupload-exists\" data-dismiss=\"fileupload\">".Yii::t('backend/layout','Remove')."</a>

                                </div>
                            </div>
                            {hint}\n
                            {error}\n
                            </div>
                            <div class=\"text-center col-md-3\">
                            {$imgLink}
                            </div>
                        </div>",
                'labelOptions' => [
                    'class' => 'col-md-3 control-label'
                ]
            ]
        )->fileInput([]);
        ?>

        <?php

        echo $form->field( $model, 'content',
            [
                'template' => "
                {label}\n
                <div class=\"col-md-12\">
                    {input}\n
                    {hint}\n
                    {error}\n
                </div>",
                'labelOptions' => [
                    'class' => 'col-md-12 control-label'
                ],
            ]
        )->textarea(
            [
                'class'=>'textarea-editor-tinymce',
                'data-plugin-tinymce'=>'',
                'plugin-options' => '',
            ]
        );
        ?>

        <?php echo $form->field( $model, 'position');?>

        <div class="text-center">
            <button class="btn btn-success btn-lg">
                <i class="fa <?php echo ($model->isNewRecord)?' fa-plus':' fa-cog';?>"></i> <?php echo Yii::t('backend/layout',($model->isNewRecord)?'Save':'Update'); ?>
            </button>
        </div>
    </div>
</section>

<?php ActiveForm::end(); ?>
<!-- </div>
</section> -->

