<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 28.02.16
 * Time: 15:47
 */
use app\components\BaseController;
use app\models\ext\BannerExt;


/* @var $this \yii\web\View */
/* @var $model BannerExt */
/* @var $result array */

$this->title = Yii::t('backend/layout','Add banner');
$this->params['breadcrumbs'][] = [
    'url'=>Yii::$app->urlManager->createUrl(['/backend/banner-group/index']),
    'label'=>Yii::t('backend/layout', 'Banner group')
];
$this->params['breadcrumbs'][] = Yii::t('backend/layout', 'Add');

if( !empty( $result ) ) {
?>
    <div class="alert alert-<?php if( $result['status'] == BaseController::SUCCESS ){ ?>success<?php } else {?>danger<?php } ?>" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <p><?php echo $result['message'] ?></p>
        <?php if( !empty( $result['errors'] ) ){?>
            <?php foreach( $result['errors'] as $error ){?>
                <?php if( is_array( $error ) ){ ?>
                    <p><?php echo implode('</p><p>',$error); ?></p>
                <?php } else { ?>
                    <p><?php echo $error; ?></p>
                <?php } ?>
            <?php }?>
        <?php }?>
    </div>
<?php
}

echo $this->render(
    'banner-form',
    [
        'model' => $model
    ]
);

