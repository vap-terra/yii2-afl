<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 22.02.16
 * Time: 18:31
 */
use app\models\ext\BannerExt;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $dataProviderList \yii\data\ActiveDataProvider */
/* @var $paginationBannerList \yii\data\Pagination */
$model = new BannerExt();
?>
<?php
    echo GridView::widget(
        [
            'dataProvider' => $dataProviderList,
            //'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'id',
                'image'=>[
                    'class' => 'yii\grid\DataColumn',
                    'value' => function ($data) {
                        /* @var $data BannerExt */
                        if( !empty( $data->image ) ) {
                            return '<img src="' . $data->getImageHref() . '" width="50" height="50" alt=""/>';
                        }
                        $noPhotoSrc = Yii::$app->storage->getFileHref( 'no-photo-item.png' );
                        return '<img src="' . $noPhotoSrc . '" width="50" height="50" alt=""/>';
                    },
                    'label' => $model->getAttributeLabel('image'),
                    'format' => 'html',
                ],
                'name',
                'active'=>[
                    'class' => 'yii\grid\DataColumn',
                    'value' => function ($data) { return '<span class="fa '.($data->active?'fa-eye':'fa-eye-slash').'"></span>'; },
                    //'label' => 'Активный',
                    'format' => 'html',
                ],
                'position',
                /*[
                    'class' => 'yii\grid\DataColumn',
                    'value' => function ($data) { return ''; },
                    'label' => 'Контакты',
                    'format' => 'html',
                ],*/
                [
                    'class' => 'yii\grid\ActionColumn',
                    'headerOptions' => ['width' => '90'],
                    'template' => '{changeActive} {edit} {delete}',
                    'buttons' => [
                        'changeActive' => function ($url, $model, $key) {
                            $url = Yii::$app->urlManager->createUrl(['/backend/banner/change-active','id'=>$model->id]);
                            $link = Html::a('<span class="fa fa-toggle-'.($model->active?'off':'on').'"></span>',$url,[]);
                            return $link;
                        },
                        'edit' => function ($url, $model, $key) {
                            $url = Yii::$app->urlManager->createUrl(['/backend/banner/edit','id'=>$model->id]);
                            $link = Html::a('<span class="glyphicon glyphicon-pencil"></span>',$url,[]);
                            return $link;
                        },
                        'delete' => function ($url, $model, $key) {
                            $url = Yii::$app->urlManager->createUrl(['/backend/banner/delete','id'=>$model->id]);
                            $link = Html::a('<span class="glyphicon glyphicon-trash"></span>',$url,[]);
                            return $link;
                        },
                    ]
                ],
            ],
            'tableOptions' => ['class' => 'table table-striped table-hover'],
        ]
    );
?>
