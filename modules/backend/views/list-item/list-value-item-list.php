<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 22.02.16
 * Time: 18:31
 */
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $dataProviderListValueItemList \yii\data\ActiveDataProvider */
/* @var $paginationListValueItemList \yii\data\Pagination */
?>
<?php
    echo GridView::widget(
        [
            'dataProvider' => $dataProviderListValueItemList,
            //'filterModel' => $searchModelListValueItemList,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'id',
                'name',
                'active'=>[
                    'class' => 'yii\grid\DataColumn',
                    'value' => function ($data) { return '<span class="fa '.($data->active?'fa-eye':'fa-eye-slash').'"></span>'; },
                    //'label' => 'Активный',
                    'format' => 'html',
                ],
                'position',
                /*[
                    'class' => 'yii\grid\DataColumn',
                    'value' => function ($data) { return ''; },
                    'label' => 'Контакты',
                    'format' => 'html',
                ],*/
                [
                    'class' => 'yii\grid\ActionColumn',
                    'headerOptions' => ['width' => '90'],
                    'template' => '{changeActive} {edit} {delete}',
                    'buttons' => [
                        'changeActive' => function ($url, $model, $key) {
                            $url = Yii::$app->urlManager->createUrl(['/backend/list-item/change-active','id'=>$model->id]);
                            $link = Html::a('<span class="fa fa-toggle-'.($model->active?'off':'on').'"></span>',$url,[]);
                            return $link;
                        },
                        'edit' => function ($url, $model, $key) {
                            $url = Yii::$app->urlManager->createUrl(['/backend/list-item/edit','id'=>$model->id]);
                            $link = Html::a('<span class="glyphicon glyphicon-pencil"></span>',$url,[]);
                            return $link;
                        },
                        'delete' => function ($url, $model, $key) {
                            $url = Yii::$app->urlManager->createUrl(['/backend/list-item/delete','id'=>$model->id]);
                            $link = Html::a('<span class="glyphicon glyphicon-trash"></span>',$url,[]);
                            return $link;
                        },
                    ]
                ],
            ],
            'tableOptions' => ['class' => 'table table-striped table-hover'],
        ]
    );
?>
