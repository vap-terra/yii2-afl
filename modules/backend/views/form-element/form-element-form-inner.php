<?php
use app\models\ext\FormElementExt;
use app\models\ext\FormExt;
use yii\bootstrap\ActiveForm;

/* @var $model FormElementExt */
/* @var $form FormExt */
/* @var $activeForm ActiveForm */

?>

<div class="form-element" data-id="<?php echo $model->id ?>" data-position="<?php echo $model->position ?>" style="">
    <span
        data-href="<?php echo Yii::$app->urlManager->createUrl(['/backend/form-element/reposition', 'id' => $model->id]) ?>"
        class="desc inline-block move-form-element" data-toggle="tooltip" title="Переместить"><i
            class="fa fa-arrows fa-fw"></i></span>

    <a href="<?php echo Yii::$app->urlManager->createUrl(['/backend/form-element/save', 'id' => $model->id]) ?>"
       class="desc inline-block margin-left fs-12 save-form-element" data-toggle="tooltip" title="сохранить"><i
            class="fa fa-floppy-o fa-fw"></i></a>
    <a href="<?php echo Yii::$app->urlManager->createUrl(['/backend/form-element/delete', 'id' => $model->id]) ?>"
       class="desc inline-block margin-left fs-12 remove-form-element" data-toggle="tooltip" title="удалить"><i
            class="fa fa-minus-circle fa-fw"></i></a>

    <?php echo $activeForm->field( $model, 'name');?>

    <div class="panel-group" id="data_<?php echo $model->id ?>" role="tablist" aria-multiselectable="true">

        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">

                    <a role="button" data-toggle="collapse" data-parent="#data_<?php echo $model->id ?>"
                       href="#collapse_<?php echo $model->id ?>" aria-expanded="true" aria-controls="collapseOne">
                        параметры
                    </a>

                </h4>
            </div>
            <div id="collapse_<?php echo $model->id ?>" class="panel-collapse collapse" role="tabpanel"
                 aria-labelledby="headingOne">
                <div class="panel-body">

                    <?php echo $activeForm->field( $model, 'tid');?>

                    <?php
                    echo $activeForm->field( $model, 'type')->dropDownList(
                        FormElementExt::getTypeList(),
                        [
                            'onchange'=>'',
                            'class'=>'form-control populate',
                            'data-plugin-selectTwo' => '',
                            //'groups' => [],
                        ]
                    );
                    ?>

                    <?php
                    $inputId = 'FormElement_active_' . $model->id;
                    echo $activeForm->field($model, 'active', [
                            'template' => "
                {label}\n
                <div class=\"col-md-6\">
                    <div class=\"switch switch-sm switch-success\">
                        {input}\n
                    </div>
                    {hint}\n
                    {error}\n
                </div>",
                            'labelOptions' => [
                                'class' => 'col-md-3 control-label'
                            ],

                        ]
                    )->checkbox(
                        [
                            'id' => $inputId,
                            'data-plugin-ios-switch' => ""

                        ],
                        false
                    );
                    ?>

                    <?php
                    $inputId = 'FormElement_required_' . $model->id;
                    echo $activeForm->field($model, 'required', [
                            'template' => "
                {label}\n
                <div class=\"col-md-6\">
                    <div class=\"switch switch-sm switch-success\">
                        {input}\n
                    </div>
                    {hint}\n
                    {error}\n
                </div>",
                            'labelOptions' => [
                                'class' => 'col-md-3 control-label'
                            ],

                        ]
                    )->checkbox(
                        [
                            'id' => $inputId,
                            'data-plugin-ios-switch' => ""

                        ],
                        false
                    );
                    ?>

                    <?php
                    $inputId = 'FormElement_description_' . $model->id;
                    echo $activeForm->field( $model, 'description',
                        [
                            'template' => "
                            {label}\n
                            <div class=\"col-md-12\">
                                {input}\n
                                {hint}\n
                                {error}\n
                            </div>",
                            'labelOptions' => [
                                'class' => 'col-md-12 control-label'
                            ],
                        ]
                    )->textarea(
                        [
                            'id' => $inputId,
                            'rows' => 6,
                            'style'=>'width:100%;height:150px;',
                            'class'=>'textarea-editor-tinymce',
                            'data-plugin-tinymce'=>'',
                            'plugin-options' => '',
                        ]
                    );
                    ?>

                </div>
            </div>

        </div>

    </div>

</div>