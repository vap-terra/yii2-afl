<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 10.02.16
 * Time: 9:28
 */
use app\components\bootstrap\SortableNav;
use app\models\ext\InformationExt;
use app\widgets\Alert;
use yii\widgets\ListView;

/* @var $this \yii\base\View */
/* @var $oInformation InformationExt */
/* @var $dataProviderInformationList \yii\data\ActiveDataProvider */
/* @var $paginationInformationList \yii\data\Pagination */
/* @var $dataProviderInformationItemList \yii\data\ActiveDataProvider */
/* @var $paginationInformationItemList \yii\data\Pagination */

$this->title = Yii::t('backend/layout', 'Information <{name}>',['name'=>!empty($oInformation)?$oInformation->name:'']);
$this->params['breadcrumbs'][] = Yii::t('backend/layout', 'Information');
if( !empty($oInformation) ) {
    $this->params['breadcrumbs'][] = $oInformation->name;
}
?>


<section class="">
    <div class="row">

        <div class="inner-body mg-main col-md-9">


            <div class="inner-toolbar clearfix">

            </div>

            <div class="row mg-files" data-sort-destination data-sort-id="media-gallery">

                <?= Alert::widget() ?>

                <?php
                if( !empty( $dataProviderInformationItemList ) ) {
                    echo $this->render(
                        '../information-item/information-item-list',
                        [
                            'dataProviderInformationItemList' => $dataProviderInformationItemList,
                            'paginationInformationItemList' => $paginationInformationItemList
                        ]
                    );
                }
                ?>
            </div>

        </div>
        <menu id="content-menu" class="inner-menu col-md-3" role="menu">
            <div class="nano">
                <div class="nano-content">

                    <div class="inner-menu-content">

                        <?php if( !empty($oInformation) ){ ?>
                            <a href="<?php echo Yii::$app->urlManager->createUrl(['/backend/information-item/add','information_id'=>$oInformation->id])?>" class="btn btn-block btn-primary btn-md pt-sm pb-sm text-md">
                                <i class="fa fa-plus mr-xs"></i>
                                <?php echo Yii::t('backend/layout','Add information item' );?>
                            </a>

                            <hr class="separator" />
                            <div class="sidebar-widget m-none">
                                <div class="widget-header clearfix">
                                    <h6 class="title pull-left mt-xs"><?php echo Yii::t('backend/layout','Groups' );?></h6>
                                    <div class="pull-right">
                                        <a href="<?php echo Yii::$app->urlManager->createUrl(['/backend/information-group/add','information_id'=>$oInformation->id])?>" class="btn btn-dark btn-sm btn-widget-act">
                                            <i class="fa fa-plus mr-xs"></i>
                                            <?php echo Yii::t('backend/layout','Add information group' );?>
                                        </a>
                                    </div>
                                </div>


                                <div class="widget-content">
                                    <?php
                                    /*echo '<pre>';
                                    print_r(InformationExt::getArMenuNavItems( $oInformation->id ));
                                    die();*/
                                    echo SortableNav::widget(
                                        [
                                            'items' => InformationExt::getArMenuNavItems( $oInformation->id ),
                                            'route' => '/backend/information-group/',
                                            'itemRoute' => '/backend/information/index',
                                            'itemRouteParameter' => 'information_group_id',
                                            'options' => [
                                                'class' => 'mg-folders nav-vertical sortable-nested-set',
                                                'data-url'=>Yii::$app->urlManager->createUrl(['/backend/information-group/relocation']),
                                                /*'dropDownCaret' => '',
                                                'dropDownOptions' => [
                                                    'class'=>'nav nav-children'
                                                ],*/
                                            ]
                                        ]
                                    );
                                    ?>

                                </div>


                                <hr class="separator" />

                                <div class="sidebar-widget m-none">
                                    <div class="widget-header">
                                        <h6 class="title"><?php echo Yii::t('backend/layout','Information Properties');?></h6>
                                        <span class="widget-toggle">+</span>
                                    </div>
                                    <div class="widget-content">
                                        <?php
                                        if( !empty( $dataProviderInformationPropertyList ) ) {
                                            //print_r( $dataProviderInformationPropertyList->query->params );
                                            //print_r( $dataProviderInformationPropertyList->query );
                                            echo ListView::widget(
                                                [
                                                    'dataProvider' => $dataProviderInformationPropertyList,
                                                    'itemOptions' => ['tag' => 'ul', 'class' => 'mg-folders'],
                                                    'itemView' => '../information-property/information-property-list-item',
                                                    'options' => ['class' => ''],
                                                ]
                                            );
                                        }
                                        ?>
                                    </div>
                                    <div class="">
                                        <a href="<?php echo Yii::$app->urlManager->createUrl(['/backend/information-property/add','information_id'=>$oInformation->id])?>" class="btn btn-dark btn-sm btn-widget-act">
                                            <i class="fa fa-plus mr-xs"></i>
                                            <?php echo Yii::t('backend/layout','Add information property' );?>
                                        </a>
                                    </div>
                                </div>


                            </div>
                        <?php } ?>

                        <hr class="separator" />
                        <div class="sidebar-widget m-none">
                            <div class="widget-header clearfix">
                                <h6 class="title pull-left mt-xs"><?php echo Yii::t('backend/layout','Informations' );?></h6>
                                <div class="pull-right">
                                    <a href="<?php echo Yii::$app->urlManager->createUrl(['/backend/information/add'])?>" class="btn btn-dark btn-sm btn-widget-act">
                                        <i class="fa fa-plus mr-xs"></i>
                                        <?php echo Yii::t('backend/layout','Add information' );?>
                                    </a>
                                </div>
                            </div>
                            <div class="widget-content">
                                <?php
                                echo ListView::widget(
                                    [
                                        'dataProvider' => $dataProviderInformationList,
                                        'itemOptions' => [ 'tag'=>'ul', 'class' => 'mg-folders' ],
                                        'itemView' => 'information-list-item',
                                        'options' => ['class' => ''],
                                    ]
                                );
                                ?>
                            </div>
                        </div>

                        <hr class="separator" />

                        <?php /*<div class="sidebar-widget m-none">
								<div class="widget-header">
									<h6 class="title">Labels</h6>
									<span class="widget-toggle">+</span>
								</div>
								<div class="widget-content">
									<ul class="mg-tags">
										<li><a href="pages-media-gallery.html#">Design</a></li>
										<li><a href="pages-media-gallery.html#">Projects</a></li>
										<li><a href="pages-media-gallery.html#">Photos</a></li>
										<li><a href="pages-media-gallery.html#">Websites</a></li>
										<li><a href="pages-media-gallery.html#">Documentation</a></li>
										<li><a href="pages-media-gallery.html#">Download</a></li>
										<li><a href="pages-media-gallery.html#">Images</a></li>
										<li><a href="pages-media-gallery.html#">Vacation</a></li>
									</ul>
								</div>
							</div> */ ?>
                    </div>
                </div>
            </div>
        </menu>

    </div>
</section>