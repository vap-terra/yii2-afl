<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 27.02.16
 * Time: 23:25
 */
use app\components\BaseController;
use app\models\ext\InformationExt;


/* @var $this \yii\web\View */
/* @var $model InformationExt */

$this->title = Yii::t('backend/layout', 'Edit information <{name}>',['name'=>!empty($model)?$model->name:'']);
$this->params['breadcrumbs'][] = [
    'url'=>Yii::$app->urlManager->createUrl(['/backend/information/index']),
    'label'=>Yii::t('backend/layout', 'Information')
];
$this->params['breadcrumbs'][] = [
    'url'=>Yii::$app->urlManager->createUrl(['/backend/information/index','id'=>$model->id]),
    'label'=>$model->name
];
$this->params['breadcrumbs'][] = Yii::t('backend/layout', 'Edit');

if( !empty( $result ) ) {
    echo \app\widgets\AlertActionResult::widget(['result'=>$result]);
}

echo $this->render(
    'information-form',
    [
        'model'=>$model
    ]
);
?>