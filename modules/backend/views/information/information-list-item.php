<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 27.02.16
 * Time: 10:22
 */
use app\models\ext\InformationExt;

/* @var $model InformationExt */
?>
<li>
    <a href="<?php echo Yii::$app->urlManager->createUrl(['/backend/information/index','id'=>$model->id])?>" class="menu-item"><i class="fa fa-list"></i> <?php echo $model->name?></a>
    <div class="item-options">
        <a href="<?php echo Yii::$app->urlManager->createUrl(['/backend/information/edit','id'=>$model->id])?>"><i class="fa fa-edit"></i></a>
        <a href="<?php echo Yii::$app->urlManager->createUrl(['/backend/information/delete','id'=>$model->id])?>" class="text-danger"><i class="fa fa-times"></i></a>
    </div>
</li>
