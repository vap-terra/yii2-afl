<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 27.02.16
 * Time: 23:25
 */
use app\models\ext\InformationExt;
use app\models\ext\MenuExt;

use yii\bootstrap\ActiveForm;

/* @var $this \yii\web\View */
/* @var $form \yii\bootstrap\ActiveForm */
/* @var $model InformationExt */

\app\assets\Select2Asset::register($this);
\app\assets\IOS7SwitchAsset::register($this);
\app\assets\TinymceAsset::register($this);
//\app\assets\DropzoneAsset::register($this);

$form = ActiveForm::begin(
    [
        'id' => 'information-form',
        'action' => Yii::$app->urlManager->createUrl(['/backend/information/save','id'=>$model->id]),
        'requiredCssClass' => 'has-required',
        'options' => [
            'enctype' => 'multipart/form-data',
            'class' => 'form-horizontal form-bordered'
        ],
        'fieldConfig' => [
            'template' => "

                                    {label}\n
                                    <div class=\"col-md-6\">
                    {input}\n
                    {hint}\n
                    {error}\n
                                    </div>

",
            'labelOptions' => [
                'class' => 'col-md-3 control-label'
            ],
        ],
    ]
);
?>

    <section class="panel panel-featured panel-featured-primary panel-collapsed">
        <header class="panel-heading panel-heading-transparent">
            <div class="panel-actions">
                <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                <!-- <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a> -->
            </div>

            <h2 class="panel-title">Мета теги</h2>
            <p class="panel-subtitle">SEO продвижение</p>
        </header>
        <div class="panel-body">

            <?php echo $form->field( $model, 'title'); ?>
            <?php echo $form->field( $model, 'meta_keywords')->textarea(['data-role'=>'tagsinput']); ?>
            <?php echo $form->field( $model, 'meta_description')->textarea(); ?>

            <?php echo $form->field( $model, 'title_group'); ?>
            <?php echo $form->field( $model, 'meta_keywords_group')->textarea(['data-role'=>'tagsinput']); ?>
            <?php echo $form->field( $model, 'meta_description_group')->textarea(); ?>

            <?php echo $form->field( $model, 'title_item'); ?>
            <?php echo $form->field( $model, 'meta_keywords_item')->textarea(['data-role'=>'tagsinput']); ?>
            <?php echo $form->field( $model, 'meta_description_item')->textarea(); ?>

            <div class="text-center">
                <button class="btn btn-success btn-lg">
                    <i class="fa <?php echo ($model->isNewRecord)?' fa-plus':' fa-cog';?>"></i> <?php echo Yii::t('backend/layout',($model->isNewRecord)?'Save':'Update'); ?>
                </button>
            </div>

        </div>
        <!-- <div class="panel-footer"></div> -->
    </section>

    <section class="panel panel-featured panel-featured-dark">
        <header class="panel-heading panel-heading-transparent">
            <div class="panel-actions">
                <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                <!-- <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a> -->
            </div>

            <h2 class="panel-title">Основное</h2>
            <!-- <p class="panel-subtitle"></p> -->
        </header>
        <div class="panel-body">

            <?php
            /*echo $form->field( $model, 'menu_item_id')->dropDownList(
                MenuExt::getArItemsOptionListGroupByMenu(),
                [
                    'prompt' => ' НЕТ ',
                    'onchange'=>'',
                    'class'=>'form-control populate',
                    'data-plugin-selectTwo' => '',
                    'groups' => MenuExt::getArOptionListOptgroup(),
                ]
            );*/
            ?>

            <?php
            echo $form->field( $model, 'active',[
                    'labelOptions' => [
                        'class' => 'col-md-3 control-label'
                    ],

                ]
            )->checkbox(
                [

                ],
                false
            );
            ?>


            <?php echo $form->field( $model, 'name');?>

            <?php

                    echo $form->field( $model, 'description',
                        [
                            'template' => "
                {label}\n
                <div class=\"col-md-12\">
                    {input}\n
                    {hint}\n
                    {error}\n
                </div>",
                            'labelOptions' => [
                                'class' => 'col-md-12 control-label'
                            ],
                        ]
                    )->textarea(
                        [
                            'class'=>'textarea-editor-tinymce',
                            'data-plugin-tinymce'=>'',
                            'plugin-options' => '',
                        ]
                    );
            ?>

            <?php
            echo $form->field( $model, 'allow_comment',[
                    'labelOptions' => [
                        'class' => 'col-md-3 control-label'
                    ],

                ]
            )->checkbox(
                [
                ],
                false
            );
            ?>

            <?php echo $form->field( $model, 'items_on_page');?>


            <?php echo $form->field( $model, 'format_date')->dropDownList(
                InformationExt::getArDateFormats(),
                [
                    /*'prompt' => ' ... ',*/
                    'onchange'=>'',
                    'class'=>'form-control populate',
                    //'data-plugin-selectTwo' => '',
                ]
            );?>

            <div class="text-center">
                <button class="btn btn-success btn-lg">
                    <i class="fa <?php echo ($model->isNewRecord)?' fa-plus':' fa-cog';?>"></i> <?php echo Yii::t('backend/layout',($model->isNewRecord)?'Save':'Update'); ?>
                </button>
            </div>
        </div>
    </section>

    <section class="panel panel-featured panel-featured-primary panel-collapsed">
        <header class="panel-heading panel-heading-transparent">
            <div class="panel-actions">
                <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                <!-- <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a> -->
            </div>

            <h2 class="panel-title">Связи</h2>
            <p class="panel-subtitle">Отображается на страницах</p>
        </header>
        <div class="panel-body">



            <div class="text-center">
                <button class="btn btn-success btn-lg">
                    <i class="fa <?php echo ($model->isNewRecord)?' fa-plus':' fa-cog';?>"></i> <?php echo Yii::t('backend/layout',($model->isNewRecord)?'Save':'Update'); ?>
                </button>
            </div>

        </div>
        <!-- <div class="panel-footer"></div> -->
    </section>

<?php ActiveForm::end(); ?>
    <!-- </div>
    </section> -->