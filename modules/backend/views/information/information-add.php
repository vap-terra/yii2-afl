<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 27.02.16
 * Time: 23:25
 */

/* @var $this \yii\web\View */
/* @var $model InformationExt */

$this->title = Yii::t('backend/layout', 'Add information');
$this->params['breadcrumbs'][] = [
    'url'=>Yii::$app->urlManager->createUrl(['/backend/information/index']),
    'label'=>Yii::t('backend/layout', 'Information')
];
$this->params['breadcrumbs'][] = Yii::t('backend/layout', 'Add');

if( !empty( $result ) ) {
    echo \app\widgets\AlertActionResult::widget(['result'=>$result]);
}

echo $this->render(
    'information-form',
    [
        'model'=>$model
    ]
);
?>