<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 28.02.16
 * Time: 16:00
 */
use app\models\ext\FormExt;
use yii\bootstrap\ActiveForm;
use yii\web\View;

/* @var $this \yii\web\View */
/* @var $form ActiveForm */
/* @var $model FormExt */

\app\assets\Select2Asset::register($this);
\app\assets\IOS7SwitchAsset::register($this);
\app\assets\TinymceAsset::register($this);
//\app\assets\DropzoneAsset::register($this);

\app\assets\FormFormBackendAsset::register($this);
$this->registerJs("
    $(document).ready( function() {
        $('.form-fields-container').FormControllerBackend();
    });", View::POS_END, 'form-init');

$form = ActiveForm::begin(
    [
        'id' => 'form-form',
        'action' => Yii::$app->urlManager->createUrl(['/backend/form/save','id'=>$model->id]),
        'requiredCssClass' => 'has-required',
        'options' => [
            'enctype' => 'multipart/form-data',
            'class' => 'form-horizontal form-bordered',
        ],
        'fieldConfig' => [
            'template' => "

                                    {label}\n
                                    <div class=\"col-md-6\">
                    {input}\n
                    {hint}\n
                    {error}\n
                                    </div>

",
            'labelOptions' => [
                'class' => 'col-md-3 control-label'
            ],
        ],
    ]
);
?>
<section class="panel panel-featured panel-featured-dark">
    <header class="panel-heading panel-heading-transparent">
        <div class="panel-actions">
            <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
            <!-- <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a> -->
        </div>

        <h2 class="panel-title">Основное</h2>
        <!-- <p class="panel-subtitle"></p> -->
    </header>
    <div class="panel-body">

        <?php
        echo $form->field($model, 'active', [
                'template' => "
                {label}\n
                <div class=\"col-md-6\">
                    <div class=\"switch switch-sm switch-success\">
                        {input}\n
                    </div>
                    {hint}\n
                    {error}\n
                </div>",
                'labelOptions' => [
                    'class' => 'col-md-3 control-label'
                ],

            ]
        )->checkbox(
            [
                'data-plugin-ios-switch' => ""

            ],
            false
        );
        ?>

        <?php echo $form->field( $model, 'tid');?>

        <?php echo $form->field( $model, 'name');?>

        <?php echo $form->field( $model, 'position');?>

        <?php echo $form->field( $model, 'css_class');?>

        <?php echo $form->field( $model, 'css_icon');?>

        <?php

        echo $form->field( $model, 'description',
            [
                'template' => "
                {label}\n
                <div class=\"col-md-12\">
                    {input}\n
                    {hint}\n
                    {error}\n
                </div>",
                'labelOptions' => [
                    'class' => 'col-md-12 control-label'
                ],
            ]
        )->textarea(
            [
                'rows' => 6,
                'style'=>'width:100%;height:150px;',
                'class'=>'textarea-editor-tinymce',
                'data-plugin-tinymce'=>'',
                'plugin-options' => '',
            ]
        );
        ?>

        <div class="text-center">
            <button class="btn btn-success btn-lg">
                <i class="fa <?php echo ($model->isNewRecord)?' fa-plus':' fa-cog';?>"></i> <?php echo Yii::t('backend/layout',($model->isNewRecord)?'Save':'Update'); ?>
            </button>
        </div>
    </div>
</section>

<section class="panel panel-featured panel-featured-dark panel-collapsed">
    <header class="panel-heading panel-heading-transparent">
        <div class="panel-actions">
            <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
            <!-- <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a> -->
        </div>

        <h2 class="panel-title">Поля формы</h2>
        <!-- <p class="panel-subtitle"></p> -->
    </header>
    <div class="panel-body">

        <?php if (!$model->isNewRecord) { ?>
            <div class="form-fields-container">
                <div class="form-fields">
                    <?php
                    foreach ($model->formElements as $element) {
                        echo $this->render(
                            '../form-element/form-element-form-inner',
                            [
                                'model' => $element,
                                'form' => $model,
                                'activeForm' => $form,
                            ]
                        );
                    }
                    ?>
                </div>
                <button type="submit" class="btn btn-success btn-sm add-form-field"
                        data-href="<?php echo Yii::$app->urlManager->createUrl(['/backend/form-element/create', 'id' => $model->id]) ?>">
                    Добавить
                </button>
            </div>
        <?php } ?>

        <?php /* <div class="text-center">
            <button class="btn btn-success btn-lg">
                <i class="fa <?php echo ($model->isNewRecord)?' fa-plus':' fa-cog';?>"></i> <?php echo Yii::t('backend/layout',($model->isNewRecord)?'Save':'Update'); ?>
            </button>
        </div> */ ?>
    </div>
</section>

<section class="panel panel-featured panel-featured-primary panel-collapsed">
    <header class="panel-heading panel-heading-transparent">
        <div class="panel-actions">
            <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
            <!-- <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a> -->
        </div>

        <h2 class="panel-title">Оповещения</h2>
        <p class="panel-subtitle"></p>
    </header>
    <div class="panel-body">
        <div class="hind">
            поддерживаемые теги:
            <b>{datetime}</b> - дата и время<br/>
            <b>{date}</b> - дата<br/>
            <b>{time}</b> - время<br/>
            <b>{domain}</b> - имя домена<br/>
            <b>{form_id}</b> - идентификатор формы<br/>
            <b>{form_name}</b> - название формы<br/>
            <b>{Тег(только латиница) поля формы}</b> - значение переданное в поле формы<br/>
        </div>

        <?php
        echo $form->field($model, 'transliteration_sms', [
                'template' => "
                {label}\n
                <div class=\"col-md-6\">
                    <div class=\"switch switch-sm switch-success\">
                        {input}\n
                    </div>
                    {hint}\n
                    {error}\n
                </div>",
                'labelOptions' => [
                    'class' => 'col-md-3 control-label'
                ],

            ]
        )->checkbox(
            [
                'data-plugin-ios-switch' => ""

            ],
            false
        );
        ?>

        <h1>Куратор</h1>
        <?php
        echo $form->field($model, 'notify_admin_email', [
                'template' => "
                {label}\n
                <div class=\"col-md-6\">
                    <div class=\"switch switch-sm switch-success\">
                        {input}\n
                    </div>
                    {hint}\n
                    {error}\n
                </div>",
                'labelOptions' => [
                    'class' => 'col-md-3 control-label'
                ],

            ]
        )->checkbox(
            [
                'data-plugin-ios-switch' => ""

            ],
            false
        );
        ?>
        <?php echo $form->field( $model, 'email');?>
        <?php echo $form->field( $model, 'subject_admin');?>
        <?php echo $form->field( $model, 'template_admin')->dropDownList(['standard'=>'Стандартный']);?>
        <?php
        echo $form->field($model, 'notify_admin_sms', [
                'template' => "
                {label}\n
                <div class=\"col-md-6\">
                    <div class=\"switch switch-sm switch-success\">
                        {input}\n
                    </div>
                    {hint}\n
                    {error}\n
                </div>",
                'labelOptions' => [
                    'class' => 'col-md-3 control-label'
                ],

            ]
        )->checkbox(
            [
                'data-plugin-ios-switch' => ""

            ],
            false
        );
        ?>
        <?php echo $form->field( $model, 'phone');?>
        <?php echo $form->field( $model, 'sms_admin_text');?>


        <h1>Пользователь</h1>
        <?php
        echo $form->field($model, 'notify_user_email', [
                'template' => "
                {label}\n
                <div class=\"col-md-6\">
                    <div class=\"switch switch-sm switch-success\">
                        {input}\n
                    </div>
                    {hint}\n
                    {error}\n
                </div>",
                'labelOptions' => [
                    'class' => 'col-md-3 control-label'
                ],

            ]
        )->checkbox(
            [
                'data-plugin-ios-switch' => ""

            ],
            false
        );
        ?>
        <?php echo $form->field( $model, 'field_email');?>
        <?php echo $form->field( $model, 'subject_user');?>
        <?php echo $form->field( $model, 'template_user')->dropDownList(['standard'=>'Стандартный']);?>
        <?php
        echo $form->field($model, 'notify_user_sms', [
                'template' => "
                {label}\n
                <div class=\"col-md-6\">
                    <div class=\"switch switch-sm switch-success\">
                        {input}\n
                    </div>
                    {hint}\n
                    {error}\n
                </div>",
                'labelOptions' => [
                    'class' => 'col-md-3 control-label'
                ],

            ]
        )->checkbox(
            [
                'data-plugin-ios-switch' => ""

            ],
            false
        );
        ?>
        <?php echo $form->field( $model, 'field_phone');?>
        <?php echo $form->field( $model, 'sms_user_text');?>

        <div class="text-center">
            <button class="btn btn-success btn-lg">
                <i class="fa <?php echo ($model->isNewRecord)?' fa-plus':' fa-cog';?>"></i> <?php echo Yii::t('backend/layout',($model->isNewRecord)?'Save':'Update'); ?>
            </button>
        </div>
    </div>
    <!-- <div class="panel-footer"></div> -->
</section>

<section class="panel panel-featured panel-featured-primary panel-collapsed">
    <header class="panel-heading panel-heading-transparent">
        <div class="panel-actions">
            <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
            <!-- <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a> -->
        </div>

        <h2 class="panel-title">Статистика</h2>
        <!-- <p class="panel-subtitle"></p> -->
    </header>
    <div class="panel-body">

        <?php
        echo $form->field($model, 'target_separately', [
                'template' => "
                {label}\n
                <div class=\"col-md-6\">
                    <div class=\"switch switch-sm switch-success\">
                        {input}\n
                    </div>
                    {hint}\n
                    {error}\n
                </div>",
                'labelOptions' => [
                    'class' => 'col-md-3 control-label'
                ],

            ]
        )->checkbox(
            [
                'data-plugin-ios-switch' => ""

            ],
            false
        );
        ?>


        <?php echo $form->field( $model, 'prefix_target_button');?>

        <?php echo $form->field( $model, 'prefix_target_success');?>

        <?php
        echo $form->field($model, 'use_yandex_target_success', [
                'template' => "
                {label}\n
                <div class=\"col-md-6\">
                    <div class=\"switch switch-sm switch-success\">
                        {input}\n
                    </div>
                    {hint}\n
                    {error}\n
                </div>",
                'labelOptions' => [
                    'class' => 'col-md-3 control-label'
                ],

            ]
        )->checkbox(
            [
                'data-plugin-ios-switch' => ""

            ],
            false
        );
        ?>
        <?php
        echo $form->field($model, 'use_yandex_target_button', [
                'template' => "
                {label}\n
                <div class=\"col-md-6\">
                    <div class=\"switch switch-sm switch-success\">
                        {input}\n
                    </div>
                    {hint}\n
                    {error}\n
                </div>",
                'labelOptions' => [
                    'class' => 'col-md-3 control-label'
                ],

            ]
        )->checkbox(
            [
                'data-plugin-ios-switch' => ""

            ],
            false
        );
        ?>

        <?php
        echo $form->field($model, 'use_google_target_success', [
                'template' => "
                {label}\n
                <div class=\"col-md-6\">
                    <div class=\"switch switch-sm switch-success\">
                        {input}\n
                    </div>
                    {hint}\n
                    {error}\n
                </div>",
                'labelOptions' => [
                    'class' => 'col-md-3 control-label'
                ],

            ]
        )->checkbox(
            [
                'data-plugin-ios-switch' => ""

            ],
            false
        );
        ?>
        <?php
        echo $form->field($model, 'use_google_target_button', [
                'template' => "
                {label}\n
                <div class=\"col-md-6\">
                    <div class=\"switch switch-sm switch-success\">
                        {input}\n
                    </div>
                    {hint}\n
                    {error}\n
                </div>",
                'labelOptions' => [
                    'class' => 'col-md-3 control-label'
                ],

            ]
        )->checkbox(
            [
                'data-plugin-ios-switch' => ""

            ],
            false
        );
        ?>


        <div class="text-center">
            <button class="btn btn-success btn-lg">
                <i class="fa <?php echo ($model->isNewRecord)?' fa-plus':' fa-cog';?>"></i> <?php echo Yii::t('backend/layout',($model->isNewRecord)?'Save':'Update'); ?>
            </button>
        </div>

    </div>
</section>

<?php ActiveForm::end(); ?>
<!-- </div>
</section> -->

