<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 12.02.16
 * Time: 13:22
 */
use app\models\ext\FormCompletedExt;
use yii\helpers\Html;
use yii\grid\GridView;
/* @var $form \app\models\ext\FormExt */

$model = new FormCompletedExt();

$this->params['breadcrumbs'][] = Yii::t('backend/layout', 'Completed forms');
if( !empty( $form ) ){
    $this->title = Yii::t('backend/layout', 'Completed forms').(!empty($form)?' "'.$form->name.'"':'');
    $this->params['breadcrumbs'][] = $form->name;
}
?>

<?php /* <div class="inner-toolbar clearfix">
    <ul>
        <li>
            <a href="<?php echo Yii::$app->urlManager->createUrl(['/backend/form-completed/add'])?>">
                <i class="fa fa-plus"></i>
                <span><?php echo Yii::t('backend/layout','Add formCompleted' );?></span>
            </a>
        </li>
    </ul>
</div> */ ?>

<?php
echo GridView::widget([
    'dataProvider' => $formCompletedDataProvider,
    //'filterModel' => $formCompletedSearchModel,
    'columns' => [
        [
            'class' => 'yii\grid\SerialColumn'
        ],
        'id',
        'form_id' => [
            'class' => 'yii\grid\DataColumn',
            'headerOptions' => ['width' => '100'],
            'value' => function ($data) {
                /* @var $data \app\models\ext\FormCompletedExt */
                return $data->form->name;
            },
            'label' => $model->getAttributeLabel('form_id'),
            'format' => 'html',
        ],
        'dt_creation',
        'dt_view',
        'ip',
        'elements' => [
            'class' => 'yii\grid\DataColumn',
            'headerOptions' => [],
            'value' => function ($data) {
                /* @var $data \app\models\ext\FormCompletedExt */
                return $data->getStrValues();
            },
            'label' => $model->getAttributeLabel('elements'),
            'format' => 'html',
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'headerOptions' => [
                'width' => '80'
            ],
            'template' => '{view} {delete}',
            'buttons' => [
                'view' => function ($url, $model, $key) {
                    $link = Html::a('<span class="glyphicon glyphicon-eye"></span>',$url,[]);
                    return $link;
                },
                'delete' => function ($url, $model, $key) {
                    if( !Yii::$app->user->identity->isRoot && $model->system ) {
                        return '';
                    }
                    $link = Html::a('<span class="glyphicon glyphicon-trash"></span>',$url,[]);
                    return $link;
                },
            ]
        ],
    ]
]);
?>

