<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 12.02.16
 * Time: 13:22
 */
use app\models\ext\CommentExt;
use yii\helpers\Html;
use yii\grid\GridView;
$model = new CommentExt();
?>

<div class="inner-toolbar clearfix">
    <ul>
        <li>
            <a href="<?php echo Yii::$app->urlManager->createUrl(['/backend/comment/add'])?>">
                <i class="fa fa-plus"></i>
                <span><?php echo Yii::t('backend/layout','Add comment' );?></span>
            </a>
        </li>
    </ul>
</div>

<?php
echo GridView::widget([
    'dataProvider' => $dataProviderList,
    //'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'dt_creation',
        'id',
        'author_photo'=>[
            'class' => 'yii\grid\DataColumn',
            'value' => function ($data) {
                /* @var $data \app\models\ext\CommentExt */
                if( !empty( $data->author_photo ) ) {
                    return '<img src="' . $data->getImageHref() . '" width="50" height="50" alt=""/>';
                }
                $noPhotoSrc = Yii::$app->storage->getFileHref( 'no-photo-item.png' );
                return '<img src="' . $noPhotoSrc . '" width="50" height="50" alt=""/>';
            },
            'label' => $model->getAttributeLabel('image'),
            'format' => 'html',
        ],
        'author_name',
        'content',
        [
            'class' => 'yii\grid\ActionColumn',
            'headerOptions' => ['width' => '80'],
            'template' => '{changeActive} {edit} {delete}',
            'buttons' => [
                'changeActive' => function ($url, $model, $key) {
                    $url = Yii::$app->urlManager->createUrl(['/backend/comment/change-active','id'=>$model->id]);
                    $link = Html::a('<span class="fa fa-toggle-'.($model->active?'off':'on').'"></span>',$url,[]);
                    return $link;
                },
                'edit' => function ($url, $model, $key) {
                    $link = Html::a('<span class="glyphicon glyphicon-pencil"></span>',$url,[]);
                    return $link;
                },
                'delete' => function ($url, $model, $key) {
                    $link = Html::a('<span class="glyphicon glyphicon-trash"></span>',$url,[]);
                    return $link;
                },
            ]
        ],
    ]
]);
?>

