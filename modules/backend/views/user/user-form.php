<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 28.02.16
 * Time: 16:00
 */

use app\models\ext\LanguageExt;
use app\models\ext\GeoCountryExt;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use app\models\ext\GeoLocalityExt;
use app\models\ext\UserExt;

/* @var $this \yii\web\View */
/* @var $form ActiveForm */
/* @var $model UserExt */

\app\assets\Select2Asset::register($this);
\app\assets\IOS7SwitchAsset::register($this);
//\app\assets\TinymceAsset::register($this);
//\app\assets\DropzoneAsset::register($this);
\app\assets\BootstrapFileuploadAsset::register($this);

$form = ActiveForm::begin(
    [
        'id' => 'board-form',
        'action' => Yii::$app->urlManager->createUrl(['/backend/user/save','id'=>$model->id]),
        'requiredCssClass' => 'has-required',
        'options' => [
            'enctype' => 'multipart/form-data',
            'class' => 'form-horizontal form-bordered',
        ],
        'fieldConfig' => [
            'template' => "

                                    {label}\n
                                    <div class=\"col-md-6\">
                    {input}\n
                    {hint}\n
                    {error}\n
                                    </div>

",
            'labelOptions' => [
                'class' => 'col-md-3 control-label'
            ],
        ],
    ]
);
?>

<section class="panel panel-featured panel-featured-dark">
    <header class="panel-heading panel-heading-transparent">
        <div class="panel-actions">
            <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
            <!-- <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a> -->
        </div>

        <h2 class="panel-title"></h2>
        <!-- <p class="panel-subtitle"></p> -->
    </header>
    <div class="panel-body">

        <?php
        echo $form->field( $model, 'is_blocked',[
                'template' => "
                {label}\n
                <div class=\"col-md-6\">
                    <div class=\"switch switch-sm switch-success\">
                        {input}\n
                    </div>
                    {hint}\n
                    {error}\n
                </div>",
                'labelOptions' => [
                    'class' => 'col-md-3 control-label'
                ],

            ]
        )->checkbox(
            [
                'data-plugin-ios-switch'=>""

            ],
            false
        );
        ?>

        <?php echo $form->field( $model, 'role')->dropDownList(
            UserExt::getOptionsRoleList(),
            [
                'prompt' => ' ... ',
                'onchange'=>'',
                'class'=>'form-control populate',
                'data-plugin-selectTwo' => '',
            ]
        );?>

        <?php echo $form->field( $model, 'language_id', ['options'=>['class'=>'form-group']])->dropDownList(
            LanguageExt::getArOptionList(),
            [
                'prompt' => ' ... ',
                'onchange'=>'',
                'class'=>'form-control populate',
                'data-plugin-selectTwo' => '',
            ]
        );
        ?>

        <?php echo $form->field( $model, 'geo_country_id', ['options'=>['class'=>'form-group']])->dropDownList(
            GeoCountryExt::getArOptionList(),
            [
                'prompt' => ' ... ',
                'onchange'=>'',
                'class'=>'form-control populate',
                'data-plugin-selectTwo' => '',
                'data-linked-element' => '',
                'data-leading-dependent-element'=>'',
                'data-dependent-element' => '#userext-geo_locality_id',
                'data-url' => Yii::$app->urlManager->createUrl(['board/board-item/load-geo-locality-list']),
                'data-parameter' => 'geo_country_id',
                'data-request' => ''
            ]
        );
        ?>

        <?php
        echo $form->field( $model, 'geo_locality_id',['options'=>['class'=>'form-group','style' => ( !$model->geo_country_id )?'display:none':'']])->dropDownList(
            $model->geo_country_id ? GeoLocalityExt::getArOptionList( $model->geo_country_id ) : [],
            [
                'prompt' => ' ... ',
                'onchange'=>'',
                'class'=>'form-control populate',
                'data-plugin-selectTwo' => '',
            ]
        );
        ?>

        <?php echo $form->field( $model, 'login');?>

        <?php echo $form->field( $model, 'phone');?>

        <?php echo $form->field( $model, 'email');?>

        <?php echo $form->field( $model, 'surname');?>

        <?php echo $form->field( $model, 'name');?>

        <?php echo $form->field( $model, 'patronymic');?>

        <?php echo $form->field( $model, 'password')->passwordInput();?>

        <?php echo $form->field( $model, 'repeat_password')->passwordInput();?>

        <?php
        $imgLink = '';
        if( !empty( $model->photo ) && is_file($model->getImagePath()))
        {
            $img = Html::img($model->getImageHref(), ['class'=>'img-responsive img-thumbnail', 'width'=>'100']);
            $imgLink = Html::a($img,$model->getImageHref(),['target'=>'_blank']);
            //$form->field($model,'del_img')->checkBox(['class'=>'span-1']);
        }
        ?>

        <?php
        echo $form->field( $model, 'photo',
            [
                'template' => "
                        {label}\n
                        <div class=\"col-md-9\">
                            <div class=\"col-md-9\">
                            <div class=\"fileupload fileupload-new\" data-provides=\"fileupload\">
                                <div class=\"input-append\">

                                    <div class=\"uneditable-input\">
                                        <i class=\"fa fa-file fileupload-exists\"></i>
                                        <span class=\"fileupload-preview\"></span>
                                    </div>
                                    <span class=\"btn btn-default btn-file\">
                                        <span class=\"fileupload-exists\">".Yii::t('backend/layout','Change')."</span>
                                        <span class=\"fileupload-new\">".Yii::t('backend/layout','Select file')."</span>
                                        {input}\n
                                    </span>
                                    <a href=\"#\" class=\"btn btn-default fileupload-exists\" data-dismiss=\"fileupload\">".Yii::t('backend/layout','Remove')."</a>

                                </div>
                            </div>
                            {hint}\n
                            {error}\n
                            </div>
                            <div class=\"text-center col-md-3\">
                            {$imgLink}
                            </div>
                        </div>",
                'labelOptions' => [
                    'class' => 'col-md-3 control-label'
                ]
            ]
        )->fileInput([]);
        ?>

        <?php echo $form->field( $model, 'gender')->dropDownList(
            UserExt::getOptionsGenderList(),
            [
                'prompt' => ' ... ',
                'onchange'=>'',
                'class'=>'form-control populate',
                'data-plugin-selectTwo' => '',
            ]
        );?>

        <?php /*echo $form->field( $model, 'geo_locality_id')->dropDownList(
            GeoLocalityExt::getArOptionList(),
            [
                'prompt' => ' ... ',
                'onchange'=>'',
                'class'=>'form-control populate',
                'data-plugin-selectTwo' => '',
            ]
        );*/?>

        <div class="text-center">
            <button class="btn btn-success btn-lg">
                <i class="fa <?php echo ($model->isNewRecord)?' fa-plus':' fa-cog';?>"></i> <?php echo Yii::t('backend/layout',($model->isNewRecord)?'Save':'Update'); ?>
            </button>
        </div>
    </div>
</section>

<?php ActiveForm::end(); ?>
<!-- </div>
</section> -->

