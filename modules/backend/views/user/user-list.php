<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 22.02.16
 * Time: 18:31
 */
use app\models\ext\UserExt;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $userDataProvider ActiveDataProvider */
?>
<?php
    echo GridView::widget(
        [
            'dataProvider' => $userDataProvider,
            //'filterModel' => $userSearchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'id',
                [
                    'class' => 'yii\grid\DataColumn',
                    'value' => function ($data) {
                        /* @var $data UserExt */
                        if( !empty( $data->photo ) ) {
                            return '<img src="' . $data->getImageHref() . '" width="50" height="50" alt=""/>';
                        }
                        $noPhotoSrc = Yii::$app->storage->getFileHref( ($data->gender==1) ? 'no-photo-male.jpg' : 'no-photo-female.jpg' );
                        return '<img src="' . $noPhotoSrc . '" width="50" height="50" alt=""/>';
                    },
                    'label' => 'Фото',
                    'format' => 'html',
                ],
                'login',
                'fullName',
                [
                    'class' => 'yii\grid\DataColumn',
                    'value' => function ($data) {
                        /* @var $data UserExt */
                        return '<a href="tel:'.$data->phone.'">'.$data->phone. '</a>'.
                        '<br/><a href="mailto:'.$data->email.'">' . $data->email.'</a>'.
                        '<br/>'.$data->geoLocality->name;
                    },
                    'label' => 'Контакты',
                    'format' => 'html',
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'headerOptions' => ['width' => '80'],
                    'template' => '{edit} {delete}',
                    'buttons' => [
                        'edit' => function ($url, $model, $key) {
                            $link = Html::a('<span class="glyphicon glyphicon-pencil"></span>',$url,[]);
                            return $link;
                        },
                        'delete' => function ($url, $model, $key) {
                            $link = Html::a('<span class="glyphicon glyphicon-trash"></span>',$url,[]);
                            return $link;
                        },
                    ]
                ],
            ]
        ]
    );
?>
