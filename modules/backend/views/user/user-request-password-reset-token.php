<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model PasswordResetRequestForm */

use app\models\forms\PasswordResetRequestForm;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Request password reset';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="panel panel-sign">


    <div class="panel-title-sign mt-xl text-right">
        <h2 class="title text-uppercase text-weight-bold m-none"><i class="fa fa-user mr-xs"></i> <?php echo Yii::t('backend/layout','Request password reset'); ?></h2>
    </div>

    <div class="panel-body">


        <p>Please fill out your email. A link to reset password will be sent there.</p>

        <?php
        $form = ActiveForm::begin(
            [
                'id' => 'request-password-reset-form',
                'options' => [
                    'enctype' => 'multipart/form-data',
                    'class' => 'form-horizontal'
                ],
                'requiredCssClass' => 'has-required',
                'fieldConfig' => [
                    'template' => "
                {label}\n
                <div class=\"input-group input-group-icon\">
                    {input}\n
                    {hint}
                </div>
                {error}\n
",
                    'labelOptions' => ['class' => ''],
                ],
            ]
        );
        ?>

        <?php echo $form->field(
            $model,
            'email',
            [
                'options' => [
                    'class' => 'form-group mb-lg',
                ],
                'labelOptions' => [
                    'class' => '',
                ],
                'inputOptions' => [
                    'class' => 'form-control input-lg',
                    'tabindex' => 1,
                ],
                'hintOptions' => [
                    'class' => 'input-group-addon',
                    'tag' => 'span',
                ]
            ]
        )->hint(
            "<span class=\"icon icon-lg\">
                    <i class=\"fa fa-envelope\"></i>
                </span>
                "
        );
        ?>
        <div class="row">

            <div class="col-sm-8">

            </div>

            <div class="col-sm-4 text-right">
                <?= Html::submitButton(Yii::t('backend/layout','Reset'), ['class' => 'btn btn-primary hidden-xs', 'type' => 'submit', 'name' => 'sign-in','tabindex' => 5]) ?>
                <?= Html::submitButton(Yii::t('backend/layout','Reset'), ['class' => 'btn btn-primary btn-block btn-lg visible-xs mt-lg', 'type' => 'submit', 'name' => 'sign-in-v','tabindex' => 6]) ?>
            </div>


        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
