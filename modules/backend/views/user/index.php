<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 22.02.16
 * Time: 13:45
 */
use app\models\ext\UserExt;

/* @var $arUserDataProviders ActiveDataProvider[] */
/* @var $model UserExt */
/* @var $arRoles [] @see UserExt::getOptionsRoleList()*/

$this->title = Yii::t('backend/layout', 'Users');
$this->params['breadcrumbs'][] = Yii::t('backend/layout', 'Users');
?>
<div class="row">
    <div class="col-md-8">
        <div class="tabs tabs-dark">

            <ul class="nav nav-tabs">
                <?php
                foreach( $arRoles as $key => $item ) {
                ?>
                <li class="<?php if( $key == UserExt::EMPLOYEE ){ ?>active<?php }?>">
                    <a href="#tab_<?php echo $key?>" data-toggle="tab"> <?php echo $item; ?></a>
                </li>
                <?php
                }
                ?>
                <li class="">
                    <a href="#tab_add_user" data-toggle="tab"><i class="fa fa-plus"></i> <?php echo Yii::t('backend/layout', 'add user')?></a>
                </li>
            </ul>
            <div class="tab-content">
                <?php
                foreach( $arRoles as $key => $item ) {
                ?>
                <div id="tab_<?php echo $key?>" class="tab-pane<?php if( $key == UserExt::EMPLOYEE ){ ?> active<?php }?>">
                    <?php
                    if( !empty( $arUserDataProviders[$key] ) ) {
                        echo $this->render(
                            'user-list',
                            [
                                'userDataProvider' => $arUserDataProviders[$key],
                            ]
                        );
                    }
                    ?>
                </div>
                <?php } ?>
                <div id="tab_add_user" class="tab-pane">
                    <?php
                        echo $this->render(
                            'user-form',
                            [
                                'model' => $model
                            ]
                        );
                    ?>
                </div>
            </div>
        </div>

        <?php /* <div class="tabs tabs-info">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="ui-elements-tabs.html#popular5" data-toggle="tab"><i class="fa fa-star"></i> Popular</a>
                </li>
                <li>
                    <a href="ui-elements-tabs.html#recent5" data-toggle="tab">Recent</a>
                </li>
            </ul>
            <div class="tab-content">
                <div id="popular5" class="tab-pane active">
                    <p>Popular</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitat.</p>
                </div>
                <div id="recent5" class="tab-pane">
                    <p>Recent</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitat.</p>
                </div>
            </div>
        </div> */ ?>

    </div>
    <div class="col-md-4">

    </div>
</div>
