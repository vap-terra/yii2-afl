<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 28.02.16
 * Time: 15:47
 */
use app\models\ext\UserExt;

/* @var $this \yii\web\View */
/* @var $model UserExt */
/* @var $result array */

$this->title = Yii::t('backend/layout','Change user profile <{name}>',['name'=>$model->name]);
$this->params['breadcrumbs'][] = [
    'url'=>Yii::$app->urlManager->createUrl(['/backend/user/security']),
    'label'=>Yii::t('backend/layout', 'Security')
];
$this->params['breadcrumbs'][] = Yii::t('backend/layout', 'Change');

echo $this->render(
    'user-change-form',
    [
        'model' => $model
    ]
);