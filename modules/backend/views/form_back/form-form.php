<?php
/* @var $this FormController */
/* @var $model FormExt */
/* @var $form ActiveForm */

?>






<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'form-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
        'class' => 'form-horizontal',
    ),

));
?>

<?php echo $form->errorSummary($model); ?>


<div>

    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#basic" aria-controls="home" role="tab" data-toggle="tab">Основное</a>
        </li>
        <li role="presentation"><a href="#fields" aria-controls="messages" role="tab"
                                   data-toggle="tab">Поля формы</a></li>
        <li role="presentation"><a href="#notifications" aria-controls="profile" role="tab" data-toggle="tab">Оповещения</a></li>
        <li role="presentation"><a href="#statistic" aria-controls="messages" role="tab"
                                   data-toggle="tab">Статистика</a></li>
        <li role="presentation"><a href="#addition" aria-controls="messages" role="tab"
                                   data-toggle="tab">Дополнительно</a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="basic">

            <div class="form-group">
                <?php echo $form->labelEx($model, 'name', array('class' => 'control-label')); ?>
                <?php echo $form->textField($model, 'name', array('size' => 60, 'maxlength' => 128, 'class' => 'text form-control')); ?>
                <?php echo $form->error($model, 'name'); ?>
            </div>

            <div class="form-group">
                <?php echo $form->labelEx($model, 'action', array('class' => 'control-label')); ?>
                <?php echo $form->textField($model, 'action', array('size' => 60, 'maxlength' => 128, 'class' => 'text form-control')); ?>
                <?php echo $form->error($model, 'action'); ?>
            </div>

            <div class="form-group">
                <?php echo $form->labelEx($model, 'description', array('class' => 'control-label')); ?>
                <?php echo $form->textArea($model, 'description', array('class' => 'ckeditor form-control')); ?>
                <?php echo $form->error($model, 'description'); ?>
            </div>

            <div class="form-group">
                <?php echo $form->labelEx($model, 'button_title', array('class' => 'control-label')); ?>
                <?php echo $form->textField($model, 'button_title', array('size' => 60, 'maxlength' => 128, 'class' => 'text form-control')); ?>
                <?php echo $form->error($model, 'button_title'); ?>
            </div>

        </div>
        <div role="tabpanel" class="tab-pane" id="fields">



            <?php if( !$model->isNewRecord ){?>
                <div class="form-fields-container">
                    <div class="form-fields">

                        <?php
                        foreach( $model->formElements as $element ){
                            ?>

                            <?php
                            $this->renderPartial(
                                '../formElement/item',
                                [
                                    'model'=>$element,
                                    'form'=>$model,
                                    'activeForm'=>$form,
                                ]
                            );
                            ?>

                            <?php
                        }
                        ?>
                    </div>
                    <button type="submit" class="btn btn-success btn-sm add-form-field" data-href="<?php echo Yii::app()->urlManager->createUrl( '/form/backend/formElement/create',['id'=>$model->id]) ?>">Добавить</button>
                </div>
            <?php } ?>



        </div>
        <div role="tabpanel" class="tab-pane" id="notifications">

            <div class="form-group">
                <div class="inline-block">
                    <label class="checkbox checked margin-right" for="Form_transliteration_sms">
                        <?php echo $form->checkBox($model, 'transliteration_sms'); ?>
                    </label>
                </div>
                <?php echo $form->labelEx($model, 'transliteration_sms', array('class' => 'control-label')); ?>
            </div>

            <h1>Пользователь</h1>
            <div class="form-group">
                <div class="inline-block">
                    <label class="checkbox checked margin-right" for="Form_notify_user_email">
                        <?php echo $form->checkBox($model, 'notify_user_email'); ?>
                    </label>
                </div>
                <?php echo $form->labelEx($model, 'notify_user_email', array('class' => 'control-label')); ?>
            </div>
            <div class="form-group">
                <?php echo $form->labelEx($model, 'field_email', array('class' => 'control-label')); ?>
                <?php echo $form->textField($model, 'field_email', array('size' => 60, 'maxlength' => 128, 'class' => 'text form-control')); ?>
                <?php echo $form->error($model, 'field_email'); ?>
            </div>
            <div class="form-group">
                <?php echo $form->labelEx($model, 'subject_user', array('class' => 'control-label')); ?>
                <?php echo $form->textField($model, 'subject_user', array('size' => 60, 'maxlength' => 128, 'class' => 'text form-control')); ?>
                <?php echo $form->error($model, 'subject_user'); ?>
            </div>
            <div class="form-group">
                <?php echo $form->labelEx($model, 'template_user', array('class' => 'control-label')); ?>
                <?php echo $form->textField($model, 'template_user', array('size' => 60, 'maxlength' => 128, 'class' => 'text form-control')); ?>
                <?php echo $form->error($model, 'template_user'); ?>
            </div>

            <div class="form-group">
                <div class="inline-block">
                    <label class="checkbox checked margin-right" for="Form_notify_user_sms">
                        <?php echo $form->checkBox($model, 'notify_user_sms'); ?>
                    </label>
                </div>
                <?php echo $form->labelEx($model, 'notify_user_sms', array('class' => 'control-label')); ?>
            </div>
            <div class="form-group">
                <?php echo $form->labelEx($model, 'field_phone', array('class' => 'control-label')); ?>
                <?php echo $form->textField($model, 'field_phone', array('size' => 60, 'maxlength' => 128, 'class' => 'text form-control')); ?>
                <?php echo $form->error($model, 'field_phone'); ?>
            </div>


            <h1>Куратор</h1>
            <div class="form-group">
                <div class="inline-block">
                    <label class="checkbox checked margin-right" for="Form_notify_admin_email">
                        <?php echo $form->checkBox($model, 'notify_admin_email'); ?>
                    </label>
                </div>
                <?php echo $form->labelEx($model, 'notify_admin_email', array('class' => 'control-label')); ?>
            </div>
            <div class="form-group">
                <?php echo $form->labelEx($model, 'email', array('class' => 'control-label')); ?>
                <?php echo $form->textField($model, 'email', array('size' => 60, 'maxlength' => 128, 'class' => 'text form-control')); ?>
                <?php echo $form->error($model, 'email'); ?>
            </div>
            <div class="form-group">
                <?php echo $form->labelEx($model, 'subject_admin', array('class' => 'control-label')); ?>
                <?php echo $form->textField($model, 'subject_admin', array('size' => 60, 'maxlength' => 128, 'class' => 'text form-control')); ?>
                <?php echo $form->error($model, 'subject_admin'); ?>
            </div>
            <div class="form-group">
                <?php echo $form->labelEx($model, 'template_admin', array('class' => 'control-label')); ?>
                <?php echo $form->textField($model, 'template_admin', array('size' => 60, 'maxlength' => 128, 'class' => 'text form-control')); ?>
                <?php echo $form->error($model, 'template_admin'); ?>
            </div>


            <div class="form-group">
                <div class="inline-block">
                    <label class="checkbox checked margin-right" for="Form_notify_admin_sms">
                        <?php echo $form->checkBox($model, 'notify_admin_sms'); ?>
                    </label>
                </div>
                <?php echo $form->labelEx($model, 'notify_admin_sms', array('class' => 'control-label')); ?>
            </div>
            <div class="form-group">
                <?php echo $form->labelEx($model, 'phone', array('class' => 'control-label')); ?>
                <?php echo $form->textField($model, 'phone', array('size' => 60, 'maxlength' => 128, 'class' => 'text form-control')); ?>
                <?php echo $form->error($model, 'phone'); ?>
            </div>

        </div>
        <div role="tabpanel" class="tab-pane" id="statistic">

            <div class="form-group">
                <div class="inline-block">
                    <label class="checkbox checked margin-right" for="Form_target_separately">
                        <?php echo $form->checkBox($model, 'target_separately'); ?>
                    </label>
                </div>
                <?php echo $form->labelEx($model, 'target_separately', array('class' => 'control-label')); ?>
            </div>
            <div class="form-group">
                <?php echo $form->labelEx($model, 'prefix_target_button', array('class' => 'control-label')); ?>
                <?php echo $form->textField($model, 'prefix_target_button', array('size' => 60, 'maxlength' => 128, 'class' => 'text form-control')); ?>
                <?php echo $form->error($model, 'prefix_target_button'); ?>
            </div>
            <div class="form-group">
                <?php echo $form->labelEx($model, 'prefix_target_success', array('class' => 'control-label')); ?>
                <?php echo $form->textField($model, 'prefix_target_success', array('size' => 60, 'maxlength' => 128, 'class' => 'text form-control')); ?>
                <?php echo $form->error($model, 'prefix_target_success'); ?>
            </div>

            <div class="form-group">
                <div class="inline-block">
                    <label class="checkbox checked margin-right" for="Form_use_yandex_target_success">
                        <?php echo $form->checkBox($model, 'use_yandex_target_success'); ?>
                    </label>
                </div>
                <?php echo $form->labelEx($model, 'use_yandex_target_success', array('class' => 'control-label')); ?>
            </div>
            <div class="form-group">
                <div class="inline-block">
                    <label class="checkbox checked margin-right" for="Form_use_yandex_target_button">
                        <?php echo $form->checkBox($model, 'use_yandex_target_button'); ?>
                    </label>
                </div>
                <?php echo $form->labelEx($model, 'use_yandex_target_button', array('class' => 'control-label')); ?>
            </div>
            <div class="form-group">
                <div class="inline-block">
                    <label class="checkbox checked margin-right" for="Form_use_google_target_success">
                        <?php echo $form->checkBox($model, 'use_google_target_success'); ?>
                    </label>
                </div>
                <?php echo $form->labelEx($model, 'use_google_target_success', array('class' => 'control-label')); ?>
            </div>
            <div class="form-group">
                <div class="inline-block">
                    <label class="checkbox checked margin-right" for="Form_use_google_target_button">
                        <?php echo $form->checkBox($model, 'use_google_target_button'); ?>
                    </label>
                </div>
                <?php echo $form->labelEx($model, 'use_google_target_button', array('class' => 'control-label')); ?>
            </div>


        </div>
        <div role="tabpanel" class="tab-pane" id="addition">

            <div class="form-group">
                <?php echo $form->labelEx($model, 'css_class', array('class' => 'control-label')); ?>
                <?php echo $form->textField($model, 'css_class', array('size' => 60, 'maxlength' => 128, 'class' => 'text form-control')); ?>
                <?php echo $form->error($model, 'css_class'); ?>
            </div>

        </div>

    </div>

</div>


<div class="text-center">
    <div class="btn-group">
        <button type="submit" name="save" value="1" class="btn btn-success btn-lg">Сохранить</button>
        <button type="submit" name="save_and_return" value="1" class="btn btn-primary btn-lg">Сохранить и продолжить</button>
    </div>
</div>

<?php $this->endWidget(); ?>


