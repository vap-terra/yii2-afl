<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 28.02.16
 * Time: 16:00
 */
use app\models\ext\BlockExt;
use app\models\ext\ConstantExt;
use yii\bootstrap\ActiveForm;

/* @var $this \yii\web\View */
/* @var $form ActiveForm */
/* @var $model ConstantExt */

\app\assets\Select2Asset::register($this);
\app\assets\IOS7SwitchAsset::register($this);
\app\assets\TinymceAsset::register($this);
//\app\assets\DropzoneAsset::register($this);

$form = ActiveForm::begin(
    [
        'id' => 'board-form',
        'action' => Yii::$app->urlManager->createUrl(['/backend/block/save','id'=>$model->id]),
        'requiredCssClass' => 'has-required',
        'options' => [
            'enctype' => 'multipart/form-data',
            'class' => 'form-horizontal form-bordered',
        ],
        'fieldConfig' => [
            'template' => "

                                    {label}\n
                                    <div class=\"col-md-6\">
                    {input}\n
                    {hint}\n
                    {error}\n
                                    </div>

",
            'labelOptions' => [
                'class' => 'col-md-3 control-label'
            ],
        ],
    ]
);
?>

<section class="panel panel-featured panel-featured-dark">
    <header class="panel-heading panel-heading-transparent">
        <div class="panel-actions">
            <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
            <!-- <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a> -->
        </div>

        <h2 class="panel-title"></h2>
        <!-- <p class="panel-subtitle"></p> -->
    </header>
    <div class="panel-body">

        <?php
            echo $form->field($model, 'active', [
                    'template' => "
                {label}\n
                <div class=\"col-md-6\">
                    <div class=\"switch switch-sm switch-success\">
                        {input}\n
                    </div>
                    {hint}\n
                    {error}\n
                </div>",
                    'labelOptions' => [
                        'class' => 'col-md-3 control-label'
                    ],

                ]
            )->checkbox(
                [
                    'data-plugin-ios-switch' => ""

                ],
                false
            );
        ?>

        <?php echo $form->field( $model, 'name');?>

        <?php echo $form->field( $model, 'header');?>

        <?php echo $form->field( $model, 'position');?>

        <?php
        echo $form->field( $model, 'type')->dropDownList(
            BlockExt::getArTypesOptionList(),
            [
                'prompt' => ' ... ',
                'onchange'=>'',
                'class'=>'form-control populate',
                'data-plugin-selectTwo' => '',
            ]
        );
        ?>

        <?php echo $form->field( $model, 'widget');?>

        <?php echo $form->field( $model, 'parameters');?>

        <?php echo $form->field( $model, 'template');?>

        <?php

        echo $form->field( $model, 'description',
            [
                'template' => "
                {label}\n
                <div class=\"col-md-12\">
                    {input}\n
                    {hint}\n
                    {error}\n
                </div>",
                'labelOptions' => [
                    'class' => 'col-md-12 control-label'
                ],
            ]
        )->textarea(
            [
                'rows' => 6,
                'style'=>'width:100%;height:150px;',
                'class'=>'textarea-editor-tinymce',
                'data-plugin-tinymce'=>'',
                'plugin-options' => '',
            ]
        );
        ?>

        <?php

        echo $form->field( $model, 'content',
            [
                'template' => "
                {label}\n
                <div class=\"col-md-12\">
                    {input}\n
                    {hint}\n
                    {error}\n
                </div>",
                'labelOptions' => [
                    'class' => 'col-md-12 control-label'
                ],
            ]
        )->textarea(
            [
                'rows' => 6,
                'style'=>'width:100%;height:150px;',
                'class'=>'textarea-editor-tinymce',
                'data-plugin-tinymce'=>'',
                'plugin-options' => '',
            ]
        );
        ?>

        <div class="text-center">
            <button class="btn btn-success btn-lg">
                <i class="fa <?php echo ($model->isNewRecord)?' fa-plus':' fa-cog';?>"></i> <?php echo Yii::t('backend/layout',($model->isNewRecord)?'Save':'Update'); ?>
            </button>
        </div>
    </div>
</section>

<?php ActiveForm::end(); ?>
<!-- </div>
</section> -->

