<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 08.02.16
 * Time: 15:41
 */
use app\components\bootstrap\SortableNav;
use app\models\BaseActiveQuery;
use app\models\ext\MenuExt;


/* @var $this \yii\web\View */
/* @var $oMenu MenuExt */

app\assets\BootstrapConfirmationAsset::register($this);

if( !empty( $oMenu ) ) {
?>
    <section class="panel panel-featured panel-featured-primary panel-transparent">

        <header class="panel-heading">
            <div class="panel-actions">
                <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                <!-- <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a> -->
            </div>
            <h2 class="panel-title"><?php echo $oMenu->title?></h2>
            <!-- <p class="panel-subtitle"></p> -->
        </header>

        <div class="panel-body">
            <?php
            /*$arMenuItems = MenuExt::getArMenuNavItems($oMenu->tid, 0, 0, BaseActiveQuery::ALL);
            if (!empty($arMenuItems)) {*/
                ?>
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                    data-target="#bs-navbar-collapse-<?php echo $oMenu->tid; ?>" aria-expanded="false">
                                <span class="sr-only">Переключить навигацию</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <!-- <a class="navbar-brand" href="#">Brand</a> -->
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-navbar-collapse-<?php echo $oMenu->tid; ?>">


                            <?php
                            echo SortableNav::widget(
                                [
                                    'items' => MenuExt::getArMenuNavItems($oMenu->tid, 0, 0, BaseActiveQuery::ALL),
                                    'route' => '/backend/menu-item/',
                                    'itemRoute' => '/backend/menu-item/index',
                                    'itemRouteParameter' => '',
                                    'options' => [
                                        'class' => 'nav navbar-nav mg-folders nav-horizontal sortable-menu sortable-nested-set',
                                        'data-url' => Yii::$app->urlManager->createUrl(['/backend/menu-item/relocation']),
                                        /*'dropDownCaret' => '',
                                        'dropDownOptions' => [
                                            'class'=>'nav nav-children'
                                        ]*/
                                    ]
                                ]
                            );

                            ?>

                        </div>
                    </div>
                </nav>
                <?php
           /* } */
            ?>
        </div>

        <?php /*<div class="panel-footer">
            <a class="btn btn-primary add-menu-item btn-sm" href="<?php echo Yii::$app->urlManager->createUrl(['/backend/menu-item/index','menu_id'=>$oMenu->id])?>">
                <i class="fa fa-plus-square"></i> <?php echo Yii::t('backend/layout','Add menu item'); ?>
            </a>
        </div>*/ ?>

    </section>
<?php
}
?>