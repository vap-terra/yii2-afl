<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 29.01.16
 * Time: 13:41
 */
use app\components\bootstrap\SortableNav;
use app\models\BaseActiveQuery;
use app\models\ext\MenuExt;
use app\models\ext\MenuItemExt;

use app\models\ext\MenuItemRelationExt;
use app\widgets\Alert;
use yii\bootstrap\Button;
use yii\data\ActiveDataProvider;
use yii\widgets\ListView;


/* @var $this \yii\web\View */
/* @var $dataProviderMenuList ActiveDataProvider */
/* @var $oMenu MenuExt */
/* @var $oMenuItem MenuItemExt */
/* @var $arMenuItemRelations MenuItemRelationExt[] */


$this->title = Yii::t('backend/layout', 'Pages');
$this->params['breadcrumbs'][] = Yii::t('backend/layout', 'Pages');
?>
<section class="">
    <div class="row">

        <div class="inner-body mg-main  col-md-9">

            <div class="inner-toolbar clearfix">

            </div>

            <div class="row mg-files" data-sort-destination data-sort-id="media-gallery">

                <?= Alert::widget() ?>

                <h1>
                    <?php if( $oMenuItem->isNewRecord ){?>
                        Добавление<?php if( !empty($oMenu) ){ ?> в <?php echo $oMenu->title; } ?>
                    <?php } else { ?>
                        Редактирование <?php echo $oMenuItem->name; ?>
                    <?php } ?>
                </h1>

                <?php
                echo $this->render(
                    'menu-item-form',
                    [
                        'oMenuItem' => $oMenuItem,
                        'arMenuItemRelations' => $arMenuItemRelations,
                        'oMenu' => $oMenu,
                    ]
                );
                ?>

            </div>

        </div>

        <menu id="content-menu" class="inner-menu col-md-3" role="menu">
            <div class="nano">
                <div class="nano-content">

                    <div class="inner-menu-content">

                        <?php if( !empty($oMenu) ){ ?>
                            <a href="<?php echo Yii::$app->urlManager->createUrl(['/backend/menu-item/index','menu_id'=>$oMenu->id])?>" class="btn btn-block btn-primary btn-md pt-sm pb-sm text-md">
                                <i class="fa fa-plus mr-xs"></i>
                                <?php echo Yii::t('backend/layout','Add menu item' );?>
                            </a>

                            <hr class="separator" />
                            <div class="sidebar-widget m-none">
                                <div class="widget-header clearfix">
                                    <h6 class="title pull-left mt-xs"><?php echo Yii::t('backend/layout','Menu items' );?></h6>
                                    <div class="pull-right"></div>
                                </div>

                                <div class="widget-content">
                                    <?php
                                    echo SortableNav::widget(
                                        [
                                            'items' => MenuExt::getArMenuNavItems($oMenu->tid, 0, 0, BaseActiveQuery::ALL),
                                            'route' => '/backend/menu-item/',
                                            'itemRoute' => '',
                                            'itemRouteParameter' => '',
                                            'options' => [
                                                'class' => 'mg-folders nav-vertical sortable-nested-set',
                                                'data-url'=>Yii::$app->urlManager->createUrl(['/backend/menu-item/relocation']),
                                                /*'dropDownCaret' => '',
                                                'dropDownOptions' => [
                                                    'class'=>'nav nav-children'
                                                ],*/
                                            ]
                                        ]
                                    );
                                    ?>

                                </div>

                            </div>
                        <?php } ?>


                        <hr class="separator" />
                        <div class="sidebar-widget m-none">
                            <div class="widget-header clearfix">
                                <h6 class="title pull-left mt-xs"><?php echo Yii::t('backend/layout','Menus' );?></h6>
                                <div class="pull-right">
                                    <a href="<?php echo Yii::$app->urlManager->createUrl(['/backend/menu/add'])?>" class="btn btn-dark btn-sm btn-widget-act">
                                        <i class="fa fa-plus mr-xs"></i>
                                        <?php echo Yii::t('backend/layout','Add menu' );?>
                                    </a>
                                </div>
                            </div>
                            <div class="widget-content">
                                <?php
                                echo ListView::widget(
                                    [
                                        'dataProvider' => $dataProviderMenuList,
                                        'itemOptions' => [ 'tag'=>'ul', 'class' => 'mg-folders' ],
                                        'itemView' => '../menu/menu-list-item',
                                        'options' => ['class' => ''],
                                    ]
                                );
                                ?>
                            </div>
                        </div>

                        <hr class="separator" />
                        <div class="sidebar-widget m-none">
                            <div class="widget-content">
                                <?php
                                echo Button::widget([
                                    'tagName' => 'a',
                                    'label' => '<i class="fa fa-code"></i> '.Yii::t('backend/layout','Generate Sitemap XML' ),
                                    'encodeLabel' => false,
                                    'options' => [
                                        'class'=>'btn btn-default btn-sm',
                                        'href'=>Yii::$app->urlManager->createUrl(['/sitemap.xml']),
                                        'target'=>'_blank',
                                    ]
                                ]);
                                ?>
                            </div>
                        </div>

                        <hr class="separator" />

                    </div>
                </div>
            </div>
        </menu>

    </div>
</section>
