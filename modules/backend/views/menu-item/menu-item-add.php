<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 28.02.16
 * Time: 15:47
 */
use app\components\BaseController;
use app\models\ext\ContentExt;
use app\models\ext\MenuItemExt;

/* @var $this \yii\web\View */
/* @var $oMenuItem MenuItemExt */
/* @var $arMenuItems MenuItemExt[] */
/* @var $oMenuItem MenuItemExt */
/* @var $arMenuItemContents ContentExt[] */
/* @var $result array */

$this->title = Yii::t('backend/layout','Add item');
$this->params['breadcrumbs'][] = [
    'url'=>Yii::$app->urlManager->createUrl(['/backend/menu-item/index']),
    'label'=>Yii::t('backend/layout', 'Pages')
];
$this->params['breadcrumbs'][] = Yii::t('backend/layout', 'Add');

if( !empty( $result ) ) {
    echo \app\widgets\AlertActionResult::widget(['result'=>$result]);
}

echo $this->render(
    'menu-item-form-panel',
    [
        'oMenuItem' => $oMenuItem,
        'oMenu' => $oMenu,
        'arMenuItemContents' => $arMenuItemContents,
        'arMenuItemContentBlocks'=>$arMenuItemContentBlocks,
    ]
);

