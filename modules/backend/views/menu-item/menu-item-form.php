<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 08.02.16
 * Time: 15:50
 */

use app\components\web\View;
use app\models\ext\MenuExt;
use app\models\ext\MenuItemExt;
use app\models\ext\MenuItemRelationExt;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $form \yii\bootstrap\ActiveForm */
/* @var $arMenuItems MenuItemExt[] */
/* @var $oMenuItem MenuItemExt */
/* @var $arMenuItemRelations MenuItemRelationExt[]
/* @var $oMenu MenuExt */

$this->registerJs("
    $(document).ready(
        function()
        {
            var \$nameInput = \$('input[name=\"MenuItemExt[name]\"]');
            var \$tidInput = \$('input[name=\"MenuItemExt[tid]\"]');
            if( \$tidInput.val().length == 0 ){
                \$nameInput.on(
                    'keyup',
                    function()
                    {
                        var url = '/backend/menu-item/generate-tid';
                        var data = {
                            text: \$nameInput.val(),
                            type: 'MenuItem'
                        };
                        \$.ajax({url:url,data:data,type:'get',dataType:'json'})
                        .done(function(response){\$tidInput.val(response.tid);})
                        .fail(function(){})
                        .always(function(){});
                    }
                );
            }
            $('.menu-relations').MenuItemControllerBackend();
        }
    );",
    View::POS_END,
    'menu-item-init'
);
\app\assets\Select2Asset::register($this);
\app\assets\IOS7SwitchAsset::register($this);
\app\assets\TinymceAsset::register($this);
//\app\assets\DropzoneAsset::register($this);
\app\assets\BootstrapFileuploadAsset::register($this);
\app\assets\BootstrapDatepickerAsset::register($this);
\app\modules\assets\MenuItemFormBackendAsset::register($this);
?>
<!-- <section class="panel panel-dark">
    <header class="panel-heading">
        <div class="panel-actions">
            <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
        </div>

        <h2 class="panel-title">Страница</h2>
    </header>
    <div class="panel-body"> -->
<?php
$form = ActiveForm::begin(
    [
        'id' => 'menu-item-form',
        'action' => Yii::$app->urlManager->createUrl(['/backend/menu-item/save','id'=>$oMenuItem->id,'menu_id'=>!empty($oMenu)?$oMenu->id:0]),
        'requiredCssClass' => 'has-required',
        'options' => [
            'enctype' => 'multipart/form-data',
            'class' => 'form-horizontal form-bordered'
        ],
        'fieldConfig' => [
            'template' => "

                                    {label}\n
                                    <div class=\"col-md-6\">
                    {input}\n
                    {hint}\n
                    {error}\n
                                    </div>

",
            'labelOptions' => [
                'class' => 'col-md-3 control-label'
            ],
        ],
    ]
);
?>

<section class="panel panel-featured panel-featured-primary panel-collapsed">
    <header class="panel-heading panel-heading-transparent">
        <div class="panel-actions">
            <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
            <!-- <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a> -->
        </div>

        <h2 class="panel-title">Мета теги</h2>
        <p class="panel-subtitle">SEO продвижение</p>
    </header>
    <div class="panel-body">

        <?php echo $form->field( $oMenuItem, 'title'); ?>
        <?php echo $form->field( $oMenuItem, 'meta_keywords')->textarea(['data-role'=>'tagsinput']); ?>
        <?php echo $form->field( $oMenuItem, 'meta_description')->textarea(); ?>

    </div>
    <!-- <div class="panel-footer"></div> -->
</section>

<section class="panel panel-featured panel-featured-primary panel-collapsed">
    <header class="panel-heading panel-heading-transparent">
        <div class="panel-actions">
            <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
            <!-- <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a> -->
        </div>

        <h2 class="panel-title">Внешний вид</h2>
        <!-- <p class="panel-subtitle"></p> -->
    </header>
    <div class="panel-body">

        <?php echo $form->field( $oMenuItem, 'layout')->dropDownList(
            Yii::$app->params['layouts'],
            [
                'onchange'=>'',
                'class'=>'form-control populate',
            ]
        );?>
        <?php echo $form->field( $oMenuItem, 'template')->dropDownList(
            Yii::$app->params['templates'],
            [
                'onchange'=>'',
                'class'=>'form-control populate',
            ]
        );?>

    </div>
</section>

<section class="panel panel-featured panel-featured-dark">
    <header class="panel-heading panel-heading-transparent">
        <div class="panel-actions">
            <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
            <!-- <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a> -->
        </div>

        <h2 class="panel-title">Основное</h2>
        <!-- <p class="panel-subtitle"></p> -->
    </header>
    <div class="panel-body">

        <?php
        if( empty( $oMenu ) ){

            echo $form->field( $oMenuItem, 'menu_id')->dropDownList(
                MenuExt::getArOptionList(),
                [
                    'onchange'=>'',
                    'class'=>'form-control populate',
                ]
            );

            echo $form->field( $oMenuItem, 'menu_item_id')->dropDownList(
                MenuExt::getArItemsOptionListGroupByMenu(),
                [
                    'prompt' => ' НЕТ ',
                    'onchange'=>'',
                    'class'=>'form-control populate',
                    'data-plugin-selectTwo' => '',
                    'groups' => MenuExt::getArOptionListOptgroup(),
                ]
            );

        } else {
            echo $form->field( $oMenuItem, 'menu_id',['template' => '{input}'])->hiddenInput();
            echo $form->field( $oMenuItem, 'menu_item_id')->dropDownList(
                MenuExt::getArItemsOptionList($oMenu->id),
                [
                    'prompt' => ' НЕТ ',
                    'onchange'=>'',
                    'class'=>'form-control populate',
                    'data-plugin-selectTwo' => '',
                    //'groups' => MenuExt::getArOptionListOptgroup(),
                ]
            );
        }
        ?>

        <?php
            echo $form->field( $oMenuItem, 'active',[
                        'template' => "
                {label}\n
                <div class=\"col-md-6\">
                    <div class=\"switch switch-sm switch-success\">
                        {input}\n
                    </div>
                    {hint}\n
                    {error}\n
                </div>",
                        'labelOptions' => [
                            'class' => 'col-md-3 control-label'
                        ],

                    ]
                )->checkbox(
                    [
                        'data-plugin-ios-switch'=>""

                    ],
                    false
                );
        ?>


        <?php echo $form->field( $oMenuItem, 'name');?>
        <?php echo $form->field( $oMenuItem, 'tid');?>

        <?php echo $form->field( $oMenuItem, 'header');?>
        <?php echo $form->field( $oMenuItem, 'sub_header');?>

        <?php
        $imgLink = '';
        if( !empty( $oMenuItem->image ) && is_file($oMenuItem->getImagePath()))
        {
            $img = Html::img($oMenuItem->getImageHref(), ['class'=>'img-responsive img-thumbnail', 'width'=>'100']);
            $imgLink = Html::a($img,$oMenuItem->getImageHref(),['target'=>'_blank']);
            //$form->field($model,'del_img')->checkBox(['class'=>'span-1']);
        }
        ?>

        <?php
        echo $form->field( $oMenuItem, 'image',
            [
                'template' => "
                        {label}\n
                        <div class=\"col-md-9\">
                            <div class=\"col-md-9\">
                            <div class=\"fileupload fileupload-new\" data-provides=\"fileupload\">
                                <div class=\"input-append\">

                                    <div class=\"uneditable-input\">
                                        <i class=\"fa fa-file fileupload-exists\"></i>
                                        <span class=\"fileupload-preview\"></span>
                                    </div>
                                    <span class=\"btn btn-default btn-file\">
                                        <span class=\"fileupload-exists\">".Yii::t('backend/layout','Change')."</span>
                                        <span class=\"fileupload-new\">".Yii::t('backend/layout','Select file')."</span>
                                        {input}\n
                                    </span>
                                    <a href=\"#\" class=\"btn btn-default fileupload-exists\" data-dismiss=\"fileupload\">".Yii::t('backend/layout','Remove')."</a>

                                </div>
                            </div>
                            {hint}\n
                            {error}\n
                            </div>
                            <div class=\"text-center col-md-3\">
                            {$imgLink}
                            </div>
                        </div>",
                'labelOptions' => [
                    'class' => 'col-md-3 control-label'
                ]
            ]
        )->fileInput([]);
        ?>

        <?php /*
        echo $form->field( $oMenuItem, 'image')->fileInput(
            [
                //'class'=>'',
                'multiple' => '',
            ]
        );*/
        ?>
        <div class="text-center">
            <button class="btn btn-success btn-lg"><?php echo Yii::t('backend/layout','Save'); ?></button>
        </div>
    </div>
</section>

<section class="panel panel-featured panel-featured-dark">
    <header class="panel-heading panel-heading-transparent">
        <div class="panel-actions">
            <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
            <!-- <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a> -->
        </div>

        <h2 class="panel-title">Наполнение</h2>
        <!-- <p class="panel-subtitle"></p> -->
    </header>
    <div class="panel-body">
        <div class="menu-relations">
            <div class="menu-item-relation-list sortable">
            <?php
                if( !empty( $arMenuItemRelations ) ){
                    foreach( $arMenuItemRelations as $num=>$oMenuRelation ){
                        echo $this->render(
                            '../menu-item-relation/menu-item-relation-form-inner',
                            [
                                'num' => $num,
                                'oMenuRelation' => $oMenuRelation,
                                'oMenu' => $oMenu,
                                'activeForm' => $form
                            ]
                        );
                    }
                }
            ?>
            </div>
            <?php
            //if( !$oMenuItem->isNewRecord ){
            ?>
            <a href="<?php echo Yii::$app->urlManager->createUrl(['/backend/menu-item-relation/create','menu_item_id'=>$oMenuItem->id])?>" class="add-menu-item-relation">
                <i class="fa fa-plus"></i>
                <span><?php echo Yii::t('backend/layout','Add relation'); ?></span>
            </a>
            <?php
            //}
            ?>
        </div>

    </div>
</section>

    <section class="panel panel-featured panel-featured-dark panel-collapsed">
        <header class="panel-heading panel-heading-transparent">
            <div class="panel-actions">
                <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                <!-- <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a> -->
            </div>

            <h2 class="panel-title">Параметры</h2>
            <!-- <p class="panel-subtitle"></p> -->
        </header>
        <div class="panel-body">
            <?php /* echo $form->field( $oMenuItem, 'type')->dropDownList(
                MenuExt::getMenuItemTypes(),
                [
                    'onchange'=>'',
                    'class'=>'form-control populate',
                ]
            );?>
            <?php echo $form->field( $oMenuItem, 'type_name'); */?>
            <?php echo $form->field( $oMenuItem, 'link');?>
            <?php echo $form->field( $oMenuItem, 'position');?>
        </div>
    </section>


<?php ActiveForm::end(); ?>
<!-- </div>
</section> -->