<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 28.02.16
 * Time: 15:47
 */
use app\components\BaseController;
use app\models\ext\FormExt;

/* @var $this \yii\web\View */
/* @var $model FormExt */
/* @var $result array */

$this->title = Yii::t('backend/layout','Edit item <{name}>',['name'=>$model->name]);
$this->params['breadcrumbs'][] = [
    'url'=>Yii::$app->urlManager->createUrl(['/backend/form/index']),
    'label'=>Yii::t('backend/layout', 'Forms')
];
$this->params['breadcrumbs'][] = Yii::t('backend/layout', 'Edit');

if( !empty( $result ) ) {
    echo \app\widgets\AlertActionResult::widget(['result'=>$result]);
}

echo $this->render(
    'form-form',
    [
        'model' => $model
    ]
);

