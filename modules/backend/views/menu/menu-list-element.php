<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 27.02.16
 * Time: 10:22
 */
use app\models\ext\MenuExt;

/* @var $model MenuExt */
?>
<li>
    <a href="#" class="menu-item"><i class="fa fa-bars"></i> <?php echo $model->title?></a>
    <div class="item-options">
        <a href="<?php echo Yii::$app->urlManager->createUrl(['page/menu/edit','id'=>$model->id])?>"><i class="fa fa-edit"></i></a>
        <a href="<?php echo Yii::$app->urlManager->createUrl(['page/menu/delete','id'=>$model->id])?>" class="text-danger"><i class="fa fa-times"></i></a>
        <a href="#" class="text-primary draggable-handler ui-sortable-handle">
            <i class="fa fa-arrows"></i>
        </a>
    </div>
</li>
