<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 12.02.16
 * Time: 13:22
 */
use yii\helpers\Html;
use yii\grid\GridView;

?>

<div class="inner-toolbar clearfix">
    <ul>
        <li>
            <a href="<?php echo Yii::$app->urlManager->createUrl(['/backend/menu/add'])?>">
                <i class="fa fa-plus"></i>
                <span><?php echo Yii::t('backend/layout','Add menu' );?></span>
            </a>
        </li>
    </ul>
</div>

<?php
echo GridView::widget([
    'dataProvider' => $dataProviderMenus,
    //'filterModel' => $constantSearchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'tid',
        'id',
        'title',
        'active'=>[
            'class' => 'yii\grid\DataColumn',
            'value' => function ($data) { return '<span class="fa '.($data->active?'fa-eye':'fa-eye-slash').'"></span>'; },
            //'label' => 'Активный',
            'format' => 'html',
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'headerOptions' => ['width' => '80'],
            'template' => '{changeActive} {edit} {delete}',
            'buttons' => [
                'changeActive' => function ($url, $model, $key) {
                    $url = Yii::$app->urlManager->createUrl(['/backend/menu/change-active','id'=>$model->id]);
                    $link = Html::a('<span class="fa fa-toggle-'.($model->active?'off':'on').'"></span>',$url,[]);
                    return $link;
                },
                'edit' => function ($url, $model, $key) {
                    $link = Html::a('<span class="glyphicon glyphicon-pencil"></span>',$url,[]);
                    return $link;
                },
                'delete' => function ($url, $model, $key) {
                    $link = Html::a('<span class="glyphicon glyphicon-trash"></span>',$url,[]);
                    return $link;
                },
            ]
        ],
    ]
]);
?>

