<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 27.02.16
 * Time: 10:22
 */
use app\models\ext\MenuExt;

/* @var $model MenuExt */
?>
<li>
    <a href="<?php echo Yii::$app->urlManager->createUrl(['/backend/menu-item/index','menu_id'=>$model->id])?>" class="menu-item">
        <i class="<?php echo !empty($model->css_icon)?$model->css_icon:'fa fa-pagelines'; ?>"></i> <?php echo $model->title?>
    </a>
    <div class="item-options">
        <a href="<?php echo Yii::$app->urlManager->createUrl(['backend/menu/edit','id'=>$model->id])?>"><i class="fa fa-edit"></i></a>
        <a href="<?php echo Yii::$app->urlManager->createUrl(['backend/menu/delete','id'=>$model->id])?>" class="text-danger"><i class="fa fa-times"></i></a>
    </div>
</li>
