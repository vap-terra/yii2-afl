<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 28.02.16
 * Time: 15:47
 */
use app\components\BaseController;
use app\models\ext\MenuExt;


/* @var $this \yii\web\View */
/* @var $model MenuExt */
/* @var $result array */

$this->title = Yii::t('backend/layout','Add item');
$this->params['breadcrumbs'][] = [
    'url'=>Yii::$app->urlManager->createUrl(['/backend/menu/index']),
    'label'=>Yii::t('backend/layout', 'Pages')
];
$this->params['breadcrumbs'][] = Yii::t('backend/layout', 'Add');

if( !empty( $result ) ) {
    echo \app\widgets\AlertActionResult::widget(['result'=>$result]);
}

echo $this->render(
    'menu-form',
    [
        'model' => $model
    ]
);

