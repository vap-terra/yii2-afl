<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 28.02.16
 * Time: 15:47
 */
use app\components\BaseController;
use app\models\ext\ListValueExt;

/* @var $this \yii\web\View */
/* @var $model ListValueExt */
/* @var $result array */

$this->title = Yii::t('backend/layout','Edit list <{name}>',['name'=>$model->name]);
$this->params['breadcrumbs'][] = [
    'url'=>Yii::$app->urlManager->createUrl(['/backend/list/index']),
    'label'=>Yii::t('backend/layout', 'List')
];
$this->params['breadcrumbs'][] = Yii::t('backend/layout', 'Edit');

if( !empty( $result ) ) {
    ?>
    <div class="alert alert-<?php if( $result['status'] == BaseController::SUCCESS ){ ?>success<?php } else {?>danger<?php } ?>" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <p><?php echo $result['message'] ?></p>
        <?php if( !empty( $result['errors'] ) ){?>
            <?php foreach( $result['errors'] as $error ){?>
                <?php if( is_array( $error ) ){ ?>
                    <p><?php echo implode('</p><p>',$error); ?></p>
                <?php } else { ?>
                    <p><?php echo $error; ?></p>
                <?php } ?>
            <?php }?>
        <?php }?>
    </div>
    <?php
}

echo $this->render(
    'list-value-form',
    [
        'model' => $model
    ]
);

