<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 12.02.16
 * Time: 13:22
 */

use app\models\ext\ListValueExt;
use app\widgets\Alert;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\widgets\ListView;

/* @var $this \yii\base\View */
/* @var $oListValue ListValueExt */
/* @var $dataProviderListValueList ActiveDataProvider */
/* @var $paginationListValueList Pagination */
/* @var $dataProviderListValueItemList ActiveDataProvider */
/* @var $paginationListValueItemList Pagination */

$this->title = Yii::t('backend/layout', 'List of values <{name}>',['name'=>(!empty($oListValue)?$oListValue->name:'')]);
$this->params['breadcrumbs'][] = Yii::t('backend/layout', 'Lists of values');
if( !empty($oListValue) ) {
    $this->params['breadcrumbs'][] = $oListValue->name;
}
?>

<section class="">
    <div class="row">

        <div class="inner-body mg-main col-md-9">

            <div class="inner-toolbar clearfix">

            </div>

            <?= Alert::widget() ?>

            <div class="row mg-files" data-sort-destination data-sort-id="media-gallery">
                <?php
                if( !empty( $dataProviderListValueItemList ) ) {
                    echo $this->render(
                        '../list-item/list-value-item-list',
                        [
                            'dataProviderListValueItemList' => $dataProviderListValueItemList,
                            //'paginationListValueItemList' => $paginationListValueItemList
                        ]
                    );
                }
                ?>
            </div>

        </div>
        <menu id="content-menu" class="inner-menu col-md-3" role="menu">
            <div class="nano">
                <div class="nano-content">

                    <div class="inner-menu-content">

                        <?php if( !empty( $oListValue ) ){ ?>
                            <a href="<?php echo Yii::$app->urlManager->createUrl(['/backend/list-item/add','list_value_id'=>$oListValue->id])?>" class="btn btn-block btn-primary btn-md pt-sm pb-sm text-md">
                                <i class="fa fa-plus mr-xs"></i>
                                <?php echo Yii::t('backend/layout','Add list value item' );?>
                            </a>
                        <?php } ?>

                        <hr class="separator" />
                        <div class="sidebar-widget m-none">
                            <div class="widget-header clearfix">
                                <h6 class="title pull-left mt-xs"><?php echo Yii::t('backend/layout','Lists of values' );?></h6>
                                <div class="pull-right">
                                    <a href="<?php echo Yii::$app->urlManager->createUrl(['/backend/list/add'])?>" class="btn btn-dark btn-sm btn-widget-act">
                                        <i class="fa fa-plus mr-xs"></i>
                                        <?php echo Yii::t('backend/layout','Add list' );?>
                                    </a>
                                </div>
                            </div>
                            <div class="widget-content">
                                <?php
                                echo ListView::widget(
                                    [
                                        'dataProvider' => $dataProviderListValueList,
                                        'itemOptions' => [ 'tag'=>'ul', 'class' => 'mg-folders' ],
                                        'itemView' => 'list-value-list-item',
                                        'options' => ['class' => ''],
                                    ]
                                );
                                ?>
                            </div>
                        </div>

                        <hr class="separator" />

                    </div>
                </div>
            </div>
        </menu>

    </div>
</section>


