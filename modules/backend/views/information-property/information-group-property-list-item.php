<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 27.02.16
 * Time: 10:22
 */
use app\components\bootstrap\AdjacencyListView;
use app\models\ext\InformationGroupExt;
use app\models\ext\InformationItemGroupPropertyExt;
use app\models\ext\InformationPropertyExt;

/* @var $model InformationGroupExt */
/* @var $key  */
/* @var $index */
/* @var $widget AdjacencyListView*/
/* @var $content string */
/* @var $owner InformationPropertyExt */

/* @var $oInformationItemGroupProperty \app\models\ext\InformationItemGroupPropertyExt*/
$oInformationItemGroupProperty = InformationItemGroupPropertyExt::findOne(
    [
        'information_group_id' => $model->id,
        'information_property_id' => $owner->id
    ]
);
$titleLinked = Yii::t('backend/layout','select property to group');
$titleRecursive = Yii::t('backend/layout','property apply to group recursive');
$classLinked = 'fa-square-o';
$classRecursive = 'fa-square-o';
if( !empty( $oInformationItemGroupProperty ) ) {
    $classLinked = 'fa-check-square-o';
    $titleLinked = Yii::t('backend/layout','unselect property to group');
    if( $oInformationItemGroupProperty->recursive ) {
        $classRecursive = 'fa-check-square-o';
        $titleRecursive = Yii::t('backend/layout','property cancel to group recursive');
    }
}
?>
    <div class="item-options-1">
        <a href="<?php echo Yii::$app->urlManager->createUrl(['information/information-property/change-select-group','id'=>$owner->id,'information_group_id'=>$model->id])?>" class="text-danger" title="<?php echo $titleLinked;?>"><i class="fa <?php echo $classLinked;?>"></i></a>
        <a href="<?php echo Yii::$app->urlManager->createUrl(['information/information-property/change-recursive','id'=>$owner->id,'information_group_id'=>$model->id])?>" title="<?php echo $titleRecursive;?>"><i class="fa <?php echo $classRecursive;?>"></i></a>
    </div>
    <a href="#" class="menu-item"><?php if($content){?><span class="fa caret-dropdown"></span>  <?php }?><?php echo $model->name?></a>
    <?php echo $content;?>