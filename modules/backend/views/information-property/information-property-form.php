<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 28.02.16
 * Time: 16:00
 */
use app\components\web\View;
use app\models\ext\InformationExt;
use app\models\ext\InformationPropertyExt;
use app\models\ext\ListValueExt;
use yii\bootstrap\ActiveForm;


/* @var $this View */
/* @var $form ActiveForm */
/* @var $model InformationPropertyExt */

\app\assets\Select2Asset::register($this);
\app\assets\IOS7SwitchAsset::register($this);
\app\assets\TinymceAsset::register($this);
//\app\assets\DropzoneAsset::register($this);

$form = ActiveForm::begin(
    [
        'id' => 'information-form',
        'action' => Yii::$app->urlManager->createUrl(['/backend/information-property/save','id'=>$model->id]),
        'requiredCssClass' => 'has-required',
        'options' => [
            'enctype' => 'multipart/form-data',
            'class' => 'form-horizontal form-bordered',
        ],
        'fieldConfig' => [
            'template' => "

                                    {label}\n
                                    <div class=\"col-md-6\">
                    {input}\n
                    {hint}\n
                    {error}\n
                                    </div>

",
            'labelOptions' => [
                'class' => 'col-md-3 control-label'
            ],
        ],
    ]
);
?>

<section class="panel panel-featured panel-featured-dark">
    <header class="panel-heading panel-heading-transparent">
        <div class="panel-actions">
            <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
            <!-- <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a> -->
        </div>

        <h2 class="panel-title">Основное</h2>
        <!-- <p class="panel-subtitle"></p> -->
    </header>
    <div class="panel-body">

        <?php echo $form->field( $model, 'title');?>

        <?php echo $form->field( $model, 'name');?>

        <?php
        echo $form->field($model, 'multiple', [
                'template' => "
                {label}\n
                <div class=\"col-md-6\">
                    <div class=\"switch switch-sm switch-success\">
                        {input}\n
                    </div>
                    {hint}\n
                    {error}\n
                </div>",
                'labelOptions' => [
                    'class' => 'col-md-3 control-label'
                ],

            ]
        )->checkbox(
            [
                'data-plugin-ios-switch' => ""

            ],
            false
        );
        ?>

        <?php
        echo $form->field( $model, 'information_id')->dropDownList(
            InformationExt::getArOptionList(),
            [
                'prompt' => ' ... ',
                'onchange'=>'',
                'class'=>'form-control populate',
                'data-plugin-selectTwo' => '',
            ]
        );
        ?>

        <?php
        echo $form->field( $model, 'type')->dropDownList(
            InformationPropertyExt::getArTypesOptionList(),
            [
                'prompt' => ' ... ',
                'onchange'=>'if( $(this).val()==\''.InformationPropertyExt::TYPE_LIST_VALUE.'\' ){$(\'.field-informationpropertyext-list_value_id\').show();} else {$(\'.field-informationpropertyext-list_value_id\').hide();}',
                'class'=>'form-control populate',
                //'data-plugin-selectTwo' => '',
            ]
        );
        ?>

        <?php
        echo $form->field( $model, 'list_value_id', ['options'=>['style'=> ($model->type !== InformationPropertyExt::TYPE_LIST_VALUE)?'display:none':'']])->dropDownList(
            ListValueExt::getArOptionList(),
            [
                'prompt' => ' ... ',
                'onchange'=>'',
                'class'=>'form-control populate',
                //'data-plugin-selectTwo' => '',
            ]
        );
        ?>

        <div class="text-center">
            <button class="btn btn-success btn-lg">
                <i class="fa <?php echo ($model->isNewRecord)?' fa-plus':' fa-cog';?>"></i> <?php echo Yii::t('backend/layout',($model->isNewRecord)?'Save':'Update'); ?>
            </button>
        </div>
    </div>
</section>

<section class="panel panel-featured panel-featured-primary panel-collapsed">
    <header class="panel-heading panel-heading-transparent">
        <div class="panel-actions">
            <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
            <!-- <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a> -->
        </div>

        <h2 class="panel-title">Список групп для которых применяются</h2>
    </header>
    <div class="panel-body">



    </div>
    <!-- <div class="panel-footer"></div> -->
</section>

<?php ActiveForm::end(); ?>
<!-- </div>
</section> -->

