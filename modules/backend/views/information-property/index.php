<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 10.02.16
 * Time: 9:28
 */
use app\components\bootstrap\AdjacencyListView;
use app\components\web\View;
use app\models\ext\InformationExt;
use app\models\ext\InformationPropertyExt;
use app\widgets\Alert;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\widgets\ListView;


/* @var $this View */
/* @var $oInformationProperty InformationPropertyExt */
/* @var $oInformation InformationExt */
/* @var $dataProviderInformationGroupList ActiveDataProvider */
/* @var $paginationInformationGroupList Pagination */
/* @var $dataProviderInformationPropertyList ActiveDataProvider */


$this->title = Yii::t('backend/layout', 'Select groups for property ').(!empty($oInformationProperty)?' "'.$oInformationProperty->title.'"':'');

$this->params['breadcrumbs'][] = [
    'url'=>Yii::$app->urlManager->createUrl(['information/default/index']),
    'label'=>Yii::t('backend/layout', 'Information')
];

if( !empty($oInformation) ) {
    $this->params['breadcrumbs'][] = [
        'url'=>Yii::$app->urlManager->createUrl(['information/default/index','id'=>$oInformation->id]),
        'label'=>$oInformation->name
    ];
}
$this->params['breadcrumbs'][] = Yii::t('backend/layout', 'Select');
?>

    <section class="content-with-menu content-with-menu-has-toolbar media-gallery">
        <div class="content-with-menu-container">
            <div class="inner-menu-toggle">
                <a href="#" class="inner-menu-expand" data-open="inner-menu">
                    <?php echo Yii::t('backend/layout','Show Bar'); ?> <i class="fa fa-chevron-right"></i>
                </a>
            </div>

            <menu id="content-menu" class="inner-menu" role="menu">
                <div class="nano">
                    <div class="nano-content">

                        <div class="inner-menu-toggle-inside">
                            <a href="#" class="inner-menu-collapse">
                                <i class="fa fa-chevron-up visible-xs-inline"></i><i class="fa fa-chevron-left hidden-xs-inline"></i> <?php echo Yii::t('backend/layout','Hide Bar'); ?>
                            </a>
                            <a href="#" class="inner-menu-expand" data-open="inner-menu">
                                <?php echo Yii::t('backend/layout','Show Bar');?> <i class="fa fa-chevron-down"></i>
                            </a>
                        </div>

                        <div class="inner-menu-content">

                            <?php if( !empty($oInformation) ){ ?>

                                <div class="sidebar-widget m-none">
                                    <div class="widget-header">
                                        <h6 class="title"><?php echo Yii::t('backend/layout','Information Properties');?></h6>
                                        <span class="widget-toggle">+</span>
                                    </div>
                                    <div class="widget-content">
                                        <?php
                                        if( !empty( $dataProviderInformationPropertyList ) ) {
                                            echo ListView::widget(
                                                [
                                                    'dataProvider' => $dataProviderInformationPropertyList,
                                                    'itemOptions' => ['tag' => 'ul', 'class' => 'mg-folders'],
                                                    'itemView' => 'information-property-list-item',
                                                    'options' => ['class' => ''],
                                                ]
                                            );
                                        }
                                        ?>
                                    </div>
                                    <div class="">
                                        <a href="<?php echo Yii::$app->urlManager->createUrl(['information/information-property/add','information_id'=>$oInformation->id])?>" class="btn btn-dark btn-sm btn-widget-act">
                                            <i class="fa fa-plus mr-xs"></i>
                                            <?php echo Yii::t('backend/layout','Add information property' );?>
                                        </a>
                                    </div>
                                </div>

                            <?php } ?>

                        </div>
                    </div>
                </div>
            </menu>
            <div class="inner-body mg-main">


                <div class="inner-toolbar clearfix">
                </div>
                <?= Alert::widget() ?>

                <div class="row mg-files" data-sort-destination data-sort-id="media-gallery">
                    <?php
                    echo AdjacencyListView::widget(
                        [
                            'owner' => $oInformationProperty,
                            'dataProvider' => $dataProviderInformationGroupList,
                            'containerOptions' => ['tag'=>'ul', 'class' => 'mg-folders nav nav-vertical'],
                            'itemOptions' => [ 'tag'=>'li'  ],
                            'subContainerOptions' => ['tag'=>'ul', 'class' => 'nav nav-children'],
                            'subItemOptions' => [ 'tag'=>'li' ],
                            'itemView' => 'information-group-property-list-item',
                            'options' => ['class' => ''],
                        ]
                    );
                    ?>
                </div>

            </div>
        </div>
    </section>

