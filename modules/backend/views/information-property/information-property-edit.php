<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 28.02.16
 * Time: 15:47
 */
use app\components\BaseController;
use app\components\web\View;
use app\models\ext\InformationExt;
use app\models\ext\InformationPropertyExt;

/* @var $this View */
/* @var $model InformationPropertyExt */
/* @var $oInformation InformationExt */
/* @var $result array */

$this->title = Yii::t('backend/layout','Edit information property <{name}>',['name'=>$model->title]);
$this->params['breadcrumbs'][] = [
    'url'=>Yii::$app->urlManager->createUrl(['/backend/information/index']),
    'label'=>Yii::t('backend/layout', 'Information')
];
if( !empty($oInformation) ) {
    $this->params['breadcrumbs'][] = [
        'url' => Yii::$app->urlManager->createUrl(['/backend/information/index', 'id' => $oInformation->id]),
        'label' => $oInformation->name
    ];
}
//$this->params['breadcrumbs'][] = $model->name;
$this->params['breadcrumbs'][] = Yii::t('backend/layout', 'Edit');

if( !empty( $result ) ) {
    echo \app\widgets\AlertActionResult::widget(['result'=>$result]);
}

echo $this->render(
    'information-property-form',
    [
        'model' => $model
    ]
);

