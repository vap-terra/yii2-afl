<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 20.10.16
 * Time: 9:29
 */
/* @var $item [] */
?>
<h1>Меню</h1>

<?php
if( !empty( $item['items'] ) ) {
    foreach ($item['items'] as $subItem) {
        ?>
        <section id="<?php echo $subItem['id'] ?>">
            <?php
            echo $this->render($item['content'].'/'.$subItem['content'], ['item' => $subItem]);
            ?>
        </section>
        <?php
    }
}
?>
