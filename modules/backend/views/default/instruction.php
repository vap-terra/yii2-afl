<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 30.06.16
 * Time: 23:23
 */

use app\components\web\View;

$this->registerJs("
    $(document).ready( function() {
        $('[data-spy=\"scroll\"]').each(function () {
            var \$spy = $(this).scrollspy('refresh');
        });
    });
", View::POS_END, 'instruction-init');

$arContents = [
    [
        'id'=>$id='video-instruction',
        'url'=>'#'.$id,
        'label' => 'Видео инструкция',
        'content' => $id,
    ],
    [
        'id'=>$id='content',
        'url'=>'#'.$id,
        'label' => 'Контент',
        'content' => $id,
        'items' => [
            [
                'id'=>$id='menu',
                'url'=>'#'.$id,
                'label' => 'Меню',
                'content' => $id,
                'items' => [
                    [
                        'id'=>$id='manage-menu',
                        'url'=>'#'.$id,
                        'label' => 'Работа с меню',
                        'content' => $id,
                    ],
                    [
                        'id'=>$id='manage-menu-item',
                        'url'=>'#'.$id,
                        'label' => 'Работа с пунктами меню',
                        'content' => $id,
                    ]
                ]
            ],
        ],
    ],
    [
        'id'=>$id='notifications',
        'url'=>'#'.$id,
        'label' => 'Оповещения',
        'content' => $id,
        'items' => [
        ]
    ],
    [
        'id'=>$id='settings',
        'url'=>'#'.$id,
        'label' => 'Настройки',
        'content' => $id,
        'items' => [
        ],
    ],
    [
        'id'=>$id='useful-links',
        'url'=>'#'.$id,
        'label' => 'Полезные ссылки',
        'content' => $id,
        'items' => [
        ],
    ],
];
?>

<div class="div-body-no-bg grey-not-hover full-screen align-center bottom-shadow" data-spy="scroll" data-target="#navbar-example">
    <div class="row">
        <div class="col-md-9">
            <div class="help-block block-center">
                <?php foreach( $arContents as $item ){ ?>
                    <section id="<?php echo $item['id']?>">
                    <?php
                       echo $this->render('instructions/'.$item['content'],['item'=>$item]);
                    ?>
                    </section>
                <?php } ?>
            </div>
        </div>
        <div class="col-md-3">
            <div id="navbar-example">

                <?php
                    echo \yii\bootstrap\Nav::widget([
                        'options' => ['class' => 'nav'],
                        'items' => $arContents,
                        'activateItems' => true,
                        'activateParents' => true,
                    ]);
                ?>

            </div>
        </div>
    </div>
</div>