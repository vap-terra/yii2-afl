<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 11.10.16
 * Time: 14:22
 */

namespace app\modules\grid\bootstrap;


use app\modules\grid\assets\GridAsset;
use app\modules\grid\bootstrap\columns\InfoPanelColumn;
use yii\helpers\Html;

class GridView extends \yii\grid\GridView
{
    public $routerConfiguration;

    /**
     * @var string the layout that determines how different sections of the list view should be organized.
     * The following tokens will be replaced with the corresponding section contents:
     * - `{filter}`: the . See [[renderFilter()]].
     * - `{summary}`: the summary section. See [[renderSummary()]].
     * - `{errors}`: the filter model error summary. See [[renderErrors()]].
     * - `{items}`: the list items. See [[renderItems()]].
     * - `{sorter}`: the sorter. See [[renderSorter()]].
     * - `{action}`: the actions for grid. See [[renderAction()]].
     * - `{pager}`: the pager. See [[renderPager()]].
     */
    public $layout = "{filter}\n{pager}\n{summary}\n{configurator}\n{items}\n{action}\n{pager}";

    /**
     * Renders the config.
     */
    public function renderConfigurator() {
        $options = [];
        return Html::tag( 'div', ' configurator ',$options);
    }

    /**
     * Renders the actions.
     */
    public function renderAction() {
        $options = [];
        return Html::tag( 'div', ' actions ',$options);
    }

    /**
     * Renders the filter.
     */
    public function renderFilter() {
        $options = [];
        return Html::tag( 'div', ' filter ',$options);
    }

    /**
     * @inheritdoc
     */
    public function renderSection($name)
    {
        switch ($name) {
            case '{filter}':
                return $this->renderFilter();
            case '{action}':
                return $this->renderAction();
            case '{configurator}':
                return $this->renderConfigurator();
            default:
                return parent::renderSection($name);
        }
    }

    /**
     * Renders the data models for the grid view.
     */
    public function renderItems()
    {
        $caption = $this->renderCaption();
        $columnGroup = $this->renderColumnGroup();
        $tableHeader = $this->showHeader ? $this->renderTableHeader() : false;
        $tableBody = $this->renderTableBody();
        $tableFooter = $this->showFooter ? $this->renderTableFooter() : false;
        $content = array_filter([
            $caption,
            $columnGroup,
            $tableHeader,
            $tableFooter,
            $tableBody,
        ]);

        return Html::tag('table', implode("\n", $content), $this->tableOptions);
    }

    /**
     * Renders the table body.
     * @return string the rendering result.
     */
    public function renderTableBody()
    {
        $needSubRow = false;
        foreach( $this->columns as $column ) {
            if(  $column instanceof InfoPanelColumn !==false ) {
                $needSubRow = true;
                break;
            }
        }
        if( !$this->afterRow && $needSubRow ){
            $this->afterRow = function($model, $key, $index, $oGridView) {
                /* @var $oGridView GridView */
                $colspan = count($oGridView->columns);
                return '<tr style="display:none;" class="info-panel-wrapper"><td colspan="'.$colspan.'"></td></tr>';
            };
            GridAsset::register($this->view);
        }
        return parent::renderTableBody();
        /*$models = array_values($this->dataProvider->getModels());
        $keys = $this->dataProvider->getKeys();
        $rows = [];
        foreach ($models as $index => $model) {
            $key = $keys[$index];
            if ($this->beforeRow !== null) {
                $row = call_user_func($this->beforeRow, $model, $key, $index, $this);
                if (!empty($row)) {
                    $rows[] = $row;
                }
            }

            $rows[] = $this->renderTableRow($model, $key, $index);

            if ($this->afterRow !== null) {
                $row = call_user_func($this->afterRow, $model, $key, $index, $this);
                if (!empty($row)) {
                    $rows[] = $row;
                }
            }
        }

        if (empty($rows)) {
            $colspan = count($this->columns);

            return "<tbody>\n<tr><td colspan=\"$colspan\">" . $this->renderEmpty() . "</td></tr>\n</tbody>";
        } else {
            return "<tbody>\n" . implode("\n", $rows) . "\n</tbody>";
        }*/
    }
}