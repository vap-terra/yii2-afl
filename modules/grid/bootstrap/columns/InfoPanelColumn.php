<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 12.10.16
 * Time: 14:12
 */

namespace app\modules\grid\bootstrap\columns;

use Yii;
use yii\bootstrap\Html;
use yii\grid\Column;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class InfoPanelColumn extends Column
{
    public $action = 'view';

    public $controller;

    /**
     * @var callable a callback that creates a button URL using the specified model information.
     * The signature of the callback should be the same as that of [[createUrl()]].
     * If this property is not set, button URLs will be created using [[createUrl()]].
     */
    public $urlCreator;

    /**
     * Renders the data cell content.
     * @param mixed $model the data model
     * @param mixed $key the key associated with the data model
     * @param integer $index the zero-based index of the data model among the models array returned by [[GridView::dataProvider]].
     * @return string the rendering result
     */
    protected function renderDataCellContent($model, $key, $index)
    {
        if ($this->content !== null ) {
            return call_user_func($this->content, $model, $key, $index, $this);
        } else {
            Html::addCssClass($this->options,'toggle-info-panel');
            return Html::a(
                Html::tag('span','',['class'=>'glyphicon glyphicon-chevron-down']),
                $this->createUrl($this->action,$model,$key,$index),
                $this->options
            );
        }
    }

    /**
     * Creates a URL for the given action and model.
     * This method is called for each button and each row.
     * @param string $action the button name (or action ID)
     * @param \yii\db\ActiveRecord $model the data model
     * @param mixed $key the key associated with the data model
     * @param integer $index the current row index
     * @return string the created URL
     */
    public function createUrl($action, $model, $key, $index)
    {
        if (is_callable($this->urlCreator)) {
            return call_user_func($this->urlCreator, $action, $model, $key, $index);
        } else {
            $params = is_array($key) ? $key : ['id' => (string) $key];
            $params[0] = $this->controller ? $this->controller . '/' . $action : $action;

            return Url::toRoute($params);
        }
    }
}