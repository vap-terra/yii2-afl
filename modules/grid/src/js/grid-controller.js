/**
 * Created by volhv on 14.10.16.
 */
(function($){
    'use strict';

    var instanceName = 'GridController';
    var GridController = function ($container, options) {
        return this.initialize($container, options);
    };

    GridController.prototype = {
        options: {},
        xslResponder: null,
        $container: null,
        $rows: null,
        defaults: {},
        initialize: function ($container, options)
        {
            this.$container = $container;
            this
                .setData()
                .setOptions(options)
                .setVars()
                .build()
                .events();
            return this;
        },
        setData: function ()
        {
            this.$container.data(instanceName, this);
            return this;
        },
        setVars: function ()
        {
            var self = this;
            return this;
        },
        setOptions: function (opts)
        {
            this.options = $.extend(true, {}, this.defaults, opts);
            return this;
        },
        build: function ()
        {
            var self = this;
            this.$rows = this.$container.find('tr');
            return this;
        },
        loadData: function(eventName,$element,params)
        {
            var self = this;
            $.ajax(
                params
            ).done(
                function (response, textStatus, jqXHR )
                {
                    var status = 'done';
                    try {
                        response = $.parseJSON(response);
                    } catch( e ){
                        //console.log( e );
                    }
                    if( typeof( response.status ) !== 'undefined' && response.status == 'error' ){
                        status = 'fail';
                        console.log(status + ' ' + eventName);
                    }
                    self.$container.trigger(status+eventName, [$element, response, params]);
                    //$element.trigger(status+eventName, [response, params]);
                }
            ).fail(
                function ( jqXHR, textStatus, errorThrown )
                {
                    self.$container.trigger('fail'+eventName, [$element, jqXHR, params]);
                    //$element.trigger('fail'+eventName);
                }
            ).always(
                function ( data, textStatus, errorThrown ) //data|jqXHR, textStatus, jqXHR|errorThrown
                {
                    self.$container.trigger('always'+eventName, [$element, data, params]);
                    //$element.trigger('always'+eventName, [params]);
                }
            );
        },
        events: function ()
        {
            var self = this;
            /*
            this.$rows.sortable({
                revert: true,
                delay: 300,
                cursor: "move",
                items: "tr",
                stop: function (event, ui) {
                    //var position = self.$list.find('.menu-item-relation').index(ui.item);
                    //self.repositionAction(ui.item, position);
                },
                activate: function (event, ui) {}
            });
            this.$container.on(
                'errorReposition',
                function (event)
                {
                    self.$rows.sortable("cancel");
                }
            );
            */

            this.$rows.each( function(){
                var $row = $(this);
                $row.find('.toggle-info-panel').on('click',function(){

                    try {
                        var $element = $(this);
                        if( $element.hasClass('active') ) {
                            self.hideInfoPanels();
                        } else {
                            self.viewInfoPanel($element);
                        }

                    } catch( e ) {
                        console.log(e);
                    }
                    return false;
                });
            } );
            this.$container.on('doneViewInfoPanel',function(event, $element, response, params){
                self.hideInfoPanels();

                var $row = $element.parents('tr').next();
                $row.find('td').html('');
                $row.find('td').append($(response));
                $row.slideDown(0);
                $element.addClass('active');
                $element.find('> .glyphicon').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
            });
            return this;
        },
        viewInfoPanel: function($element)
        {
            var url = $element.attr('href');
            if (typeof( url ) == 'undefined') {
                url = $element.attr('data-href');
            }
            var params = {
                url: url,
                data: {},
                type: 'get',
            };
            this.loadData('ViewInfoPanel',$element,params);
        },
        showInfoPanel:function($panel){},
        hideInfoPanels:function(){
            this.$container.find('.info-panel-wrapper').slideUp(0);
            this.$container.find('.toggle-info-panel').removeClass('active');
            this.$container.find('.toggle-info-panel > .glyphicon').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
        }
    };


    if (typeof( $.fn.GridController ) == 'undefined')
    {
        $.fn.GridController = function ()
        {
            var options = {};
            if (
                typeof(arguments[0]) !== 'undefined'
                && arguments[0] != null
                && arguments[0] instanceof Object != false
            ) {
                $.extend(options, arguments[0]);
            }

            return $(this).each(
                function ()
                {
                    var $this = $(this);
                    if ($this.data(instanceName)) {
                        return $this.data(instanceName);
                    } else {
                        return new GridController($this,options);
                    }
                }
            );
        }
    }

})(jQuery);
