<?php

namespace app\modules\frontend\controllers;

use app\components\FrontendController;
use app\models\ext\CommentExt;
use app\models\ext\InformationExt;
use app\models\ext\InformationGroupExt;
use app\models\ext\InformationItemExt;
use app\models\ext\MenuItemRelationExt;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\web\NotFoundHttpException;

class CommentController extends FrontendController
{

    /**
     * @param $route
     * @param array $params
     * @return mixed|string
     * @throws NotFoundHttpException
     */
    public function runAsUrl($route, $params = [])
    {
        $this->layout = false;
        return $this->actionIndex();
    }

    public function partOfPage($route, $params = [])
    {
        $this->layout = false;
        return $this->actionIndex();
    }

    public function actionIndex()
    {
        $qItems = CommentExt::find();
        $filter = Yii::$app->request->getQueryParam('filter');

        $countQuery = clone $qItems;
        $pagination = new Pagination(
            [
                'totalCount' => $countQuery->count(),
                //'route'=>,
                'params' => [
                    //'filter' => $filter,
                    'page'=>Yii::$app->request->get('page')
                ]
            ]
        );

        $dataProvider = new ActiveDataProvider([
            'query' => $qItems,
        ]);

        return $this->render(
            'index',
            [
                'dataProviderList'=>$dataProvider,
                'pagination'=>$pagination
            ]
        );
    }

}