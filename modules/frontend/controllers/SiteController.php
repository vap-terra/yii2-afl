<?php
namespace app\modules\frontend\controllers;

use app\components\FrontendController;
use app\models\BaseActiveQuery;
use app\models\ext\MenuExt;
use app\models\ext\MenuItemExt;
use Yii;
use yii\debug\models\search\Base;
use yii\web\Response;


/**
 * Site controller
 */
class SiteController extends FrontendController
{
    /**
     * @param string $id
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionPage( $id = '' )
    {

        if( $id === '' ){
            $id = Yii::$app->params['menu_item_index_tid'];
        }


        /* @var $oMenuItem MenuItemExt */
        $arUnsortedPath = [];
        $oMenuItem = MenuExt::$currentItem = MenuExt::getByPath( $id, $arUnsortedPath );
        $this->layout = MenuExt::getMenuItemLayout( $oMenuItem, $this->layout );
        $viewName = MenuExt::getMenuItemTemplate( $oMenuItem );

        /* @var $view \app\components\web\View */
        $view = $this->view;
        // menu item
        $view->menuItem = $oMenuItem;
        // SEO
        if( $oMenuItem->meta_description ) {
            $view->registerMetaTag(
                [
                    'name' =>'description',
                    'content' => $oMenuItem->meta_description
                ]
            );
        }
        if( $oMenuItem->meta_keywords ) {
            $view->registerMetaTag(
                [
                    'name' =>'keywords',
                    'content' => $oMenuItem->meta_keywords
                ]
            );
        }
        $view->title = ( ( $oMenuItem->title ) ? $oMenuItem->title : $oMenuItem->name );

        // breadcrumbs
        $view->params['breadcrumbs'] = MenuExt::getBreadCrumbs( $oMenuItem->id, 'main' );

        $view->header = ( ( $oMenuItem->header ) ? $oMenuItem->header : $oMenuItem->name );
        $view->subHeader = ( ( $oMenuItem->sub_header ) ? $oMenuItem->sub_header : '' );

        return $this->render(
            $viewName,
            [
                'oMenuItem' => $oMenuItem,
                'arStrContents' => $oMenuItem->getArRelationContents($view,$arUnsortedPath),
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'resize' => [
                'class' => 'vapterra\yii2vtimager\ResizeAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
}
