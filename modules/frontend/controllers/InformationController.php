<?php

namespace app\modules\frontend\controllers;

use app\components\FrontendController;
use app\components\web\View;
use app\models\BaseActiveQuery;
use app\models\ext\InformationExt;
use app\models\ext\InformationGroupExt;
use app\models\ext\InformationItemExt;
use app\models\ext\MenuItemRelationExt;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii\web\NotFoundHttpException;

class InformationController extends FrontendController
{

    /**
     * @param $route
     * @param array $params
     * @return mixed|string
     * @throws NotFoundHttpException
     */
    public function runAsUrl($route, $params = [])
    {
        $this->layout = false;

        /* @var $oRelation MenuItemRelationExt */
        $oRelation = $params[0];
        /* @var $view View*/
        $view = $params[1];

        /* @var $oInformation InformationExt */
        $oInformation = InformationExt::getById($oRelation->item_id);
        if( empty( $oInformation ) ) {
            throw new NotFoundHttpException( Yii::t('app/app', '[404] Not Found information {id}',[ 'id'=>$route ] ) );
        }

        $view->structureItem = $oInformation;
        InformationExt::$currentInformation = $oInformation;

        try {
            $oItems = $oInformation->getByPath($route);
            $oItem = array_pop($oItems);
        } catch ( NotFoundHttpException $e ){
            if( $route != '' ) {
                throw new NotFoundHttpException( Yii::t('app/app', '[404] Not Found information item {id}',[ 'id'=>$route ] ) );
            }
            $oItem = $oInformation->getRootInformationGroup()->one();
        }


        if( $oRelation->main && !empty( $oItem ) && ( $oItem instanceof InformationItemExt || ($oItem instanceof InformationGroupExt && $oItem->information_group_id > 0) ) ){
            $view->currentItem = $oItem;
            $view->chainItems = $oItems;
            /*// SEO
            if( $oItem->meta_description ) {
                $view->registerMetaTag(
                    [
                        'name' =>'description',
                        'content' => $oItem->meta_description
                    ]
                );
            }
            if( $oItem->meta_keywords ) {
                $view->registerMetaTag(
                    [
                        'name' =>'keywords',
                        'content' => $oItem->meta_keywords
                    ]
                );
            }*/
            $view->title = ( ( $oItem->title ) ? $oItem->title : $oItem->name );
            // breadcrumbs
            $view->params['breadcrumbs'] = $oItem->getBreadCrumbs();
            if(
                ( $oItem instanceof InformationGroupExt && $oItem->information_group_id != 0 )
                ||
                $oItem instanceof InformationGroupExt == false
            ) {
                $view->header = $oItem->name;
            }
        }

        if( empty($oItem) && empty( $route ) ) {
            return $this->actionGroup( 0 );
        } else if( $oItem instanceof InformationGroupExt ) {
            /* @var $oItem InformationGroupExt */
            //InformationExt::$currentGroup = $oItem;
            if( $oItem->information_group_id ) {
                $view->onlyMain = true;
            }
            return $this->actionGroup( $oItem->id );
        } else if ( $oItem instanceof InformationItemExt ) {
            /* @var $oItem InformationItemExt */
            //InformationExt::$currentItem = $oItem;
            $view->onlyMain = true;
            return $this->actionItem($oItem->id);
        }

        return parent::run($route, $params);
    }

    public function partOfPage($route, $params = [])
    {
        $this->layout = false;
        /* @var $oRelation MenuItemRelationExt */
        $oRelation = $params[0];

        /* @var $oInformation InformationExt */
        //$oInformation = InformationExt::getById($oRelation->item_id);

        $module = Yii::$app->getModule('frontend');
        if( !empty( $module ) ) {
            return $module->widget(
                'InformationItemList',
                [
                    'informationId' => $oRelation->item_id,
                    'informationGroupId' => -1,
                    'subGroups' => false,
                    'limit' => 3,
                    'order' => 'dt_creation',
                    'orderDirection' => SORT_DESC,
                    'template' => 'information-item-list',
                ]
            );
        }
    }

    public function actionGroup( $id )
    {
        /* @var $oInformation InformationExt */
        /* @var $oInformationGroup InformationGroupExt */
        /* @var $qInformationItem BaseActiveQuery */

        $oInformationGroup = null;

        if( $id ) {
            $oInformationGroup = InformationGroupExt::getByTid($id);
            $oInformation = $oInformationGroup->information;
        } else if( !empty( InformationExt::$currentInformation ) ) {
            $oInformation = InformationExt::$currentInformation;
        } else {
            throw new NotFoundHttpException( Yii::t('app/app', '[404] Not Found information group {id}',[ 'id'=>$id ] ) );
        }

        $qInformationItem = $oInformation->getInformationItems();
        $qInformationItem->active();
        if( !empty( $oInformationGroup ) && $oInformationGroup->information_group_id ) {
            $qInformationItem->andWhere(['IN', 'information_group_id', [ $id ] ]);
        }
        $countQuery = clone $qInformationItem;

        $route = !empty($oInformationGroup)?$oInformationGroup->getUrl():$oInformation->getUrl();
        $sort = new Sort([
            'route' => $route,
            'defaultOrder' => [
                'dt_creation' => SORT_ASC,
            ],
            'attributes' => [
                'dt_creation' => [
                    'default' => SORT_ASC,
                    'label'=>Yii::t('frontend/layout','Date time of creation')
                ],
                'name' => [
                    'default' => SORT_ASC,
                    'label' => Yii::t('frontend/layout','Name'),
                ],
                'position' => [
                    'label'=>Yii::t('frontend/layout','Position')
                ],
            ],
        ]);

        $dataProviderInformationItemList = new ActiveDataProvider(
            [
                'query' => $qInformationItem,
                'sort' => $sort,
                'pagination' => [
                    'route' => $route,
                    'totalCount' => $countQuery->count(),
                    'pageSize' => $oInformation->items_on_page?$oInformation->items_on_page:12,
                    'pageParam' => 'page'
                ],
            ]
        );
        
        return $this->render(
            'group',
            [
                'oInformation' => $oInformation,
                'oInformationGroup' => $oInformationGroup,
                //'arOInformationGroups' => $oInformation->getInformationGroups()->where(['=','information_group_id',0])->andWhere(['=','active',1])->all(),
                'dataProviderInformationItemList' => $dataProviderInformationItemList,
            ]
        );
    }

    public function actionItem( $id )
    {
        /* @var $oInformationItem InformationItemExt */
        /* @var $oInformation InformationExt */
        /* @var $oInformationGroup InformationGroupExt */

        $oInformationItem = InformationItemExt::getByTid( $id );
        $oInformationGroup = $oInformationItem->getInformationGroup()->one();
        $oInformation = $oInformationItem->getInformation()->one();

        return $this->render(
            'item',
            [
                'oInformation' => $oInformation,
                'oInformationGroup' => $oInformationGroup,
                'oInformationItem' => $oInformationItem,
            ]
        );
    }


}