<?php
namespace app\modules\frontend\controllers;

use app\components\FrontendController;
use app\models\ext\FormExt;
use app\models\ext\MenuItemRelationExt;
use Yii;
use yii\web\Response;

/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 29.06.16
 * Time: 10:21
 */
class FormController extends FrontendController
{
    public function partOfPage($route, $params = [])
    {
        $this->layout = false;
        /* @var $oRelation MenuItemRelationExt */
        $oRelation = $params[0];
        /* @var $oForm FormExt */
        return $this->actionView($oRelation->item_id);
    }

    public function actionView($id)
    {
        $form = FormExt::getByTid($id);
        $view = 'view';
        $context = [
            'forAjax' => Yii::$app->request->isAjax,
            'model'=>$form,
            'elements'=>$form->getFormElements()->andWhere(['=','active',1])->all(),
            'rowTemplate' => '<div class="form-group">{label}<div>{description}</div>{input}</div>',
        ];
        if( Yii::$app->request->isAjax ){
            return $this->renderPartial(
                $view,
                $context
            );
        }
        return $this->render(
            $view,
            $context
        );
    }

    public function actionSend($id)
    {

        $status = self::ERROR;
        /* @var $form FormExt */
        $form = FormExt::getByTid($id);
        if( $form->completed( $_POST ) ) {
            $status = self::SUCCESS;
        }

        if( Yii::$app->request->isAjax ){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'status'=>$status,
                'response' => [
                    'message' => $form->getCompletedMessage(),
                    'errors' => $form->getErrors(),
                ]
            ];
        }

        $message = '';
        if( $form->hasErrors() ){
            foreach ( $form->getErrors() as $errors ) {
                $message .= '<p>'.implode(', ',$errors).'</p>';
            }
        }
        $this->setFlash($status,$form->getCompletedMessage().$message);

        return $this->render(
            'view',
            [
                'forAjax' => Yii::$app->request->isAjax,
                'model'=>$form,
                'elements'=>$form->getFormElements()->andWhere(['=','active',1])->all(),
                'rowTemplate' => '<div class="form-group">{label}<div>{description}</div>{input}</div>',
            ]
        );
    }
}