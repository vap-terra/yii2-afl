<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 10.09.16
 * Time: 15:51
 */
use app\models\ext\ShopItemExt;
use yii\bootstrap\Button;
use yii\helpers\Html;

/* @var $oShopItem ShopItemExt */
/* @var $item [] */
if ($oShopItem->shop_item_id == 0) {
    $url = $oShopItem->getUrl();
} else {
    $url = $oShopItem->shopItem->getUrl();
}
?>
<div class="shopping-cart-item row" data-position="<?php echo $item['position']?>">
    <div class="text-center">
        <a href="<?php echo $url; ?>" class="name"><?php echo $oShopItem->name; ?></a>
    </div>
    <div class="col-md-6">
        <a href="<?php echo $url; ?>" class="">
            <?php
            $src = Yii::$app->imager->getSrcResize(
                50, 50,
                (!$oShopItem->getImageHref()&&$oShopItem->shop_item_id)
                    ? $oShopItem->shopItem->getImageHref()
                    : $oShopItem->getImageHref()
            );
            echo Html::img(
                $src,
                [
                    'class' => 'img-thumbnail img-circle',
                    'alt' => ($oShopItem->header ? $oShopItem->header : $oShopItem->name),
                ]
            );
            ?>
        </a>
    </div>
    <div class="col-md-6">
        <div class="price">
            <span class="price-value"><?php echo number_format($oShopItem->getPrice(), 0, ',', ' '); ?></span>
            <span class="fa fa-rub"><i class="text-hide"><?php echo $oShopItem->currency->short_name;?></i></span>
        </div>
        <div class="quantity">
            <span class="quantity-value"><?php echo $item['quantity']; ?></span>
            <span class="quantity-unit-value" title="<?php echo $oShopItem->quantityUnit->description;?>"><?php echo $oShopItem->quantityUnit->name;?></span>
        </div>
        <div class="amount-payable">
           <span class="price-value"><?php echo number_format($oShopItem->getPrice() * $item['quantity'], 0, ',', ' '); ?></span>
            <span class="price-unit-value <?php echo $oShopItem->currency->css;?>"><i class="text-hide"><?php echo $oShopItem->currency->short_name;?></i></span>
        </div>
    </div>
    <?php
    echo Button::widget([
        'tagName' => 'a',
        'label' => '<i class="fa fa-times"></i> удалить',
        'encodeLabel' => false,
        'options' => [
            'class'=>'btn-cart-item-delete shopping-cart-item-delete',
            'href'=>Yii::$app->urlManager->createUrl(['/frontend/shop-cart/remove/','id'=>$item['position']]),
        ]
    ]);
    ?>
</div>
