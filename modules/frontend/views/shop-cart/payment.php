<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 09.10.16
 * Time: 13:06
 */


use app\models\ext\PaymentExt;
use app\models\ext\ShopOrderExt;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $oOrder ShopOrderExt */
/* @var $form ActiveForm */
/* @var $arOPayment PaymentExt[]*/

?>
<div class="shopping-cart-payments full-shopping-cart-payments">
    <?php foreach( $arOPayment as $item ){?>
        <div class="media shop-cart-payment" data-free-price="<?php echo $item->free_price?>"
             data-minimum-price="<?php echo $item->minimum_price?>" data-maximum-price="<?php echo $item->maximum_price?>" data-id="<?php echo $item->id?>">
            <div class="media-left">
                <?php
                $src = Yii::$app->imager->getSrcResize(64, 64, $item->getImageHref());
                echo Html::img(
                    $src,
                    [
                        'class' => 'media-object',
                        'alt' => $item->name,
                    ]
                );
                ?>
            </div>
            <div class="media-body">
                <h4 class="media-heading"><?php echo $item->name?></h4>
                <?php echo $item->description;?>
            </div>
        </div>
    <?php } ?>
</div>
