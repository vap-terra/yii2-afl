<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 11.09.16
 * Time: 13:21
 */

use app\models\ext\ShopOrderExt;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $oOrder ShopOrderExt */
/* @var $form ActiveForm */

?>
<div class="user-data">

    <h3>Информация для заказа</h3>

    <?php echo $form->field($oOrder,'name')->textInput();?>

    <?php echo $form->field($oOrder,'phone')->textInput();?>

    <?php echo $form->field($oOrder,'email')->textInput();?>

    <?php echo $form->field($oOrder,'address')->textInput();?>

    <?php echo $form->field($oOrder,'comment')->textarea(); ?>

    <?php echo Html::submitButton('Заказать',['class'=>'btn btn-info']);?>

</div>
<?php

?>
