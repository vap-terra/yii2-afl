<?php
/**
 * Created by PhpStorm.
 * short.php
 * @package /
 * @author Polyankin Semen Vicktorovich Vol][v, http://vap-terra.ru
 * @date 13.09.15
 * @time 16:24
 * @version 0.0.1
 * Copyright © 2014 VAP-TERRA, http://vap-terra.ru
 */
use app\components\web\View;
use app\models\data\ShopCart;
use app\models\ext\DeliveryExt;
use app\models\ext\PaymentExt;
use app\models\ext\ShopExt;
use app\models\ext\ShopItemExt;
use app\models\ext\ShopOrderExt;
use yii\bootstrap\ActiveForm;

/* @var $this View*/
/* @var $oShop ShopExt */
/* @var $items [] */
/* @var $oOrder ShopOrderExt */
/* @var $arODelivery DeliveryExt[]*/
/* @var $arOPayment PaymentExt[]*/

/* @var $function string */
/* @var $sum double */
/* @var $allCount double */
/* @var $form ActiveForm */

\app\modules\assets\ShopFrontendAsset::register($this);
$function = 'afterLoadForShopCart' . $oShop->id;
$this->registerJs("
        var {$function} = function(){
            $('.full-shopping-cart .shopping-cart-item').ShopCartItemController();
        }
        if (window.addEventListener) {
            window.addEventListener('load', {$function}, false);
        } else if (window.attachEvent) {
            window.attachEvent('onload', {$function});
        }
    ",
    View::POS_END,
    'shop-cart-init_'.$oShop->id
);

?>
<?php
if( !empty( $items ) ){
    $sum = ShopCart::counted();
    $allCount = ShopCart::getCount();
?>
    <h3>Корзина</h3>
    <div class="shopping-cart full-shopping-cart">
        <?php
        $form = ActiveForm::begin();
        ?>

            <div class="shopping-cart-items full-shopping-cart-items">
                <?php
                    foreach( $items as $item ) {
                        $oShopItem = ShopItemExt::findOne($item['itemId']);
                        if( !is_null($oShopItem) ) {
                            echo $this->render(
                                'cart-item',
                                [
                                    'oShopItem' => $oShopItem,
                                    'item' => $item,
                                ]
                            );
                        }
                    }
                ?>
            </div>

            <div class="shopping-cart-footer full-shopping-cart-footer h3">
                <div class="row shopping-cart-total-wrapper">
                    <div class="col-md-6 text-center">Итого:&nbsp;</div>
                    <div class="col-md-3 shopping-cart-total-quantity">
                        <span class="quantity-value"><?php echo $allCount;?></span>
                        <span class="quantity-unit-value"><?php echo $oShop->quantityUnit->name;?></span>
                    </div>
                    <div class="col-md-3 shopping-cart-total-amount-payable">
                        <span class="price-value"><?php echo number_format($sum,0,',',' ');?></span>
                        <span class="price-currency-value <?php echo $oShop->currency->css;?>"><i class="text-hide"><?php echo $oShop->currency->short_name;?></i></span>
                    </div>
                </div>
            </div>

            <?php
            echo $this->render(
                'payment',
                [
                    'oOrder' => $oOrder,
                    'form' => $form,
                    'arOPayment' => $arOPayment,
                ]
            );
            ?>

            <?php
                echo $this->render(
                    'delivery',
                    [
                        'oOrder' => $oOrder,
                        'form' => $form,
                        'arODelivery' => $arODelivery,
                    ]
                );
            ?>

            <?php
                echo $this->render(
                    'user-data',
                    [
                        'oOrder' => $oOrder,
                        'form' => $form,
                    ]
                );
            ?>

        <?php
        $form->end();
        ?>

    </div>

<?php
} else {
?>
    <h3>Корзина пуста</h3>
<?php
}
?>