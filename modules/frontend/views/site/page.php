<?php

use app\components\web\View;
use app\models\ext\MenuItemExt;

/* @var $this View */
/* @var $oMenuItem MenuItemExt */
/* @var $arStrContents string[] */
/* @var $arStrContentBlocks string[] */
?>
<h1 class="page-header">
    <?php echo $this->header?>
    <small><?php echo $this->subHeader?></small>
</h1>


    <?php foreach( $arStrContents as $content ) {?>
        <?php echo $content?>
    <?php } ?>


