<?php
/**
 * Created by PhpStorm.
 * shop-item.php
 * @package /
 * @author Polyankin Semen Vicktorovich Vol][v, http://vap-terra.ru
 * @date 07.09.15
 * @time 22:18
 * @version 0.0.1
 * Copyright © 2014 VAP-TERRA, http://vap-terra.ru
 */
use app\models\ext\ShopItemExt;
use yii\helpers\Html;
use yii\helpers\StringHelper;

/** @var $item ShopItemExt  */
/** @var $url string */
/** @var $price number | null */
?>
<?php if( $item->shopGroup ){ ?>
<a href="<?php echo $item->shopGroup->getUrl();?>" class="category-name"><?php echo $item->shopGroup->title;?></a>
<?php } ?>
<a href="<?php echo $url;?>" class="name"><?php echo $item->name?></a>
<figure>
    <a href="<?php echo $url;?>" class="img-thumbnail img-circle">
        <?php
        $src = Yii::$app->imager->getSrcResize(200,200,$item->getImageHref());
        echo Html::img(
            $src,
            [
                'class'=>'img-responsive img-circle',
                'alt'=>( $item->header ? $item->header : $item->name ),
            ]
        );
        ?>
    </a>
</figure>
<?php
/*if( !empty( $item->description ) ) {*/
    echo $item->description;
/*} else if ( $item->content ) {
    echo StringHelper::truncate( strip_tags($item->content), 200, '...' );
}*/
?>
<div class="price">
    <?php if( isset( $price ) ){ ?>
    от <span class="price-value"><?php echo number_format($price,0,',',' ');?></span> <span class="fa fa-rub"><i class="text-hide"><?php echo $item->currency->short_name?></i></span>
    <?php } else { ?>
    <span class="price-value"><?php echo number_format($item->getPrice(),0,',',' ');?></span> <span class="fa fa-rub"><i class="text-hide"><?php echo $item->currency->short_name?></i></span>
    <?php } ?>
</div>
<a href="<?php echo $url;?>" class="link"><span class="fa fa-eye"></span> смотреть</a>
