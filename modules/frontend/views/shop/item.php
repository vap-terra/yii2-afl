<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 14.07.16
 * Time: 23:42
 */
use app\components\web\View;
use app\models\ext\ShopItemExt;
use yii\helpers\Html;
use yii\bootstrap\Button;
use yii\bootstrap\ButtonGroup;

/* @var $this \yii\web\View */
/* @var $oShop \app\models\ext\ShopExt */
/* @var $oShopGroup \app\models\ext\ShopGroupExt */
/* @var $oShopItem \app\models\ext\ShopItemExt */

\app\modules\assets\ShopFrontendAsset::register($this);
$function = 'afterLoadForShop' . $oShop->id;
$this->registerJs("
        var {$function} = function(){
            $('.shop-item-wrapper').ShopItemController();
        }
        if (window.addEventListener) {
            window.addEventListener('load', {$function}, false);
        } else if (window.attachEvent) {
            window.attachEvent('onload', {$function});
        }
    ",
    View::POS_END,
    'shop-init_'.$oShop->id
);

$modifs = $oShopItem->getModifications()->orderBy(['position'=>SORT_ASC])->andWhere(['=','active',1])->all();
$price = $oShopItem->getPrice();
$name = $oShopItem->name;
$idToCart = $oShopItem->id;
if( !empty( $modifs ) ) {
    $price = $modifs[0]->getPrice();
    $name = $modifs[0]->name;
    $idToCart = $modifs[0]->id;
    /*foreach( $modifs as $modif ) {
        $modifPrice = $modif->getPrice();
        if( !$price || ($price > $modifPrice && $modifPrice) ){
            $price = $modifPrice;
        }
    }*/
}
?>
<div class="shop-item-wrapper">
    <div class="row">
        <div class="col-md-4">
            <div class="full-views">
                <ul>
                    <li class="img-thumbnail img-circle main active">
                        <?php
                        $src = Yii::$app->imager->getSrcResize(400,400,$oShopItem->getImageHref());
                        echo Html::img(
                            $src,
                            [
                                'class'=>'img-responsive img-circle',
                                'alt'=>( $oShopItem->header ? $oShopItem->header : $oShopItem->name ),
                            ]
                        );
                        ?>
                    </li>
                </ul>
            </div>
            <div class="small-views">
                <ul>
                    <li class="img-thumbnail img-circle main active">
                        <?php
                        $src = Yii::$app->imager->getSrcResize(200,200,$oShopItem->getImageHref());
                        echo Html::img(
                            $src,
                            [
                                'class'=>'img-responsive img-circle',
                                'alt'=>( $oShopItem->header ? $oShopItem->header : $oShopItem->name ),
                            ]
                        );
                        ?>
                    </li>
                </ul>
                <a class="imgNav img-prev" href="#">
                    <span class="fa fa-angle-double-up"></span>
                </a>
                <a class="imgNav img-next" href="#">
                    <span class="fa fa-angle-double-down"></span>
                </a>
            </div>
        </div>
        <div class="col-md-8">
            <h1><?php echo $oShopItem->name?></h1>
            <div class="cart-action">
                <div class="name text-center">
                    <?php echo $name; ?>
                </div>
                <div class="price text-center">
                    <span class="price-value"><?php echo number_format($price,0,',',' ');?></span>
                    <span class="<?php echo $oShopItem->currency->css;?>"><i class="text-hide"><?php echo $oShopItem->currency->short_name;?></i></span>
                </div>
                <div class="cart-action-form-wrapper">
                    <form action="/shop/add-to-cart/<?php echo $idToCart;?>" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="input-group input-group-md">
                                    <span class="input-group-addon btn minus"><i class="fa fa-minus-circle"></i></span>
                                    <input class="form-control" type="text" name="quantity" value="1" aria-label="Quantity"/>
                                    <span class="input-group-addon btn plus"><i class="fa fa-plus-circle"></i></span>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <?php
                                $buttons = [];
                                $buttons[] = Button::widget([
                                    'tagName' => 'a',
                                    'label' => '<i class="fa fa-cart-plus"></i> В корзину',
                                    'encodeLabel' => false,
                                    'options' => [
                                        'class'=>'btn btn-info btn-add-to-cart',
                                        'href'=>Yii::$app->urlManager->createUrl(['/frontend/shop-cart/add/','shopItemId'=>$idToCart]),
                                    ]
                                ]);
                                $buttons[] = Button::widget([
                                    'tagName' => 'a',
                                    'label' => '<i class="fa fa-shopping-cart"></i> Быстрая покупка',
                                    'encodeLabel' => false,
                                    'options' => [
                                        'class'=>'btn btn-info btn-fast-order',
                                        'href'=>Yii::$app->urlManager->createUrl(['/frontend/shop-cart/fast-order/','shopItemId'=>$idToCart]),
                                    ]
                                ]);
                                /*$buttons[] = ButtonPopupActivator::widget([
                                    'tag'=>'a',
                                    'href'=>'/shop/fast-order/'.$idToCart,
                                    'label' => '<i class="fa fa-shopping-cart"></i> Быстрая покупка',
                                    //'encodeLabel' => false,
                                    'options'=>[
                                        'class'=>'btn btn-default btn-fast-order',
                                        'data-request-method' => 'get'
                                    ]
                                ]);*/
                                echo ButtonGroup::widget( [ 'buttons' =>$buttons, 'options'=>['class'=>'btn-group-md'] ] );
                                ?>
                            </div>
                        </div>
                        <input type="hidden" name="shopItemId" value="<?php echo $idToCart;?>" />
                    </form>
                </div>
            </div>
            <?php if( !empty( $modifs ) ){ ?>
                <?php foreach( $modifs as $num=>$modif ){
                    /* @var $modif ShopItemExt */
                    ?>
                    <div class="col-md-4 shop-item-modification<?php if($num==0){?> active<?php }?>" data-id="<?php echo $modif->id; ?>">
                        <div class="name"><?php echo $modif->name; ?></div>
                        <div class="img-thumbnail img-circle">
                            <div class="your-choose">Ваш выбор</div>
                            <?php
                            $src = Yii::$app->imager->getSrcResizeImage(200,200,$modif->getImageHref());
                            echo Html::img(
                                $src,
                                [
                                    'class'=>'img-responsive img-circle',
                                    'alt'=>( $modif->header ? $modif->header : $modif->name ),
                                ]
                            );
                            ?>
                            <i class="fa fa-check"></i>
                            <div class="price">
                                <span class="price-value"><?php echo number_format($modif->getPrice(),0,',',' ');?></span> <span class="<?php echo $modif->currency->css;?>"><i class="text-hide"><?php echo $modif->currency->short_name;?></i></span>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>
            <div class="col-md-12">
                <?php echo $oShopItem->description; ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="description-tab">
            <?php echo $oShopItem->content; ?>
                    </div>
    </div>
</div>

