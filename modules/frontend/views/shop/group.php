<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 14.07.16
 * Time: 23:42
 */
use app\models\ext\ShopItemExt;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\widgets\LinkPager;

/* @var $this \yii\web\View */
/* @var $oShop \app\models\ext\ShopExt */
/* @var $oShopGroup \app\models\ext\ShopGroupExt */
/* @var $arOShopGroups \app\models\ext\ShopGroupExt[] */
/* @var $dataProviderShopItemList ActiveDataProvider */
?>

<div class="shop-category-wrapper">

        <div class="shop-sorting-items">
            <?php echo $dataProviderShopItemList->sort->link('name') ?> | <?php echo $dataProviderShopItemList->sort->link('price')?>
        </div>

        <?php
        echo LinkPager::widget(
            [
                'pagination' => $dataProviderShopItemList->pagination,
            ]
        );
        ?>

        <div class="shop-container-items">
            <?php
            $classColumn = 'col-md-6';
            if( $dataProviderShopItemList->pagination->totalCount > 2 ) {
                $classColumn = 'col-md-4';
            }
            foreach( $dataProviderShopItemList->getModels() as $item ) {
                /* @var $item ShopItemExt */
                /* @var $modifs ShopItemExt[] */
                $group = $item->shopGroup;
                $url = $item->getUrl();
                $modifs = $item->getModifications()->active()->all();
                $price = $item->getPrice();
                $currency = $item->currency;
                if( !empty( $modifs ) ) {
                    foreach( $modifs as $modif ) {
                        $modifPrice = $modif->getPrice();
                        if( !$price || ($price > $modifPrice && $modifPrice) ){
                            $price = $modifPrice;
                        }
                    }
                }
                ?>
                <div class="<?php echo $classColumn;?> shop-item">
                    <?php
                    echo $this->render(
                        'partials/item/short',
                        [
                            'item' => $item,
                            'url' => $url,
                            'price' => $price,
                            'currency' => 1
                        ]
                    );
                    ?>

                    <div class="modifications">
                        <div class="full-views">
                            <ul>
                                <?php if( count($modifs) ){ ?>
                                    <?php foreach( $modifs as $num=>$modif ){?>
                                        <li id="<?php echo $modif->id; ?>"<?php if( $num == 0 ){ ?> class="active"<?php } ?>>
                                            <?php
                                            echo $this->render(
                                                'partials/item/short',
                                                [
                                                    'item' => $modif,
                                                    'url' => $url
                                                ]
                                            );
                                            ?>
                                        </li>
                                    <?php } ?>
                                <?php } else {?>
                                    <li id="<?php echo $item->id;?>" class="active">
                                        <?php
                                        echo $this->render(
                                            'partials/item/short',
                                            [
                                                'item' => $item,
                                                'url' => $url
                                            ]
                                        );
                                        ?>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                        <div class="small-views">
                            <ul>
                                <?php if( count($modifs) ){ ?>
                                    <?php foreach( $modifs as $num=>$modif ){?>
                                        <li data-id="<?php echo $modif->id;?>"<?php if( $num == 0 ){ ?> class="active"<?php } ?>>
                                            <?php
                                            $src = Yii::$app->imager->getSrcResize(200,200,$modif->getImageHref());
                                            echo Html::img(
                                                $src,
                                                [
                                                    'class'=>'img-responsive img-circle',
                                                    'alt'=>( $modif->header ? $modif->header : $modif->name ),
                                                ]
                                            );
                                            ?>
                                        </li>
                                    <?php } ?>
                                <?php } else { ?>
                                    <li data-id="<?php echo $item->id;?>" class="active">
                                        <?php
                                        $src = Yii::$app->imager->getSrcResize(200,200,$item->getImageHref());
                                        echo Html::img(
                                            $src,
                                            [
                                                'class'=>'img-responsive img-circle',
                                                'alt'=>( $item->header ? $item->header : $item->name ),
                                            ]
                                        );
                                        ?>
                                    </li>
                                <?php } ?>
                            </ul>
                            <a class="imgNav img-prev" href="#">
                                <span class="fa fa-angle-double-up"></span>
                            </a>
                            <a class="imgNav img-next" href="#">
                                <span class="fa fa-angle-double-down"></span>
                            </a>
                        </div>
                    </div>
                </div>

                <?php
            }
            ?>
        </div>
        <div class="clearfix"></div>
        <?php
        echo LinkPager::widget([
            'pagination' => $dataProviderShopItemList->pagination,
        ]);
        ?>

    <?php /*<div class="col-md-3">
        <div class="shop-catalog-menu">
        <?php
            echo ListShopCategory::widget(['view'=>'@frontend/views/shop/partials/category/menu']);
        ?>
        </div>
    </div>*/ ?>
    <div class="clearfix"></div>
</div>

