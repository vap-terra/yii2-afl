<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 29.06.16
 * Time: 10:30
 */

use app\components\web\View;
use app\models\ext\FormElementExt;
use app\models\ext\FormExt;
use yii\helpers\Html;
/* @var $this \yii\web\View */
/* @var $model FormExt */
/* @var $elements FormElementExt[] */
/* @var $forAjax boolean */
/* @var $rowTemplate string */
if($forAjax) {
    //\app\assets\AnimatedSVGIconsAsset::register($this);
}
\app\modules\assets\FormFormFrontendAsset::register($this);

$formWrapperId = 'wrapper_'.$model->generateId();
$formId = $model->generateId();
?>
    <div class="from-wrapper" id="<?php echo $formWrapperId; ?>">

        <?php if( $forAjax ) { ?>
            <div class="form-close-wrapper">
        <span class="si-icons si-icons-hover">
            <span class="si-icon si-icon-hamburger-cross" data-icon-name="hamburgerCross"></span>
        </span>
            </div>
        <?php } ?>

        <div>

            <h1><?php echo $model->name?></h1>

            <div class="form-hide-part">

                <div><?php echo $model->description?></div>

                <form action="<?php echo $model->getAction();?>" class="" method="post" enctype="multipart/form-data">
                    <?php

                    foreach( $elements as $element ) {

                        echo $this->render(
                            'element/view',
                            [
                                'element'=>$element,
                                'template'=>$rowTemplate,
                            ]
                        );

                    }

                    echo Html::hiddenInput('guid',$model->getSessionGuid());
                    echo Html::hiddenInput('email2',$model->getSessionNumber());

                    echo Html::submitButton($model->button_title,['class'=>'']);
                    ?>
                </form>
            </div>
        </div>
    </div>
<?php
$function = 'afterLoadForForm' . $model->getSessionNumber();
if( !$forAjax ){
    $this->registerJs("
        var {$function} = function(){
            $('#{$formWrapperId}').FormController();
        }
        if (window.addEventListener) {
            window.addEventListener('load', {$function}, false);
        } else if (window.attachEvent) {
            window.attachEvent('onload', {$function});
        }
    ", View::POS_END, 'form-init_'.$formWrapperId);
} else {
    $this->registerJs("
        $('#{$formWrapperId}').FormController();
        $(document).on('form-send-error',function(event,controller,\$form,\$container){});
        $(document).on('form-send-success',function(event,controller,\$form,\$container){\$.fancybox.close();});
        //new svgIcon( document.querySelector( '.si-icons-hover .si-icon-hamburger-cross' ), svgIconConfig, { easing : mina.elastic, speed: 600, evtoggle : 'mouseover', size : { w : 24, h : 24 } } );
    ", View::POS_END, 'form-init_'.$formWrapperId);
    $this->endBody();
    $this->endPage();
}