<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 05.08.16
 * Time: 23:21
 */
use app\models\ext\FormElementExt;
use yii\helpers\Html;

/* @var $element FormElementExt */
/* @var $template string */

if( empty( $template ) ){
    $template = '<div class="row">{label}<div>{description}</div>{input}</div>';
}
if( !empty( $element->template ) ){
    $template = $element->template;
}

$id = 'form_element_'.$element->form_id.'_'.$element->id.'_'.$element->form->getSessionNumber();
$htmlOptions = [
    'id'=>$id
];
if( $element->required ){
    $htmlOptions['required'] = 'true';
    $htmlOptions['class'] = 'form-control required';
}
if( !empty( $element->hint ) ){
    $htmlOptions['placeholder'] = $element->hint;
}

        // label element
        $label = $element->name;
        if( !empty($element->required) ) {
            $label .= Html::tag('span','*',['class'=>'mark']);
        }
        $label = Html::label(
            $label,
            $id,
            ['class'=>'']
        );

        // description element
        $description = '';
        if( !empty($element->description) ) {
            $description = $element->description;
        }
        // element
        switch( $element->type ){
            case FormElementExt::TYPE_TEXTEDIT:
                //break;
            case FormElementExt::TYPE_TEXTAREA:
                $input = Html::textArea(
                    $element->tid,'',
                    $htmlOptions
                );
                break;
            case FormElementExt::TYPE_LIST:
                $input = Html::dropDownList(
                    $element->tid,
                    0,
                    $element->getListValues(),
                    $htmlOptions
                );
                break;
            case FormElementExt::TYPE_CHECKBOX:
                $input = Html::checkBox(
                    $element->tid,
                    false,
                    $htmlOptions
                );
                break;
            case FormElementExt::TYPE_PASSWORD:
                $input = Html::passwordInput(
                    $element->tid,
                    '',
                    $htmlOptions
                );
                break;
            case FormElementExt::TYPE_DATE:
                $htmlOptions['data-type']='date';
                $input = Html::textInput(
                    $element->tid,
                    '',
                    $htmlOptions
                );
                break;
            case FormElementExt::TYPE_TIME:
                $htmlOptions['data-type']='time';
                $input = Html::textInput(
                    $element->tid,
                    '',
                    $htmlOptions
                );
                break;
            case FormElementExt::TYPE_DATETIME:
                $htmlOptions['data-type']='datetime';
                $input = Html::textInput(
                    $element->tid,
                    '',
                    $htmlOptions
                );
                break;
            case FormElementExt::TYPE_EMAIL:
                $htmlOptions['data-type']='email';
                $input = Html::textInput(
                    $element->tid,
                    '',
                    $htmlOptions
                );
                break;
            case FormElementExt::TYPE_PHONE:
                $htmlOptions['data-type']='phone';
                $input = Html::textInput(
                    $element->tid,
                    '',
                    $htmlOptions
                );
                break;
            case FormElementExt::TYPE_URL:
                $htmlOptions['data-type']='url';
                $input = Html::textInput(
                    $element->tid,
                    '',
                    $htmlOptions
                );
                break;
            case FormElementExt::TYPE_IMAGE:
            case FormElementExt::TYPE_FILE:
                $htmlOptions['data-type']='url';
                $input = Html::fileInput(
                    $element->tid,
                    '',
                    $htmlOptions
                );
                break;
            //case FormElementExt::TYPE_INPUT:
            default:
                $input = Html::textInput(
                    $element->tid,
                    '',
                    $htmlOptions
                );
                break;
        }
echo strtr( $template, ['{label}'=>$label,'{description}'=>$description,'{input}'=>$input] );