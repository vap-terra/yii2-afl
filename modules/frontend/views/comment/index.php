<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 23.08.16
 * Time: 19:11
 */
use app\models\ext\CommentExt;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\helpers\Html;
use yii\widgets\LinkPager;

/* @var $dataProviderList ActiveDataProvider */
/* @var $pagination Pagination */

echo LinkPager::widget(
    [
        'pagination' => $pagination
    ]
);
?>
<div class="list-reviews">

    <?php
    foreach( $dataProviderList->getModels() as $oComment ){
        /* @var $oComment CommentExt */
        ?>
        <div class="media">
            <div class="media-left media-middle">
                    <?php
                    $src = Yii::$app->imager->getSrcResize(200,200,$oComment->getImageHref());
                    echo Html::img(
                        $src,
                        [
                            'class'=>'media-object',
                            'alt'=>$oComment->author_name,
                        ]
                    );
                    ?>
            </div>

            <div class="media-body">
                <span class="date"><span class="glyphicon glyphicon-time"></span> <?php echo Yii::$app->formatter->asDate( $oComment->dt_creation )?></span>
                <h4 class="media-heading"><?php echo  $oComment->author_name ;?>, <?php echo  $oComment->author_company ;?></h4>
                <p><?php echo  $oComment->author_locality ;?></p>
                <?php echo  $oComment->content ;?>
            </div>
        </div>
    <?php } ?>

    <?php
    $module = Yii::$app->getModule('frontend');
    if( !empty($module) ){
        ?>
        <div class="text-center">
            <?php
            echo $module->widget(
                'FormLink',
                [
                    'formId' => 3,
                    'title' => 'Оставить отзыв',
                    'cssIcon' => 'fa fa-commenting-o',
                    'options' => [
                        'class'=>'btn btn-default btn-sm'
                    ],
                    'template' => 'form-button',
                ]
            );
            ?>
        </div>
        <?php
    }
    ?>

</div>