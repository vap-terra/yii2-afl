<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 14.07.16
 * Time: 23:42
 */
use vapterra\yii2vtimager\Image;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\helpers\Html;
use yii\widgets\LinkPager;

/* @var $this \yii\web\View */
/* @var $oInformation \app\models\ext\InformationExt */
/* @var $oInformationGroup \app\models\ext\InformationGroupExt */
/* @var $arOInformationGroups \app\models\ext\InformationGroupExt[] */
/* @var $dataProviderInformationItemList ActiveDataProvider */

?>

<?php
echo LinkPager::widget(
    [
        'pagination' => $dataProviderInformationItemList->pagination,
    ]
);
?>

<div class="list-news">
    <?php if( $dataProviderInformationItemList->pagination->totalCount > 0 ) {?>
        <?php
            foreach( $dataProviderInformationItemList->getModels() as $oInformationItem ){
                $url = $oInformationItem->getUrl();
        ?>
            <div class="media">

                <div class="media-left media-middle">
                    <a href="<?php echo $url;?>">
                        <?php

                        $src = Yii::$app->imager->getSrcResize(170,170,$oInformationItem->getImageHref(),true,[],Image::RESIZE_PROPORTIONAL_CUTE);
                        echo Html::img(
                            $src,
                            [
                                'class'=>'media-object',
                                'alt'=>( $oInformationItem->header ? $oInformationItem->header : $oInformationItem->name ),
                            ]
                        );
                        ?>
                    </a>
                </div>

                <div class="media-body">
                    <span class="date"><span class="glyphicon glyphicon-time"></span> <?php echo Yii::$app->formatter->asDate( $oInformationItem->dt_creation )?></span>
                    <h4 class="media-heading"><a href="<?php echo $url;?>"><?php echo  ( $oInformationItem->header ) ? $oInformationItem->header : $oInformationItem->name ;?></a></h4>
                    <?php
                    if( !empty( $oInformationItem->description ) ) {
                        echo $oInformationItem->description;
                    } else if ( $oInformationItem->content ) {
                        echo \yii\helpers\StringHelper::truncate( strip_tags($oInformationItem->content), 200, '...' );
                    }
                    ?>
                </div>
            </div>
        <?php } ?>
    <?php } ?>
</div>

<?php
echo LinkPager::widget(
    [
        'pagination' => $dataProviderInformationItemList->pagination,
    ]
);
?>
