<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 14.07.16
 * Time: 23:42
 */
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $oInformation \app\models\ext\InformationExt */
/* @var $oInformationGroup \app\models\ext\InformationGroupExt */
/* @var $oInformationItem \app\models\ext\InformationItemExt */

?>
<div class="news_view">
    <?php /* <h1 style="margin-bottom: 14px;"><?php echo  ( $oInformationItem->header ) ? $oInformationItem->header : $oInformationItem->name ;?></h1> */ ?>

    <?php if( $oInformationItem->description ) {?>
        <div class="info">
            <?php echo $oInformationItem->description ?>
        </div>
    <?php } ?>

    <?php
    if( $oInformationItem->image ) { ?>
        <div class="text-center">
    <?php
        $src = Yii::$app->imager->getSrcResize(600, 600, $oInformationItem->getImageHref());
        echo Html::img(
            $src,
            [
                'class' => 'img-thumbnail',
                'alt' => ($oInformationItem->header ? $oInformationItem->header : $oInformationItem->name),
            ]
        );
    ?>
        </div>
    <?php
    }
    ?>

    <?php if( $oInformationItem->content ) {?>
        <div>
            <?php echo $oInformationItem->content ?>
        </div>
    <?php } ?>
    <div class="date"><span class="glyphicon glyphicon-time"></span> <?php echo Yii::$app->formatter->asDate( $oInformationItem->dt_creation )?></div>
</div>
