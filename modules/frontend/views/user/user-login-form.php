<?php

/* @var $forAjax boolean */
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model LoginForm */

use app\models\forms\LoginForm;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<div class="panel panel-sign">


    <div class="panel-title-sign mt-xl text-right">
        <h2 class="title text-uppercase text-weight-bold m-none"><i class="fa fa-user mr-xs"></i> <?php echo Yii::t('backend/layout','Sing In'); ?></h2>
    </div>

    <div class="panel-body">


        <?php
        $form = ActiveForm::begin(
            [
                'id' => 'login-form',
                'options' => [
                    'enctype' => 'multipart/form-data',
                    'class' => 'form-horizontal'
                ],
                'requiredCssClass' => 'has-required',
                'fieldConfig' => [
                    'template' => "
                {label}\n
                <div class=\"input-group input-group-icon\">
                    {input}\n
                    {hint}
                </div>
                {error}\n
",
                    'labelOptions' => ['class' => ''],
                ],
            ]
        );
        ?>

        <?php echo $form->field(
            $model,
            'login',
            [
                'options' => [
                    'class' => 'form-group mb-lg',
                ],
                'labelOptions' => [
                    'class' => '',
                ],
                'inputOptions' => [
                    'class' => 'form-control input-lg',
                    'tabindex' => 1,
                ],
                'hintOptions' => [
                    'class' => 'input-group-addon',
                    'tag' => 'span',
                ]
            ]
        )->hint(
            "<span class=\"icon icon-lg\">
                    <i class=\"fa fa-user\"></i>
                </span>
                "
        );
        ?>

        <?php /*<div class="form-group mb-lg">
                <label>Username</label>
                <div class="input-group input-group-icon">

                    <input name="username" type="text" class="form-control input-lg" />
                        <span class="input-group-addon">
                            <span class="icon icon-lg">
                                <i class="fa fa-user"></i>
                            </span>
                        </span>
                </div>
            </div> */ ?>

        <?php echo $form->field(
            $model,
            'password',
            [
                'options' => [
                    'class' => 'form-group mb-lg',
                ],
                'labelOptions' => [
                    'class' => 'pull-left',
                ],
                'inputOptions' => [
                    'class' => 'form-control input-lg',
                    'tabindex' => 2,
                ],
                'hintOptions' => [
                    'class' => 'input-group-addon',
                    'tag' => 'span',
                ],
                'template' => "
                <div class=\"clearfix\">
                    {label}\n
                    <a href=\"".Yii::$app->urlManager->createUrl('/frontend/user/request-password-reset')."\" class=\"pull-right\">".Yii::t('backend/layout','Lost password')."?</a>
                </div>
                <div class=\"input-group input-group-icon\">
                    {input}\n
                    {hint}
                </div>
                {error}\n
",
            ]
        )->hint(
            "<span class=\"icon icon-lg\">
                    <i class=\"fa fa-lock\"></i>
                </span>
                "
        )->passwordInput();
        ?>

        <?php /*<div class="form-group mb-lg">
                <div class="clearfix">
                    <label class="pull-left">Password</label>
                    <a href="<?php echo Yii::$app->urlManager->createUrl('/site/recover-password')?>" class="pull-right">Lost Password?</a>
                </div>
                <div class="input-group input-group-icon">
                    <input name="pwd" type="password" class="form-control input-lg" />
                        <span class="input-group-addon">
                            <span class="icon icon-lg">
                                <i class="fa fa-lock"></i>
                            </span>
                        </span>
                </div>
            </div> */ ?>

        <div class="row">
            <?php /*<div class="col-sm-8">
                    <div class="checkbox-custom checkbox-default">
                        <input id="RememberMe" name="rememberme" type="checkbox"/>
                        <label for="RememberMe">Remember Me</label>
                    </div>
                </div> */?>

            <?php
            echo $form->field(
                $model,
                'rememberMe',
                [
                    'options'=>[
                        'class' => 'col-sm-8',
                    ],
                    'labelOptions' => [
                        'class' => '',
                    ],
                    'template' => "
                    <div class=\"checkbox-custom checkbox-default\">
                        {input}\n
                        {label}\n
                    </div>
                            ",
                ]
            )->checkbox(['tabindex' => 3])
            ?>

            <div class="col-sm-4 text-right">
                <?= Html::submitButton(Yii::t('frontend/layout','Sing In'), ['class' => 'btn btn-primary hidden-xs', 'type' => 'submit', 'name' => 'sign-in','tabindex' => 5]) ?>
                <?= Html::submitButton(Yii::t('frontend/layout','Sing In'), ['class' => 'btn btn-primary btn-block btn-lg visible-xs mt-lg', 'type' => 'submit', 'name' => 'sign-in-v','tabindex' => 6]) ?>
            </div>


        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>

