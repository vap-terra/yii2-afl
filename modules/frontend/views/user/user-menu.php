<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 19.10.16
 * Time: 7:34
 */
use app\components\web\View;
use app\models\ext\UserExt;

/* @var $this View*/
/* @var $model UserExt */
$items = [];
$items[] = [
    'id' => 'setting',
    'label' => 'Мои данные',
    'url' => Yii::$app->urlManager->createUrl('/frontend/user/profile'),
    'active' => true,
];
$items[] = [
    'id' => 'security',
    'label' => 'Безопасность',
    'url' => Yii::$app->urlManager->createUrl('/frontend/user/change-password'),
    'active' => false,
];
$items[] = [
    'id' => 'orders',
    'label' => 'Заказы',
    'url' => Yii::$app->urlManager->createUrl('/frontend/user/order-list'),
    'active' => false,
];
?>
<form action="" method="POST">
    <ul>
        <?php foreach( $items as $item ){?>
        <li>
            <label>
                <input type="radio"
                       href="<?php echo $item['url']?>"
                       class="cabinet-select"
                       name="data-security-orders"
                       data-id="<?php echo $item['id']?>"
                    <?php if( $item['active'] ){ ?> checked="checked"<?php } ?>
                /> <?php echo $item['label'];?>
            </label>
        </li>
        <?php } ?>
    </ul>
</form>
