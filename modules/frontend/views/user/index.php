<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 22.02.16
 * Time: 13:45
 */
use app\components\web\View;
use app\models\ext\UserExt;

/* @var $this View*/
/* @var $model UserExt */

$this->title = Yii::t('backend/layout', 'User profile');
$this->params['breadcrumbs'][] = Yii::t('backend/layout', 'User profile');
?>
<?php
    echo $this->render(
        'user-menu',
        [
            'model' => $model
        ]
    );
?>
<?php

?>
