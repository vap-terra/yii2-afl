<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 28.02.16
 * Time: 15:47
 */
use app\models\ext\UserExt;

/* @var $this \yii\web\View */
/* @var $model UserExt */
/* @var $result array */

$this->title = Yii::t('backend/layout','Edit user <{name}>',['name'=>$model->name]);
$this->params['breadcrumbs'][] = [
    'url'=>Yii::$app->urlManager->createUrl(['user/default/index']),
    'label'=>Yii::t('backend/layout', 'Users')
];
$this->params['breadcrumbs'][] = Yii::t('backend/layout', 'Edit');

echo $this->render(
    'user-form',
    [
        'model' => $model
    ]
);

