<?php

use app\components\web\View;
use app\models\forms\SignupForm;

/* @var $forAjax boolean */
/* @var $this View */
/* @var $model SignUpForm */

$this->title = Yii::t('frontend/layout', 'Signup');
$this->params['breadcrumbs'][] = $this->title;

echo $this->render(
    'user-sing-up-form',
    [
        'model'=>$model,
        'forAjax' => $forAjax
    ]
);
