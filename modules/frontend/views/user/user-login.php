<?php

/* @var $forAjax boolean */
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model LoginForm */

use app\models\forms\LoginForm;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('backend/layout','Sing In');
$this->params['breadcrumbs'][] = $this->title;

echo $this->render(
    'user-login-form',
    [
        'model' => $model,
        'forAjax' => $forAjax,
    ]
);

