<?php
/**
 * Created by PhpStorm.
 * profile.php
 * @package /
 * @author Polyankin Semen Vicktorovich Vol][v, http://vap-terra.ru
 * @date 14.07.15
 * @time 0:19
 * @version 0.0.1
 * Copyright © 2014 VAP-TERRA, http://vap-terra.ru
 */

/* @var $this yii\web\View */
/* @var $model personal\models\forms\ProfileForm */
$this->title = 'Профиль / Личный кабинет / LotusGrill Club';
?>
<div class="row">
    <h1 class="page-header">Профиль</h1>

    <?php
    echo $this->render(
        'partials/forms/profile-form',
        [
            'model' => $model,
            'user' => $user
        ]
    );
    ?>

</div>