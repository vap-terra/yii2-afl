<?php
use common\assets\jquery\file_upload\FileUploadAsset;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model ProfileForm */
$noPhotoMale = Yii::$app->storage->getFileHref( 'no-photo-male.jpg' );
$noPhotoFemale = Yii::$app->storage->getFileHref( 'no-photo-female.jpg' );
FileUploadAsset::register($this);
?>
<h3 class="panel-title"><?php echo Yii::t('lk/layout','Your status');?>: <?php echo Yii::t('lk/layout',$user->role);?></h3>
    <div class="col-xs-8 col-sm-5 upload-image-container partner-line">
        <?php
        $form = ActiveForm::begin([
            'id' => 'profile-photo-form',
            'options' => [
                'enctype' => 'multipart/form-data',
                'class' => 'form-horizontal upload-image-form'
            ],
            'fieldConfig' => [
                'template' => "{label}\n{input}\n<div class=\"col-lg-12\">{error}</div>",
                'labelOptions' => ['class' => 'col-lg-11 control-label'],
            ],
        ]);
        ?>
        <div class="row upload-image-drop-down text-center">
            <?php if( $user->photo ){?>
            <i class="fa fa-trash-o"> <?php echo Yii::t('lk/layout','delete');?></i>
            <?php } ?>
            <div class="preview-image">
            <?php if( $user->photo ){?>
                <img class="img-responsive img-circle" alt="" src="<?php echo $user->getItemPhotoHref();?>"/>
            <?php } else { ?>
                <?php if( $user->gender == 1 ){?>
                    <img class="img-responsive img-circle" alt="" src="<?php echo $noPhotoMale;?>"/>
                <?php } else { ?>
                    <img class="img-responsive img-circle" alt="" src="<?php echo $noPhotoFemale;?>"/>
                <?php } ?>
            <?php }?>
            </div>
            <?php echo $form->field($model, 'photo')->fileInput(['class' => 'form-control']); ?>
            <?php echo Html::hiddenInput('photo-tmp','');?>
            <i class="fa fa-upload"> <?php echo Yii::t('lk/layout','load');?></i>
            <!--<i class="fa fa-plus-square"></i>-->
        </div>
        <?php echo Html::hiddenInput('photo-button','1');?>
        <div class="form-group">
            <div class="progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="min-width: 0em;">
                    0%
                </div>
            </div>
            <div class="col-lg-offset-0 col-lg-12 text-center">
                <?= Html::submitButton( Yii::t('lk/layout','Save'), ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>


    <div class="col-xs-4 col-sm-7">
        <div class="row">

            <div class="col-md-6">
                <?php
                $form = ActiveForm::begin([
                    'id' => 'profile-data-form',
                    'options' => [
                        'enctype' => 'multipart/form-data',
                        'class' => 'form-horizontal'
                    ],
                    'fieldConfig' => [
                        'template' => "{label}\n<div class=\"col-lg-12\">{input}</div>\n<div class=\"col-lg-12\">{error}</div>",
                        'labelOptions' => ['class' => 'col-lg-11 control-label'],
                    ],
                ]);
                ?>
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h4 class="panel-title"><?php echo Yii::t('lk/layout','Change my profile');?></h4>
                    </div>
                    <div class="panel-body">
                        <?php echo $form->field($model, 'login'); ?>
                        <?php echo $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
                            'mask' => '+7(999) 999 99-99',
                        ]) ?>

                        <?php echo $form->field($model, 'email')->widget(\yii\widgets\MaskedInput::className(), [
                            'clientOptions' => [
                                'alias' =>  'email'
                            ],
                        ]) ?>
                        <?php echo Html::hiddenInput('data-button','1');?>
                        <div class="form-group">
                            <div class="col-lg-offset-0 col-lg-12 text-center">
                                <?= Html::submitButton(Yii::t('lk/layout','Save'), ['class' => 'btn btn-primary']) ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>

            <div class="col-md-6">
                <?php
                $form = ActiveForm::begin([
                    'id' => 'profile-password-form',
                    'options' => [
                        'enctype' => 'multipart/form-data',
                        'class' => 'form-horizontal'
                    ],
                    'fieldConfig' => [
                        'template' => "{label}\n<div class=\"col-lg-12\">{input}</div>\n<div class=\"col-lg-12\">{error}</div>",
                        'labelOptions' => ['class' => 'col-lg-11 control-label'],
                    ],
                ]);
                ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title"><?php echo Yii::t('lk/layout','Change password');?></h4>
                    </div>
                    <div class="panel-body">
                        <?php echo $form->field($model, 'password')->passwordInput();?>
                        <?php echo $form->field($model, 'new_password')->passwordInput();?>
                        <?php echo $form->field($model, 'repeat_password')->passwordInput();?>
                        <?php echo Html::hiddenInput('password-button','1');?>
                        <div class="form-group">
                            <div class="col-lg-offset-0 col-lg-12 text-center">
                                <?= Html::submitButton(Yii::t('lk/layout','Change'), ['class' => 'btn btn-primary']) ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>

        </div>
        <?php /* <div class="row">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">Привязано к соц. сетям</h4>
                </div>
                <div class="panel-body">

                </div>
            </div>
        </div>*/ ?>

    </div>


