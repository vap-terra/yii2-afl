<?php
use app\models\ext\GeoCountryExt;
use app\models\ext\GeoLocalityExt;
use app\models\ext\UserExt;
use app\models\forms\SignupForm;
use yii\bootstrap\Dropdown;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\MaskedInput;

/**
 * Created by PhpStorm.
 * sing-up-form.php
 * @package /
 * @author Polyankin Semen Vicktorovich Vol][v, http://vap-terra.ru
 * @date 19.07.15
 * @time 23:49
 * @version 0.0.1
 * Copyright © 2014 VAP-TERRA, http://vap-terra.ru
 */

/* @var $forAjax boolean */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model SignUpForm */
/* @var $registrar string */
?>
<div class="site-signup">
    <h1><?php echo Yii::t('frontend/layout', 'Sing Up'); ?></h1>

    <div class="row">
        <div class="col-lg-12">

<?php $form = ActiveForm::begin([
    'id' => 'sign-up-form',
    'options' => [
        'enctype' => 'multipart/form-data',
        'class' => 'form-horizontal'
    ],
    'action' => Yii::$app->urlManager->createUrl('frontend/user/sign-up'),
    'enableAjaxValidation' => true,
]); ?>
    <div class="row">
        <div class="col-md-3">
            <?php /*  echo $form->field($model, 'partner_id')->widget(MaskedInput::className(), [
                'mask' => '99999999999',
            ]) */ ?>
            <?php echo $form->field($model, 'login') ?>
            <?php echo $form->field($model, 'password')->passwordInput() ?>
            <?php echo $form->field($model, 'repeat_password')->passwordInput() ?>
        </div>

        <div class="col-md-3 col-md-offset-1">
            <?php echo $form->field($model, 'email')->widget(MaskedInput::className(), [
                'clientOptions' => [
                    'alias' => 'email'
                ],
            ]) ?>
            <?php echo $form->field($model, 'geo_country_id')->hiddenInput(); ?>
            <?php $country = $form->field($model, 'geo_country_id')
                ->dropDownList(
                    GeoCountryExt::getArOptionList(),
                    [
                        'prompt' => Yii::t('frontend/layout','select country'),
                        'onchange'=>'changeSingUpCountry(this);'
                    ]
                );
            echo $country;
            ?>

            <div class="form-group input-group phone-input">
                <div class="input-group-btn">
                    <?php
                    $items = GeoCountryExt::getArOptionList();
                    if (!empty($items)) {
                        ?>
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                            <?php if ($model->geo_country_id && !empty($items[$model->geo_country_id])) { ?>
                                <?php echo $items[$model->geo_country_id]['label']; ?>
                            <?php } else { ?>
                                <?php echo Yii::t('frontend/layout', 'code'); ?>
                            <?php } ?>
                            &nbsp;<span class="caret"></span>
                        </button>
                        <?php
                        echo Dropdown::widget([
                            'items' => $items,
                            'options' => [
                                'class' => 'dropdown-menu',
                            ]
                        ]);
                        ?>
                    <?php
                    }
                    ?>
                </div>
                <?php echo $form->field(
                    $model,
                    'phone_code',
                    [
                        'template'=>'{input}{error}',
                        'options'=>['style'=>'position: absolute;bottom: -32px;left: 0px;']
                    ]
                )->hiddenInput();?>
                <?php
                    echo $form->field(
                        $model,
                        'phone',
                        [
                            'template'=>'{input}{error}'
                        ]
                    )->widget(
                        MaskedInput::className(),
                        [
                            'mask' => '(999) 999 99-99'
                        ]
                    );
                ?>
                <div class="input-group-addon"><span class="glyphicon glyphicon-phone-alt"></span></div>
            </div>

            <?php
            $options = [];
            /*if (!$model->country_id) {
                $options['options'] = [];
                $options['options']['class'] = 'form-group hidden';
            }*/
            ?>
            <?php
            $items = [];//GeoLocalityExt::getOptionsList($model->country_id);
            /*$items['self'] = Yii::t('frontend/layout','not in list');*/
            /*echo $form->field($model, 'geo_locality_id', $options)->dropDownList( $items, ['prompt' => Yii::t('frontend/layout','select locality')]);*/ ?>
            <?php /*echo $form->field($model, 'self_geo_locality');*/ ?>
            <?php /*echo Yii::t('frontend/layout','');*/?>
            <?php echo $form->field($model, 'photo')->fileInput(); ?>
        </div>

        <div class="col-md-3 col-md-offset-1">
            <?php echo $form->field($model, 'surname'); ?>
            <?php echo $form->field($model, 'name'); ?>
            <?php echo $form->field($model, 'patronymic'); ?>
            <?php echo $form->field($model, 'gender')->dropDownList(UserExt::getOptionsGenderList(), ['prompt' => Yii::t('frontend/layout','select gender')]); ?>
        </div>

    </div>

    <div class="row form-group">
        <?php echo Html::submitButton(Yii::t('frontend/layout', 'Registration'), ['class' => 'btn btn-primary', 'name' => 'sig-nup-button']) ?>
    </div>
<?php ActiveForm::end(); ?>

    </div>
    </div>
</div>