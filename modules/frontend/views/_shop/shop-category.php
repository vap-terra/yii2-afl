<?php
use frontend\widgets\ListShopCategory;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
//$this->title = 'Shop';
//$this->params['breadcrumbs'][] = $this->title;
$noPhotoItem = Yii::$app->storage->getFileHref( 'no-photo-item.png' );
?>
<?php
if( !empty($category) && $category->id ){
?>
<div class="" style="background-color: rgba(0,0,0,1);">
    <div class="container">
        <h1><?php echo $category->title; ?></h1>
    </div>
</div>
<?php
}
?>
<div class="container shop-category-wrapper">

    <div class="col-md-9">
        <div class="shop-sorting-items">

        </div>

        <div class="shop-container-items">
        <?php
            $classColumn = 'col-md-6';
            if( count( $items ) > 2 ) {
                $classColumn = 'col-md-4';
            }
            foreach( $items as $item ) {
                $category = $item->shopCategory;
                $url = $item->getUrl();
                $modifs = $item->getModifications()->orderBy(['position'=>SORT_ASC])->andWhere(['=','active',1])->all();
                $price = $item->getPrice();
                if( !empty( $modifs ) ) {
                    foreach( $modifs as $modif ) {
                        $modifPrice = $modif->getPrice();
                        if( !$price || ($price > $modifPrice && $modifPrice) ){
                            $price = $modifPrice;
                        }
                    }
                }
            ?>
                <div class="<?php echo $classColumn;?> shop-item">
                    <?php
                        echo $this->render(
                            'partials/item/short',
                            [
                                'item' => $item,
                                'category' => $category,
                                'url' => $url,
                                'price'=>$price,
                                'currency' => 1
                            ]
                        );
                    ?>

                    <div class="modifications">
                        <div class="full-views">
                            <ul>
                            <?php if( count($modifs) ){ ?>
                                <?php foreach( $modifs as $num=>$modif ){?>
                                <li id="<?php echo $modif->id; ?>"<?php if( $num == 0 ){ ?> class="active"<?php } ?>>
                                    <?php
                                    echo $this->render(
                                        'partials/item/short',
                                        [
                                            'item' => $modif,
                                            'category' => $modif->shopCategory,
                                            'url' => $url
                                        ]
                                    );
                                    ?>
                                </li>
                                <?php } ?>
                            <?php } else {?>
                                <li id="<?php echo $item->id;?>" class="active">
                                    <?php
                                    echo $this->render(
                                        'partials/item/short',
                                        [
                                            'item' => $item,
                                            'category' => $category,
                                            'url' => $url
                                        ]
                                    );
                                    ?>
                                </li>
                            <?php } ?>
                            </ul>
                        </div>
                        <div class="small-views">
                            <ul>
                            <?php if( count($modifs) ){ ?>
                                <?php foreach( $modifs as $num=>$modif ){?>
                                    <li data-id="<?php echo $modif->id;?>"<?php if( $num == 0 ){ ?> class="active"<?php } ?>>
                                        <?php if( $modif->photo ) { ?>
                                            <img src="<?php echo $modif->getItemPhotoHref(); ?>" class="img-responsive img-circle" alt="<?php echo $modif->name;?>" />
                                        <?php } else { ?>
                                            <img src="<?php echo $noPhotoItem;?>" alt="<?php echo $modif->name;?>" class="img-responsive img-circle"/>
                                        <?php }?>
                                    </li>
                                <?php } ?>
                            <?php } else { ?>
                                <li data-id="<?php echo $item->id;?>" class="active">
                                    <?php if( $item->photo ) { ?>
                                        <img src="<?php echo $item->getItemPhotoHref(); ?>" alt="<?php echo $item->name;?>" class="img-responsive img-circle"/>
                                    <?php } else { ?>
                                        <img src="<?php echo $noPhotoItem;?>" alt="<?php echo $item->name;?>" class="img-responsive img-circle"/>
                                    <?php }?>
                                </li>
                            <?php } ?>
                            </ul>
                            <a class="imgNav img-prev" href="#">
                                <span class="fa fa-angle-double-up"></span>
                            </a>
                            <a class="imgNav img-next" href="#">
                                <span class="fa fa-angle-double-down"></span>
                            </a>
                        </div>
                    </div>
                </div>

            <?php
            }
        ?>
        </div>
    </div>
    <?php /*<div class="col-md-3">
        <div class="shop-catalog-menu">
        <?php
            echo ListShopCategory::widget(['view'=>'@frontend/views/shop/partials/category/menu']);
        ?>
        </div>
    </div>*/ ?>

</div>
