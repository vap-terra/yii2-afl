<?php
/**
 * Created by PhpStorm.
 * shop-item.php
 * @package /
 * @author Polyankin Semen Vicktorovich Vol][v, http://vap-terra.ru
 * @date 07.09.15
 * @time 16:56
 * @version 0.0.1
 * Copyright © 2014 VAP-TERRA, http://vap-terra.ru
 */
use common\vendor\vapt\jquerygui\widget\ButtonPopupActivator;
use yii\bootstrap\Button;
use yii\bootstrap\ButtonGroup;

$noPhotoItem = Yii::$app->storage->getFileHref( 'no-photo-item.png' );
$modifs = $item->getModifications()->orderBy(['position'=>SORT_ASC])->andWhere(['=','active',1])->all();
$price = $item->getPrice();
$name = $item->name;
$idToCart = $item->id;
if( !empty( $modifs ) ) {
    $price = $modifs[0]->getPrice();
    $name = $modifs[0]->name;
    $idToCart = $modifs[0]->id;
    /*foreach( $modifs as $modif ) {
        $modifPrice = $modif->getPrice();
        if( !$price || ($price > $modifPrice && $modifPrice) ){
            $price = $modifPrice;
        }
    }*/
}
?>
<div class="container shop-item-wrapper">
    <div class="row">
        <div class="col-md-6">
            <div class="full-views">
                <ul>
                    <li class="img-thumbnail img-circle main active">
                        <?php if( $item->photo ) { ?>
                            <img src="<?php echo $item->getItemPhotoHref(); ?>" alt="<?php echo $item->name;?>" class="img-responsive img-circle"/>
                        <?php } else { ?>
                            <img src="<?php echo $noPhotoItem;?>" alt="<?php echo $item->name;?>" class="img-responsive img-circle"/>
                        <?php }?>
                    </li>
                </ul>
            </div>
            <div class="small-views">
                <ul>
                    <li class="img-thumbnail img-circle main active">
                        <?php if( $item->photo ) { ?>
                            <img src="<?php echo $item->getItemPhotoHref(); ?>" alt="<?php echo $item->name;?>" class="img-responsive img-circle"/>
                        <?php } else { ?>
                            <img src="<?php echo $noPhotoItem;?>" alt="<?php echo $item->name;?>" class="img-responsive img-circle"/>
                        <?php }?>
                    </li>
                </ul>
                <a class="imgNav img-prev" href="#">
                    <span class="fa fa-angle-double-up"></span>
                </a>
                <a class="imgNav img-next" href="#">
                    <span class="fa fa-angle-double-down"></span>
                </a>
            </div>
            <div class="cart-action">
                <div class="name text-center">
                    <?php echo $name; ?>
                </div>
                <div class="price text-center">
                    <span class="price-value"><?php echo number_format($price,0,',',' ');?></span> <span class="fa fa-rub"><i class="text-hide">рублей</i></span>
                </div>
                <div class="cart-action-form-wrapper">
                    <form action="/shop/add-to-cart/<?php echo $idToCart;?>" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="input-group input-group-lg">
                                    <span class="input-group-addon btn minus"><i class="fa fa-minus-circle"></i></span>
                                    <input class="form-control" type="text" name="quantity" value="1" aria-label="Quantity"/>
                                    <span class="input-group-addon btn plus"><i class="fa fa-plus-circle"></i></span>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <?php
                                $buttons = [];
                                $buttons[] = Button::widget([
                                    'tagName' => 'a',
                                    'label' => '<i class="fa fa-cart-plus"></i> В корзину',
                                    'encodeLabel' => false,
                                    'options' => [
                                        'class'=>'btn btn-info btn-add-to-cart',
                                        'href'=>'/shop/add-to-cart/'.$idToCart,
                                    ]
                                ]);
                                $buttons[] = ButtonPopupActivator::widget([
                                    'tag'=>'a',
                                    'href'=>'/shop/fast-order/'.$idToCart,
                                    'label' => '<i class="fa fa-shopping-cart"></i> Быстрая покупка',
                                    //'encodeLabel' => false,
                                    'options'=>[
                                        'class'=>'btn btn-default btn-fast-order',
                                        'data-request-method' => 'get'
                                    ]
                                ]);
                                echo ButtonGroup::widget( [ 'buttons' =>$buttons, 'options'=>['class'=>'btn-group-lg'] ] );
                                ?>

                                <!-- <div class="btn-group btn-group-lg" role="group" aria-label="">
                                    <a href="/shop/add-to-cart/<?php echo $idToCart;?>" class="btn btn-info btn-add-to-cart"><i class="fa fa-cart-plus"></i> В корзину</a>
                                    <a href="/shop/fast-order/<?php echo $idToCart;?>" class="btn btn-default btn-fast-order">
                                        <?php /* <span class="fa-stack fa-lg">
                                            <i class="fa fa-shopping-cart fa-stack-2x"></i>
                                            <i class="fa fa-clock-o fa-stack-1x text-danger"></i>
                                        </span>*/ ?>
                                        <i class="fa fa-shopping-cart"></i> Быстрая покупка
                                    </a>
                                </div> -->
                            </div>
                        </div>
                        <input type="hidden" name="id" value="<?php echo $idToCart;?>" />
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <h1><?php echo $item->name?></h1>
            <?php if( !empty( $modifs ) ){ ?>
                <?php foreach( $modifs as $num=>$modif ){ ?>
                    <div class="col-md-4 shop-item-modification<?php if($num==0){?> active<?php }?>" data-id="<?php echo $modif->id; ?>">
                        <div class="name"><?php echo $modif->name; ?></div>
                        <div class="img-thumbnail img-circle">
                            <div class="your-choose">Ваш выбор</div>
                            <?php if( $modif->photo ) { ?>
                                <img src="<?php echo $modif->getItemPhotoHref(); ?>" data-src="<?php echo $modif->getItemPhotoHref(); ?>" alt="<?php echo $modif->name;?>" class="img-responsive img-circle"/>
                            <?php } else { ?>
                                <img src="<?php echo $noPhotoItem;?>" data-src="<?php echo $noPhotoItem; ?>" alt="<?php echo $modif->name;?>" class="img-responsive img-circle"/>
                            <?php }?>
                            <i class="fa fa-check"></i>
                            <div class="price">
                                <span class="price-value"><?php echo number_format($modif->getPrice(),0,',',' ');?></span> <span class="fa fa-rub"><i class="text-hide">рублей</i></span>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>
            <div class="col-md-12">
                <?php echo $item->description; ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="description-tab">

            <p class="p1"><strong>Техническая информация</strong></p>
            <p class="p1"><span class="s1">Приблизительный вес —&nbsp;3,7 кг</span></p>
            <p class="p1"><strong><span class="s1">Размеры:</span></strong></p>
            <ul>
                <li class="p1"><span class="s1">Диаметр верха приблизительно —&nbsp;350 мм</span></li>
                <li class="p1"><span class="s1">Диаметр дна приблизительно —&nbsp;260 мм</span></li>
                <li class="p1"><span class="s1">Высота приблизительно —&nbsp;234 мм</span></li>
                <li class="p1"><span class="s1">Диаметр решетки приблизительно —&nbsp;320 мм</span></li>
            </ul>
            <p class="p1"><strong><span class="s1">Материалы:</span></strong></p>
            <ul>
                <li class="p1"><span class="s1">Сталь и ударопрочный пластик</span></li>
                <li class="p1"><span class="s1">Решетка, внутренняя чаша, угольный контейнер и защелки: нержавеющая сталь</span></li>
            </ul>
            <p class="p1"><strong><span class="s1">В комплект входит:</span></strong></p>
            <ul>
                <li class="p1"><span class="s1">Гриль (готовый к использованию)</span></li>
                <li class="p1"><span class="s1">Сумка</span></li>
                <li class="p1"><span class="s1">4 батарейки типа АА</span></li>
                <li class="p1"><span class="s1">Руководство по использованию</span></li>
                <li class="p1">Гель для розжига</li>
            </ul>

        </div>
    </div>
</div>
