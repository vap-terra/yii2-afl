<?php
/**
 * Created by PhpStorm.
 * short.php
 * @package /
 * @author Polyankin Semen Vicktorovich Vol][v, http://vap-terra.ru
 * @date 13.09.15
 * @time 16:24
 * @version 0.0.1
 * Copyright © 2014 VAP-TERRA, http://vap-terra.ru
 */
$noPhotoItem = Yii::$app->storage->getFileHref( 'no-photo-item.png' );
$sum = 0;
$allCount = 0;
foreach( $items as $item ) {
    $sum += $item->shopItem->getPrice();
    $allCount += $item->quantity;
}
?>
<div class="shopping-cart">
    <div class="shopping-cart-title">
        <!-- <span class="text-muted">Your shopping cart</span> -->
        <div class="row">
            <div class="col-md-4 shop-cart-left">
                <i class="fa fa-cart-arrow-down"></i>
                <span class="badge badge-success"><?php echo $allCount;?></span>
            </div>
            <div class="col-md-8 shop-cart-right">
                <span class="price-value"><?php echo number_format($sum,0,',',' ');?></span> <span class="fa fa-rub"><i class="text-hide">рублей</i></span>
            </div>
        </div>
    </div>
    <div class="shopping-cart-items">
        <?php
        if( !empty( $items ) ) {
            foreach ($items as $item) {
                $shopItem = $item->shopItem;
                if ($shopItem->shop_item_id == 0) {
                    $url = $shopItem->getUrl();
                } else {
                    $url = $shopItem->shopItem->getUrl();
                }
                ?>
                <div class="shopping-cart-item row">
                    <div class="text-center">
                        <a href="<?php echo $url; ?>" class="name"><?php echo $shopItem->name; ?></a>
                    </div>
                    <div class="col-md-6">
                        <a href="<?php echo $url; ?>" class="img-thumbnail img-circle">
                            <?php if ($shopItem->photo) { ?>
                                <img src="<?php echo $shopItem->getItemPhotoHref(); ?>"
                                     alt="<?php echo $shopItem->name; ?>" class="img-responsive img-circle"/>
                            <?php } else { ?>
                                <img src="<?php echo $noPhotoItem; ?>" alt="<?php echo $shopItem->name; ?>"
                                     class="img-responsive img-circle"/>
                            <?php } ?>
                        </a>
                    </div>
                    <div class="col-md-6">
                        <div>
                            <span class="price-value"><?php echo number_format($shopItem->price, 0, ',', ' '); ?></span>
                            <span class="fa fa-rub"><i class="text-hide">рублей</i></span>
                        </div>
                        <div>
                            <span class="number"><?php echo $item->quantity; ?></span> <span>шт.</span>
                        </div>
                        <div>
                            <span
                                class="price-value"><?php echo number_format($shopItem->price * $item->quantity, 0, ',', ' '); ?></span>
                            <span class="fa fa-rub"><i class="text-hide">рублей</i></span>
                        </div>
                    </div>
                    <a href="#" class="shopping-cart-item-delete">удалить</a>
                </div>
                <?php
            }
        }
        ?>
        <div class="shopping-cart-buttons"<?php if(!$allCount){ ?>style="display: none;"<?php } ?>>
            <a href="/shop/cart" class="btn">Оформит заказ</a>
        </div>
    </div>
</div>
