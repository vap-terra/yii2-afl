<?php
/**
 * Created by PhpStorm.
 * short.php
 * @package /
 * @author Polyankin Semen Vicktorovich Vol][v, http://vap-terra.ru
 * @date 13.09.15
 * @time 16:24
 * @version 0.0.1
 * Copyright © 2014 VAP-TERRA, http://vap-terra.ru
 */
$noPhotoItem = Yii::$app->storage->getFileHref( 'no-photo-item.png' );
$sum = 0;
$allCount = 0;
foreach( $items as $item ) {
    $sum += $item->shopItem->getPrice();
    $allCount += $item->quantity;
}
?>
<h3>Корзина</h3>
<div class="full-shopping-cart">
    <div class="full-shopping-cart-items">
        <?php
            foreach( $items as $item ) {
                $shopItem = $item->shopItem;
                if( $shopItem->shop_item_id == 0 ) {
                    $url = $shopItem->getUrl();
                } else {
                    $url = $shopItem->shopItem->getUrl();
                }
        ?>
        <div class="full-shopping-cart-item row">
            <div class="col-md-2">
                <div class="table-middle">
                    <a href="<?php echo $url;?>" class="img-thumbnail img-circle shop-item-photo-wrapper">
                        <?php if( $shopItem->photo ) { ?>
                            <img src="<?php echo $shopItem->getItemPhotoHref(); ?>" alt="<?php echo $shopItem->name;?>" class="img-responsive img-circle"/>
                        <?php } else { ?>
                            <img src="<?php echo $noPhotoItem;?>" alt="<?php echo $shopItem->name;?>" class="img-responsive img-circle"/>
                        <?php }?>
                    </a>
                </div>
            </div>
            <div class="col-md-3">
                <div class="table-middle">
                    <a href="<?php echo $url;?>" class="name"><?php echo $shopItem->name;?></a>
                </div>
            </div>
            <div class="col-md-2 col-lg-2">
                <div class="table-middle">
                    <div class="price">
                        <span class="price-value"><?php echo number_format($shopItem->price,0,',',' ');?></span> <span class="fa fa-rub"><i class="text-hide">рублей</i></span>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-lg-2">
                <div class="table-middle">
                    <div class="input-group input-group-sm">
                        <span class="input-group-addon btn minus"><i class="fa fa-minus-circle"></i></span>
                        <input class="form-control" type="text" name="quantity" value="<?php echo $item->quantity;?>" aria-label="Quantity"/>
                        <span class="input-group-addon btn plus"><i class="fa fa-plus-circle"></i></span>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="table-middle">
                    <div class="price">
                        <span class="price-value"><?php echo number_format($shopItem->price * $item->quantity,0,',',' ');?></span> <span class="fa fa-rub"><i class="text-hide">рублей</i></span>
                    </div>
                </div>
            </div>
            <a href="#" class="full-shopping-cart-item-delete">удалить</a>
        </div>
        <?php
            }
        ?>
    </div>
    <div class="full-shopping-cart-footer h3">
        <div class="row">
            <div class="col-md-6 text-center">Итого:&nbsp;</div>
            <div class="col-md-3">
                <?php echo $allCount;?>
            </div>
            <div class="col-md-3">
                <span class="price-value"><?php echo number_format($sum,0,',',' ');?></span> <span class="fa fa-rub"><i class="text-hide">рублей</i></span>
            </div>
        </div>
    </div>
</div>
