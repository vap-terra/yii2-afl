<?php
/**
 * Created by PhpStorm.
 * shop-item.php
 * @package /
 * @author Polyankin Semen Vicktorovich Vol][v, http://vap-terra.ru
 * @date 07.09.15
 * @time 22:18
 * @version 0.0.1
 * Copyright © 2014 VAP-TERRA, http://vap-terra.ru
 */
/* @var */
?>
<?php echo $item->generatePhotoFileName( 'jpg' );?>
<a href="<?php echo $category->getUrl();?>" class="category-name"><?php echo $category->title;?></a>
<a href="<?php echo $url;?>" class="name"><?php echo $item->name?></a>
<figure>
    <a href="<?php echo $url;?>" class="img-thumbnail img-circle">
        <?php if( $item->photo ) { ?>
            <img src="<?php echo $item->getItemPhotoHref(); ?>" alt="<?php echo $item->name;?>" class="img-responsive img-circle"/>
        <?php } else { ?>
            <img src="<?php echo $noPhotoItem;?>" alt="<?php echo $item->name;?>" class="img-responsive img-circle"/>
        <?php }?>
    </a>
</figure>
<div class="price">
    <?php if( !empty( $price ) && $price ){ ?>
    от <span class="price-value"><?php echo number_format($price,0,',',' ');?></span> <span class="fa fa-rub"><i class="text-hide">рублей</i></span>
    <?php } else { ?>
    <span class="price-value"><?php echo number_format($item->getPrice(),0,',',' ');?></span> <span class="fa fa-rub"><i class="text-hide">рублей</i></span>
    <?php } ?>
</div>
<a href="<?php echo $url;?>" class="link"><span class="fa fa-eye"></span> смотреть</a>
