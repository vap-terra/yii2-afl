<?php
/**
 * Created by PhpStorm.
 * list.php
 * @package /
 * @author Polyankin Semen Vicktorovich Vol][v, http://vap-terra.ru
 * @date 30.08.15
 * @time 23:46
 * @version 0.0.1
 * Copyright © 2014 VAP-TERRA, http://vap-terra.ru
 */
use yii\helpers\Html;

$noPhotoItem = Yii::$app->storage->getFileHref( 'no-photo-item.png' );
?>
<?php
if( !empty( $items ) && is_array($items) ) {
    ?>
    <div class="nav-shop-category-list">
        <ul>
            <?php
            foreach ($items as $item) {
                ?>
                <li>
                    <?php
                    if ($item->image) {
                        $categoryImg = $item->getFileHref();
                    } else {
                        $categoryImg = $noPhotoItem;
                    }
                    $url = $item->getUrl();
                    $img = Html::img($categoryImg, ['alt' => $item->title, 'title' => $item->title, 'class' => 'img-circle']);
                    echo Html::a($item->title, $url );
                    echo Html::a($img, $url,['title'=> $item->title, 'class'=>'img-link']);

                    ?>
                </li>
                <?php
            }
            ?>
        </ul>
    </div>
    <?php
}
?>