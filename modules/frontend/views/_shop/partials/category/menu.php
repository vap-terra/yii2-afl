<?php
/**
 * Created by PhpStorm.
 * list.php
 * @package /
 * @author Polyankin Semen Vicktorovich Vol][v, http://vap-terra.ru
 * @date 30.08.15
 * @time 23:46
 * @version 0.0.1
 * Copyright © 2014 VAP-TERRA, http://vap-terra.ru
 */
use yii\helpers\Html;

$noPhotoItem = Yii::$app->storage->getFileHref( 'no-photo-item.png' );
?>
<?php
if( !empty( $items ) && is_array($items) ) {
    ?>
        <ul class="list-group">
            <?php
            foreach ($items as $item) {
                ?>
                <li class="list-group-item">
                    <?php
                    /*if ($item->image) {
                        $categoryImg = Yii::$app->storage->getFileHref($item->image);
                    } else {
                        $categoryImg = $noPhotoItem;
                    }*/
                    $url = $item->getUrl();
                    //$img = Html::img($categoryImg, ['alt' => $item->title, 'title' => $item->title, 'class' => 'img-circle']);
                    echo Html::a( '<i class="fa fa-power-off fa-fw"></i>'.$item->title, $url );
                    //echo Html::a($img, $url,['title'=> $item->title, 'class'=>'img-link']);
                    ?>
                    <?php /*<span class="caret">
                    <i class="fa fa-plus-circle"></i>
                    <i class="fa fa-minus-circle"></i>
                    </span>*/ ?>
                </li>
                <?php
            }
            ?>
        </ul>
    <?php
}
?>