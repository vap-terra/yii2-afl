<?php
/**
 * Created by PhpStorm.
 * shop-cart.php
 * @package /
 * @author Polyankin Semen Vicktorovich Vol][v, http://vap-terra.ru
 * @date 12.09.15
 * @time 16:42
 * @version 0.0.1
 * Copyright © 2014 VAP-TERRA, http://vap-terra.ru
 */
$noPhotoItem = Yii::$app->storage->getFileHref( 'no-photo-item.png' );
?>
<div style="background-color: rgba(0,0,0,1);">
    <div class="container">
        <h1>Оформление заказа</h1>
    </div>
</div>
<div class="container">
    <div id="full-shopping-cart" class="col-md-8 col-md-offset-2">
    <?php
        if( !empty( $items ) ) {
            echo $this->render(
                'partials/cart/list',
                [
                    'items' => $items
                ]
            );
        } else {
    ?>
    <h2>Ваша корзина пуста. Для оформления заказа, добавьте товар из <a href="/shop/">каталога</a>.</h2>
    <?php
        }
    ?>
    </div>
<?php
    if( !empty( $items ) ) {
?>
        <div>
            <div class="col-md-6">
                <form>

                    <h3 class="text-center">Адрес доставки</h3>
                    <div>

                    </div>
                    <h3 class="text-center">Кому</h3>
                    <div>

                    </div>
                </form>
            </div>
            <div class="col-md-6">
                <h3 class="text-center">Способ доставки</h3>
                <div>

                </div>
                <h3 class="text-center">Способ оплаты</h3>
                <div>

                </div>
            </div>
        </div>
<?php
    }
?>
</div>

