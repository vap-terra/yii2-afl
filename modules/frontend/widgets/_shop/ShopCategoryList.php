<?php
/**
 * Created by PhpStorm.
 * MenuShopCategoryWidget.php
 * @package /
 * @author Polyankin Semen Vicktorovich Vol][v, http://vap-terra.ru
 * @date 30.08.15
 * @time 23:28
 * @version 0.0.1
 * Copyright © 2014 VAP-TERRA, http://vap-terra.ru
 */

namespace frontend\widgets;

use common\models\ext\ShopCategoryExt;
use Yii;
use yii\bootstrap\Nav;
use yii\bootstrap\Widget;

class ListShopCategory extends Widget
{
    public $view = 'shop/category/list';
    public $depth = 0;
    public $onlyActive = true;
    public $shopCategoryId = 0;
    public $orderBy = 'position';
    public $orderDirect = SORT_ASC;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $qCategory = ShopCategoryExt::find();
        if( $this->onlyActive ) {
            $qCategory->where(['=','active','1']);
        }

        if( $this->shopCategoryId ) {
            $shopCategory = ShopCategoryExt::find()->where(['=','id',$this->shopCategoryId])->one();
            $qCategory->where(['=','shop_category_id',$this->shopCategoryId]);
        }
        if( $this->depth  ) {
            $qCategory->where(
                [
                    '<=',
                    'depth',
                    (!empty($shopCategory)?$shopCategory->depth:0) + $this->depth
                ]
            );
        }
        $qCategory
            ->orderBy([$this->orderBy=>$this->orderDirect])
            ->addOrderBy([ 'left_border' => SORT_ASC]);
        return $this->render(
            $this->view,
            [
                'items'=>$qCategory->all()
            ]
        );
    }
}