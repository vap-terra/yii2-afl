<?php
/**
 * Created by PhpStorm.
 * MenuShopCategoryWidget.php
 * @package /
 * @author Polyankin Semen Vicktorovich Vol][v, http://vap-terra.ru
 * @date 30.08.15
 * @time 23:28
 * @version 0.0.1
 * Copyright © 2014 VAP-TERRA, http://vap-terra.ru
 */

namespace frontend\widgets;

use common\models\ext\ShopCategoryExt;
use Yii;
use yii\bootstrap\Nav;
use yii\bootstrap\Widget;

class MenuShopCategory extends Widget
{
    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $menuItems = [];
        $categoryItems = ShopCategoryExt::findForMenu()->all();
        foreach( $categoryItems as $categoryItem ) {
            $menuItems[] = [
                'label' => $categoryItem->title,
                'url' => ['/shop/'.$categoryItem->tid.'/']
            ];
        }
        return Nav::widget([
            'options' => ['class' => 'navbar-nav nav-shop-category'],
            'items' => $menuItems,
        ]);
    }
}