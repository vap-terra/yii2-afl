<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 07.08.16
 * Time: 11:00
 */

use app\components\web\View;
use app\models\ext\FormExt;
use yii\helpers\Html;

/* @var $this View */
/* @var $form FormExt */
/* @var $title string */
/* @var $icon string */
/* @var $cssIcon string */
/* @var $options string */

\app\assets\FancyboxAsset::register($this);

if( !empty( $cssIcon ) ) {
    $title = Html::tag('i', '', ['class' => $cssIcon]).' '.$title;
}
if( empty( $options['class'] ) ) {
    $options['class'] = '';
}
$options['class'] .= ' ajax-load-form-link fancybox fancybox.ajax';
echo Html::a($title,$form->getUrl(),$options);
