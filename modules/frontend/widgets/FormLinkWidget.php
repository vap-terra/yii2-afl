<?php
namespace app\modules\frontend\widgets;

use app\components\Widget;
use app\models\ext\FormExt;

/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 27.07.16
 * Time: 17:47
 */
class FormLinkWidget extends Widget
{
    public $formId;
    public $title;
    public $icon;
    public $cssIcon;
    public $options = [];
    public $template = 'form-link';

    public function run()
    {
        return $this->render(
            $this->template,
            [
                'form'=>FormExt::getByTid($this->formId),
                'title'=>$this->title,
                'icon' => $this->icon,
                'cssIcon'=>$this->cssIcon,
                'options'=>$this->options,
            ]
        );
    }
}