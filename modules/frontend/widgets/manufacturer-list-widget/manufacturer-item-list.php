<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 17.10.16
 * Time: 8:02
 */
use app\components\web\View;
use app\models\ext\ManufacturerExt;
use yii\helpers\Html;

/* @var $this View*/
/* @var $items ManufacturerExt[] */
/* @var $title string */
if( !empty( $items ) ) {

    app\themes\remax\assets\Owl2CarouselAsset::register($this);

    $this->registerJs("
$(document).ready(function(){
    var owlCarouselMain = $('.home-producers_slider').owlCarousel(
        {
            center: true,
            items:7,
            loop:".(count($items)==1?'false':'true').",
            margin:0,
            autoplay:true,
            autoplayTimeout:5000,
            autoplayHoverPause:true,
            responsive:{
                600:{
                    items:7
                }
            }
        }
    );
    $('.home-producers_slider_NextBtn').click(function() {
        owlCarouselMain.trigger('next.owl.carousel');
    });
    $('.home-producers_slider_PrevBtn').click(function() {
        owlCarouselMain.trigger('prev.owl.carousel');
    });
});
", View::POS_END, 'manufacturer_carousel_init_main');

    ?>
    <div class="home-producers clearfix">

        <?php if ($title) { ?>
            <h2><?php echo $title; ?></h2>
        <?php } ?>

        <div class="home-producers_slider owl-carousel owl-theme">

            <?php foreach($items as $item){ ?>
            <div class="home-producers_slider_img item">
                <?php
                $src = Yii::$app->imager->getSrcResize(128,56,$item->getImageHref());
                echo Html::img(
                    $src,
                    [
                        'class'=>'',
                        'alt'=>$item->name,
                    ]
                );
                ?>
            </div>
            <?php } ?>

        </div>

    </div>
<?php
}
?>