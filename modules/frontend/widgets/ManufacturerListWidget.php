<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 01.08.16
 * Time: 21:49
 */
namespace app\modules\frontend\widgets;

use app\components\Widget;
use app\models\ext\ManufacturerExt;


class ManufacturerListWidget extends Widget
{

    public $route = '';
    public $limit = 7;
    public $order = 'position';
    public $orderDirection = SORT_DESC;
    public $template = 'manufacturer-item-list';
    public $scopes = [];

    public $title;

    public function run()
    {
        /* @var $items ManufacturerExt[] */
        $qItems = ManufacturerExt::find();
        if( !empty( $this->scopes ) ){
            $qItems->andWhere($this->scopes);
        }
        if( $this->limit > 0 ){
            $qItems->limit($this->limit);
        }
        if( $this->order ) {
            $qItems->orderBy([$this->order=>$this->orderDirection?$this->orderDirection:SORT_DESC]);
        }
        return $this->render(
            $this->template,
            [
                'items'=>$qItems->all(),
                'title' => $this->title,
            ]
        );
    }
}