<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 01.08.16
 * Time: 21:49
 */
namespace app\modules\frontend\widgets;

use app\components\Widget;
use app\models\ext\InformationExt;
use app\models\ext\InformationGroupExt;
use app\models\ext\InformationItemExt;


class InformationItemListWidget extends Widget
{

    public $informationId;
    public $informationGroupId = -1;//from all groups
    public $subGroups = 0;// search in sub groups
    public $limit = 3;
    public $order = 'dt_creation';
    public $orderDirection = SORT_DESC;
    public $template = 'information-item-list';
    public $title;

    public function run()
    {
        /* @var $information InformationExt */
        /* @var $items InformationItemExt[] */
        $information = InformationExt::getByTid($this->informationId);
        $group = null;
        if( $this->informationGroupId > 0 ) {
            $group = InformationGroupExt::getByTid($this->informationGroupId);
        }
        $qItems = $information->getInformationItems();
        if( $this->informationGroupId > -1 ){
            $qItems->andWhere(['=','information_group_id',$this->informationGroupId]);
        }
        if( $this->subGroups ) {
            //TODO
        }
        if( $this->limit > 0 ){
            $qItems->limit($this->limit);
        }
        if( $this->order ) {
            $qItems->orderBy([$this->order=>$this->orderDirection?$this->orderDirection:SORT_DESC]);
        }
        return $this->render(
            $this->template,
            [
                'title' => $this->title,
                'information' => $information,
                'group' => $group,
                'items' => $qItems->all()
            ]
        );
    }
}