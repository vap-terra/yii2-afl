<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 01.08.16
 * Time: 21:49
 */
namespace app\modules\frontend\widgets;

use app\components\Widget;
use app\models\ext\BannerExt;
use app\models\ext\BannerGroupExt;



class BannerListWidget extends Widget
{

    public $bannerGroupId = -1;//from all groups
    public $subGroups = 0;// search in sub groups
    public $limit = 3;
    public $order = 'position';
    public $orderDirection = SORT_DESC;
    public $template = 'banner-item-list';
    public $scopes = [];

    public function run()
    {
        /* @var $items BannerExt[] */
        $group = null;
        if( $this->bannerGroupId > 0 ) {
            $group = BannerGroupExt::getByTid($this->bannerGroupId);
        }
        $qItems = BannerExt::find();
        if( $this->bannerGroupId > -1 ){
            $qItems->andWhere(['=','banner_group_id',$this->bannerGroupId]);
        }
        if( $this->subGroups ) {
            //TODO
        }
        if( !empty( $this->scopes ) ){
            $qItems->andWhere($this->scopes);
        }
        if( $this->limit > 0 ){
            $qItems->limit($this->limit);
        }
        if( $this->order ) {
            $qItems->orderBy([$this->order=>$this->orderDirection?$this->orderDirection:SORT_DESC]);
        }
        return $this->render(
            $this->template,
            [
                'group' => $group,
                'items'=>$qItems->all()
            ]
        );
    }
}