<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 01.08.16
 * Time: 21:58
 */
use app\models\ext\InformationExt;
use app\models\ext\InformationGroupExt;
use app\models\ext\InformationItemExt;
use vapterra\yii2vtimager\Image;
use yii\helpers\Html;

/* @var $information InformationExt */
/* @var $title string */
/* @var $group InformationGroupExt | null */
/* @var $items InformationItemExt[] */
?>
<?php if( $this->title){ ?>
    <h2><?php echo $this->title;?></h2>
<?php } ?>
<?
if( !empty( $items ) ) {
    foreach( $items as $item ) {
        $url = $item->getUrl();
        ?>

        <div class="media">
            <div class="media-left">
                <a href="<?php echo $url ?>">
                    <?php

                    $src = Yii::$app->imager->getSrcResize(70,70,$item->getImageHref(),true,[],Image::RESIZE_PROPORTIONAL_CUTE);
                    echo Html::img(
                        $src,
                        [
                            'class'=>'media-object',
                            'alt'=>( $item->header ? $item->header : $item->name ),
                        ]
                    );
                    ?>
                </a>
            </div>
            <div class="media-body">
                <span class="date"><span class="glyphicon glyphicon-time"></span> <?php echo Yii::$app->formatter->asDate( $item->dt_creation )?></span>
                <h4 class="media-heading"><a href="<?php echo $url ?>"><?php echo $item->header ? $item->header : $item->name ?></a></h4>
            </div>
            <div>
                <?php if( $item->description ){ ?>
                    <?php echo $item->description ?>
                <?php } else if( $item->content ) {?>
                    <?php echo \yii\helpers\StringHelper::truncate( strip_tags($item->content), 200, '...' ); ?>
                <?php } ?>
            </div>
        </div>
        <a class="btn btn-primary btn-sm" href="<?php echo $url ?>"><?php echo Yii::t('frontend/layout','Read more'); ?> <span class="glyphicon glyphicon-chevron-right"></span></a>
        <?php
    }
}
?>