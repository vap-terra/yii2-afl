<?php
namespace app\modules\frontend\widgets;

use app\components\Widget;
use app\models\ext\FormExt;
use Yii;

/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 27.07.16
 * Time: 17:47
 */
class FormWidget extends Widget
{
    public $formId;
    public $orderFields = 'position';
    public $orderDirectionFields = SORT_ASC;
    public $template = 'form';

    public function run()
    {
        $form = FormExt::getByTid($this->formId);
        return $this->render(
            $this->template,
            [

                'forAjax' => Yii::$app->request->isAjax,
                'model'=>$form,
                'elements'=>$form->getFormElements()->andWhere(['=','active',1])->all(),
                'rowTemplate' => '<div class="form-group">{label}<div>{description}</div>{input}</div>',
            ]
        );
    }
}