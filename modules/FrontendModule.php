<?php
namespace app\modules;
use app\components\Theme;
use app\models\BaseActiveQuery;
use app\models\ext\FormExt;
use app\models\ext\InformationExt;
use Yii;


/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 27.06.16
 * Time: 23:14
 */
class FrontendModule extends \yii\base\Module
{
    protected static $_widgets = [];
    public function init()
    {
        if( strlen( trim( $themeName = Yii::$app->session->get('frontend_theme') ) ) == 0 ) {
            $themeName = Yii::$app->params['default_theme'];
        }
        $theme = new Theme(['type'=>Theme::TYPE_FRONTEND]);
        $theme->setBasePath('@app/themes/'.$themeName.'/');
        $theme->setBaseUrl('/themes/'.$themeName.'/');
        $theme->pathMap = [
            '@app/modules/frontend/views/layouts/' => '@app/themes/'.$themeName.'/layouts',
            '@app/modules/frontend/widgets/views/' => '@app/themes/'.$themeName.'/widgets',
            '@app/modules/frontend/widgets/' => '@app/themes/'.$themeName.'/widgets',
            '@app/modules/frontend/views/' => '@app/themes/'.$themeName,
            '@app/modules/frontend/' => '@app/themes/'.$themeName,
            '@app/views/layouts/' => '@app/themes/'.$themeName.'/layouts',
            '@app/views/widgets/views/' => '@app/themes/'.$themeName.'/widgets',
            '@app/views/widgets/' => '@app/themes/'.$themeName.'/widgets',
            '@app/views/' => '@app/themes/'.$themeName,
        ];
        Yii::$app->view->theme = $theme;
        $this->controllerNamespace = 'app\modules\frontend\controllers';
        $this->setViewPath('@app/modules/frontend/views');
        //$this->setLayoutPath($theme->getBasePath().'/layouts');
        Yii::$app->user->loginUrl = Yii::$app->urlManager->createUrl(['/frontend/user/login']);
        parent::init();
    }

    /**
     * @param $widget string
     * @return null | array
     */
    public static function getWidgetSchema( $widget )
    {
        self::getWidgets();
        if( isset( self::$_widgets[$widget] ) ) {
            return self::$_widgets[$widget];
        }
        return null;
    }

    public function widget( $widget, $params )
    {
        $schema = self::getWidgetSchema($widget);
        $class = $schema['class'];
        $params['module'] = $this;
        return $class::widget($params);
    }

    /**
     * @return array
     */
    public static function getWidgets()
    {
        if( empty(self::$_widgets) ){
            self::$_widgets = [
                'FormLink' => [
                    'class' => '\app\modules\frontend\widgets\FormLinkWidget',
                    'title' => Yii::t('frontend/layout','Show form'),
                    'description' => '',
                    'parameters' => [
                        'formId' => [
                            'label'=>Yii::t('frontend/layout','Form'),
                            'type'=>'select',
                            'default_value'=>null,
                            'values'=>FormExt::getArOptionList(BaseActiveQuery::ACTIVE),
                        ],
                        'template' => [
                            'label'=>Yii::t('frontend/layout','Template'),
                            'type'=>'select',
                            'default_value'=>'form-link',
                            'values' => [
                                'form'=>Yii::t('frontend/layout','Standard form link')
                            ]
                        ],
                    ]
                ],
                'Form' => [
                    'class' => '\app\modules\frontend\widgets\FormWidget',
                    'title' => Yii::t('frontend/layout','Show form'),
                    'description' => '',
                    'parameters' => [
                        'formId' => [
                            'label'=>Yii::t('frontend/layout','Form'),
                            'type'=>'select',
                            'default_value'=>null,
                            'values'=>FormExt::getArOptionList(BaseActiveQuery::ACTIVE),
                        ],
                        'orderFields' => [
                            'label'=>Yii::t('frontend/layout','Order fields'),
                            'type'=>'select',
                            'multiple'=>true,
                            'default_value'=>'position',
                            'values' => [
                                'position' => Yii::t('frontend/layout','Position'),
                                'id' => Yii::t('frontend/layout','Id'),
                                'name' => Yii::t('frontend/layout','Name'),
                            ]
                        ],
                        'orderDirectionFields' => [
                            'label'=>Yii::t('frontend/layout','Order direction fields'),
                            'type'=>'select',
                            'default_value'=>SORT_ASC,
                            'values' => [
                                SORT_ASC=>Yii::t('frontend/layout','Ascending'),//по возрастанию
                                SORT_DESC=>Yii::t('frontend/layout','Descending'),//по убыванию
                            ]
                        ],
                        'template' => [
                            'label'=>Yii::t('frontend/layout','Template'),
                            'type'=>'select',
                            'default_value'=>'form',
                            'values' => [
                                'form'=>Yii::t('frontend/layout','Standard form')
                            ]
                        ],
                    ]
                ],
                'InformationItemList' => [
                    'class' => '\app\modules\frontend\widgets\InformationItemListWidget',
                    'title' => Yii::t('frontend/layout','Information blocks'),
                    'description' => '',
                    'parameters' => [
                        'informationId' => [
                            'label'=>Yii::t('frontend/layout','Information system'),
                            'type'=>'select',
                            'default_value'=>null,
                            'values'=>InformationExt::getArOptionList(BaseActiveQuery::ACTIVE),
                        ],
                        'informationGroupId' => [
                            'label'=>Yii::t('frontend/layout','Information group'),
                            'type'=>'select',
                            'depends'=>'informationId',
                            'default_value'=>null,
                            'values'=>[],
                        ],
                        'subGroups' => [
                            'label'=>Yii::t('frontend/layout','Show from sub groups'),
                            'type'=>'checkbox',
                            'default_value'=>false
                        ],
                        'limit' => [
                            'label'=>Yii::t('frontend/layout','The number on one page'),
                            'type'=>'input',
                            'default_value'=>3
                        ],
                        'order' => [
                            'label'=>Yii::t('frontend/layout','Order fields'),
                            'type'=>'select',
                            'multiple'=>true,
                            'default_value'=>'dt_creation',
                            'values' => [
                                'dt_creation' => Yii::t('frontend/layout','Date time of creation'),
                                'position' => Yii::t('frontend/layout','Position'),
                                'id' => Yii::t('frontend/layout','Id'),
                                'name' => Yii::t('frontend/layout','Name'),
                            ]
                        ],
                        'orderDirection' => [
                            'label'=>Yii::t('frontend/layout','Order direction'),
                            'type'=>'select',
                            'default_value'=>SORT_DESC,
                            'values' => [
                                SORT_ASC=>Yii::t('frontend/layout','Ascending'),//по возрастанию
                                SORT_DESC=>Yii::t('frontend/layout','Descending'),//по убыванию
                            ]
                        ],
                        'template' => [
                            'label'=>Yii::t('frontend/layout','Template'),
                            'type'=>'select',
                            'default_value'=>'information-item-list',
                            'values' => [
                                'form'=>Yii::t('frontend/layout','Standard form')
                            ]
                        ],
                        'title' => [
                            'label'=>Yii::t('frontend/layout','Title block'),
                            'type'=>'string',
                            'default_value'=>Yii::t('frontend/layout','News'),
                        ]
                    ]
                ],
                'BannerList' => [
                    'class' => '\app\modules\frontend\widgets\BannerListWidget',
                    'title' => Yii::t('frontend/layout','Banner blocks'),
                    'description' => '',
                    'parameters' => [
                        'bannerGroupId' => [
                            'label'=>Yii::t('frontend/layout','Banner group'),
                            'type'=>'select',
                            'depends'=>'informationId',
                            'default_value'=>null,
                            'values'=>[],
                        ],
                        'subGroups' => [
                            'label'=>Yii::t('frontend/layout','Show from sub groups'),
                            'type'=>'checkbox',
                            'default_value'=>false
                        ],
                        'limit' => [
                            'label'=>Yii::t('frontend/layout','The number on one page'),
                            'type'=>'input',
                            'default_value'=>3
                        ],
                        'order' => [
                            'label'=>Yii::t('frontend/layout','Order fields'),
                            'type'=>'select',
                            'multiple'=>true,
                            'default_value'=>'position',
                            'values' => [
                                'position' => Yii::t('frontend/layout','Position'),
                                'id' => Yii::t('frontend/layout','Id'),
                                'name' => Yii::t('frontend/layout','Name'),
                            ]
                        ],
                        'orderDirection' => [
                            'label'=>Yii::t('frontend/layout','Order direction'),
                            'type'=>'select',
                            'default_value'=>SORT_ASC,
                            'values' => [
                                SORT_ASC=>Yii::t('frontend/layout','Ascending'),//по возрастанию
                                SORT_DESC=>Yii::t('frontend/layout','Descending'),//по убыванию
                            ]
                        ],
                        'template' => [
                            'label'=>Yii::t('frontend/layout','Template'),
                            'type'=>'select',
                            'default_value'=>'banner-item-list',
                            'values' => [
                                'banner-item-list'=>Yii::t('frontend/layout','Standard list'),
                                'banner-item-list-carousel'=>Yii::t('frontend/layout','Standard banner carousel'),
                            ]
                        ],
                    ]
                ],
                'ManufacturerList' => [
                    'class' => '\app\modules\frontend\widgets\ManufacturerListWidget',
                    'title' => Yii::t('frontend/layout','Manufacturer blocks'),
                    'description' => '',
                    'parameters' => [
                        'route' => [
                            'label'=>Yii::t('frontend/layout','The number on one page'),
                            'type'=>'input',
                            'default_value'=>''
                        ],
                        'limit' => [
                            'label'=>Yii::t('frontend/layout','The number on one page'),
                            'type'=>'input',
                            'default_value'=>7
                        ],
                        'order' => [
                            'label'=>Yii::t('frontend/layout','Order fields'),
                            'type'=>'select',
                            'multiple'=>true,
                            'default_value'=>'position',
                            'values' => [
                                'position' => Yii::t('frontend/layout','Position'),
                                'id' => Yii::t('frontend/layout','Id'),
                                'name' => Yii::t('frontend/layout','Name'),
                            ]
                        ],
                        'orderDirection' => [
                            'label'=>Yii::t('frontend/layout','Order direction'),
                            'type'=>'select',
                            'default_value'=>SORT_ASC,
                            'values' => [
                                SORT_ASC=>Yii::t('frontend/layout','Ascending'),//по возрастанию
                                SORT_DESC=>Yii::t('frontend/layout','Descending'),//по убыванию
                            ]
                        ],
                        'template' => [
                            'label'=>Yii::t('frontend/layout','Template'),
                            'type'=>'select',
                            'default_value'=>'manufacturer-items-list',
                            'values' => [
                                'manufacturer-items-list'=>Yii::t('frontend/layout','Standard manufacturer list'),
                            ]
                        ],
                        'title'=>[
                            'label'=>Yii::t('frontend/layout','Title block'),
                            'type'=>'string',
                            'default_value'=>'',
                        ],
                    ]
                ],
                'ShopCartShort' => [
                    'class' => '\app\modules\frontend\widgets\ShopCartShortWidget',
                    'title' => Yii::t('frontend/layout','Shop Cart Short'),
                    'description' => '',
                    'parameters' => [
                        'shopId' => [
                            'label'=>Yii::t('frontend/layout','Shop'),
                            'type'=>'select',
                            'default_value'=>null,
                            'values'=>[],
                        ],
                        'order' => [
                            'label'=>Yii::t('frontend/layout','Order fields'),
                            'type'=>'select',
                            'multiple'=>true,
                            'default_value'=>'id',
                            'values' => [
                                'id' => Yii::t('frontend/layout','Id'),
                                'name' => Yii::t('frontend/layout','Name'),
                            ]
                        ],
                        'orderDirection' => [
                            'label'=>Yii::t('frontend/layout','Order direction'),
                            'type'=>'select',
                            'default_value'=>SORT_ASC,
                            'values' => [
                                SORT_ASC=>Yii::t('frontend/layout','Ascending'),//по возрастанию
                                SORT_DESC=>Yii::t('frontend/layout','Descending'),//по убыванию
                            ]
                        ],
                        'template' => [
                            'label'=>Yii::t('frontend/layout','Template'),
                            'type'=>'select',
                            'default_value'=>'shop-cart-short',
                            'values' => [
                                'shop-cart-short'=>Yii::t('frontend/layout','Standard short cart'),
                            ]
                        ],
                    ]
                ],
                'ShopCartInfoPanel' => [
                    'class' => '\app\modules\frontend\widgets\ShopCartInfoPanelWidget',
                    'title' => Yii::t('frontend/layout','Information about Shop Cart'),
                    'description' => '',
                    'parameters' => [
                        'shopId' => [
                            'label'=>Yii::t('frontend/layout','Shop'),
                            'type'=>'select',
                            'default_value'=>null,
                            'values'=>[],
                        ],
                        'template' => [
                            'label'=>Yii::t('frontend/layout','Template'),
                            'type'=>'select',
                            'default_value'=>'shop-cart-info-panel',
                            'values' => [
                                'shop-cart-info-panel'=>Yii::t('frontend/layout','Standard shop cart info panel'),
                            ]
                        ],
                    ]
                ],
                'ShopGroupsList' => [
                    'class' => '\app\modules\frontend\widgets\ShopGroupsListWidget',
                    'title' => Yii::t('frontend/layout','List of shop groups'),
                    'description' => '',
                    'parameters' => [
                        'shopGroupId' => [
                            'label'=>Yii::t('frontend/layout','Shop group'),
                            'type'=>'select',
                            'depends'=>'informationId',
                            'default_value'=>null,
                            'values'=>[],
                        ],
                        'subGroups' => [
                            'label'=>Yii::t('frontend/layout','Show from sub groups'),
                            'type'=>'checkbox',
                            'default_value'=>false
                        ],
                        'limit' => [
                            'label'=>Yii::t('frontend/layout','The number on one page'),
                            'type'=>'input',
                            'default_value'=>100
                        ],
                        'order' => [
                            'label'=>Yii::t('frontend/layout','Order fields'),
                            'type'=>'select',
                            'multiple'=>true,
                            'default_value'=>'lb',
                            'values' => [
                                'lb' => Yii::t('frontend/layout','Tree sort'),
                                'position' => Yii::t('frontend/layout','Position'),
                                'id' => Yii::t('frontend/layout','Id'),
                                'name' => Yii::t('frontend/layout','Name'),
                            ]
                        ],
                        'orderDirection' => [
                            'label'=>Yii::t('frontend/layout','Order direction'),
                            'type'=>'select',
                            'default_value'=>SORT_ASC,
                            'values' => [
                                SORT_ASC=>Yii::t('frontend/layout','Ascending'),//по возрастанию
                                SORT_DESC=>Yii::t('frontend/layout','Descending'),//по убыванию
                            ]
                        ],
                        'template' => [
                            'label'=>Yii::t('frontend/layout','Template'),
                            'type'=>'select',
                            'default_value'=>'shop-groups-list',
                            'values' => [
                                'shop-groups-list'=>Yii::t('frontend/layout','Standard shop groups list'),
                            ]
                        ],
                        'title'=>[
                            'label'=>Yii::t('frontend/layout','Title block'),
                            'type'=>'string',
                            'default_value'=>'',
                        ],
                        'showCountItems'=>[
                            'label'=>Yii::t('frontend/layout','Show count items in group'),
                            'type'=>'boolean',
                            'default_value'=>0,
                        ]
                    ]
                ],
                'ShopItemsList' => [
                    'class' => '\app\modules\frontend\widgets\ShopItemsListWidget',
                    'title' => Yii::t('frontend/layout','List of shop items'),
                    'description' => '',
                    'parameters' => [
                        'shopGroupId' => [
                            'label'=>Yii::t('frontend/layout','Shop group'),
                            'type'=>'select',
                            'depends'=>'informationId',
                            'default_value'=>null,
                            'values'=>[],
                        ],
                        'subGroups' => [
                            'label'=>Yii::t('frontend/layout','Show from sub groups'),
                            'type'=>'checkbox',
                            'default_value'=>false
                        ],
                        'limit' => [
                            'label'=>Yii::t('frontend/layout','The number on one page'),
                            'type'=>'input',
                            'default_value'=>100
                        ],
                        'order' => [
                            'label'=>Yii::t('frontend/layout','Order fields'),
                            'type'=>'select',
                            'multiple'=>true,
                            'default_value'=>'position',
                            'values' => [
                                'position' => Yii::t('frontend/layout','Position'),
                                'id' => Yii::t('frontend/layout','Id'),
                                'name' => Yii::t('frontend/layout','Name'),
                            ]
                        ],
                        'orderDirection' => [
                            'label'=>Yii::t('frontend/layout','Order direction'),
                            'type'=>'select',
                            'default_value'=>SORT_ASC,
                            'values' => [
                                SORT_ASC=>Yii::t('frontend/layout','Ascending'),//по возрастанию
                                SORT_DESC=>Yii::t('frontend/layout','Descending'),//по убыванию
                            ]
                        ],
                        'template' => [
                            'label'=>Yii::t('frontend/layout','Template'),
                            'type'=>'select',
                            'default_value'=>'shop-items-list',
                            'values' => [
                                'shop-items-list'=>Yii::t('frontend/layout','Standard shop items list'),
                            ]
                        ],
                        'title'=>[
                            'label'=>Yii::t('frontend/layout','Title block'),
                            'type'=>'string',
                            'default_value'=>'',
                        ],
                    ]
                ],
            ];
        }
        return self::$_widgets;
    }
}