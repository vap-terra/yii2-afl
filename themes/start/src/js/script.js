/**
 * Created by volhv on 07.08.16.
 */
(function($) {
    if( typeof($.fn.carousel)!=='undefined' ) {
        /*$(document).ready(function () {
            $('.carousel').carousel();
        });*/
        $(window).on('load', function () {
            $('.carousel').carousel();
        });
    }




    if( typeof($.fn.tooltip)!=='undefined' ) {
        $('[data-toggle="tooltip"]').tooltip(
            {
                delay: {"show": 300, "hide": 300}
            }
        )
    }


    /* view shop category */
    $(window).on(
        'load',
        function() {
            $('.shop-item').each(
                function ()
                {

                    var $item = $(this);
                    var $modItemsWrapper = $item.find('.modifications');
                    var $fullViews = $modItemsWrapper.find('.full-views');
                    var $smallViews = $modItemsWrapper.find('.small-views');
                    var width = parseInt($item.width());
                    var height = parseInt($item.height());
                    var widthF = parseInt($item.outerWidth());
                    var heightF = parseInt($item.outerHeight());

                    $fullViews.css(
                        {
                            width: (width) + 'px',
                            height: (height) + 'px'
                        }
                    );
                    $smallViews.css(
                        {
                            width: 100 + 'px',
                            height: (height) + 'px'
                        }
                    );
                    $modItemsWrapper.css(
                        {
                            width: (widthF + 100) + 'px',
                            height: (heightF + 20) + 'px'
                        }
                    );
                    $smallViews.find('li').click(
                        function()
                        {
                            $fullViews.find('ul').trigger('slideTo', '#' + $(this).attr('data-id').split('#').pop());
                            $smallViews.find('ul').trigger('slideTo', [this, -1]);
                            $smallViews.find('li.active').removeClass('active');
                            $(this).addClass('active');
                            return false;
                        }
                    );
                    $item.on(
                        {
                            mouseenter: function (event) {
                                $item.css('z-index', 20);
                                $modItemsWrapper.fadeIn(300);
                                if ($fullViews.find('ul').length) {
                                    /*$fullViews.find('li').each(
                                     function () {
                                     $(this).css({width: (width) + 'px'})
                                     }
                                     );*/
                                    $fullViews.find('ul')/*.css(
                                     {
                                     height: (height) + 'px'
                                     }
                                     )*/.carouFredSel(
                                        {
                                            onCreate: function (data) {},
                                            responsive: true,
                                            circular: true,
                                            auto: false,
                                            //width: '100%',
                                            //height: '100%',
                                            items: {
                                                visible: 1
                                            },
                                            scroll: {
                                                fx: 'directscroll'
                                            }
                                        }
                                    );
                                    $smallViews.find('ul').css(
                                        {
                                            width: 100 + 'px'
                                        }
                                    ).carouFredSel(
                                        {
                                            circular: true,
                                            infinite: false,
                                            auto: false,
                                            direction: 'down',
                                            width: '100%',
                                            height: '100%',
                                            //align:"top",
                                            prev: '.img-prev',
                                            next: '.img-next',
                                            scroll: {
                                                fx: 'directscroll',
                                                items: 1
                                            },
                                            items: {
                                                start: -1,
                                                visible: {
                                                    min: 1,
                                                    max: 10
                                                }
                                            }
                                        }
                                    );
                                }
                                return false;
                            },
                            mouseleave: function (event) {
                                $modItemsWrapper.stop(true, true)
                                    .fadeOut(
                                        300,
                                        function () {
                                            $item.css('z-index', 6);
                                        }
                                    );
                                return false;
                            }
                        }
                    );
                }
            );
        }
    );

    /* view shop item */
    $(window).on(
        'load',
        function() {
            var $fullViews = $('.shop-item-wrapper .full-views');
            var $smallViews = $('.shop-item-wrapper .small-views');
            $fullViews.find('ul').carouFredSel(
                {
                    onCreate: function (data) {},
                    responsive: true,
                    circular: true,
                    auto: false,
                    /*width: '100%',
                     height: '100%',*/
                    items: {
                        visible: 1
                    },
                    scroll: {
                        fx: 'directscroll'
                    }
                }
            );
            $smallViews.find('ul').carouFredSel(
                {
                    circular: true,
                    infinite: false,
                    auto: false,
                    //direction: 'down',
                    /*width: '100%',
                     height: '100%',*/
                    //align:"top",
                    prev: '.img-prev',
                    next: '.img-next',
                    scroll: {
                        fx: 'directscroll',
                        items: 1
                    },
                    items: {
                        start: -1,
                        visible: {
                            min: 1,
                            max: 10
                        }
                    }
                }
            );
        }
    );

})(jQuery);
