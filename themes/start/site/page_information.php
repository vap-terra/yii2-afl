<?php

use app\components\web\View;
use app\models\ext\MenuItemExt;
use yii\helpers\Html;

/* @var $this View */
/* @var $oMenuItem MenuItemExt */
/* @var $arStrContents string[] */
/* @var $arStrContentBlocks string[] */

?>
    <div class="home-news-post clearfix">
        <?php if(!empty($this->menuItem)){ ?>
        <h1><?php echo $this->menuItem->header?$this->menuItem->header:$this->menuItem->name; ?></h1>
        <?php } ?>

        <div class="home-news-post_box">

            <?php
            if( $this->menuItem && $this->menuItem->image ){
            ?>
                <?php
                $src = Yii::$app->imager->getSrcResize(405, 253, $this->menuItem->getImageHref());
                echo Html::img(
                    $src,
                    [
                        'class' => 'img-thumbnail',
                        'alt' => ($this->menuItem->header ? $this->menuItem->header : $this->menuItem->name),
                    ]
                );
                ?>
            <?php
            }
            ?>

            <?php if( $this->subHeader ){ ?>
                <h2><?php echo $this->subHeader?></h2>
            <?php } ?>

            <?php foreach( $arStrContents as $content ) {?>
                <?php echo $content?>
            <?php } ?>

            <?php /*
            $arStrContentBlocks = $oMenuItem->getStrBlocks('content');
            foreach( $arStrContentBlocks  as $block ) {
                ?>
                <?php echo $block ?>
                <?php
            }
            */ ?>

        </div>

    </div>