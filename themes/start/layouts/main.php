<?php

/* @var $this \app\components\web\View */
/* @var $content string */


use app\models\ext\ConstantExt;
use app\models\ext\MenuExt;
use app\widgets\Alert;
use yii\bootstrap\Nav;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use yii\widgets\Menu;


\app\themes\start\assets\AppAsset::register($this);
$this->registerLinkTag(['rel' => 'icon', 'type' => 'image/ico', 'href' => '/favicon.ico']);
?>
<?php $this->beginPage() ?><!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
    <?php $this->beginBody() ?>
    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/"><?php echo ConstantExt::getValueByTid('site_name');?></a>
                <?php /* echo ConstantExt::getValueByTid('site_slogan');*/?>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <?php
                echo Nav::widget(
                    [
                        'options' => [
                            'class' => 'nav navbar-nav navbar-left'
                        ],
                        'items' => MenuExt::getArMenuNavItems('main'),
                    ]
                );
                ?>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <?php if( !empty( $this->menuItem ) && $this->menuItem->tid == Yii::$app->params['menu_item_index_tid'] ){ ?>
        <?php
        $module = Yii::$app->getModule('frontend');
        if( !empty( $module ) ) {
            echo $module->widget(
                'BannerList',
                [
                    'bannerGroupId' => 1,
                    'subGroups' => false,
                    'limit' => 10,
                    'order' => 'position',
                    'orderDirection' => SORT_ASC,
                    'template' => 'banner-item-list-carousel',
                ]
            );
        }
        ?>
    <?php } ?>

    <!-- Page Content -->
    <div class="container middle-wrapper">

        <?php echo Alert::widget() ?>

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-8 main-column">

                <?php
                    echo Breadcrumbs::widget(
                        [
                            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                        ]
                    );
                ?>
                <?php echo $content; ?>
                <div class="clearfix"></div>
            </div>

            <!-- Blog Sidebar Widgets Column -->
            <div class="col-md-4 ">

                <?php /* <!-- Blog Search Well -->
                <div class="well">
                    <h4>Поиск</h4>
                    <div class="input-group">
                        <input type="text" class="form-control">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">
                                <span class="glyphicon glyphicon-search"></span>
                        </button>
                        </span>
                    </div>
                    <!-- /.input-group -->
                </div> */ ?>
                <div class="well">
                    <h4>Дополнительное меню</h4>
                    <div class="row">
                        <?php
                        echo Nav::widget(
                            [
                                'options' => [
                                    'class' => 'nav nav-pills nav-stacked'
                                ],
                                'items' => MenuExt::getArMenuNavItems('additional'),
                            ]
                        );
                        ?>
                    </div>
                    <!-- /.row -->
                </div>

                <?php /* <!-- Blog Categories Well -->
                <div class="well">
                    <h4>Blog Categories</h4>
                    <div class="row">
                        <div class="col-lg-6">
                            <ul class="list-unstyled">
                                <li><a href="#">Category Name</a>
                                </li>
                                <li><a href="#">Category Name</a>
                                </li>
                                <li><a href="#">Category Name</a>
                                </li>
                                <li><a href="#">Category Name</a>
                                </li>
                            </ul>
                        </div>
                        <!-- /.col-lg-6 -->
                        <div class="col-lg-6">
                            <ul class="list-unstyled">
                                <li><a href="#">Category Name</a>
                                </li>
                                <li><a href="#">Category Name</a>
                                </li>
                                <li><a href="#">Category Name</a>
                                </li>
                                <li><a href="#">Category Name</a>
                                </li>
                            </ul>
                        </div>
                        <!-- /.col-lg-6 -->
                    </div>
                    <!-- /.row -->
                </div> */ ?>

                <div class="well">
                    <h4>Новости</h4>
                    <?php
                    $module = Yii::$app->getModule('frontend');
                    if( !empty( $module ) ) {
                        echo $module->widget(
                            'InformationItemList',
                            [
                                'informationId' => 1,
                                'informationGroupId' => -1,
                                'subGroups' => false,
                                'limit' => 3,
                                'order' => 'dt_creation',
                                'orderDirection' => SORT_DESC,
                                'template' => 'information-item-list',
                            ]
                        );
                    }
                    ?>
                </div>

                <!-- Side Widget Well -->
                <div class="well">
                    <h4>Формы</h4>
                    <?php
                        $module = Yii::$app->getModule('frontend');
                        if( !empty( $module ) ) {
                            echo $module->widget(
                                'Form',
                                [
                                    'formId' => 1,
                                    'template' => 'form',
                                    'orderFields' => 'position',
                                    'orderDirectionFields' => SORT_ASC,
                                ]
                            );
                        }
                    ?>
                    <?php
                    $module = Yii::$app->getModule('frontend');
                    if( !empty( $module ) ) {
                        echo $module->widget(
                            'Form',
                            [
                                'formId' => 2,
                                'template' => 'form',
                                'orderFields' => 'position',
                                'orderDirectionFields' => SORT_ASC,
                            ]
                        );
                    }
                    ?>
                </div>

            </div>

        </div>
        <!-- /.row -->

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; <?php echo ConstantExt::getValueByTid('site_name');?> <?= date('Y') ?></p>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

            <?php echo ConstantExt::getValueByTid('site_email');?>
            <?php
                $module = Yii::$app->getModule('frontend');
                if( !empty($module) ){
                    echo $module->widget(
                        'FormLink',
                        [
                            'formId'=>2,
                            'title'=>'Написать сообщение',
                            'cssIcon' => 'glyphicon glyphicon-bullhorn',
                            'options' => [],
                            'template' => 'form-link',
                        ]
                    );
                }
            ?><br/>
            <?php echo ConstantExt::getValueByTid('site_phone');?>
            <?php
            $module = Yii::$app->getModule('frontend');
            if( !empty($module) ){
                echo $module->widget(
                    'FormLink',
                    [
                        'formId'=>1,
                        'title'=>'Заказать обратный звонок',
                        'cssIcon' => 'glyphicon glyphicon-earphone',
                        'options' => [],
                        'template' => 'form-link',
                    ]
                );
            }
            ?><br/>
            <?php echo ConstantExt::getValueByTid('site_address');?><br/>

        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery
    <script src="js/jquery.js"></script> -->

    <!-- Bootstrap Core JavaScript
    <script src="js/bootstrap.min.js"></script> -->
    <?php $this->endBody() ?>

    <?php echo ConstantExt::getValueByTid('yandex_counter');?>
    <?php echo ConstantExt::getValueByTid('google_counter');?>

</body>

</html>
<?php $this->endPage() ?>