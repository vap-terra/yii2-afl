<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 01.08.16
 * Time: 21:58
 */
use app\models\ext\BannerExt;
use app\models\ext\BannerGroupExt;
use yii\helpers\Html;

/* @var $group BannerGroupExt | null */
/* @var $items BannerExt[] */

if( !empty( $items ) ) {
?>
    <header class="business-header">

        <div id="carousel-banner-list" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <?php for($i=0,$cnt=count($items);$i<$cnt;$i++){ ?>
                <li data-target="#carousel-banner-list" data-slide-to="<?php echo $i?>" class="<?php if( $i==0 ){ ?> active<?php } ?>"></li>
                <?php }?>
            </ol>
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
    <?php
    foreach ($items as $num=>$item) {
    ?>
                    <div class="item<?php if( $num == 0 ){ ?> active<?php } ?>">
                        <?php
                        $src = $item->getImageHref();//Yii::$app->imager->getSrcResize(100,100,$item->getImageHref());
                        echo Html::img(
                            $src,
                            [
                                'class'=>'media-object',
                                'alt'=>$item->name,
                            ]
                        );
                        ?>
                        <div class="carousel-caption">
                            <h1 class="tagline"><?php
                                if( $item->link ) {
                                    echo Html::a($item->name,$item->link,['target'=>'_blank']);
                                } else {
                                    echo $item->name;
                                }
                                ?></h1>
                            <?php echo $item->content;?>
                        </div>
                    </div>
        <?php
    }
    ?>
            </div>

    <!-- Controls -->
    <a class="left carousel-control" href="#carousel-banner-list" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#carousel-banner-list" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
    </div>

    </header>
<?php
}
?>