<?php
require(__DIR__ . '/../components/Configurator.php');
$configurator = new \app\components\Configurator(
    __DIR__.'/../config/',
    \app\components\Configurator::TYPE_WEB,
    \app\components\Configurator::ENV_DEVELOPMENT
);
// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');

//$config = require(__DIR__ . '/../config/web.php');
$config = $configurator->make();

(
    new yii\web\Application( $config )
)->run();
