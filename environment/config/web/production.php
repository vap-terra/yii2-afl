<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 20.03.16
 * Time: 14:12
 */
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=',
            'username' => '',
            'password' => '',
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@app/mail',
            'useFileTransport' => false,
        ],
    ],
    'modules' => [
    ],
    'params' => [
    ]
];