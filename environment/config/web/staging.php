<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 20.03.16
 * Time: 14:29
 */
return [
    'components' => [
        'assetManager' => [
            'linkAssets' => true,
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=',
            'username' => '',
            'password' => '',
            'charset' => 'utf8',
        ],
        'storage' => [
            'class' => 'app\components\FileStorageManager',
            'domainName' => '',
            'basePath' => 'upload'
        ],
    ],
    'modules' => [
    ],
    'params' => [
    ],
];