<?php
return [
    'adminEmail' => 'volhv@vap-terra.ru',
    'supportEmail' => 'volhv@vap-terra.ru',
    'adminPhone' => '+79831288885',
    'accepted_origins' => [
        'http://afl.loc','http://www.afl.loc',
    ],
];
