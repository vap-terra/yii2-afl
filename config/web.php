<?php
$config = [
    'id' => 'web',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'sourceLanguage' => 'en-US',
    'language' => 'ru-RU',
    //'controllerNamespace' => '',
    'components' => [
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                ['pattern' => 'sitemap', 'route' => 'sitemap/default/index', 'suffix' => '.xml'],

                '/debug/<controller:[\w|\-|\d]+>/<action:[\w|\-|\d]+><_:[/]{0,1}>' => '/debug/<controller>/<action>',

                '/backend<_:[/]{0,1}>' => '/backend/default/index',
                '/backend/<controller:[\w|\-|\d]+>/<action:[\w|\-|\d]+><_:[/]{0,1}>' => '/backend/<controller>/<action>',
                '/backend/<controller:[\w|\-|\d]+>/<action:[\w|\-|\d]+><_:[/]{0,1}>' => '/backend/<controller>/<action>',

                '/admin<_:[/]{0,1}>' => '/backend/default/index',
                '/admin/<controller:[\w|\-|\d]+><_:[/]{0,1}>' => '/backend/<controller>/index',
                '/admin/<controller:[\w|\-|\d]+>/<action:[\w|\-|\d]+><_:[/]{0,1}>' => '/backend/<controller>/<action>',

                '/frontend/<controller:[\w|\-|\d]+>/<action:[\w|\-|\d]+><_:[/]{0,1}>' => '/frontend/<controller>/<action>',
                '/frontend/<controller:[\w|\-|\d]+>/<action:[\w|\-|\d]+><_:[/]{0,1}>' => '/frontend/<controller>/<action>',

                '/login<_:[/]{0,1}>' => '/backend/user/login',
                '/logout<_:[/]{0,1}>' => '/backend/user/logout',
                '/request-password-reset<_:[/]{0,1}>' => '/backend/user/request-password-reset',

                '/resize/<id:[^\s]+><_:[/]{0,1}>' => '/frontend/site/resize',
                '/form/<id:[\w|\-|\d|_]+>' => '/frontend/form/view',
                '/form/send/<id:[\w|\-|\d|_]+>' => '/frontend/form/send',
                '/' => '/frontend/site/page',
                '/<id:[^\s]+><_:[/]{0,1}>' => '/frontend/site/page',


            ],
        ],
        'request' => [
            'enableCsrfValidation'=>false,
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'Qt2aW3OiHjnoQAVAHvQ1ornRgh9yHSkJ',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'errorHandler' => [
            'errorAction' => '/frontend/site/error',
        ],
        'view' => [
            'class' => 'app\components\web\View',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@app/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'smser' => [
            'class' => 'app\components\SmsruSender',
            'apiId' => '',
            'login' => '',
            'password' => '',
            'sender' => '',
            'test' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'defaultRoles' => [
                'root',
                'admin',
                'user',
                'guest'
            ],
        ],
        'user' => [
            'class' => 'yii\web\User',
            'identityClass' => 'app\models\ext\UserExt',
            'enableAutoLogin' => true,
            'loginUrl'=> ['/login'],
        ],
        'i18n' => [
            'translations' => require('translations.php'),
        ],
        'formatter' => [
            'dateFormat' => 'php:d F Y',
            'decimalSeparator' => '.',
            'thousandSeparator' => ' ',
            'currencyCode' => 'RUR'
        ],
        'storage' => [
            'class' => 'app\components\FileStorageManager',
            'domainName' => '',
            'basePath' => 'upload'
        ],
        'imager' => [
            'class'=>'vapterra\yii2vtimager\Imager',
            'lifetime' => 60*60*24*3,//, ms; 0 - eternal
            'resizeControllerActionName' => '/resize/',
            'rootPath' => realpath( dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'web'.DIRECTORY_SEPARATOR ).DIRECTORY_SEPARATOR,
            'cachePath' => realpath( dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'web'.DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR ).DIRECTORY_SEPARATOR,
            'storageSrc' => DIRECTORY_SEPARATOR.'upload'.DIRECTORY_SEPARATOR,
            'emptyImagePath' => realpath( dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'web'.DIRECTORY_SEPARATOR.'upload'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR ).DIRECTORY_SEPARATOR.'no-photo-item.png',
            'defaultBgRGBAColor' => [255,255,255,127],
            'defaultResizeType' => 'proportional_exactly',// Image::RESIZE_EXACTLY,
            'defaultWatermark' => [
                /* 'margin_left'=>0,
                'margin_bottom'=>0,
                'type'=>'image', //image | text | both
                'file'=>'',
                'text'=>'',
                'placement'=>'tile', // emboss | tile */
            ],
            'defaultCorners' => [
                /*'top_left'=>'',
                'bottom_left'=>'',
                'top_right'=>'',
                'bottom_right'=>'',*/
            ],
            'defaultFilters' => [
                /*'blur' => [
                    'type'=>'gaussian', // gaussian | selective
                ],
                'grayScale' => [],
                'inverse' => [],
                'colorize' => [
                    'r'=>0,
                    'g'=>0,
                    'b'=>0,
                    'a'=>0,
                ],
                'contrast' => [
                    'level' => 0,
                ],
                'brightness' => [
                    'level' => 0,
                ],
                'pixelate' => [
                    'blockSize' => 3,
                    'improved' => false,
                ],
                'rotate' => [],*/
            ],
        ],
    ],
    'modules' => [
        'backend' => [
            'class' => 'app\modules\BackendModule',
        ],
        'frontend' => [
            'class' => 'app\modules\FrontendModule',
        ],
        'sitemap'=>[
            'class'=>'app\modules\sitemap\SiteMapXml',
            'enableGzip' => true, // default is false
            'cacheExpire' => 86400, // 24 hours. Default is 24 hours
        ],
    ],
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];
    $config['modules']['debug']['allowedIPs'] = ['127.0.0.1', '::1'];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
    $config['modules']['gii']['allowedIPs'] = ['127.0.0.1', '::1'];
}

return $config;
