<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 17.10.16
 * Time: 0:44
 */

return [
    'app*' => [
        'class' => 'yii\i18n\PhpMessageSource',
        //'sourceLanguage' => 'en-US',
        'basePath' => '@app/messages/app',
        'fileMap' => [
            'app/app' => 'app.php',
            'app/models'=>'models.php',
            'app/banner' => 'banner.php',
            'app/banner-group' => 'banner-group.php',
            'app/block' => 'block.php',
            'app/content' => 'content.php',
            'app/constant' => 'constant.php',
            'app/comment' => 'comment.php',
            'app/menu' => 'menu.php',
            'app/menu-item' => 'menu-item.php',
            'app/menu-item-relation' => 'menu-item-relation.php',
            'app/form' => 'form.php',
            'app/form-element' => 'form-element.php',
            'app/form-completed' => 'form-completed.php',
            'app/list' => 'list.php',
            'app/list-item' => 'list-item.php',
            'app/information' => 'information.php',
            'app/information-group' => 'information-group.php',
            'app/information-item' => 'information-item.php',
            'app/user' => 'user.php',
        ]
    ],
    'backend*' => [
        'class' => 'yii\i18n\PhpMessageSource',
        //'sourceLanguage' => 'en-US',
        'basePath' => '@app/messages/backend',
        'fileMap' => [
            'backend' => 'backend.php',
            'backend/layout'=>'layout.php',
        ]
    ],
    'frontend*' => [
        'class' => 'yii\i18n\PhpMessageSource',
        //'sourceLanguage' => 'en-US',
        'basePath' => '@app/messages/frontend',
        'fileMap' => [
            'frontend/frontend' => 'frontend.php',
            'frontend/layout'=>'layout.php',
        ]
    ]

];