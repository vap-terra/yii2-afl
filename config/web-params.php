<?php
return [
    'adminEmail' => '',
    'supportEmail' => '',
    'adminPhone' => '',
    'user.passwordResetTokenExpire' => 3600,
    'menu_item_index_tid' => '/',//TODO move to menu item as parameter 'index page'
    'default_layout'   => 'main',
    'main_template'    => 'index',
    'default_template' => 'page',
    'default_theme' => 'start',
    'layouts' => [
        'main' => 'Стандартный',
    ],
    'themes' => [
        'start' => 'Тема по умолчанию[bootstrap]',
    ],
    'templates' => [
        'page' => 'Стандартный',
        'index' => 'Для главной',
    ],
    'information_templates' => [
        'item'  => [
            'item'=>'Стандартный',
        ],
        'group' => [
            'group'=>'Стандартный',
            'gallery'=>'Галлерея',
        ]
    ],
    'block_templates' => [
        'default' => 'Стандартный',
        'no-layout' => 'Без разметки',
    ],
    'accepted_origins' => [

    ],
    'img_extensions' => [
        'png', 'jpe', 'jpeg', 'jpg', 'gif', 'bmp', 'ico', 'tiff', 'tif', 'svg', 'svgz'
    ],
    'file_manager_exclude_folders' => [

    ]
];
