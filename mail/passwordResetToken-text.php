<?php
use app\models\ext\UserExt;

/* @var $this yii\web\View */
/* @var $user UserExt */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
?>
Hello <?= $user->login ?>,

Follow the link below to reset your password:

<?= $resetLink ?>
