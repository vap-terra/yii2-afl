<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 30.07.16
 * Time: 22:59
 */

use app\models\ext\FormCompletedExt;
use app\models\ext\FormElementExt;
use app\models\ext\FormExt;

/* @var $form FormExt */
/* @var $formElements FormElementExt[] */
/* @var $completedForm FormCompletedExt */
/* @var $signature string */
$values = $completedForm->getValues();
?>
<h1><?php echo $form->name; ?></h1>

<p>Вы указали данные:</p>

<?php foreach( $formElements as $field ){ ?>
    <?php if( isset($values[$field->tid])){ ?>
        <p><?php echo $field->name ?>: <?php echo $values[$field->tid];?></p>
    <?php } ?>
<?php } ?>

<hr/>

<?php echo $signature; ?>
